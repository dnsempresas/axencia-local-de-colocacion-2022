@extends('layouts.app')

@section('title_pestaña')

@endsection

@section('titulo_pantalla')

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item active">Boletin de Ofertas</li>
    <!-- Breadcrumb Menu-->
</ol>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">Boletines de Ofertas</div>
            <div class="card-body">

                <div class="toolbar">
                    <div class="btn-group">
                        <a class="btn btn-block btn-outline-primary" href="boletin-ofertas/create"><i class="fa fa-plus-circle" aria-hidden="true"> </i> Nuevo</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Nombre del puesto </label>
                            <div class="col-sm-9">
                                <input type="text" id="name" placeholder="Nombre del Puesto" class="col-xs-10 col-sm-5">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">Fecha de Publicación</label>
                            <div class="col-sm-9">
                                <span class="input-icon">
                                    <input type="text" readonly id="publishedAt_min" placeholder="Desde"/>
                                </span>

                                <span class="input-icon input-icon-right">
                                    <input type="text" readonly id="publishedAt_max" placeholder="Hasta" />
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Lugar de Publicación </label>
                            <div class="col-sm-9">
                                <input type="text" name="publishedIn" id="publishedIn" placeholder="Publicado en" class="col-xs-10 col-sm-5" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6  form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Description </label>
                            <div class="col-sm-9">
                                <input type="text" name="description" id="description" placeholder="Descripción" class="col-xs-10 col-sm-5" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Requerimientos </label>
                            <div class="col-sm-9">
                                <input type="text" name="requirements" id="requirements" placeholder="Requerimientos" class="col-xs-10 col-sm-5" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> tipo de Contrato </label>
                            <div class="col-sm-9">
                                <select class="chosen-select" name="contract_type" id="contract_type" multiple="">
                                    <option value="">Seleccione una opción...</option>
                                    @foreach($contracts as $contract)
                                    <option value="{{$contract->id}}">{{$contract->nome}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Contacto</label>
                            <div class="col-sm-9">
                                <input type="text" name="contact" id="contact" placeholder="Contacto" class="col-xs-10 col-sm-5" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <a id="reset-filter" class="btn btn-sm btn-clear" style="float: right;margin-bottom: 15px;margin-top: 15px; margin-left: 10px">
                            Limpiar Filtro
                        </a>
                        <a id="newsletter-filter" class="btn btn-sm btn-success" style="float: right;margin-bottom: 15px;margin-top: 15px;">
                            Filtrar
                        </a>
                    </div>

                </div>
                <table id="newsletter-table" class="table table-striped table-bordered table-hover dataTable no-footer">
                    <thead class="thead-light">
                        <tr>
                            <th>Nombre de puesto</th>
                            <th>Fecha de publicacion</th>
                            <th>Publicado en</th>
                            <th>Localidad</th>
                            <th>Descripcion</th>
                            <th>Requisitos</th>
                            <th>Tipo de Contrato</th>
                            <th>Contacto</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>


            </div>
        </div>
    </div>
    <!-- /.col-->
</div>
<!-- /.row-->
@endsection
