@extends('layouts.app')

@section('titulo_pantalla')

@endsection

@section('content')
@if ($errors->any())
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-ban"></i>{{__('news.errors')}}</h4>
    @foreach ($errors->all() as $error)
    <p>{{ $error }}</p>
    @endforeach
</div>
@endif

@if(Session::has('alert-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
</div>
@endif
<form style="padding: 30px;" id="newsForm" method="POST" novalidate action="{{url('/panel/noticias')}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div>
                        {{__('news.new_news_label')}}
                    </div>
                </div>
                <div class="card-body">
                    <div class="row"> 
                        <div class="col-md-6">
                             <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Categoría')}}</legend>
                                <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                                    <div role="group" class="input-group">								
                                        <select id="categoria" class="form-control" name="categoria" required>
                                            <option value="" selected="selected">Seleccione</option>
                                            @foreach($categorias as $categoria)
                                            <option value="{{ $categoria->id }}"  {{ (old('$categoria') == $categoria->id ) ? 'selected' : '' }}> {{ session('lang')=='es' ?  $categoria->nombre :  $categoria->nome }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Titulo')}}</legend>
                                <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                                    <div role="group" class="input-group">								
                                        <input type="text" class="form-control" name="titulo" required>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Ante Titulo')}}</legend>
                                <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                                    <div role="group" class="input-group">								
                                        <input type="text" class="form-control" name="antetitulo" >
                                    </div>
                                </div>
                            </fieldset>
                           
                            
                             
                            
                            

                            <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Entradilla')}}</legend>
                                <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                                    <div role="group" class="input-group">
                                        <textarea class="form-control" name="entradilla" ></textarea>
                                    </div>
                                </div>
                            </fieldset>
                            
                            <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Noticia')}}</legend>
                                <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                                    <div role="group" class="">
                                        <textarea class="ckeditor form-control" name="noticia" id="noticia" rows="4" cols="80" required>{{ old('contenido') }}</textarea>
                                    </div>
                                </div>
                            </fieldset>
                            
                            
                           
                            
                            
                            <div class="row"> 
                                <div class="col-md-6"> 

                                    <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                        <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('F. Publicación')}}</legend>
                                        <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                                            <div role="group" class="input-group">
                                                <span class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </span>
                                                <input type="text" id="fecha_publicacion" name="fecha_publicacion" value="" class="form-control">
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-md-6"> 
                                    <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                        <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('F.Expiración')}}</legend>
                                        <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                                            <div role="group" class="input-group">
                                                <span class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </span>
                                                <input type="text" id="fecha_expiracion" name="fecha_expiracion" value="" class="form-control">
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">




                            <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Imagen destacada')}}</legend>
                                <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">

                                    <div class="input-group">
                                        <input id="imagen_destacada" type="file" name="imagen_destacada" accept="image/x-png,image/gif,image/jpeg" >
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Imagen 2')}}</legend>
                                <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">

                                    <div class="input-group">
                                        <input id="imagen_1" type="file" name="imagen_1" accept="image/x-png,image/gif,image/jpeg"  >
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('imagen 3')}}</legend>
                                <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                                    
                                     <div class="input-group">
                                        <input id="imagen_1" type="file" name="imagen_2" accept="image/x-png,image/gif,image/jpeg"  >
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary custom-btn" name="signup" value="Sign up">{{__('news.save')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
</div>
@endsection
