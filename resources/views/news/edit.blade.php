@extends('layouts.app')

@section('titulo_pantalla')

@endsection

@section('content')
@if ($errors->any())
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-ban"></i>{{__('news.errors')}}</h4>
    @foreach ($errors->all() as $error)
    <p>{{ $error }}</p>
    @endforeach
</div>
@endif

@if(Session::has('alert-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
</div>
@endif
<form style="padding: 30px;" id="newsForm" method="POST" action="{{route('noticias.update', $noticia->id) }}" accept-charset="UTF-8" class="form-group" enctype="multipart/form-data">
    {{method_field('PATCH')}}
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div>
                        {{__('Editar Noticia')}}
                    </div>
                </div>
                <div class="card-body">
                    <div class="row"> 
                        <div class="col-md-6">
                            <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Categoría')}}</legend>
                                <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                                    <div role="group" class="input-group">								
                                        <select id="categoria" class="form-control" name="categoria" required>
                                            <option value="" selected="selected">Seleccione</option>
                                            @foreach($categorias as $categoria)
                                            <option value="{{ $categoria->id }}"  {{ ($noticia->categoria_fk == $categoria->id ) ? 'selected' : '' }}> {{ session('lang')=='es' ?  $categoria->nombre :  $categoria->nome }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Titulo')}}</legend>
                                <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                                    <div role="group" class="input-group">								
                                        <input type="text" class="form-control" name="titulo" required value="{{$noticia->titulo}}">
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Antetitulo')}}</legend>
                                <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                                    <div role="group" class="input-group">								
                                        <input type="text" class="form-control" name="antetitulo" value="{{$noticia->antetitulo}}">
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Entradilla')}}</legend>
                                <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                                    <div role="group" class="input-group">
                                        <textarea class="form-control" name="entradilla" >{{$noticia->entradilla}}</textarea>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Noticia')}}</legend>
                                <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                                    <div role="group" class="">
                                        <textarea class="ckeditor form-control" name="noticia" id="noticia" rows="4" cols="80" required>{!! $noticia->noticia !!}</textarea>
                                    </div>
                                </div>
                            </fieldset>
                           




                            <div class="row"> 
                                <div class="col-md-6"> 

                                    <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                        <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('F.Publicación')}}</legend>
                                        <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                                            <div role="group" class="input-group">
                                                <span class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </span>
                                                <input type="text" id="fecha_publicacion" name="fecha_publicacion" value="{{$noticia->getFecha_publicacion()}}" class="form-control">
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-md-6"> 
                                    <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                        <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('F.Expiración')}}</legend>
                                        <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                                            <div role="group" class="input-group">
                                                <span class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </span>
                                                <input type="text" id="fecha_expiracion" name="fecha_expiracion" value="{{$noticia->getFecha_expiracion()}}" class="form-control">
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">




                            <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Imagen destacada')}}</legend>
                                <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">

                                    @if ($noticia->imagen_destacada !='')
                                    <a href="/public/noticias/{{$noticia->imagen_destacada}} ">{{ $noticia->imagen_destacada }}</a>
                                    <a class="removeimg" href="{{route('noticias.removeimgd', ['id'=>$noticia->id, 'img'=>$noticia->imagen_destacada] ) }}" data-id= "{{$noticia->id}}" data-img="{{$noticia->imagen_destacada}}" title='Borrar' ><span class='glyphicon glyphicon-remove' style='color:red'></span></a>
                                    @endif
                                    <br/>
                                    <div class="input-group">
                                        <input id="imagen_destacada" type="file" name="imagen_destacada" accept="image/x-png,image/gif,image/jpeg"   <?php
                                        if ($noticia->imagen_destacada == '') {
                                            // echo 'required';
                                        }
                                        ?> value="{{$noticia->imagen_destacada}}">


                                    </div>

                                </div>
                            </fieldset>
                            <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Imagen 2')}}</legend>
                                <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                                    @if ($noticia->imagen1 !='')
                                    <a href="/public/noticias/{{$noticia->imagen1}} ">{{ $noticia->imagen1 }}</a>
                                    <a class="removeimg" href="{{route('noticias.removeimg1', ['id'=>$noticia->id, 'img'=>$noticia->imagen1] ) }}" data-id= "{{$noticia->id}}" data-img="{{$noticia->imagen1}}" title='Borrar' ><span class='glyphicon glyphicon-remove' style='color:red'></span></a>
                                    @endif


                                    <br/>
                                    <div class="input-group">
                                        <input id="imagen_1" type="file" name="imagen_1" accept="image/x-png,image/gif,image/jpeg"   <?php
                                        if ($noticia->imagen1 == '') {
                                            //echo 'required';
                                        }
                                        ?> value="{{$noticia->imagen1}}">


                                    </div>
                                </div>
                            </fieldset>
                            <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Imagen 3')}}</legend>
                                <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                                    @if ($noticia->imagen2 !='')
                                    <a href="/public/noticias/{{$noticia->imagen2}} ">{{ $noticia->imagen2 }}</a>
                                    <a class="removeimg" href="{{route('noticias.removeimg2', ['id'=>$noticia->id, 'img'=>$noticia->imagen2] ) }}" data-id= "{{$noticia->id}}" data-img="{{$noticia->imagen2}}" title='Borrar' ><span class='glyphicon glyphicon-remove' style='color:red'></span></a>
                                    @endif


                                    <br/>
                                    <div class="input-group">
                                        <input id="imagen_2" type="file" name="imagen_2" accept="image/x-png,image/gif,image/jpeg"   <?php
                                        if ($noticia->imagen2 == '') {
                                            //echo 'required';
                                        }
                                        ?> value="{{$noticia->imagen2}}">


                                    </div>
                                </div>
                            </fieldset>


                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary custom-btn" name="signup" value="Sign up">{{__('news.save')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
</div>
@endsection
