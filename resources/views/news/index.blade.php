@extends('layouts.app')

@section('title_pestaña')
| Elegir Evento
@endsection

@section('titulo_pantalla')

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item active">Noticias</li>
    <!-- Breadcrumb Menu-->
</ol>
@endsection

@section('content')
@if(Session::has('alert-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
</div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">{{__('news.list')}}
                
            </div>
            <div class="card-body">
                <div class="toolbar">
                    <div class="btn-group">
                        <a class="btn btn-block btn-outline-primary" href="{{ url('/panel/noticias/create') }}"><i class="fa fa-plus-circle" aria-hidden="true"> </i> {{__('news.add')}}</a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6 form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('Id')}}</label>
                            <div class="col-sm-9">
                                <input type="text" id="id" placeholder="Buscar por id..." class="col-xs-10 col-sm-5">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('Titulo')}}</label>
                            <div class="col-sm-9">
                                <input type="text" id="titulo" placeholder="Buscar por título..." class="col-xs-10 col-sm-5">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">{{__('Antetitulo')}}</label>
                            <div class="col-sm-9">
                                <input type="text" id="antetitulo" placeholder="Buscar por antetitulo..." class="col-xs-10 col-sm-5">
                            </div>
                        </div>


                    </div>
                    <div class="col-sm-6  form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">{{__('Entradilla')}}</label>
                            <div class="col-sm-9">
                                <input type="text" name="entradilla" id="tasks" placeholder="Buscar por entradilla..." class="col-xs-10 col-sm-5" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">{{__('Fecha de Publicación')}}</label>
                            <div class="col-sm-9">
                                <span class="input-icon">
                                    <input type="text"  id="fecha_publicacion_min" placeholder="{{__('desde')}}"/>
                                </span>

                                <span class="input-icon input-icon-right">
                                    <input type="text"  id="fecha_publicacion_max" placeholder="{{__('hasta')}}"/>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">{{__('Fecha de Expiración')}}</label>
                            <div class="col-sm-9">
                                <span class="input-icon">
                                    <input type="text"  id="fecha_expiracion_min" placeholder="{{__('desde')}}"/>
                                </span>

                                <span class="input-icon input-icon-right">
                                    <input type="text"  id="fecha_expiracion_max"  placeholder="{{__('hasta')}}"/>
                                </span>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">

                        <a id="reset-filter" class="btn btn-sm btn-clear" style="float: right;margin-bottom: 15px;margin-top: 15px; margin-left: 10px">
                            Limpiar Filtros
                        </a>
                        <a id="news-filter" class="btn btn-sm btn-success" style="float: right;margin-bottom: 15px;margin-top: 15px;">
                            {{__('filtrar')}}
                        </a>
                    </div>

                </div>

                <table id="news_table" class="table table-striped table-bordered table-hover dataTable no-footer">
                    <thead class="thead-light">
                        <tr>
                            <th>{{__('Id')}}</th>
                            <th>{{__('Titulo')}}</th>
                            <th>{{__('Antetitulo')}}</th>
                            <th>{{__('Entradilla')}}</th>
                            <th>{{__('Noticia')}}</th>
                            <th>{{__('F.Publicación')}}</th>
                            <th>{{__('F.Expiración')}}</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($news as $noticia)
                        <tr>
                            <td>{{ $noticia->id }}</td>
                            <td>{{ $noticia->titulo }}</td>
                            <td>{{ $noticia->antetitulo }}</td>
                            <td>{{ $noticia->entradilla }}</td>
                            <td>{{ $noticia->noticia }}</td>
                            <td>{{ $noticia->fecha_publicacion }}</td>
                            <td>{{ $noticia->fecha_expiracion }}</td>
                            <td>{{ $noticia->options }}</td>
<!--                            <td style="text-align:center; vertical-align:middle;">
                                <a href='{{url('/panel/noticias/'.$noticia->id.'/edit')}}' title='Edit' ><span class='glyphicon glyphicon-edit'></span></a>
                                &emsp;<a id='remove_link' href='{{url('/panel/admin/noticias/'.$noticia->id.'/remove')}}' title='Delete' ><span class='glyphicon glyphicon-remove' style='color:red'></span></a>
                            </td>-->
                        </tr>
                        @endforeach	
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /.col-->
</div>
<!-- /.row-->
@endsection
