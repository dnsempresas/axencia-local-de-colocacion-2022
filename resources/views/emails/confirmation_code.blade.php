<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
</head>
<body>
    <a><img class="logo" src="{{ asset('assets/images/logo-gl.png') }}"> </a>
    <h2>Ola {{ $name }}, grazas por rexistrarte na <strong>Axencia de Local de Colocación.org</strong>!</h2>
    <p>Por confirma o teu correo electrónico.</p>
    <p>Para elo simplemente debes facer click no seguinte enlace:</p>

    <a href="{{ url('verificar/usuario/' . $confirmation_code) }}">
        Click para confirmar o teu email 
    </a>
</body>
</html>