@extends('layouts.app')

@section('title_pestaña')

@endsection

@section('titulo_pantalla')

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Panel</li>
    <!-- Breadcrumb Menu-->
</ol>
@endsection
@section('content')
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row bg-gray-300">
            <div class="col-sm-6 col-lg-2">
                <div class="row">

                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">
                            <i class="fa fa-users bg-primary p-2 font-4xl mr-3"></i>
                            <div>
                                <div id="usuarios_registrados" class="text-value-lg text-primary">-</div>
                                <div class="text-muted text-uppercase font-weight-bold small">Usuarios Rexistrados</div>
                            </div>
                        </div>
                    </div>




                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">

                            <i class="fa fa-user-o bg-info p-2 font-4xl mr-3"></i>
                            <div>
                                <div id="totalUsuariosHoy" class="text-value-lg text-info">-</div>
                                <div class="text-muted text-uppercase font-weight-bold small">Usuarios Hoxe</div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">
                            <i class="fa fa-industry bg-info p-2 font-4xl mr-3" aria-hidden="true"></i>
                            <div>
                                <div id="totalEmpresas" class="text-value-lg text-info">-</div>
                                <div class="text-muted text-uppercase font-weight-bold small">Empresas Rexistradas</div>
                            </div>
                        </div>
                    </div>



                </div>

            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-4">
                <div class="card text-white bg-info">

                    <div class="card-header">Usuarios por Xenero
                        <div class="card-header-actions">

                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart-wrapper bg-white">
                            <canvas id="canvas-usuarios"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4">
                <div class="card text-white bg-info">

                    <div class="card-header">Usuarios por Localidade
                        <div class="card-header-actions">

                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart-wrapper bg-white">
                            <canvas id="canvas-usuarios-concello"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row  ">
            <div class="col-sm-6 col-lg-2">
                <div class="row ">
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">
                            <span class="glyphicon glyphicon-tasks text-info" style="font-size: 200%;"> </span>
                            <div>
                                <div id="totalOfertasPublicadas" class="text-value-lg text-info">-</div>
                                <div class="text-muted text-uppercase font-weight-bold small"> Ofertas Totais</div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">

                            <i class="fa fa-indent bg-info p-2 font-4xl mr-3" aria-hidden="true"> </i>
                            <div>
                                <div id="totalOfertasNuevas" class="text-value-lg text-info">-</div>
                                <div class="text-muted text-uppercase font-weight-bold small"> Ofertas Hoxe</div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-4">
                <div class="card text-white bg-light">

                    <div class="card-header">Ofertas por tipo de contrato
                        <div class="card-header-actions">

                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart-wrapper bg-white">
                            <canvas id="totalOfertaContrato"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row bg-gray-300 ">
            <div class="col-sm-6 col-lg-2">
                <div class="row ">
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">
                            <i class="fa fa-graduation-cap bg-info p-2 font-4xl mr-3"></i>
                            <div>
                                <div id="totalCursosPublicados" class="text-value-lg text-info">-</div>
                                <div class="text-muted text-uppercase font-weight-bold small">Cursos Totais</div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">

                            <span class="glyphicon glyphicon-education text-info" style="font-size: 200%;"> </span>
                            <div>
                                <div id="totalCursosNuevos" class="text-value-lg text-info">-</div>
                                <div class="text-muted text-uppercase font-weight-bold small">Cursos Hoxe</div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-4">
                <div class="card text-white bg-info">

                    <div class="card-header">Tipos de Cursos
                        <div class="card-header-actions">

                        </div>
                    </div>
                    <div class="card-body bg-info">
                        <div class="chart-wrapper bg-white">
                            <canvas id="totalCursosTipo"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row ">
            <div class="col-sm-6 col-lg-2">
                <div class="row ">
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">
                             <i class="fa fa-comments-o bg-info p-2 font-4xl mr-3"></i>
                            
                            <div>
                                <div id="totalNoticiasPublicadas" class="text-value-lg text-info">-</div>
                                <div class="text-muted text-uppercase font-weight-bold small">Novas Totais</div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">

                             <i class="fa fa-commenting-o bg-info p-2 font-4xl mr-3"></i>
                            <div>
                                <div id="totalNoticiasNuevas" class="text-value-lg text-info">-</div>
                                <div class="text-muted text-uppercase font-weight-bold small">Novas Hoxe</div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-4">
                <div class="card text-white bg-light">

                    <div class="card-header">Novas por Ano
                        <div class="card-header-actions">

                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart-wrapper bg-white">
                            <canvas id="totalNoticiasCategoria"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


</div>
</div>


@endsection

@section('scripts')
<script src="{{asset('public/node_modules/chart.js/dist/Chart.min.js')}}"></script>

<script>
    
    var url1 = "{{url('/chart1')}}";
var ResumenGeneros = new Array();
var totalConcellos = new Array();
var nameConcellos = new Array();
var TotalUsuarios = 0;
var TotalUsuariosHoy = 0;
var totalOfertasPublicadas = 0;
var totalOfertaContrato = new Array();
var nametotalOfertaContrato = new Array();
var coloresdinamicos = new Array();
var totalCursosPublicados = 0;
var totalCursosTipo = new Array();
var nametotalCursosTipo = new Array();

var totalNoticiasPublicadas = 0;
var totalNoticiasCategoria = new Array();
var nameNoticiasCategoria = new Array();


function load_resumen() {
    $.ajax({
        url: "{{url('panel/admin/chart1')}}",
        method: 'POST',
        type: 'JSON',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        beforeSend: function () {},
        success: function (response) {


            $('#usuarios_registrados').html(response.totalUsuarios[0].total);

            $('#totalUsuariosHoy').html(response.totalUsuariosHoy[0].total);
            $('#totalEmpresas').html(response.totalEmpresas[0].total);

            response.totalGeneros.forEach(function (data) {
                ResumenGeneros.push(data.total);

            });
            response.totalConcellos.forEach(function (data) {
                totalConcellos.push(data.total);
                nameConcellos.push(data.name);

            });






            var pieChart = new Chart($('#canvas-usuarios'), {
                type: 'pie',
                data: {
                    labels: ['Masculino', 'Femenino', 'Otros'],
                    datasets: [{
                            data: ResumenGeneros,
                            backgroundColor: ['#FF6384', '#36A2EB', '#FFCE56'],
                            hoverBackgroundColor: ['#FF6384', '#36A2EB', '#FFCE56']
                        }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: true,
                        position: 'left'
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                var total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
                                    return previousValue + currentValue;
                                });
                                var currentValue = dataset.data[tooltipItem.index];
                                var percentage = Math.floor(((currentValue / total) * 100) + 0.5);
                                return percentage + "%";
                            }
                        }
                    }
                }
            }); // eslint-disable-next-line no-unused-vars


            var pieChart2 = new Chart($('#canvas-usuarios-concello'), {
                type: 'pie',
                data: {
                    labels: nameConcellos,
                    datasets: [{
                            data: totalConcellos,
                            backgroundColor: ['#6610f2', '#36A2EB', '#FFCE56', '#4dbd74', '#f86c6b'],
                            hoverBackgroundColor: ['#6610f2', '#36A2EB', '#FFCE56', '#4dbd74', '#f86c6b']
                        }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: true,
                        position: 'left'
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                var dataset = data.datasets[tooltipItem.datasetIndex];

                                var total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
                                    return previousValue + currentValue;
                                });
                                var currentValue = dataset.data[tooltipItem.index];

                                var percentage = Math.floor(((currentValue / total) * 100) + 0.5);
                                return percentage + "%";
                            }
                        }
                    }
                }
            });




            // *************************** OFERTAS

            $('#totalOfertasPublicadas').html(response.totalOfertasPublicadas[0].total);
            $('#totalOfertasNuevas').html(response.totalOfertasNuevas[0].total);
            coloresdinamicos = [];

            response.totalOfertaContrato.forEach(function (data) {
                totalOfertaContrato.push(data.total);
                nametotalOfertaContrato.push(data.name);
                color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                while ($.inArray(color, coloresdinamicos) > -1) {
                    color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                }
                coloresdinamicos.push(color);
            });
            var pieChart3 = new Chart($('#totalOfertaContrato'), {
                type: 'pie',
                data: {
                    labels: nametotalOfertaContrato,
                    datasets: [{
                            data: totalOfertaContrato,
                            backgroundColor: coloresdinamicos,
                            hoverBackgroundColor: coloresdinamicos
                        }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: true,
                        position: 'bottom'
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                var total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
                                    return previousValue + currentValue;
                                });
                                var currentValue = dataset.data[tooltipItem.index];
                                var percentage = Math.floor(((currentValue / total) * 100) + 0.5);
                                return percentage + "%";
                            }
                        }
                    }

                }
            });
            // *************************** FORMACION ***************************************************************

            $('#totalCursosPublicados').html(response.totalCursosPublicados[0].total);
            $('#totalCursosNuevos').html(response.totalCursosNuevos[0].total);
            coloresdinamicos = [];

            response.totalCursosTipo.forEach(function (data) {
                totalCursosTipo.push(data.total);
                nametotalCursosTipo.push(data.name);
                color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                while ($.inArray(color, coloresdinamicos) > -1) {
                    color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                }
                coloresdinamicos.push(color);
            });
            var pieChart4 = new Chart($('#totalCursosTipo'), {
                type: 'pie',
                data: {
                    labels: nametotalCursosTipo,
                    datasets: [{
                            data: totalCursosTipo,
                            backgroundColor: coloresdinamicos,
                            hoverBackgroundColor: coloresdinamicos
                        }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: true,
                        position: 'bottom'
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                var total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
                                    return previousValue + currentValue;
                                });
                                var currentValue = dataset.data[tooltipItem.index];
                                var percentage = Math.floor(((currentValue / total) * 100) + 0.5);
                                return percentage + "%";
                            }
                        }
                    }

                }
            });

            // *************************** NOTICIAS  ***************************************************************

            $('#totalNoticiasPublicadas').html(response.totalNoticiasPublicadas[0].total);
            $('#totalNoticiasNuevas').html(response.totalNoticiasNuevas[0].total);
            coloresdinamicos = [];

            response.totalNoticiasCategoria.forEach(function (data) {
                totalNoticiasCategoria.push(data.total);
                nameNoticiasCategoria.push(data.name);
                color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                while ($.inArray(color, coloresdinamicos) > -1) {
                    color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                }
                coloresdinamicos.push(color);
            });
            var pieChart5 = new Chart($('#totalNoticiasCategoria'), {
                type: 'pie',
                data: {
                    labels: nameNoticiasCategoria,
                    datasets: [{
                            data: totalNoticiasCategoria,
                            backgroundColor: coloresdinamicos,
                            hoverBackgroundColor: coloresdinamicos
                        }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: true,
                        position: 'left'
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                var total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
                                    return previousValue + currentValue;
                                });
                                var currentValue = dataset.data[tooltipItem.index];
                                var currentlabel = data.labels[tooltipItem.index];
                                var percentage = Math.floor(((currentValue / total) * 100) + 0.5);
                                return currentlabel + ' ' + percentage + "%";
                            }
                        }
                    }

                }
            });
        }
    });

}

jQuery(document).ready(function ($) {
    load_resumen();
});
</script>

@endsection
