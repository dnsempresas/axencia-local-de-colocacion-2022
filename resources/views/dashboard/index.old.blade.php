@extends('layouts.app')

@section('title_pestaña')

@endsection

@section('titulo_pantalla')

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Panel</li>
    <!-- Breadcrumb Menu-->
</ol>
@endsection
@section('content')
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row bg-gray-300">
            <div class="col-sm-6 col-lg-2">
                <div class="row">

                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">
                            <i class="fa fa-users bg-primary p-2 font-4xl mr-3"></i>
                            <div>
                                <div id="usuarios_registrados" class="text-value-lg text-primary">-</div>
                                <div class="text-muted text-uppercase font-weight-bold small">Usuarios Rexistrados</div>
                            </div>
                        </div>
                    </div>




                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">

                            <i class="fa fa-user-o bg-info p-2 font-4xl mr-3"></i>
                            <div>
                                <div id="totalUsuariosHoy" class="text-value-lg text-info">-</div>
                                <div class="text-muted text-uppercase font-weight-bold small">Usuarios Hoxe</div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">
                            <i class="fa fa-industry bg-info p-2 font-4xl mr-3" aria-hidden="true"></i>
                            <div>
                                <div id="totalEmpresas" class="text-value-lg text-info">-</div>
                                <div class="text-muted text-uppercase font-weight-bold small">Empresas Rexistradas</div>
                            </div>
                        </div>
                    </div>



                </div>

            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-4">
                <div class="card text-white bg-info">

                    <div class="card-header">Usuarios por Xenero
                        <div class="card-header-actions">

                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart-wrapper bg-white">
                            <canvas id="canvas-usuarios"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4">
                <div class="card text-white bg-info">

                    <div class="card-header">Usuarios por Localidade
                        <div class="card-header-actions">

                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart-wrapper bg-white">
                            <canvas id="canvas-usuarios-concello"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row  ">
            <div class="col-sm-6 col-lg-2">
                <div class="row ">
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">
                            <span class="glyphicon glyphicon-tasks text-info" style="font-size: 200%;"> </span>
                            <div>
                                <div id="totalOfertasPublicadas" class="text-value-lg text-info">-</div>
                                <div class="text-muted text-uppercase font-weight-bold small"> Ofertas Totais</div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">

                            <i class="fa fa-indent bg-info p-2 font-4xl mr-3" aria-hidden="true"> </i>
                            <div>
                                <div id="totalOfertasNuevas" class="text-value-lg text-info">-</div>
                                <div class="text-muted text-uppercase font-weight-bold small"> Ofertas Hoxe</div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-4">
                <div class="card text-white bg-light">

                    <div class="card-header">Ofertas por tipo de contrato
                        <div class="card-header-actions">

                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart-wrapper bg-white">
                            <canvas id="totalOfertaContrato"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row bg-gray-300 ">
            <div class="col-sm-6 col-lg-2">
                <div class="row ">
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">
                            <i class="fa fa-graduation-cap bg-info p-2 font-4xl mr-3"></i>
                            <div>
                                <div id="totalCursosPublicados" class="text-value-lg text-info">-</div>
                                <div class="text-muted text-uppercase font-weight-bold small">Cursos Totais</div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">

                            <span class="glyphicon glyphicon-education text-info" style="font-size: 200%;"> </span>
                            <div>
                                <div id="totalCursosNuevos" class="text-value-lg text-info">-</div>
                                <div class="text-muted text-uppercase font-weight-bold small">Cursos Hoxe</div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-4">
                <div class="card text-white bg-info">

                    <div class="card-header">Tipos de Cursos
                        <div class="card-header-actions">

                        </div>
                    </div>
                    <div class="card-body bg-info">
                        <div class="chart-wrapper bg-white">
                            <canvas id="totalCursosTipo"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row ">
            <div class="col-sm-6 col-lg-2">
                <div class="row ">
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">
                             <i class="fa fa-comments-o bg-info p-2 font-4xl mr-3"></i>
                            
                            <div>
                                <div id="totalNoticiasPublicadas" class="text-value-lg text-info">-</div>
                                <div class="text-muted text-uppercase font-weight-bold small">Novas Totais</div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">

                             <i class="fa fa-commenting-o bg-info p-2 font-4xl mr-3"></i>
                            <div>
                                <div id="totalNoticiasNuevas" class="text-value-lg text-info">-</div>
                                <div class="text-muted text-uppercase font-weight-bold small">Novas Hoxe</div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-4">
                <div class="card text-white bg-light">

                    <div class="card-header">Novas por Ano
                        <div class="card-header-actions">

                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart-wrapper bg-white">
                            <canvas id="totalNoticiasCategoria"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


</div>
</div>




@endsection
