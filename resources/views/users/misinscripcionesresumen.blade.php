<section class="contenido">
    <div class="container">
        <div class="row ">

            <div class="col-md-12 content-oferta" >

                <div class="col-md-12 content-oferta" style="height:200px;  overflow-y : scroll; margin-top: 10px">
                    <div class="col-md-12">

                    </div>
                    @if (count($ofertas) > 0)
                    <table id="users-table" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead class="thead-light">
                            <tr>
                                <th colspan="4"><h2 class="title color-primary" ><strong>{{__('Inscripciones Ofertas')}}</strong></h2></th>

                            </tr>
                            <tr>
                                <th style ="width:30%">{{__('Nome Oferta')}}</th>
                                <th style ="width:30%">{{__('Data Incripcion')}}</th>
                                <th style ="width:30%">{{__('Data Inicio')}}</th>
                                <th style ="width:30%">{{__('Acciones')}}</th>
    <!--                            <th style ="width:10%">{{__('Estatus')}}</th>-->

                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ofertas as $oferta)
                            <tr>
                                <td><b><a href="{{url('/panel/ofertas/'.$oferta->id.'/edit')}}" target="_blank"> {{$oferta->requested_job }} </b></td>
                                <td>{{date('d/m/Y H:m:s', strtotime($oferta->pivot->created))}} </td>
                                <td>{{date('d/m/Y H:m:s', strtotime($oferta->start_date))}} </td>
                                <td><a href="{{url('user/' . $user_id . '/certificadooferta/'.$oferta->id)}}" target="_blank" title="Certificado de inscrición"><span class='glyphicon glyphicon-check'></span></a>&nbsp; &nbsp; &nbsp;<a id="remove_link" href="{{url('/panel/admin/ofertas/'.$oferta->id.'/retirar/'.$user_id)}}" title="Retirar" ><span class="glyphicon glyphicon-remove" style="color:red"></span></a> </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                    @else 
                    <th colspan="3"><h3 class="title color-primary" ><strong>{{__('Sen rexistro de ofertas')}}</strong></h3></th>
                    @endif 

                </div>

                <div class="col-md-12 content-oferta" style="height:200px;  overflow-y : scroll; margin-top: 30px">
                    <div class="col-md-12">
                    </div>
                    @if (count($cursos) > 0)
                    <table id="users-table" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead class="thead-light">
                            <tr>
                                <th colspan="4"><h2 class="title color-primary" ><strong>{{__('Inscripciones Cursos')}}</strong></h2></th>

                            </tr>
                            <tr>
                                <th style ="width:30%">{{__('Nome Curso')}}</th>
                                <th style ="width:30%">{{__('Data Incripcion')}}</th>
                                <th style ="width:30%">{{__('Data Inicio')}}</th>
                                <th style ="width:30%">{{__('Acciones')}}</th>
    <!--                            <th style ="width:10%">{{__('Estatus')}}</th>-->

                            </tr>
                        </thead>
                        <tbody>
                            @foreach($cursos as $curso)
                            <tr>

                                <td><b><a href="{{url('/panel/cursos/'.$curso->id.'/edit')}}" target="_blank"> {{$curso->nombre }} </b></td>
                                <td>{{date('d/m/Y H:m:s', strtotime($curso->pivot->fechainscripcion))}} </td>
                                <td>{{date('d/m/Y H:m:s', strtotime($curso->fecha_inicio))}} </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                    @else 
                    <th colspan="3"><h3 class="title color-primary" ><strong>{{__('Sen rexistro de curso')}}</strong></h3></th>
                    @endif 
                    

                </div>


            </div>
        </div>
    </div>

</section><!-- /contenido -->
