
<section class="contenido">
    <div class="container">
        <div class="row ">

            <div class="col-md-12 content-oferta" >

                <div class="col-md-12 content-oferta" style="height:200px;  overflow-y : scroll; margin-top: 10px">

                    @if (count($ofertas) > 0)
                   

                    <table data-user="{{$user_id}}" id="usersoffers" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead class="thead-light">

                            <tr>
                                <th> 
                                    <!--<input type="checkbox" class="custom-checkbox" >-->
                                </th>
                                <th >{{__('Nome Oferta')}}</th>
                                <th >{{__('Data Incripcion')}}</th>
                                <th >{{__('Data Inicio')}}</th>
    <!--                            <th style ="width:10%">{{__('Estatus')}}</th>-->

                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ofertas as $oferta)
                            <tr data-user="{{$user_id}}" id="{{$oferta->id}}">
                                <td></td>
                                <td> {{$oferta->requested_job }}</td>
                                <td>{{date('d/m/Y H:m:s', strtotime($oferta->pivot->created))}} </td>
                                <td>{{date('d/m/Y H:m:s', strtotime($oferta->start_date))}} </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                    @else 
                    <th colspan="3"><h3 class="title color-primary" ><strong>{{__('Sen rexistro de ofertas')}}</strong></h3></th>
                    @endif 

                </div>

            </div>
        </div>
    </div>



</section><!-- /contenido -->

