<section class="contenido">
    <div class="container">
        <div class="row ">

            <div class="col-md-12 content-oferta" >

                <div class="col-md-12 content-oferta" style="height:200px;  overflow-y : scroll; margin-top: 10px">
                    <div class="col-md-12">

                    </div>
                    @if (count($auditorias) > 0)
                    <table id="users-table" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead class="thead-light">
                            <tr>
                                <th colspan="3"><h2 class="title color-primary" ><strong>{{__('Operaciones del Usuario')}}</strong></h2></th>

                            </tr>
                            <tr>
                                <th style ="width:30%">{{__('Operacion')}}</th>
                                <th style ="width:30%">{{__('Data operación')}}</th>
    <!--                            <th style ="width:10%">{{__('Estatus')}}</th>-->

                            </tr>
                        </thead>
                        <tbody>
                            @foreach($auditorias as $audioria)
                            

                            <tr>
                                
                                <td>{{$audioria->action}} </td>
                                <td>{{date('d/m/Y H:m:s', strtotime($audioria->publishedAt))}} </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                    @else 
                     <th colspan="3"><h3 class="title color-primary" ><strong>{{__('Sen rexistro de operaciones')}}</strong></h3></th>
                    @endif 

                </div>
            </div>
        </div>
    </div>

</section><!-- /contenido -->
