@extends('layouts.app')

@section('title_pestaña')
| Elegir Evento
@endsection

@section('titulo_pantalla')

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">{{__('oferta.Inscripciones')}} </li>
    <li class="breadcrumb-item active"> {{__('oferta.Inscripciones')}} </li>
    <!-- Breadcrumb Menu-->
</ol>
@endsection

@section('content')
@if(Session::has('alert-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
</div>
@endif
@if(Session::has('alert-danger'))
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-danger"></i> {{ Session::get('alert-danger') }}</h4>
</div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">{{__('oferta.Inscripciones')}}

            </div>
            <div class="card-body">
                <div class="toolbar">
                    Empresa: {{$oferta->propietario->name}}:  {{$oferta->requested_job}} Fecha: {{date('d/m/Y', strtotime($oferta->start_date))}} Lugar: {{$oferta->locality}}

                </div>


                <div class="row">

                    <div class="col-md-12">
                        <h2></h2>
                    </div>

                    <div class="col-md-8">
                        <div class="input-group">
                            <input type="text" id="dni" class="form-control" placeholder="Incribir usuario por DNI">
                            <span class="input-group-btn">
                                <button id="buscar-user-job" class="btn btn-default" type="button"> Buscar e Incribir...</button>
                            </span>



                        </div><!-- /input-group -->
                    </div>
                    <div class="col-md-12">
                        <span id="Datos" class="">
                        </span>
                    </div>
                    <input id="idOferta"type="hidden" value="{{$oferta->id}}">


                </div>
                </br>
                </br>

                <div class="row"> 
                    <div class="col-lg-12">
                        <table id="table-inscritos-job" class="table table-responsive-sm table-hover table-outline mb-0">
                            <thead class="thead-light">
                                <tr>
                                    <th>{{__('DNI')}}</th>
                                    <th>{{__('Nombre')}}</th>
                                    <th >{{__('Apellidos')}}</th>
                                    <th>{{__('Email')}}</th>
                                    <th>{{__('Telefono')}} </th>
                                    <th>{{__('Data Inscripcion')}} </th>
                                     <th>{{__('C.V')}} </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($oferta->postulados)
                                @foreach($oferta->postulados as $inscrito)
                                <tr>
                                    <td><b>{{ $inscrito->cif }}</b><br></td>
                                    <td>{{ $inscrito->name }}</td>
                                    <td>{{ $inscrito->surname }}</td>
                                    <td>{{ $inscrito->email }}</td>
                                    <td>{{ $inscrito->telephone }}</td>

                                    <td>{{date('d/m/Y', strtotime($inscrito->pivot->created))}}</td>
                                    <td> <a href="/bajarcv/{{$inscrito->pivot->curriculo}} " target="_black">{{ $inscrito->pivot->curriculo }}</a></td>
                                    <td style="text-align:center; vertical-align:middle;">							
                                        &emsp;<a id="remove_link" href="{{url('/panel/admin/ofertas/'.$oferta->id.'/retirar/'.$inscrito->pk)}}" title="Retirar" ><span class="glyphicon glyphicon-remove" style="color:red"></span></a>
                                    </td>
                                </tr>
                                @endforeach	
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.col-->
</div>
<!-- /.row-->
<div class="modal " tabindex="-1" role="dialog" id="modalincribir" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Inscribir en Oferta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row " style="margin-left:10px" >

                    <form action="/panel/admin/ofertas/inscribir" method="POST" class="form-horizontal" id ="formoIncribir">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="col-sm-12 " for="form-field-1">Inscribiendo en la Oferta: </label>
                            <div class="col-sm-12">
                                <h3>Empresa: {{$oferta->propietario->name}}</h3>
                                </br>
                                <h3>Oferta: {{$oferta->requested_job}}</h3>
                                </br>
                                Fecha: {{date('d/m/Y', strtotime($oferta->start_date))}} 
                                </br>
                                Lugar: {{$oferta->locality}} 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12" for="form-field-1">Datos del Usuario</label>
                            <div class="col-sm-12">
                                <span id="datosdelusuario"> </span>
                            </div>
                        </div>


                        <input name="idOferta" id="idOferta"type="hidden" value="{{$oferta->id}}">
                        <input name = "idUser" id="idUser"type="hidden" value="">

                        <button  type="submit" class="btn btn-primary">Aceptar</button>
                    </form>

                </div>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

@endsection
