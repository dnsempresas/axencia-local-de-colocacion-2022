@extends('layouts.app')

@section('titulo_pantalla')

@endsection

@section('content')
<style>
    .tags{
        width: 100% !important;
    }
</style>
@if ($errors->any())
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-ban"></i></h4>
    @foreach ($errors->all() as $error)
    <p>{{ $error }}</p>
    @endforeach
</div>
@endif
<div id="crearuser" class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <i class="icon-note"></i> {{__('users.modulo')}}
            </div>
            <div class="card-body">
                <form method="POST" action="{{url('/panel/usuarios')}}" accept-charset="UTF-8" class="form-horizontal" id="userForm" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div id="izquierda" class="col-md-6">

                            <div class="panel panel-default">
                                <div class="panel-heading">Datos de Acceso </div>
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right">Tipo de Usuario</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select name="tiporole" id="tiporole">
                                                    <option value="1" {{ (old('tiporole') == 1 ) ? 'selected' : '' }} >Demandante de emprego</option>

                                                    <option value="2" {{ (old('tiporole') == 2 ) ? 'selected' : '' }} >Empresa</option>
                                                    <option value="3" {{ (old('tiporole') == 3 ) ? 'selected' : '' }}>Administrador</option>


                                                </select>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="form-group required">
                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Correo Electrónico</label>
                                        <div class="col-sm-6">
                                            <input required type="email" name="email" id="email" placeholder="Correo electronico" class="col-xs-10 col-sm-5" value="{{old('email')}}"  />
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1">{{__('users.contrasena')}}</label>
                                        <div class="col-sm-6">
                                            <input required type="password" name="password" id="password" placeholder="{{__('users.contrasena')}}" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>
                                    <!--                                    <div class="form-group">
                                                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">{{__('users.repitacontrasena')}}</label>
                                                                            <div class="col-sm-6">
                                                                                <input type="password" name="rpassword" id="rpassword" placeholder="{{__('users.repitacontrasena')}}" class="col-xs-10 col-sm-5" />
                                                                            </div>
                                                                        </div>-->


                                </div>
                            </div>

                            <div id="panel_datosempresariales" class="panel panel-default" style="display:none" >
                                <div class="panel-heading">Datos de Empresa</div>
                                <div class="panel-body">

                                    <div class="form-group required">

                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('users.nombreempresa')}} </label>

                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="name_e" id="name_e" placeholder=" {{__('users.nombreempresa')}}" value="{{old('name')}}" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('users.denominacionSocial')}} </label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="last_name_e" id="last_name_e" placeholder="{{__('users.denominacionSocial')}}" value="{{old('last_name')}}" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> NIF/CIF </label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="cif_e" id="cif_e" placeholder="NIF/CIF" value="{{old('cif')}}"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="panel_datospersonales" class="panel panel-default">
                                <div class="panel-heading">{{__('users.datospersonales')}}  </div>
                                <div class="panel-body">

                                    <div class="form-group required">

                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('users.name')}} </label>

                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <input type="text" name="name" id="name" placeholder="{{__('users.name')}}"  value="{{old('name')}}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group required">
                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('users.last_name')}} </label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <input type="text" name="last_name" id="last_name" placeholder="{{__('users.last_name')}}" value="{{old('last_name')}}" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group required">
                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> DNI/NIE </label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <input required type="text" name="cif" id="cif" placeholder="DNI/NIE" value="{{old('cif')}}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-3 control-label no-padding-right">{{__('users.genero')}}</label>
                                        <div class="col-sm-9">

                                            <div class="input-group">
                                                <select required name="genero" id="genero">
                                                    <option value="">Selecione...</option>
                                                    @foreach($generos as $genero)
                                                    <option value="{{$genero->id}}" {{ (old('genero') == $genero->id ) ? 'selected' : '' }} >{{ session('lang')=='es' ?  $genero->nombre :  $genero->nome }} </option>
                                                    @endforeach

                                                </select>
                                                <div class="input-group">
                                                    <input required style="margin-top: 5px;display: none;" class="form-control" id="other_genero" name="other_genero" type="text" placeholder="Especifique Genero..."/>
                                                </div>
                                            </div>



                                        </div>
                                    </div>

                                    <div class="form-group required">
                                        <label class="col-sm-3 control-label no-padding-right">{{__('users.fechanacimiento')}}</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <input class="form-control date-picker" id="birth" name="birth" type="text" data-date-format="dd/mm/yyyy"   value="{{!old('birth')?"01/01/2000":old('birth')}}"/>



                                            </div>	
                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <span class="col-sm-3 control-label no-padding-right">Foto de Perfil</span>
                                        <div class="col-sm-4">
                                            <img id="imgSalida" width="120px" height="140px" src="" />
                                            <input id="foto_perfil" type="file" name="foto_perfil" accept="image/*"  >
                                            <a class="removeimg" id ="removeimg" href="" title='Borrar'style="display: none" ><span class='glyphicon glyphicon-remove' style='color:red; '></span></a>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right"> {{__('users.Discapacidad')}} </label>


                                        <div class="col-sm-8">
                                            <label class="switch switch-label switch-outline-danger">
                                                <input name="disability" type="checkbox" class="switch-input" value="{{old('disability')}}"/>
                                                <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                            </label>
                                        </div>


                                    </div>





                                </div>
                            </div>
                            <div id="panel_direccion" class="panel panel-default">
                                <div class="panel-heading"> Dirección  </div>
                                <div class="panel-body">
                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right">Provincia</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select class="" required name="province" id="province">
                                                    <option value="">Selecione...</option>
                                                    @foreach($provinces as $province)
                                                    <option value="{{$province->pk}}" <?php if ((old('province') == $province->pk) || $province->pk==15 ) echo "selected='selected'"; ?>>{{$province->name}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="input-group">
                                                    <input required style="margin-top: 5px;display: none;" class="form-control" id="other_province" name="other_province" type="text" placeholder="Especifique Provincia..."/>
                                                </div>
                                            </div>	
                                        </div>

                                    </div>

                                    

                                    <div class="form-group required">
                                        <label id="labelconcello" class="col-sm-4 control-label no-padding-right">{{__('users.ayuntamiento')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select required class="" name="concello_chosen" id="concello_chosen" data-placeholder="Seleccione...">

                                                </select>

                                                <input required style="margin-top: 5px;display: none;" class="form-control" id="other_concello" name="other_concello" type="text" placeholder="Especifique..."/>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Direccion</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="address" id="address" placeholder="Direccion" value="{{old('address')}}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Codigo Postal</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="number" name="postal_code" id="postal_code" placeholder="Codigo Postal"  value="{{old('postal_code')}}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1">Teléfono</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="number" name="phone" id="phone" maxlength=9 placeholder="Telefono" value="{{old('phone')}}" />
                                            </div>
                                        </div>
                                    </div>	
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1">{{__('frontend.Otro_telefono')}}</label>
                                        <div class="col-sm-8">
                                            <input type="number" name="other_phone" id="other_phone" maxlength=9 placeholder="{{__('frontend.Otro_telefono')}}" value="{{old('other_phone')}}"/>
                                        </div>
                                    </div>



                                </div>
                            </div>
                        </div>


                        <div id="derecha" class="col-md-6">
                            <div id="panel_datosprofesionales" class="panel panel-default">
                                <div class="panel-heading"> {{__('frontend.Datos_Profesionales')}} </div>
                                <div class="panel-body">

                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right">{{__('frontend.Estudios')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select name="studies" id="studies">

                                                    @foreach($grades as $grade)
                                                    <option value="{{$grade->id}}">{{ session('lang')=='es' ?  $grade->description :  $grade->nome }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group required" id="specialtyC">
                                        <label class="col-sm-4 control-label no-padding-right">{{__('frontend.Especialidad')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select class="chosen-select" name="specialty" id="specialty" data-placeholder="Seleccione..."></select>
                                            </div>
                                            <select  class="chosen-select" name="specialty2[]" id="specialty2" multiple data-placeholder="" >
                                            </select>


                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">{{__('frontend.Otros_Cursos')}}:</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <textarea name="others_courses" id="others_courses" value="{{old('others_courses')}}"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label id="labelpuestos" class="col-sm-4 control-label no-padding-right">{{__('frontend.Sin_experiencia_profesional')}}</label>
                                        <div class="col-sm-8">
                                            <input class="ace" type="checkbox" id="no_experience" name="no_experience" style="margin-top: 10px;"  value ="0" unchecked /> 
                                            <span class="lbl"></span>

                                        </div>

                                    </div>
                                    <div class="form-group" id="divpuestos">
                                        <label id="labelpuestos" class="col-sm-4 control-label no-padding-right">{{__('frontend.Puestos_de_Trabajo')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select  class="chosen-select" name="job_posts[]" id="job_posts" multiple="" data-placeholder="Puestos de trabajo en que tienes experiencia profesional">
                                                    @foreach($jobPosts as $jobPost)
                                                    <option value="{{$jobPost->id}}">{{ session('lang')=='es' ?  $jobPost->nombre :  $jobPost->nome }}</option>
                                                    @endforeach
                                                </select>

                                            </div>

                                        </div>

                                    </div>

                                    <div id="divprestaciones" class="form-group">
                                        <label id="labelprestaciones" class="col-sm-4 control-label no-padding-right">{{__('frontend.Percibe_prestacion_o_subsidio_por_desempleo')}}</label>
                                        <div class="col-sm-8">
                                            <input class="ace" type="checkbox" id="prestacion" name="prestacion" style="margin-top: 10px;" value="{{old('prestacion')}}"/> 
                                            <span class="lbl"></span>

                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">Idiomas</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="languages" id="languages" placeholder="Ingles avanzado..." value="{{old('languages')}}"/>
                                                <!--<select name="langs" id="langs">
                                                        @foreach($langs as $lang)
                                                                <option value="{{$lang->id}}">{{$lang->name}}</option>
                                                        @endforeach
                                                </select>
                                                <select name="lang_level" id="lang_level" style="margin-top: 5px;">
                                                        <option value="native">Nativo</option>
                                                        <option value="fluid">Avanzado</option>
                                                        <option value="intermediate">Intermedio</option>
                                                        <option value="basic">Basico</option>
                                                </select>-->

                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">Informatica</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="computer_skills" id="computer_skills" placeholder="Microsoft Access avanzado..." />
                                                <!--<select name="computer" id="computer">
                                                        <option value="access">Microsoft Access</option>
                                                        <option value="excel">Microsoft Excel</option>
                                                        <option value="word">Microsoft Word</option>
                                                        <option value="powerpoint">Microsoft PowerPoint</option>
                                                        <option value="photoshop">Microsoft PhotoShop</option>
                                                        <option value="dreamweaver">Microsoft Dreamweaver</option>
                                                        <option value="corel_draw">Corel Draw</option>
                                                </select>
                                                <select name="computer_level" id="computer_level" style="margin-top: 5px;">
                                                        <option value="fluid">Avanzado</option>
                                                        <option value="intermediate">Intermedio</option>
                                                        <option value="basic">Basico</option>
                                                </select>-->
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">Permisos de Conducir</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select class="chosen-select" name="driving_permit[]" id="driving_permit" multiple data-placeholder="Seleccione permisos de conducir...">
                                                    @foreach($permits as $permit)
                                                    <option data-img-src="/public/permisosconducir/{{$permit->icono}}" value="{{$permit->id}}">{{$permit->description}}</option>

                                                    @endforeach

                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group required row">
                                        <label class="col-sm-4 control-label no-padding-right" for="file-input">CV Principal</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input id="cvprincipal" type="file" name="cvprincipal" accept="application/pdf,application/msword,
                                                       application/vnd.openxmlformats-officedocument.wordprocessingml.document" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 control-label no-padding-right" for="file-input">CV Secundario</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input id="cvsecundario" type="file" name="cvsecundario"  accept="application/pdf,application/msword,
                                                       application/vnd.openxmlformats-officedocument.wordprocessingml.document" >
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>


                            <div id="panel_intereseprofesionales" class="panel panel-default">
                                <div class="panel-heading"> {{__('frontend.Intereses_Profesionales')}} </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">{{__('frontend.Sectores_de_Interes')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select class="chosen-select" name="sectors[]" id="sectors" multiple data-placeholder="Seleccione sectores...">
                                                    @foreach($sectors as $sector)
                                                    <option value="{{$sector->id}}">{{ session('lang')=='es' ?  $sector->nombre :  $sector->nome }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group" id="divpuestos">
                                        <label id="labelpuestos" class="col-sm-4 control-label no-padding-right">{{__('frontend.Puestos_de_Interes')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select  class="chosen-select" name="positions[]" id="positions" multiple="" data-placeholder="Puestos en que te gustaría trabajar">
                                                    @foreach($jobPosts as $jobPost)
                                                    <option value="{{$jobPost->id}}">{{ session('lang')=='es' ?  $jobPost->nombre :  $jobPost->nome }}</option>
                                                    @endforeach
                                                </select>

                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </div>

                            <div id="panel_personadecontacto" class="panel panel-default" style="display:none">
                                <div class="panel-heading"> {{__('frontend.Persona_de_contacto')}} </div>
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Persona_de_contacto')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="contacto" id="contacto" placeholder="{{__('frontend.Persona_de_contacto')}}" value="{{old('contacto')}}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Cargo')}} </label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="cargocontacto" id="cargocontacto" placeholder="{{__('frontend.Cargo_Persona_de_contacto')}}" value="{{old('cargocontacto')}}  "/>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            
                             <div id="panel_logo" class="panel panel-default" style="display:none" >
                                <div class="panel-heading"> {{__('Logo')}}  </div>
                                <div class="panel-body">
                            
                            <div class="form-group">
                                        <span class="col-sm-3 control-label no-padding-right">Logo</span>
                                        <div class="col-sm-4">
                                            <img id="imgSalida_e" width="120px" height="140px" src="" />
                                            <input id="foto_perfil_e" type="file" name="foto_perfil_e" accept="image/*"  >
                                            <a class="removeimg" id ="removeimg_e" href="" title='Borrar'style="display: none" ><span class='glyphicon glyphicon-remove' style='color:red; '></span></a>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            
                            
                            <div id="panel_otros" class="panel panel-default" >
                                <div class="panel-heading"> {{__('frontend.Otros')}} </div>
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Estatus </label>
                                        <div class="col-sm-8">
                                            <select class="" name="status" id="status">
                                                <option value = "0"  > INACTIVO</option>
                                                <option value = "1" selected > ACTIVO</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="panel_politica" class="panel panel-default">
                                <div class="panel-heading"> {{__('Politica de Privacidad y Proteccion de Datos')}}  </div>
                                <div class="panel-body">

                                    @include('privacidad/privacidad')

                                    <div class="form-group">
                                        <div class="radio">
                                            <label>
                                                <input id="aceptapolitica" name="aceptapolitica" type="checkbox" class="ace" />
                                                <span class="lbl popup-button"> Acepto a política de protección de datos </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>

                        <div id="divGuardar"  class="col-md-12">
                            <div class="form-group">
                                <button id="guardar" disabled="disabled" type="submit" class="btn btn-success" name="Guardar">{{__('frontend.Guardar')}}</button> 
                            </div>
                        </div>
                    </div>
                </form>						

            </div>
        </div>
    </div>
</div>
@endsection
