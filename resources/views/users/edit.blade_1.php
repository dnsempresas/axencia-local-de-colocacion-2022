@extends('layouts.app')

@section('titulo_pantalla')

@endsection

@section('content')
<style>
    .tags{
        width: 100% !important;
    }
</style>
@if ($errors->any())
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-ban"></i></h4>
    @foreach ($errors->all() as $error)
    <p>{{ $error }}</p>
    @endforeach
</div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <i class="icon-note"></i> Editar Usuario
            </div>
            <div class="card-body">
                <form method="POST" novalidate action="{{route('usuarios.update', $user->pk) }}"  accept-charset="UTF-8" class="form-horizontal" id="userForm" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{method_field('PATCH')}}
                    <div class="row">
                        <div class="col-md-6"> 
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <div class="form-group">

                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Nombre </label>

                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="name" id="name" placeholder="Nombre" value="{{$user->name}}" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Apellidos </label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="last_name" id="last_name" placeholder="Apellidos"  value="{{$user->surname}}"/>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> DNI/NIE/NIF/CIF </label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="cif" id="cif" placeholder="DNI/NIE/NIF" value="{{!$user->cif?$user->nif:$user->cif}}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">Genero</label>
                                        <div class="col-sm-8">

                                            <!--                                            <div class="radio">
                                                                                            <label>
                                                                                                <input name="gender" type="radio" class="ace" value="Hombre" {{$user->sex=='Hombre'?'checked':''}}/>
                                                                                                <span class="lbl"> Hombre</span>
                                                                                            </label>
                                                                                        </div>
                                            
                                                                                        <div class="radio">
                                                                                            <label>
                                                                                                <input name="gender" type="radio" class="ace" value="Mujer" {{$user->sex=='Mujer'?'checked':''}}/>
                                                                                                <span class="lbl"> Mujer</span>
                                                                                            </label>
                                                                                        </div>
                                            
                                                                                        <div class="radio">
                                                                                            <label>
                                                                                                <input name="gender" type="radio" class="ace" value="None" {{$user->sex=='None'?'checked':''}}/>
                                                                                                <span class="lbl"> Prefiero no responder</span>
                                                                                            </label>
                                                                                        </div>-->

                                            <div class="input-group">
                                                <select required name="genero" id="genero">
                                                    <option value="">Selecione...</option>
                                                    @foreach($generos as $genero)
                                                     <option {{$user->genero_fk===$genero->id?'selected':''}}  value="{{$genero->id}}">{{$genero->nombre}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="input-group">
                                                    <input required style="margin-top: 5px; display: {{$user->genero_fk==4?'block':'none'}};" class="form-control" id="other_genero" name="other_genero" type="text" placeholder="Especifique Genero..." value="{{$user->otro_genero}}"/>
                                                </div>
                                            </div>
                                            
                                            
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">Fecha de Nacimiento</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input class="form-control date-picker" id="birth" name="birth" type="text" data-date-format="dd/mm/YYYY" value="{{date('d-m-Y', strtotime($user->birth))}}"/>
                                            </div>	
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">Provincia</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select required name="province" id="province">
                                                    @foreach($provinces as $province)
                                                    <option {{$user->province_fk===$province->pk?'selected':''}} value="{{$province->pk}}">{{$province->name}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="input-group">
                                                    <input required style="margin-top: 5px;display: none;" class="form-control" id="other_province" name="other_province" type="text" placeholder="Especifique Provincia..."/>
                                                </div>
                                            </div>	
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">Ayuntamiento</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select name="concello_chosen" id="concello_chosen" data-placeholder="Seleccione ayuntamiento...">
                                                    <option value="">Seleccione...</option>
                                                    @foreach($concellos as $concello)
                                                    <option {{$user_concello===$concello->id?'selected':''}} value="{{$concello->id}}">{{$concello->name}}</option>
                                                    @endforeach
                                                    <option value="otro">Otro</option>
                                                </select>

                                                <input required style="margin-top: 5px;display: none;" class="form-control" id="other_concello" name="other_concello" type="text" placeholder="Especifique Ayuntamiento..."/>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Direccion</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="address" id="address" placeholder="Direccion" value="{{$user->address}}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Codigo Postal</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="number" name="postal_code" id="postal_code" placeholder="Codigo Postal" value="{{$user->zip}}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1">Telefono</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="number" name="phone" id="phone" maxlength=9 placeholder="Telefono"  value="{{$user->telephone}}"/>
                                            </div>
                                        </div>
                                    </div>	
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1">Otro Telefono</label>
                                        <div class="col-sm-8">
                                            <input type="number" name="other_phone" id="other_phone" maxlength=9 placeholder="Otro Telefono"  value="{{$user->other_phone}}"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1">Correo Electronico</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="email" name="email" id="email" placeholder="Correo electronico" value="{{$user->email}}"  />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-8 control-label no-padding-right">Discapacidad reconocida (igual o superior al 33%)</label>


                                        <div class="radio">
                                            <label>
                                                <input name="disability" type="checkbox" class="ace" />
                                                <span class="lbl"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">Estudios</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select name="studies" id="studies">
                                                    <option value="-1">Seleccione...</option>
                                                    @foreach($grades as $grade)
                                                    <option value="{{$grade->id}}">{{$grade->description}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group" id="specialtyC">
                                        <label class="col-sm-4 control-label no-padding-right">Especialidad</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select class="chosen-select" name="specialty" id="specialty" data-placeholder="Seleccione...">
                                                    <option value="-1">Seleccione...</option>

                                                </select>
                                            </div>

                                            <select class="chosen-select" name="specialty2[]" id="specialty2" multiple="" data-placeholder="" style="padding-top:20px" >
                                                @foreach($specialities as $specialty)
                                                <option selected value="{{$specialty->ID}}">{{$specialty->description}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right">Otros Cursos:</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <textarea name="others_courses" id="others_courses">{{$user->other_courses}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label id="labelpuestos" class="col-sm-4 control-label no-padding-right">Sin experiencia profesional</label>
                                <div class="col-sm-8">
                                    <input class="ace" type="checkbox" id="no_experience" name="no_experience" style="margin-top: 10px;" /> 
                                    <span class="lbl"></span>

                                </div>

                            </div>
                            <div class="form-group" id="divpuestos">
                                <label id="labelpuestos" class="col-sm-4 control-label no-padding-right">Puestos de Trabajo</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <select  required class="chosen-select" name="job_posts[]" id="job_posts" multiple="" data-placeholder="Puestos de trabajo en que tienes experiencia profesional">
                                            @foreach($jobPosts as $jobPost)
                                            <option <?php
                                            $userpuestos = explode('/', $user->experience);
                                            if (in_array($jobPost->id, $userpuestos)) {
                                                echo 'selected';
                                            }
                                            ?> value="{{$jobPost->id}}">{{$jobPost->nome}}</option>
                                            @endforeach
                                        </select>

                                    </div>

                                </div>

                            </div>




                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right">Idiomas</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input type="text" name="languages" id="languages" placeholder="Ingles avanzado..."  value="{{$user->languages}}" />
                                        <!--<select name="langs" id="langs">
                                                @foreach($langs as $lang)
                                                        <option value="{{$lang->id}}">{{$lang->name}}</option>
                                                @endforeach
                                        </select>
                                        <select name="lang_level" id="lang_level" style="margin-top: 5px;">
                                                <option value="native">Nativo</option>
                                                <option value="fluid">Avanzado</option>
                                                <option value="intermediate">Intermedio</option>
                                                <option value="basic">Basico</option>
                                        </select>-->

                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right">Informatica</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input type="text" name="computer_skills" id="computer_skills" placeholder="Microsoft Access avanzado..." value="{{$user->computer_skills}}" />
                                        <!--<select name="computer" id="computer">
                                                <option value="access">Microsoft Access</option>
                                                <option value="excel">Microsoft Excel</option>
                                                <option value="word">Microsoft Word</option>
                                                <option value="powerpoint">Microsoft PowerPoint</option>
                                                <option value="photoshop">Microsoft PhotoShop</option>
                                                <option value="dreamweaver">Microsoft Dreamweaver</option>
                                                <option value="corel_draw">Corel Draw</option>
                                        </select>
                                        <select name="computer_level" id="computer_level" style="margin-top: 5px;">
                                                <option value="fluid">Avanzado</option>
                                                <option value="intermediate">Intermedio</option>
                                                <option value="basic">Basico</option>
                                        </select>-->
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right">Sectores de Interes</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <select class="chosen-select" name="sectors[]" id="sectors" multiple data-placeholder="Seleccione sectores...">
                                            @foreach($sectors as $sector)
                                            <option <?php
                                            $usersectores = explode('/', $user->sector);
                                            if (in_array($sector->id, $usersectores)) {
                                                echo 'selected';
                                            }
                                            ?> value="{{$sector->id}}">{{$sector->nome}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group" id="divpuestos">
                                <label id="labelpuestos" class="col-sm-4 control-label no-padding-right">Puestos de Interes</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <select  class="chosen-select" name="positions[]" id="positions" multiple="" data-placeholder="Puestos en que te gustaría trabajar">
                                            @foreach($jobPosts as $jobPost)
                                            <option <?php
                                            $userpuestos = explode('/', $user->position);
                                            if (in_array($jobPost->id, $userpuestos)) {
                                                echo 'selected';
                                            }
                                            ?> value="{{$jobPost->id}}">{{$jobPost->nome}}</option>
                                            @endforeach
                                        </select>

                                    </div>

                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right">Permisos de Conducir</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <select class="chosen-select" name="driving_permit[]" id="driving_permit" multiple data-placeholder="Seleccione permisos de conducir...">
                                            @foreach($permits as $permit)
                                            <option data-img-src="/permisosconducir/{{$permit->icono}}"  <?php
                                            $userdriven = explode('/', $user->driving_permit);
                                            if (in_array($permit->id, $userdriven)) {
                                                echo 'selected';
                                            }
                                            ?> value="{{$permit->id}}">{{$permit->description}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-sm-4 control-label no-padding-right" for="file-input">CV Principal</label>
                                <div class="col-sm-8">
                                    <input id="cvprincipal" type="file" name="cvprincipal"  accept="application/pdf,application/msword,
                                           application/vnd.openxmlformats-officedocument.wordprocessingml.document" <?php
                                           if ($user->cvprincipal == '') {
                                               echo 'required';
                                           }
                                           ?>>
                                    <a href="/bajar/{{$user->cvprincipal}} ">{{ $user->cvprincipal }}</a>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 control-label no-padding-right" for="file-input">CV Secundario</label>
                                <div class="col-sm-8">
                                    <input id="cvsecundario" type="file" name="cvsecundario" accept="application/pdf,application/msword,
                                           application/vnd.openxmlformats-officedocument.wordprocessingml.document">
                                    <a href="/bajar/{{$user->cvsecundario}} ">{{ $user->cvsecundario }}</a>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success" name="Guardar">Guardar</button> 
                            </div>
                        </div>
                    </div>
                </form>						

            </div>
        </div>
    </div>
</div>
@endsection
