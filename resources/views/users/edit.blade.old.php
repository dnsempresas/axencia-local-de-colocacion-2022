@extends('layouts.app')

@section('titulo_pantalla')

@endsection

@section('content')
<style>
    .tags{
        width: 100% !important;
    }
</style>
@if ($errors->any())
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-ban"></i></h4>
    @foreach ($errors->all() as $error)
    <p>{{ $error }}</p>
    @endforeach
</div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <i class="icon-note"></i> {{__('frontend.Editar_Perfil')}}
            </div>
            <div class="card-body">
                <form method="POST" novalidate action="{{route('usuarios.update', $user->pk) }}"  accept-charset="UTF-8" class="form-horizontal" id="userForm" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{method_field('PATCH')}}
                    <div  class="row">
                        <div id="izquierda" class="col-md-6">

                            <div class="panel panel-default">
                                <div class="panel-heading">{{__('frontend.Datos_de_Acceso')}} </div>
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right">{{__('frontend.Tipo_de_Usuario')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select name="tiporole" id="tiporole">
                                                    <option {{$user->role_id==1?'selected':''}}  value="1">Demandante de emprego</option>

                                                    <option {{$user->role_id==2?'selected':''}}  value="2">Empresa</option>
                                                    
                                                    <option {{$user->role_id==3?'selected':''}}  value="3">Administrador</option>


                                                </select>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="form-group required">
                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1">{{__('frontend.Correo_electronico')}}</label>
                                        <div class="col-sm-6">
                                            <input type="email" name="email" id="email" placeholder="{{__('frontend.Correo_electronico')}}" class="col-xs-10 col-sm-5" value="{{$user->email}}"  />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1">{{__('frontend.Contrasenya')}}</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="password" id="password" placeholder="{{__('frontend.Contrasenya')}}" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>
<!--                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Repita Contraseña</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="rpassword" id="rpassword" placeholder="Repita Contraseña" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>-->


                                </div>
                            </div>

                            <div id="panel_datosempresariales" class="panel panel-default" style="display:{{$user->role_id==2?'block':'none'}} " >
                                <div class="panel-heading">Datos de Empresa</div>
                                <div class="panel-body">

                                    <div class="form-group required">

                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Nombre_de_la_Empresa')}} </label>

                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="name_e" id="name_e" placeholder="{{__('frontend.Nombre_de_la_Empresa')}}" value="{{$user->name}}"  />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Denominacion_Social')}} </label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="last_name_e" id="last_name_e" placeholder="{{__('frontend.Denominacion_Social')}}" value="{{$user->surname}}" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.NIF_CIF')}} </label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="cif_e" id="cif_e" placeholder="{{__('frontend.NIF_CIF')}}" value="{{$user->cif}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="panel_datospersonales" class="panel panel-default" style="display:{{($user->role_id==1||$user->role_id==3)?'block':'none'}} ">
                                <div class="panel-heading">{{__('frontend.Datos_Personales')}} </div>
                                <div class="panel-body">

                                    <div class="form-group required">

                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1">  {{__('frontend.Nombre')}} </label>

                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="name" id="name" placeholder=" {{__('frontend.Nombre')}}"  value="{{$user->name}}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Apellidos')}} </label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="last_name" id="last_name" placeholder="{{__('frontend.Apellidos')}}" value="{{$user->surname}}" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> DNI/NIE </label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="cif" id="cif" placeholder="DNI/NIE" value="{{$user->cif}}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right">{{__('frontend.Genero')}}</label>
                                        <div class="col-sm-8">

                                            <!--                                            <div class="radio">
                                                                                            <label>
                                                                                                <input name="gender" type="radio" class="ace" value="Hombre" {{$user->sex=='Hombre'?'checked':''}}/>
                                                                                                <span class="lbl"> Hombre</span>
                                                                                            </label>
                                                                                        </div>
                                            
                                                                                        <div class="radio">
                                                                                            <label>
                                                                                                <input name="gender" type="radio" class="ace" value="Mujer" {{$user->sex=='Mujer'?'checked':''}}/>
                                                                                                <span class="lbl"> Mujer</span>
                                                                                            </label>
                                                                                        </div>
                                            
                                                                                        <div class="radio">
                                                                                            <label>
                                                                                                <input name="gender" type="radio" class="ace" value="None" {{$user->sex=='None'?'checked':''}}/>
                                                                                                <span class="lbl"> Prefiero no responder</span>
                                                                                            </label>
                                                                                        </div>-->

                                            <div class="input-group">
                                                <select required name="genero" id="genero">
                                                    <option value="">Selecione...</option>
                                                    @foreach($generos as $genero)
                                                    <option {{$user->genero_fk===$genero->id?'selected':''}}  value="{{$genero->id}}">{{ session('lang')=='es' ?  $genero->nombre :  $genero->nome }} </option>
                                                    @endforeach
                                                </select>
                                                <div class="input-group">
                                                    <input required style="margin-top: 5px; display: {{$user->genero_fk==4?'block':'none'}};" class="form-control" id="other_genero" name="other_genero" type="text" placeholder="Especifique Genero..." value="{{$user->otro_genero}}"/>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right">{{__('frontend.Fecha_de_Nacimiento')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input class="form-control date-picker" id="birth" name="birth" type="text" data-date-format="dd/mm/YYYY" value="{{date('d-m-Y', strtotime($user->birth))}}"/>
                                            </div>	
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <span class="col-sm-4 control-label no-padding-right">Foto de Perfil</span>
                                        <div class="col-sm-4">
                                            @if($user->foto)
                                            <img id="imgSalida" width="120px" height="140px" src="{{asset('/public/storage/fotos/'. $user->foto)}}"/>
                                            <input id="foto_perfil" type="file" name="foto_perfil" accept="image/*" >
                                            <a class="removeimg" id ="removeimg" href="" title='Borrar' ><span class='glyphicon glyphicon-remove' style='color:red; '></span></a>

                                            @else
                                            <img id="imgSalida" width="120px" height="140px" style="border: 1px solid gray;" src="<?php
                                            if ($user->genero_fk == 1) {
                                                echo asset('assets/images/mujer.png');
                                            } else {
                                                if ($user->genero_fk == 2) {
                                                    echo asset('assets/images/hombre.png');
                                                } else {
                                                    echo asset('assets/images/otro.png');
                                                }
                                            }
                                            ?>"/>
                                            <input id="foto_perfil" type="file" name="foto_perfil" accept="image/x-png,image/gif,image/jpeg">
                                            <a class="removeimg" id ="removeimg" href="" title='Borrar'style="display: none" ><span class='glyphicon glyphicon-remove' style='color:red; '></span></a>

                                            @endif
                                             <input type="hidden" name="foto_user" id="foto_user" value ="{{ $user->foto }}" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-8 control-label no-padding-right">{{__('frontend.Discapacidad_reconocida_igual_o_superior_al_33')}}</label>


                                        <div class="radio">
                                            <label>
                                                <input class="ace" id="disability" name="disability" type="checkbox"  <?php if ($user->disability == 1) echo 'checked'; ?> />
                                                <span class="lbl"></span>
                                            </label>
                                        </div>
                                    </div>

                                </div>
                            </div>





                            <div id="panel_direccion" class="panel panel-default">
                                <div class="panel-heading"> Dirección  </div>
                                <div class="panel-body">

                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right">Provincia</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select required name="province" id="province">
                                                    <option value="">Selecione...</option>
                                                    @foreach($provinces as $province)
                                                    <option {{$user->province_fk===$province->pk?'selected':''}} value="{{$province->pk}}">{{$province->name}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="input-group">
                                                    <input required style="margin-top: 5px;display: none;" class="form-control" id="other_province" name="other_province" type="text" placeholder="Especifique Provincia..."/>
                                                </div>
                                            </div>	
                                        </div>

                                    </div>

                                    <div class="form-group required">
                                        <label id="labelconcello" class="col-sm-4 control-label no-padding-right">{{__('frontend.Ayuntamiento')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select class="" name="concello_chosen" id="concello_chosen" data-placeholder="Seleccione...">
                                                    <option value="">Seleccione...</option>
                                                    @foreach($concellos as $concello)
                                                    <option {{$user->concello_fk==$concello->id?'selected':''}} value="{{$concello->id}}">{{$concello->name}}</option>
                                                    @endforeach
                                                    <option value="otro">Otro</option>
                                                </select>


                                                <input required style="margin-top: 5px;display: none;" class="form-control" id="other_concello" name="other_concello" type="text" placeholder="Especifique..."/>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Direccion')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="address" id="address" placeholder="{{__('frontend.Direccion')}}" value="{{$user->address}}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Codigo_Postal')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="number" name="postal_code" id="postal_code" placeholder="{{__('frontend.Codigo_Postal')}}" value="{{$user->zip}}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1">{{__('frontend.Telefono')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="number" name="phone" id="phone" maxlength=9 placeholder="{{__('frontend.Telefono')}}"  value="{{$user->telephone}}"/>
                                            </div>
                                        </div>
                                    </div>	
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1">{{__('frontend.Otro_telefono')}}</label>
                                        <div class="col-sm-8">
                                            <input type="number" name="other_phone" id="other_phone" maxlength=9 placeholder="{{__('frontend.Otro_telefono')}}"  value="{{$user->other_phone}}"/>
                                        </div>
                                    </div>



                                </div>
                            </div>
                        </div>


                        <div id="derecha" class="col-md-6">
                            <div id="panel_datosprofesionales" class="panel panel-default" style="display:{{($user->role_id==1)?'block':'none'}}">
                                <div class="panel-heading"> {{__('frontend.Datos_Profesionales')}} </div>
                                <div class="panel-body">

                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right">{{__('frontend.Estudios')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select name="studies" id="studies">
                                                    <option value="-1">Seleccione...</option>
                                                    @foreach($grades as $grade)
                                                    <option value="{{$grade->id}}">{{$grade->description}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="form-group required" id="specialtyC">
                                        <label class="col-sm-4 control-label no-padding-right">{{__('frontend.Especialidad')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select class="chosen-select" name="specialty" id="specialty" data-placeholder="Seleccione...">
                                                    <option value="-1">Seleccione...</option>

                                                </select>
                                            </div>

                                            <select class="chosen-select" name="specialty2[]" id="specialty2" multiple="" data-placeholder="" style="padding-top:20px" >
                                                @foreach($specialities as $specialty)
                                                <option selected value="{{$specialty->id}}">{{$specialty->description}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">{{__('frontend.Otros_Cursos')}}:</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <textarea name="others_courses" id="others_courses">{{$user->other_courses}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label id="labelpuestos" class="col-sm-4 control-label no-padding-right">{{__('frontend.Sin_experiencia_profesional')}}</label>
                                        <div class="col-sm-8">
                                            <input class="ace" type="checkbox" id="no_experience" name="no_experience" style="margin-top: 10px;" <?php if (!$user->experiencias->count() >= 1) echo 'checked'; ?>  value = "<?php if ($user->experiencias->count() >= 1) echo '0'; else echo '1'; ?>"  />
                                            <span class="lbl"></span>

                                        </div>

                                    </div>
                                    <div class="form-group" id="divpuestos" style="display: <?php if ($user->experiencias->count() >= 1) echo 'block'; else echo 'none'; ?>" >
                                        <label id="labelpuestos" class="col-sm-4 control-label no-padding-right">{{__('frontend.Puestos_de_Trabajo')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select  class="chosen-select" name="job_posts[]" id="job_posts" multiple="" data-placeholder="{{__('frontend.Puestos_de_trabajo_en_que_tienes_experiencia_profesional')}}">
                                                    @foreach($jobPosts as $jobPost)
                                                    <option <?php
                                                    //$userpuestos = explode('/', $user->experience);
                                                    if (in_array($jobPost->id, $userpuestos)) {
                                                        echo 'selected';
                                                    }
                                                    ?> value="{{$jobPost->id}}">{{$jobPost->nome}}</option>
                                                    @endforeach
                                                </select>

                                            </div>

                                        </div>

                                    </div>

                                    <div id="divprestaciones" class="form-group">
                                        <label id="labelprestaciones" class="col-sm-4 control-label no-padding-right">{{__('frontend.Percibe_prestacion_o_subsidio_por_desempleo')}}</label>
                                        <div class="col-sm-8">
                                            <input  class="ace" type="checkbox" id="prestacion" name="prestacion" style="margin-top: 10px;"  <?php if ($user->prestacion== 1) echo 'checked'; ?> /> 
                                            <span class="lbl"></span>

                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">{{__('frontend.Idiomas')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="languages" id="languages" placeholder="Ingles avanzado..."  value="{{$user->languages}}" />
                                                <!--<select name="langs" id="langs">
                                                        @foreach($langs as $lang)
                                                                <option value="{{$lang->id}}">{{$lang->name}}</option>
                                                        @endforeach
                                                </select>
                                                <select name="lang_level" id="lang_level" style="margin-top: 5px;">
                                                        <option value="native">Nativo</option>
                                                        <option value="fluid">Avanzado</option>
                                                        <option value="intermediate">Intermedio</option>
                                                        <option value="basic">Basico</option>
                                                </select>-->

                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">{{__('frontend.Informatica')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="computer_skills" id="computer_skills" placeholder="Microsoft Access avanzado..." value="{{$user->computer_skills}}" />
                                                <!--<select name="computer" id="computer">
                                                        <option value="access">Microsoft Access</option>
                                                        <option value="excel">Microsoft Excel</option>
                                                        <option value="word">Microsoft Word</option>
                                                        <option value="powerpoint">Microsoft PowerPoint</option>
                                                        <option value="photoshop">Microsoft PhotoShop</option>
                                                        <option value="dreamweaver">Microsoft Dreamweaver</option>
                                                        <option value="corel_draw">Corel Draw</option>
                                                </select>
                                                <select name="computer_level" id="computer_level" style="margin-top: 5px;">
                                                        <option value="fluid">Avanzado</option>
                                                        <option value="intermediate">Intermedio</option>
                                                        <option value="basic">Basico</option>
                                                </select>-->
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">{{__('frontend.Permisos_de_Conducir')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select class="chosen-select" name="driving_permit[]" id="driving_permit" multiple data-placeholder="Seleccione permisos de conducir...">
                                                    @foreach($permits as $permit)
                                                    <option data-img-src="/public/permisosconducir/{{$permit->icono}}"  <?php
                                                    $userdriven = explode('/', $user->driving_permit);
                                                    if (in_array($permit->id, $userdriven)) {
                                                        echo 'selected';
                                                    }
                                                    ?> value="{{$permit->id}}">{{$permit->description}}</option>
                                                    @endforeach

                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group required row">
                                        <label class="col-sm-4 control-label no-padding-right" for="file-input">CV Principal</label>
                                        <div class="col-sm-8">
                                            <input id="cvprincipal" type="file" name="cvprincipal"  accept="application/pdf,application/msword,
                                                   application/vnd.openxmlformats-officedocument.wordprocessingml.document">
                                            <a id="nombreCv1"  href="/bajarcv/{{$user->cvprincipal}} ">{{ $user->cvprincipal }}</a>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 control-label no-padding-right" for="file-input">CV Secundario</label>
                                        <div class="col-sm-8">
                                            
                                            
                                            <input id="cvsecundario" type="file" name="cvsecundario" accept="application/pdf,application/msword,
                                                   application/vnd.openxmlformats-officedocument.wordprocessingml.document" value ="{{ $user->cvsecundario }}">
                                            <a id = "cv2" href="/bajarcv/{{$user->cvsecundario}} ">{{ $user->cvsecundario }}</a>
                                            
                                            
                                           @if( $user->cvsecundario)
                                             <a class="removeCv" id ="removeCV2" href="" title='Borrar'  ><span class='glyphicon glyphicon-remove' style='color:red; '></span></a>

                                            @else
                                              <a class="removeCv" id ="removeCV2" href="" title='Borrar' style="display: none" ><span class='glyphicon glyphicon-remove' style='color:red; '></span></a>

                                            @endif 
                                            <input type="hidden" name="cvsecundario_user" id="cvsecundario_user" value ="{{ $user->cvsecundario }}" />
                                        </div>
                                    </div>


                                </div>
                            </div>


                            <div id="panel_intereseprofesionales" class="panel panel-default" style="display:{{($user->role_id==1)?'block':'none'}} ">
                                <div class="panel-heading"> {{__('frontend.Intereses_Profesionales')}} </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">{{__('frontend.Sectores_de_Interes')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select class="chosen-select" name="sectors[]" id="sectors" multiple data-placeholder="Seleccione sectores...">
                                                    @foreach($sectors as $sector)
                                                    <option <?php
                                                    //$usersectores = explode('/', $user->sector);

                                                    if (in_array($sector->id, $usersectores)) {
                                                        echo 'selected';
                                                    }
                                                    ?> value="{{$sector->id}}">{{$sector->nome}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group" id="divpuestos">
                                        <label id="labelpuestos" class="col-sm-4 control-label no-padding-right">{{__('frontend.Puestos_de_Interes')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select  class="chosen-select" name="positions[]" id="positions" multiple="" data-placeholder="Puestos en que te gustaría trabajar">
                                                    @foreach($jobPosts as $jobPost)
                                                    <option <?php
                                                    //$userpuestos = explode('/', $user->position);
                                                    if (in_array($jobPost->id, $userposition)) {
                                                        echo 'selected';
                                                    }
                                                    ?> value="{{$jobPost->id}}">{{$jobPost->nome}}</option>
                                                    @endforeach
                                                </select>

                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </div>

                            <div id="panel_personadecontacto" class="panel panel-default" style="display:{{$user->role_id==2?'block':'none'}} ">
                                <div class="panel-heading"> {{__('frontend.Persona_de_contacto')}} </div>
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Persona_de_contacto')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="contacto" id="contacto" placeholder="{{__('frontend.Persona_de_contacto')}}" value="{{$user->contacto}}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Cargo')}} </label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="cargocontacto" id="cargocontacto" placeholder="{{__('frontend.Cargo_Persona_de_contacto')}}" value="{{$user->cargocontacto}}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="panel_logo" class="panel panel-default" style="display:{{$user->role_id==2?'block':'none'}}" >
                                <div class="panel-heading"> {{__('Logo')}}</div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <span class="col-sm-4 control-label no-padding-right">Logo Empresa</span>
                                        <div class="col-sm-4">
                                            <img id="imgSalida_e" width="120px" height="140px" src="{{asset('/public/storage/fotos/'. $user->foto)}}"/>
                                            <input id="foto_perfil_e" type="file" name="foto_perfil_e" accept="image/*" >
                                            <a class="removeimg_e" id ="removeimg_e" href="" title='Borrar' ><span class='glyphicon glyphicon-remove' style='color:red; '></span></a>

                                          
                                             <input type="hidden" name="foto_user_e" id="foto_user_e" value ="{{ $user->foto }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="panel_otros" class="panel panel-default" >
                                <div class="panel-heading"> {{__('frontend.Otros')}} </div>
                                <div class="panel-body">

                                   <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" > {{__('frontend.fecha')}} Alta: </label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                  @if($user->created_at <> '')
                                                  <span style="padding-top:6px"> {{date('d-m-Y', strtotime($user->created_at))}}</span>
                                                  @else <span> --- </span>
                                                  @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" > {{__('frontend.fecha')}} Alta {{__('frontend.nuevaweb')}}: </label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                  @if($user->fecha_alta <> '')
                                                  <span style="padding-top:6px"> {{date('d-m-Y', strtotime($user->fecha_alta))}}</span>
                                                  @else <span> --- </span>
                                                  @endif
                                            </div>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> </label>
                                       
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                  <span> {{$user->perfilcompletado==1? 'PERFIL COMPLETADO': ' PERFIL POR COMPLETAR' }}</span>
                                         </div>
                                           
                                        </div>
                                         
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Estatus </label>
                                        <div class="col-sm-8">
                                            <select class="" name="status" id="status">
                                                <option value = "0" <?php if($user->status == "0" ) echo 'selected'; ?> > INACTIVO</option>
                                                <option value = "1" <?php if($user->status == "1" ) echo 'selected'; ?> > ACTIVO</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success" name="Guardar">{{__('frontend.Guardar')}}</button> 
                            </div>
                        </div>
                    </div>
                </form>						

            </div>
        </div>
    </div>
</div>
@endsection
