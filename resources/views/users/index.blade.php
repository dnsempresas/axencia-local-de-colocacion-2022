@extends('layouts.app')

@section('title_pestaña')

@endsection

@section('titulo_pantalla')

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item active">{{__('users.list')}}</li>
    <!-- Breadcrumb Menu-->
</ol>
@endsection
@section('content')
@if(Session::has('alert-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
</div>
@endif
@if(Session::has('alert-danger'))
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-danger"></i> {{ Session::get('alert-danger') }}</h4>
</div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">{{__('users.list')}}</div>
            <div class="card-body">

                <div class="toolbar">
                    <div class="btn-group">
                        <a class="btn btn-block btn-outline-primary" href="usuarios/create"><i class="fa fa-plus-circle" aria-hidden="true"> </i> {{__('users.add')}}</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 form-horizontal">
                         <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('Id')}} </label>
                            <div class="col-sm-9">
                                <input type="text" id="id" placeholder="{{__('id')}}" class="col-xs-10 col-sm-5">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('users.razonsocial')}} </label>
                            <div class="col-sm-9">
                                <input type="text" id="name" placeholder="{{__('users.razonsocial')}}" class="col-xs-10 col-sm-5">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('users.last_name')}} </label>
                            <div class="col-sm-9">
                                <input type="text" id="surname" placeholder="{{__('users.last_name')}}" class="col-xs-10 col-sm-5">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('DNI/NIE/NIF/CIF')}} </label>
                            <div class="col-sm-9">
                                <input type="text" name="cif" id="cif" placeholder="{{__('DNI/NIE/NIF/CIF')}}" class="col-xs-10 col-sm-5" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('users.direccion')}} </label>
                            <div class="col-sm-9">
                                <input type="text" name="address" id="address" placeholder="{{__('users.direccion')}}" class="col-xs-10 col-sm-5" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">{{__('frontend.Provincia')}}</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <select class="" required name="province" id="province" required>
                                        <option value="">     {{__('frontend.Seleccione')}}...</option>
                                        @foreach($provinces as $province)
                                        <option value="{{$province->pk}}" > {{$province->name}}</option>
                                        @endforeach

                                    </select>

                                    <!--                                    <div class="input-group">
                                                                            <input style="margin-top: 5px;display: none;" class="form-control" id="other_province" name="other_province" type="text" placeholder="Especifique Provincia..."/>
                                                                        </div>-->
                                </div>	
                            </div>

                        </div>

                    </div>
                    <div class="col-sm-6  form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">{{__('frontend.Ayuntamiento')}}</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <select class=""  name="concello_chosen" id="concello_chosen" data-placeholder="{{__('frontend.Seleccione_Ayuntamiento')}}..." required>
                                        <option value="">     {{__('frontend.Seleccione')}}...</option>
                                        <option value="otro">{{__('frontend.Otro')}}</option>
                                    </select>

                                    <!--<input  style="margin-top: 5px;display: none;" class="form-control" id="other_concello" name="other_concello" type="text" placeholder="{{__('frontend.Especifique_Ayuntamiento')}}..."/>-->
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">{{__('users.age')}}</label>
                            <div class="col-sm-9">
                                <span class="input-icon">
                                    <input type="text" id="min-age" placeholder="{{__('users.min_age')}}" />
                                </span>

                                <span class="input-icon input-icon-right">
                                    <input type="text" id="max-age" placeholder="{{__('users.max_age')}}" />
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('users.email')}} </label>
                            <div class="col-sm-9">
                                <input type="text" name="email" id="email" placeholder="{{__('users.email')}}" class="col-xs-10 col-sm-5" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('users.phone')}} </label>
                            <div class="col-sm-9">
                                <input type="text" name="phone" id="phone" placeholder="{{__('users.phone')}}" class="col-xs-10 col-sm-5" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">{{__('Tipo Usuario')}}</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <select class="chosen-select" required name="role" id="role" >
                                        <option value="">     {{__('frontend.Seleccione')}}...</option>
                                        @foreach($role as $rol)
                                        <option value="{{$rol->id}}" > <?php if ($rol->id==1) echo('Demandante de emprego'); else if ($rol->id == 2) echo('Empresa'); else echo('Administrador');?> </option>
                                        @endforeach

                                    </select>
                                    
                                       

                                    <!--                                    <div class="input-group">
                                                                            <input style="margin-top: 5px;display: none;" class="form-control" id="other_province" name="other_province" type="text" placeholder="Especifique Provincia..."/>
                                                                        </div>-->
                                </div>	
                            </div>

                        </div>
                    </div>
                    
                    
                    
                    
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <a id="reset-filter" class="btn btn-sm btn-clear" style="float: right;margin-bottom: 15px;margin-top: 15px; margin-left: 10px">
                            Limpiar Filtros
                        </a>
                        <a id="user-filter" class="btn btn-sm btn-success" style="float: right;margin-bottom: 15px;margin-top: 15px;">
                            {{__('users.filter')}}
                        </a>
                    </div>
                </div>
                <!--<table id="users-table" class="table table-striped table-bordered table-hover dataTable no-footer ">-->
                <table id="users-table"  class=" table table-bordered table-striped table-hover datatable nowrap ">
                    <thead class="thead-light">
                        <tr>
                            <th style ="min-width:5px">{{__('ID')}}</th>
                            <th style ="min-width:10px">{{__('users.name')}}</th>
                            <th style ="min-width:10px">{{__('users.last_name')}}</th>
                            <th style ="min-width:4px">{{__('users.age')}}</th>
                            <th style ="min-width:6px">{{__('users.email')}}</th>
                            <th style ="min-width:5px">{{__('Dni/cif')}}</th>
                            <th style ="min-width:5px">{{__('users.phone')}}</th>
                            <th style ="min-width:8px">{{__('users.direccion')}}</th>
                            <th style ="min-width:5px">{{__('Tipo Usuario')}}</th>
                            <th style ="min-width:10px" ></th>
                            
                            <th style ="min-width:1px">{{__('Provincia')}}</th>
                            <th style ="min-width:1px">{{__('users.ayuntamiento')}}</th>


                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /.col-->
</div>

<div class="modal modal-md" tabindex="-1" role="dialog" id="modalcambiarpassword" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6>Cambiar {{__('users.contrasena')}} </h6>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 form-horizontal">
                        <form method="post" id="passwordForm" action="">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('users.email')}} </label>
                                <div class="col-sm-6">

                                    <input type="hidden" name="inputemailtochange" id="inputemailtochange" value="" />
                                    <input type="hidden" name="user_id" id="user_id" value="" />
                                    <span id="spanemailtochange" style="margin-top: 10px">  </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('users.contrasena')}} </label>
                                <div class="col-sm-6">
                                    <input type="password" name="password1" id="password1" placeholder="{{__('users.nuevacontrasena')}}" autocomplete="off" autofocus="autofocus" min="6" max="10"  />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('users.repitacontrasena')}} </label>
                                <div class="col-sm-6">
                                    <input type="password" name="password2" id="password2" placeholder="{{__('users.repitacontrasena')}}" autocomplete="off"  min="8" max="12"  />
                                </div>
                            </div>
                            <input id = "cambiarpassword" type="submit" class="col-sm-4 btn btn-primary btn-load btn-md" data-loading-text="Cambiando contrasinal..." value="Cambiar Contrasinal">
                        </form>
                    </div><!--/col-sm-6-->
                </div><!--/row-->
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('users.cerrar')}}</button>
            </div>
        </div>
    </div>
</div>

<div class="modal " tabindex="-1" role="dialog" id="modalresumeninscripciones" aria-hidden="true" >
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Inscripciones realizadas por el usuario</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            </div>


            <div class="modal-body" id="bodyresumeninscripciones">

            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('users.cerrar')}}</button>
            </div>
        </div>
    </div>
</div>
<div class="modal " tabindex="-1" role="dialog" id="modalmostrarauditorias" aria-hidden="true" >
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Operaciones del usuario</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            </div>


            <div class="modal-body" id="bodymostrarauditorias">

            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('users.cerrar')}}</button>
            </div>
        </div>
    </div>
</div>
<div class="modal modal-md" tabindex="-1" role="dialog" id="modaldardebajaadm" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>{{__('frontend.darbajaadm')}}</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="margin-left:10px; margin-right:10px">
                    <span> Advertencia: </span>
                    <span> {{__('frontend.advertenciaadm')}} </span> <br>
                    <span> {{__('frontend.nodeshacer')}} </span> <br>
                    <span> {{__('frontend.estaseguroadm')}} </span>

                </div>
                <div class="row" style="margin-left:10px; margin-right:10px">
                    <div class="">
                        <input type="checkbox" id="checkdardebajaadm" name="checkdardebajaadm" style="margin-top: 10px;" unchecked /> 
                        <span class="lbl"> {{__('frontend.siquieroadm')}} </span>

                    </div>
                </div>
                <div  id="divdardebajaadm" class="row" style="margin-left:10px" >
                    <span id="botondardebajaadm" data-id='' class="btn btn-primary"> Aceptar </span>
                </div>


            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal " tabindex="-1" role="dialog" id="modalofertasinscitas" aria-hidden="true" >
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Inscripciones realizadas por el usuario</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            </div>

            <div class="col-md-12" style ="padding-bottom: 10px"> 

                <div class="form-group" >
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="checkFiltrarFecha" checked>
                        <label class="custom-control-label" for="checkFiltrarFecha">{{__('Filtrar por Data de Inscripción')}}</label>
                    </div>

                    <div id="fitrofechas" class="col-sm-9" >
                        <span class="input-icon">
                            <input type="text"  id="date_min" placeholder="{{__('backend.Desde')}}"/>
                        </span>

                        <span class="input-icon input-icon-right">
                            <input type="text"  id="date_max" placeholder="{{__('backend.Hasta')}}"/>
                        </span>
                    </div>
                </div>
            </div>


            <div class="modal-body" id="bodymodalofertasinscitas">

            </div>
            <div class="modal-footer">
                <button id="generarCertificadoOfertas" type="button" class="btn btn-primary" data-dismiss="modal">{{__('Certificado')}}</button>

                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('users.cerrar')}}</button>
            </div>
        </div>
    </div>
</div>


<!-- /.row-->
@endsection
