@extends('layouts.app')

@section('titulo_pantalla')

@endsection

@section('content')
@if ($errors->any())
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-ban"></i>{{__('banners.errors')}}</h4>
    @foreach ($errors->all() as $error)
    <p>{{ $error }}</p>
    @endforeach
</div>
@endif

@if(Session::has('alert-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
</div>
@endif
<form style="padding: 30px;" id="bannersForm" method="POST" action="{{route('banners.update', $banner->id) }}" accept-charset="UTF-8" class="form-group" enctype="multipart/form-data">
    {{method_field('PATCH')}}
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div>
                        {{__('Editar Banner')}}
                    </div>
                </div>
                <div class="card-body">
                    <div class="row"> 
                        <div class="col-md-6">
                            
                            <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Titulo')}}</legend>
                                <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                                    <div role="group" class="input-group">								
                                        <input type="text" class="form-control" name="titulo" required value="{{$banner->titulo}}">
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Enlace')}}</legend>
                                <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                                    <div role="group" class="input-group">								
                                        <input type="url" class="form-control" name="enlace" value="{{$banner->enlace}}">
                                    </div>
                                </div>
                            </fieldset>

                           



                            <div class="row"> 
                                <div class="col-md-6"> 

                                    <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                        <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Data Publicación')}}</legend>
                                        <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                                            <div role="group" class="input-group">
                                                <span class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </span>
                                                <input type="text" id="fecha_publicacion" name="fecha_publicacion" value="{{$banner->getFecha_publicacion()}}" class="form-control">
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-md-6"> 
                                    <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                        <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Data Expiración')}}</legend>
                                        <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                                            <div role="group" class="input-group">
                                                <span class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </span>
                                                <input type="text" id="fecha_expiracion" name="fecha_expiracion" value="{{$banner->getFecha_expiracion()}}" class="form-control">
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">




                            <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                                <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Imagen')}}</legend>
                                <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">

                                    @if ($banner->imagen_destacada !='')
                                    <a href="/public/banners/{{$banner->imagen_destacada}} ">{{ $banner->imagen_destacada }}</a>
                                    <a class="removeimg" href="{{route('banners.removeimgd', ['id'=>$banner->id, 'img'=>$banner->imagen_destacada] ) }}" data-id= "{{$banner->id}}" data-img="{{$banner->imagen_destacada}}" title='Borrar' ><span class='glyphicon glyphicon-remove' style='color:red'></span></a>
                                    @endif
                                    <br/>
                                    <div class="input-group">
                                        <input  id="imagen_destacada" type="file" name="imagen_destacada" accept="image/x-png,image/gif,image/jpeg"   <?php
                                        if ($banner->imagen_destacada == '') {
                                            echo 'required';
                                        }
                                        ?> value="{{$banner->imagen_destacada}}">


                                    </div>

                                </div>
                            </fieldset>
                            

                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary custom-btn" name="signup" value="Sign up">{{__('Gardar')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
</div>
@endsection
