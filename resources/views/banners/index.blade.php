@extends('layouts.app')

@section('title_pestaña')
| Elegir Evento
@endsection

@section('titulo_pantalla')

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item active">Banners</li>
    <!-- Breadcrumb Menu-->
</ol>
@endsection

@section('content')
@if(Session::has('alert-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
</div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">{{__('Listado de Banners')}}
                
            </div>
            <div class="card-body">
                <div class="toolbar">
                    <div class="btn-group">
                        <a class="btn btn-block btn-outline-primary" href="{{ url('/panel/banners/create') }}"><i class="fa fa-plus-circle" aria-hidden="true"> </i> {{__('Engadir')}}</a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6 form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('Id')}}</label>
                            <div class="col-sm-9">
                                <input type="text" id="id" placeholder="Buscar por id..." class="col-xs-10 col-sm-5">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('Titulo')}}</label>
                            <div class="col-sm-9">
                                <input type="text" id="titulo" placeholder="Buscar por título..." class="col-xs-10 col-sm-5">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">{{__('Enlace')}}</label>
                            <div class="col-sm-9">
                                <input type="text" id="enlace" placeholder="Buscar por enlace..." class="col-xs-10 col-sm-5">
                            </div>
                        </div>


                    </div>
                    <div class="col-sm-6  form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">{{__('Data de Publicación')}}</label>
                            <div class="col-sm-9">
                                <span class="input-icon">
                                    <input type="text"  id="fecha_publicacion_min" placeholder="{{__('desde')}}"/>
                                </span>

                                <span class="input-icon input-icon-right">
                                    <input type="text"  id="fecha_publicacion_max" placeholder="{{__('hasta')}}"/>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">{{__('Data de Expiración')}}</label>
                            <div class="col-sm-9">
                                <span class="input-icon">
                                    <input type="text"  id="fecha_expiracion_min" placeholder="{{__('desde')}}"/>
                                </span>

                                <span class="input-icon input-icon-right">
                                    <input type="text"  id="fecha_expiracion_max"  placeholder="{{__('hasta')}}"/>
                                </span>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">

                        <a id="reset-filter" class="btn btn-sm btn-clear" style="float: right;margin-bottom: 15px;margin-top: 15px; margin-left: 10px">
                            Limpiar Filtros
                        </a>
                        <a id="banners-filter" class="btn btn-sm btn-success" style="float: right;margin-bottom: 15px;margin-top: 15px;">
                            {{__('filtrar')}}
                        </a>
                    </div>

                </div>

                <table id="banners_table" class="table table-striped table-bordered table-hover dataTable no-footer">
                    <thead class="thead-light">
                        <tr>
                            <th>{{__('Id')}}</th>
                            <th>{{__('Titulo')}}</th>
                            <th>{{__('Enlace')}}</th>
                            <th>{{__('D.Publicación')}}</th>
                            <th>{{__('D.Expiración')}}</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($banners as $banner)
                        <tr>
                            <td>{{ $banner->id }}</td>
                            <td>{{ $banner->titulo }}</td>
                            <td>{{ $banner->enlace }}</td>
                            <td>{{ $banner->fecha_publicacion }}</td>
                            <td>{{ $banner->fecha_expiracion }}</td>
                            <td>{{ $banner->options }}</td>
<!--                            <td style="text-align:center; vertical-align:middle;">
                                <a href='{{url('/panel/banners/'.$banner->id.'/edit')}}' title='Edit' ><span class='glyphicon glyphicon-edit'></span></a>
                                &emsp;<a id='remove_link' href='{{url('/panel/admin/banners/'.$banner->id.'/remove')}}' title='Delete' ><span class='glyphicon glyphicon-remove' style='color:red'></span></a>
                            </td>-->
                        </tr>
                        @endforeach	
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /.col-->
</div>
<!-- /.row-->
@endsection
