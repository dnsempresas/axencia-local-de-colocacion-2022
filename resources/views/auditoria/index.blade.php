@extends('layouts.app')

@section('title_pestaña')

@endsection

@section('titulo_pantalla')

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item active">Auditoria</li>
    <!-- Breadcrumb Menu-->
</ol>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">Auditoria</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6 form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Modulo </label>
                            <div class="col-sm-9">
                                <input type="text" id="model_name" placeholder="Modulo" class="col-xs-10 col-sm-5">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">Fecha</label>
                            <div class="col-sm-9">
                                <span class="input-icon">
                                    <input type="text" id="publishedAt_min" placeholder="Desde"/>
                                </span>

                                <span class="input-icon input-icon-right">
                                    <input type="text" id="publishedAt_max" placeholder="Hasta" />
                                </span>
                            </div>
                        </div>
                        

                        <!--                        <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Usuario </label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="usuario" id="usuario" placeholder="Usuario" class="col-xs-10 col-sm-5" />
                                                    </div>
                                                </div>-->
                    </div>
                    <div class="col-sm-6  form-horizontal">
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('Nombre de usuario')}} </label>
                            <div class="col-sm-9">
                                <input type="text" id="usuario" name="usuario" placeholder="{{__('Nombre de usuario')}}" class="col-xs-10 col-sm-5">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Accion </label>
                            <div class="col-sm-9">
                                <input type="text" name="action" id="action" placeholder="Accion" class="col-xs-10 col-sm-5" />
                            </div>
                        </div>
                        

<!--                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('DNI/NIE/NIF/CIF')}} </label>
                            <div class="col-sm-9">
                                <input type="text" name="cif" id="cif" placeholder="{{__('DNI/NIE/NIF/CIF')}}" class="col-xs-10 col-sm-5" />
                            </div>
                        </div>-->


                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <a id="reset-filter" class="btn btn-sm btn-clear" style="float: right;margin-bottom: 15px;margin-top: 15px; margin-left: 10px">
                            Limpiar Filtros
                        </a>
                        <a id="auditoria-filter" class="btn btn-sm btn-success" style="float: right;margin-bottom: 15px;margin-top: 15px;">
                            Filtrar
                        </a>
                    </div>

                </div>
                <table id="auditoria-table" class="table table-striped table-bordered table-hover  no-footer2" style=" table-layout:fixed;">
                    <thead class="thead-light">
                        <tr>
                            <th style="max-width: 10%">Modulo</th>
                            <th style="max-width: 10%">Fecha de publicación</th>
                            <th style="width:30%; overflow: hidden; ">Acción</th>
                            <th style="max-width:10%">Usuario</th>
                            
                            <th style="max-width:30%">Detalles</th>
                         
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>


            </div>
        </div>
    </div>
    <!-- Default bootstrap modal example -->
    <div class="modal" id="modaldetalles" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
                </div>
                <div class="modal-body">
                    <div class="row" id = "info">
                        <div class="col-md-2" id="modulo"> 
                            1
                        </div>
                        <div class="col-md-4" id="usuario"> 
                            2
                        </div>
                        <div class="col-md-2" id="fecha"> 
                            3
                        </div>
                        <div class="col-md-4" id="accion"> 
                            4
                        </div>

                    </div>
                    <div class="row"> 
                        <div class="col-md-6" id="originales"> 
                            1
                        </div>
                        <div class="col-md-6" id="modificados"> 
                            2
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</div>


@endsection
