@extends('layouts.app')


@section('titulo_pantalla')
<style>





</style>

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">{{__('Ofertas')}} </li>
    <li class="breadcrumb-item active"> {{__('Proceso de Selección')}} </li>
    <!-- Breadcrumb Menu-->
</ol>
@endsection


@section('content')
@if(Session::has('alert-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
</div>
@endif
@if(Session::has('alert-danger'))
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-danger"></i> {{ Session::get('alert-danger') }}</h4>
</div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">{{__('Proceso de Selección')}}

            </div>
            <div class="card-body">
                <div class="toolbar">
                    <div class="row">
                        <div class="col-md-8">

                            <div class="row card-header" style="margin-left:5px">
                                <label style="">Selector de Oferta</label>
                                <select id="ofertas" name="ofertas"  >
                                    <option value="" selected></option>
                                    @foreach($ofertas as $oferta)
                                    <option data-codoferta="{{$oferta->id}}" data-dataalta="{{Date('d-m-Y', strtotime($oferta->due_date))}}" data-seleccionrematada="{{$oferta->seleccion_rematada}}" data-expedienterematado="{{$oferta->expediente_rematado}}" data-nomeempresa="{{$oferta->propietario->name}}" data-cifempresa="{{$oferta->propietario->cif}}" data-razonempresa="{{$oferta->propietario->surname}}"  data-nomeoferta="{{$oferta->requested_job}}" data-puestosofertados="{{$oferta->vacancy}}" data-puestoscubiertos="{{count($oferta->postulados->where('pivot.contratado','=',1))}}" value="{{$oferta->id}}"> {{'(COD: '. $oferta->id . ') - OFERTA: '. $oferta->requested_job . ' - FECHA: '. Date('d-m-Y', strtotime($oferta->due_date))}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row card-header" style="margin-left:5px">
                                <div class="col-md-5"> Empresa: 
                                    <h4 id ="nomeEmpresa"></h4>
                                </div>
                                <div class="col-md-2"> CIF;: 
                                    <h4 id ="cifEmpresa"></h4>
                                </div>
                                <div class="col-md-5"> Razón Social: 
                                    <h4 id ="razonEmpresa"></h4>
                                </div>
                                <!--                                <div class="col-md-2"> CODIGO: 
                                                                    <h4 id ="codOferta"></h4>
                                                                </div>
                                
                                                                <div class="col-md-4"> NOME OFERTA:
                                                                    <h4 id ="nomeOferta"></h4>
                                                                </div>
                                                                <div class="col-md-2"> DATA ALTA:
                                                                    <h4 id ="dataAlta"></h4>
                                                                </div>-->

                            </div>

                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label  for="">Puestos Ofertados</label>
                                <span  class="form-control" id="puestosOfertados"></span>
                            </div>
                            <div class="form-group">
                                <label  for="">Puestos Cubiertos</label>
                                <span  class="form-control" id="puestosCubiertos"></span>
                            </div>

                        </div>

                        <div class="col-md-2">
                            <input data-id=""  data-campo="seleccion_rematada" class="rematado" id="seleccionRematada" type="checkbox" > Selección Rematada
                            <br>

                            <input data-id="" data-campo="expediente_rematado" class="rematado" id="expedienteRematado" type="checkbox"> Expediente Rematado
                        </div>

                    </div>
                </div>
                <div class="row"> 
                    <div class="col-lg-12">
                        <table id="table-inscritos-cv" class="table table-striped table-bordered display nowrap" style="width:100%">
                            <thead class="thead-light">
                                <tr>
                                    <th >{{__('Data Inscripcion')}}</th>
                                    <th >{{__('DNI')}}</th>
                                    <th >{{__('Nombre')}}</th>
                                    <th >{{__('Apellidos')}}</th>
                                    <th >{{__('cv_pasa')}}</th>
                                    <th >{{__('cv_no_pasa')}}</th>
                                    <th >{{__('cv_enviado')}}</th>
                                    <th >{{__('Entrevistado')}}</th>
                                    <th >{{__('Contratado')}}</th>
                                    <th >{{__('Data Contrato')}}</th>
                                    <th >{{__('Tipo Contrato')}}</th>
                                    <th >{{__('Empresa Contrato')}}</th>
                                    <th >{{__('Cif')}}</th>


                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.col-->
</div>
<!-- /.row-->

<div class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="modalContrato">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label no-padding-right">Fecha de Contrato</label>
                    <div class="">
                        <input type="text"  class="form-control" id="fechaContrato" name="fechaContrato" >
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label no-padding-right">Tipo de contrato</label>
                    <select name="tipoContrato" id="tipoContrato" required style="margin-right: 0;margin-left: 0;width: 100%;">
                        <option  value="001"> De tiempo indefinido - Tiempo Completo</option>
                        <option  value="003"> De tiempo Indefiido - Tiempo Parcial</option>
                        <option  value="401"> De tiempo Determinado - Tiempo Completo</option>
                        <option  value="501"> De tiempo Determinado - Tiempo Parcial</option>
                    </select>
                </div>

                <div class="form-group">
                    <label class="control-label no-padding-right">Razón Social Empresa</label>
                    <div class="">
                        <input type="text"  class="form-control" id="razonSocialEEmpresa" name="razonSocialEEmpresa" >
                    </div>
                </div>
                <div class="form-group ">
                    <label class="control-label no-padding-right">CIF Empresa</label>
                    <div class="">
                        <input type="text"  class="form-control" id="cifEmpresa2" name="cifEmpresa2" >
                    </div>
                </div>



            </div>
            <div class="modal-footer">
                <button id="aceptarContratado" type="button" class="btn btn-default" id="modal-btn-si">Aceptar</button>
                <button id="cancelarContratado" type="button" class="btn btn-primary" id="modal-btn-no">Cancelar</button>
            </div>
        </div>
    </div>
</div>



<script src="{{asset('assets/js/jquery-2.1.4.min.js')}}"></script>
<script>

var table;
var checkContratado;
$(document).ready(function () {
    $.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
    $.fn.modal.prototype.constructor.Constructor.DEFAULTS.keyboard = false;

    $('#fechaContrato').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl',
//        startDate: new Date()
    });

    $('#ofertas').chosen({
        disable_search_threshold: 10,
        enable_split_word_search: true,
        search_contains: true,
        no_results_text: "No hay resultados en la bussqueda",
        width: "95%",
        parser_config: {copy_data_attributes: true}
    });
    $('#ofertas').val('').trigger('chosen:updated');

    $(document).on('change', 'select#ofertas', function (e) {
        // $('#ofertas').trigger('chosen:updated');
        var codOferta = $("#ofertas").chosen().find("option:selected").attr('data-codoferta');
        var nomeOferta = $("#ofertas").chosen().find("option:selected").attr('data-nomeoferta');
        var dataAlta = $("#ofertas").chosen().find("option:selected").attr('data-dataalta');
        var nomeEmpresa = $("#ofertas").chosen().find("option:selected").attr('data-nomeempresa');
        var cifEmpresa = $("#ofertas").chosen().find("option:selected").attr('data-cifempresa');
        var razonEmpresa = $("#ofertas").chosen().find("option:selected").attr('data-razonempresa');
        var puestosOfertados = $("#ofertas").chosen().find("option:selected").attr('data-puestosofertados');
        var puestosCubiertos = $("#ofertas").chosen().find("option:selected").attr('data-puestoscubiertos');
         var seleccionRematada = $("#ofertas").chosen().find("option:selected").attr('data-seleccionrematada');
         var expedienteRematado = $("#ofertas").chosen().find("option:selected").attr('data-expedienterematado');

        $("#codOferta").text(codOferta)
        $("#nomeOferta").text(nomeOferta);
        $("#dataAlta").text(dataAlta);
        $("#nomeEmpresa").text(nomeEmpresa);
        $("#cifEmpresa").text(cifEmpresa);
        $("#razonEmpresa").text(razonEmpresa);
        $("#puestosOfertados").text(puestosOfertados);
        $("#puestosCubiertos").text(puestosCubiertos);
        
        if (expedienteRematado==1) {$("#expedienteRematado").prop("checked", true)} else {$("#expedienteRematado").prop("checked", false)};
        if (seleccionRematada==1) {$("#seleccionRematada").prop("checked", true)} else {$("#seleccionRematada").prop("checked", false)};
        
        $("#expedienteRematado").attr('data-id', codOferta);
        $("#seleccionRematada").attr('data-id', codOferta);

        actualizar(codOferta);

    });

    $(document).on('click', '.check', function (e) {

        rowId = $(this).parent().parent().attr('pk');
        campo = $(this).attr('data-campo');
        valor = this.checked;

        switch (campo) {
            case 'cv_pasa':
            case 'cv_no_pasa':
            case 'cv_enviado':
            case 'entrevistado':
                actualizar_campo(rowId, campo, valor);
                break;
            case 'contratado':

                checkContratado = $(this);

//                event.preventDefault();
//                event.stopPropagation();

                if (valor == true)
                    $('#modalContrato').modal('show');
                else {//remove datos del contrato
                    actualizar_campo(rowId, campo, valor);
                }




                break;

        }



    });
    
     $(document).on('click', '.rematado', function (e) {

        id= $(this).attr('data-id');
        campo = $(this).attr('data-campo');
        valor = this.checked;
        
        actualizar_rematado(id, campo, valor )
        
      

     
    });
    
    
    $(document).on('click', '#cancelarContratado', function (e) {

        checkContratado.prop("checked", !checkContratado.prop("checked"));
        $('#modalContrato').modal('hide');

    });

    $(document).on('click', '#aceptarContratado', function (e) {

        rowId = checkContratado.parent().parent().attr('pk');

        if ($('#razonSocialEEmpresa').val() != '' && $('#cifEmpresa2').val() != '' && $('#fechaContrato').val() != '') {
            checkContratado.prop("checked", true);


            actualizar_contrato(rowId, 'contratado', 'true', $('#fechaContrato').val(), $('#tipoContrato').val(), $('#razonSocialEEmpresa').val(), $('#cifEmpresa2').val());
            var codOferta = $("#ofertas").chosen().find("option:selected").attr('data-codoferta');
            $('#modalContrato').modal('hide');


        } else {
            alert('Debe completar los datos');
            return false;
        }


    });



});

function actualizar(oferta) {
    $('tbody').html('');
    table = $('#table-inscritos-cv').DataTable();
    lista_inscritos = '';

    $.ajax({
        type: "GET",
        url: "/panel/admin/ofertas/inscritos",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        data: {
            'oferta': oferta
        },
        success: function (response) {

            if (response.state == 'success') {

                var inscritos = JSON.parse(response.data, true);
                var d = dc = di = '';
                contratados = 0;

                inscritos.forEach(function (item) {


                    if (item.pivot.cv_pasa == 1)
                        check_cv_pasa = "checked";
                    else
                        check_cv_pasa = "";

                    if (item.pivot.cv_no_pasa == 1)
                        check_cv_no_pasa = "checked";
                    else
                        check_cv_no_pasa = "";

                    if (item.pivot.cv_enviado == 1)
                        check_cv_enviado = "checked";
                    else
                        check_cv_enviado = "";

                    if (item.pivot.entrevistado == 1)
                        check_entrevistado = "checked";
                    else
                        check_entrevistado = "";

                    if (item.pivot.contratado == 1) {
                        check_contratado = "checked";
                        contratados = contratados + 1;
                    } else
                        check_contratado = "";

//                    var d = item.pivot.data_contrato.toDate("dd/mm/yyyy");

                    if (item.pivot.created != null) {
                        //di = new Date( item.pivot.created).toDateString("d/m/Y");

                        d = item.pivot.created.slice(0, 10).split('-');
                        di = d[2] + '/' + d[1] + '/' + d[0];


                    } else
                        di = '';

                    if (item.pivot.data_contrato != null) {
                        d = item.pivot.data_contrato.slice(0, 10).split('-');
                        dc = d[2] + '/' + d[1] + '/' + d[0];
                    } else
                        dc = '';

                    tipoContrato = "";

//                    if (item.pivot.tipo_contrato == "501") {
//                        tipoContrato = "TIEMPO INDEFINIDO"
//                    } else if (item.pivot.tipo_contrato == "508")
//                        tipoContrato = "TIEMPOS DETERMINADO";

                    if (item.pivot.empresa == null)
                        item.pivot.empresa = '';
                    if (item.pivot.cif_empresa == null)
                        item.pivot.cif_empresa = '';


                    lista_inscritos = lista_inscritos + '<tr pk=' + item.pivot.pk + '>' +
                            '<td>' + di + '</td>' +
                            '<td>' + item.cif + '</td>' +
                            '<td>' + item.name + '</td>' +
                            '<td>' + item.surname + '</td>' +
                            '<td style="text-align:center">' + '<input class="check" data-campo="cv_pasa" type="checkbox"' + check_cv_pasa + '></td>' +
                            '<td style="text-align:center">' + '<input class="check" data-campo="cv_no_pasa" type="checkbox"' + check_cv_no_pasa + '></td>' +
                            '<td style="text-align:center">' + '<input class="check" data-campo="cv_enviado" type="checkbox"' + check_cv_enviado + '></td>' +
                            '<td style="text-align:center">' + '<input class="check" data-campo="entrevistado" type="checkbox"' + check_entrevistado + '></td>' +
                            '<td style="text-align:center">' + '<input class="check" data-campo="contratado" type="checkbox"' + check_contratado + '></td>' +
                            '<td>' + dc + '</td>' +
                            '<td>' + tipoContrato + '</td>' +
                            '<td>' + item.pivot.empresa + '</td>' +
                            '<td>' + item.pivot.cif_empresa + '</td>' +
                            '</tr>';

                });
                $("#puestosCubiertos").text(contratados);

                table.destroy();
                $('tbody').html(lista_inscritos);

                table = $('#table-inscritos-cv').DataTable({
                    "scrollY": 200,
                    "scrollX": true,
                    autoWidth: true,

                    language: {

                        url: '/assets/js/datatable_' + $('#imgidiomaselect').attr('alt') + '.json'//Ubicacion del archivo con el json del idioma.
                    },

                    buttons: [
                        {
                            extend: 'excel',
                            text: '<span class="fa fa-file-excel-o"></span> Exportar a Excel',
                            exportOptions: {

                                modifier: {
                                    search: 'applied',
                                    page: 'all',
                                    order: 'applied'
                                }
                            }
                        },
                        {
                            extend: 'csv',
                            text: '<span class="glyphicon-export"></span> Exportar a Csv',
                            exportOptions: {
                                modifier: {
                                    search: 'applied',
                                    page: 'all',
                                    order: 'applied'
                                }
                            }
                        },
                        {
                            extend: 'pdf',
                            text: '<span class="fa fa-file-pdf-o"></span> Exportar a Pdf',
                            exportOptions: {
                                modifier: {
                                    search: 'applied',
                                    page: 'all',
                                    order: 'applied'
                                }
                            }
                        },
                        {
                            extend: 'print',
                            text: '<span class="fa fa-print bigger-110"></span> Imprimir',
                            exportOptions: {
                                modifier: {
                                    search: 'applied',
                                    page: 'all',
                                    order: 'applied'
                                }
                            }
                        }
                    ],

                });

                $('#table-inscritos-cv').DataTable().columns.adjust();


            }
        },
        error: function (error) {

        }
    });

}

function actualizar_campo(id, campo, valor, ) {


    $.ajax({
        type: "post",
        url: "/panel/admin/ofertas/actualizar_campo",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        data: {
            'id': id, 'campo': campo, 'valor': valor
        },
        success: function (response) {

            if (response.state == 'success') {

                alert('Actualización satisfactoria');
            }
            $('select#ofertas').change();
        },
        error: function (error) {

        }
    });
}

function actualizar_rematado(id, campo, valor, ) {


    $.ajax({
        type: "post",
        url: "/panel/admin/ofertas/actualizar_rematado",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        data: {
            'id': id, 'campo': campo, 'valor': valor
        },
        success: function (response) {

            if (response.state == 'success') {
                
                var rematados = JSON.parse(response.data, true);
                
                  
         $("#ofertas").chosen().find("option:selected").attr('data-seleccionrematada',rematados.seleccion_rematada);
     
        
         $("#ofertas").chosen().find("option:selected").attr('data-expedienterematado',rematados.expediente_rematado);
                
                

                alert('Actualización satisfactoria');
                
            }
           // $('select#ofertas').change();
        },
        error: function (error) {

        }
    });
}

function actualizar_contrato(id, campo, valor, fechaContrato, tipoContrato, razonSocial, cifEmpresa) {


    $.ajax({
        type: "post",
        url: "/panel/admin/ofertas/actualizar_campo",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        data: {
            'id': id, 'campo': campo, 'valor': valor, 'fechaContrato': fechaContrato, 'tipoContrato': tipoContrato, 'razonSocial': razonSocial, 'cifEmpresa': cifEmpresa
        },
        success: function (response) {

            if (response.state == 'success') {

                alert('Actualización satisfactoria');
            }
            $('select#ofertas').change();
        },
        error: function (error) {

        }
    });
}
;
</script>
@endsection
