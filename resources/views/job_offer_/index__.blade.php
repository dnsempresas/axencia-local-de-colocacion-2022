@extends('layouts.app')

@section('title_pestaña')
| Elegir Evento
@endsection

@section('titulo_pantalla')

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item active">Ofertas</li>
    <!-- Breadcrumb Menu-->
</ol>
@endsection

@section('content')
@if(Session::has('alert-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
</div>
@endif
<div class="row ">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">Ofertas</div>
            <div class="card-body">
                <div class="toolbar">
                    <div class="btn-group">
                        <a class="btn btn-block btn-outline-primary" href="{{ url('/panel/ofertas/create') }}"><i class="fa fa-plus-circle" aria-hidden="true"> </i> {{__('backend.Agregar')}}</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">{{__('ID')}}</label>
                            <div class="col-sm-9">
                                <input type="number" name="id" id="id" placeholder="" class="col-xs-10 col-sm-5">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">{{__('backend.Empresa')}}</label>
                            <div class="col-sm-9">
                                <select class="chosen-select" name="propietario" id="propietario" multiple="">
                                    @foreach($empresas as $empresa)
                                    <option value="{{$empresa->pk}}">Cod.: {{$empresa->pk}} - {{$empresa->name}}-{{$empresa->company}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('backend.Ocupacion_Solicitada')}}</label>
                            <div class="col-sm-9">
                                <input type="text" id="requested_job" placeholder="" class="col-xs-10 col-sm-5">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">{{__('backend.N_de_Puestos')}}</label>
                            <div class="col-sm-9">
                                <input type="text" id="vacancy" placeholder="" class="col-xs-10 col-sm-5">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">{{__('backend.Tareas_a_Desenvolver')}}</label>
                            <div class="col-sm-9">
                                <input type="text" name="tasks" id="tasks" placeholder="" class="col-xs-10 col-sm-5" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">{{__('backend.Tipo_Contrato')}}</label>
                            <div class="col-sm-9">
                                <select class="chosen-select" name="contract_type" id="contract_type" multiple="">
                                    @foreach($contracts as $contract)
                                    <option value="{{$contract->id}}">{{ session('lang')=='es' ?  $contract->nombre :  $contract->nome }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!--                        <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">{{__('backend.Disponibilidad')}}</label>
                                                    <div class="col-sm-9">
                                                        <select class="chosen-select" name="disponibilidad" id="disponibilidad" multiple="">
                                                            @foreach($disponibilidades as $disponibilidad)
                                                            <option value="{{$disponibilidad->id}}">{{ session('lang')=='es' ?  $disponibilidad->nombre :  $disponibilidad->nome }} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>-->
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">{{__('Tipo de oferta')}}</label>
                            <div class="col-sm-9">
                                <select name="tipooferta" id="tipooferta" required style="margin-right: 0;margin-left: 0;width: 100%;">
                                    <option value="">{{__('seleccione')}}</option>
                                    @foreach($tipoofertas as $tipooferta)
                                    <option value="{{$tipooferta->id}}">{{ session('lang')=='es' ?  $tipooferta->nombre :  $tipooferta->nome }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right">Provincia</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <select class="provincejob " required name="province" id="provincejob">
                                            <option value="">Selecione...</option>
                                            @foreach($provinces as $province)
                                            <option value="{{$province->pk}}">{{$province->name}}</option>
                                            @endforeach
                                        </select>

                                    </div>	
                                </div>

                            </div>

                            <div class="form-group">
                                <label id="labelconcello" class="col-sm-3 control-label no-padding-right">{{__('frontend.Ayuntamiento')}}</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <select class="" name="concello_chosen" id="concellojob" data-placeholder="Seleccione...">
                                            <option value="">Selecione...</option>
                                            

                                        </select>
                                    </div>
                                </div>
                            </div>

                    </div>
                    <div class="col-sm-6  form-horizontal">



                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">{{__('backend.Plazo_CV')}}</label>
                            <div class="col-sm-9">
                                <span class="input-icon">
                                    <input type="text"  id="due_date_min" placeholder="{{__('backend.Desde')}}"/>
                                </span>

                                <span class="input-icon input-icon-right">
                                    <input type="text"  id="due_date_max" placeholder="{{__('backend.Hasta')}}"/>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">{{__('backend.Fecha_Incorporación')}}</label>
                            <div class="col-sm-9">
                                <span class="input-icon">
                                    <input type="text"  id="start_date_min" placeholder="{{__('backend.Desde')}}"/>
                                </span>

                                <span class="input-icon input-icon-right">
                                    <input type="text"  id="start_date_max"  placeholder="{{__('backend.Hasta')}}"/>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">{{__('backend.Duración_Contrato')}}</label>
                            <div class="col-sm-9">
                                <span class="input-icon">
                                    <input type="text" id="duration_min" placeholder="Min"/>
                                </span>

                                <span class="input-icon input-icon-right">
                                    <input type="text" id="duration_max"  placeholder="Max" />
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">{{__('backend.Salario')}}</label>
                            <div class="col-sm-9">
                                <span class="input-icon">
                                    <input type="number" id="salary_min" placeholder="Min"/>
                                </span>

                                <span class="input-icon input-icon-right">
                                    <input type="number" id="salary_max"  placeholder="Max"/>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">{{__('Estatus')}}</label>
                            <div class="col-sm-8">
                                <select name="publicada" id="publicada" required style="margin-right: 0;margin-left: 0;width: 100%;">
                                    <option value=""  ></option>
                                    <option value="1"  >{{__('backend.Pendiente')}}</option>
                                    <option value="2"  >PUBLICADA</option>
                                    <option value="3"  >{{__('backend.Rechazada')}}</option>
                                    <option value="4"  >PAUSADA</option>
                                     <option value="5"  >{{__('backend.Cerrada')}}</option>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row  ">
                    <div class="col-sm-12">

                        <a id="reset-filter" class="btn btn-sm btn-clear" style="float: right;margin-bottom: 15px;margin-top: 15px; margin-left: 10px">
                            Limpiar Filtros
                        </a>
                        <a id="offer-filter" class="btn btn-sm btn-success" style="float: right;margin-bottom: 15px;margin-top: 15px;">
                            {{__('offers.filter')}}
                        </a>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="offer-table" class="table table-striped table-bordered table-hover no-footer dataTable">
                       



                            <thead class="thead-light">
                                <tr>
                                    <th style="width: 5%">{{__('ID')}}</th>
                                    <th style="width: 20%">{{__('backend.Empresa')}}</th>
                                    <th style="width: 20%">{{__('backend.Oferta')}}</th>
                                    <th style="width: 5%">{{__('backend.Puestos')}}</th>
                                    <th>{{__('Tareas')}}</th>
                                    <th>{{__('backend.Plazo_CV')}}</th>
                                    <th>{{__('Tipo de Contrato')}}</th>
                                    <th>{{__('F. Inicio')}}</th>
                                    <th>{{__('Duración')}}</th>
                                    <th>{{__('backend.Localidad')}}</th>
                                    <th>{{__('Horas')}}</th>
                                    <th>{{__('Salario')}}</th>
                                    <th>{{__('Publicada')}}</th>
                                    <th>{{__('Province')}}</th>
                                    <th>{{__('Tipooferta')}}</th>
                                    <th style="width: 10%"></th>
                                    <th>{{__('Concello')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.col-->
</div>
<!-- /.row-->
@endsection
