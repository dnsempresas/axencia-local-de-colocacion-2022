@extends('layouts.panelempresas')

@section('titulo_pantalla')

@endsection

@section('content')
@if ($errors->any())
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-ban"></i>{{__('offers.errors')}}</h4>
    @foreach ($errors->all() as $error)
    <p>{{ $error }}</p>
    @endforeach
</div>
@endif

@if(Session::has('alert-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
</div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <i class="icon-note"></i> {{__('offers.edit_offer_label')}}
            </div>
            <div class="card-body">
                
                <form style="padding: 30px;" id="offerForm" method="POST" novalidate action="{{route('ofertas.update', $job_offer->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                    {{method_field('PATCH')}}
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-1">{{__('ID')}}</label>
                                <div class="col-sm-8">
                                    <span  class="form-control" name="id">{{ strtoupper($job_offer->id) }}</span>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-1">{{__('Empresa')}}</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                    <select class="chosen-select" name="propietario" id="propietario" required>
                                        @foreach($empresas as $empresa)
                                        <option value="{{$empresa->pk}}" <?php if ($job_offer->propietario_fk == $empresa->pk) echo "selected='selected'"; ?> >Cod.: {{$empresa->pk}} - {{$empresa->name}} - {{$empresa->company}}</option>
                                        @endforeach
                                    </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-4 control-label no-padding-right">{{ __('offers.requested_job') }}</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                    <input type="text" class="form-control" name="requested_job" required value="{{ strtoupper($job_offer->requested_job) }}">
                                </div>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-4 control-label no-padding-right">{{ __('offers.vacancy') }}</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                    <input type="number" class="form-control" name="vacancy" required value="{{ strtoupper($job_offer->vacancy) }}">
                                </div>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-4 control-label no-padding-right">{{__('offers.tasks')}}</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                    <textarea class="form-control" name="tasks" required>{{ strtoupper($job_offer->tasks) }}</textarea>
                                </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right">{{__('offers.due_date')}}</label>
                                <div class="col-sm-8">
                                    <input type="text"  class="form-control" id="due_date" name="due_date"  value="{{ $job_offer->getDueDate()}}">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-4 control-label no-padding-right">{{__('offers.contract_type')}}</label>
                                <div class="col-sm-8">
                                    <select name="contract_type" id="contract_type" required style="margin-right: 0;margin-left: 0;width: 100%;">
                                        <option value="">{{__('seleccione')}}</option>
                                        @foreach($contracts as $contract)
                                        <option value="{{$contract->id}}"  <?php if ($job_offer->contract_type == $contract->id) echo "selected='selected'"; ?>  >{{ session('lang')=='es' ?  $contract->nombre :  $contract->nome }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group " id="divTiempo" style="display: <?php
                            if ($job_offer->contract_type == "1")
                                echo 'block';
                            else
                                echo 'none'
                                ?>">
                                <label class="col-sm-4 control-label no-padding-right">{{__('offers.duration')}}</label>
                                <div class="col-sm-8">
                                    <div class="col-sm-6">
                                        <div class="input-group">
                                        <input type="number" class="form-control" name="duration" id ="duration" value="{{ $job_offer->duration}}" min="1">
                                    </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <select name="tiempo" id="tiempo" style="margin-right: 0;margin-left: 0;width: 100%;">
                                            <option <?php if ($job_offer->tiempo == "MESES") echo "selected='selected'"; ?> value="MESES" >MESES</option>
                                            <option <?php if ($job_offer->tiempo == "AÑOS") echo "selected='selected'"; ?> value="AÑOS" >AÑOS</option>
                                            <option <?php if ($job_offer->tiempo == "DIAS") echo "selected='selected'"; ?> value="DIAS" >DIAS</option>
                                        </select>
                                    </div>

                                </div>

                            </div>
                            <div class="form-group required">
                                <label class="col-sm-4 control-label no-padding-right">{{__('Xornada Laboral')}}</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                    <select name="disponibilidad" id="disponibilidad" required style="margin-right: 0;margin-left: 0;width: 100%;">
                                        <option value="">{{__('seleccione')}}</option>
                                        @foreach($disponibilidades as $disponibilidad)
                                        <option value="{{$disponibilidad->id}}"  <?php if ($job_offer->disponibilidad_fk == $disponibilidad->id) echo "selected='selected'"; ?>  >{{ session('lang')=='es' ?  $disponibilidad->nombre :  $disponibilidad->nome }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                </div>
                            </div>
                            <input type="hidden" class="form-control" name="tipooferta" value="{{$job_offer->tipooferta}}">
                            <input type="hidden" class="form-control" name="pertenece_fie" value="{{$job_offer->pertenece_fie}}">
                           

                            <div class="form-group required">
                                <label class="col-sm-4 control-label no-padding-right">{{__('offers.start_date')}}</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                    <input type="text" readonly class="form-control" id="start_date" name="start_date" required value="{{ strtoupper($job_offer->getStartdate()) }}">
                                </div>
                                    </div>
                            </div>

                            

                        </div>
                        <div class="col-md-6">
                            
                            <div class="form-group required">
                                <label class="col-sm-4 control-label no-padding-right">Provincia</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <select class="provincejob " required name="province" id="provincejob">
                                            <option value="">Selecione...</option>
                                            @foreach($provinces as $province)
                                            <option value="{{$province->pk}}" <?php if ($job_offer->province_fk == $province->pk) echo "selected='selected'"; ?> >{{$province->name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="input-group">
                                            <input required style="margin-top: 5px;display: none;" class="form-control" id="other_province" name="other_province" type="text" placeholder="Especifique Provincia..."/>
                                        </div>
                                    </div>	
                                </div>

                            </div>

                            <div class="form-group required">
                                <label id="labelconcello" class="col-sm-4 control-label no-padding-right">{{__('frontend.Ayuntamiento')}}</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <select class="" name="concello_chosen" id="concellojob" data-placeholder="Seleccione...">
                                            <option value="">Seleccione...</option>
                                            @foreach($concellos as $concello)
                                            <option {{$job_offer->concello_fk===$concello->id?'selected':''}} value="{{$concello->id}}">{{$concello->name}}</option>
                                            @endforeach
                                            <option value="otro">Otro</option>
                                        </select>

                                        <input required style="margin-top: 5px;display: none;" class="form-control" id="other_concello" name="other_concello" type="text" placeholder="Especifique..."/>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right">{{__('offers.hours')}}</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="hours"  value="{{ strtoupper($job_offer->hours) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right">{{__('offers.salary')}}</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" name="salary" value="{{ strtoupper($job_offer->salary) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right">{{__('Requisitos-Formacion')}}</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                    <textarea class="form-control" name="rformacion" >{{ strtoupper($job_offer->rformacion) }}</textarea>
                                </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right">{{__('Requisitos-Experiencia')}}</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="rexperiencia" >{{ strtoupper($job_offer->rexperiencia) }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right">{{__('Requisitos-Outros:')}}</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="rotros" >{{ strtoupper($job_offer->rotros) }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">Sector</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select class="chosen-select" name="sector" id="sector" data-placeholder="Seleccione sector...">
                                                    <option  value="-1">No Definido</option>
                                                    @foreach($sectores as $sector)
                                                    <option <?php
                                                    //$usersectores = explode('/', $user->sector);

                                                    if ($sector->id== $job_offer->sector_fk) {
                                                        echo 'selected';
                                                    }
                                                    ?> value="{{$sector->id}}">{{$sector->nome}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                            </div>

                            <div class="form-group">
                                <span class="col-sm-4 control-label no-padding-right">Imaxe Destacada</span>
                                <div class="col-sm-8">
                                    <a href="/public/offers/{{$job_offer->imagen}} ">{{ $job_offer->imagen }}</a>
                                    <br/>
                                    <input id="imagen_destacada" type="file" name="imagen_destacada" accept="image/x-png,image/gif,image/jpeg">
                                </div>
                            </div>




                        </div>
                        <!--                        <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>{{__('offers.requirements')}}</label>
                                                        <div class="checkbox" style="width: 150px">
                                                            <label>
                                                                <input name="requirements[]" type="checkbox" value="training" class="ace ace-checkbox-2" />
                                                                <span class="lbl"> {{__('offers.training')}}</span>
                                                            </label>
                                                        </div>
                                                        <div class="checkbox" style="width: 150px">
                                                            <label>
                                                                <input name="requirements[]" class="ace ace-checkbox-2" value="experience" type="checkbox" />
                                                                <span class="lbl">{{__('offers.experience')}}</span>
                                                            </label>
                                                        </div>
                                                        <div class="checkbox" style="width: 150px">
                                                            <label>
                                                                <input id="others_check" name="requirements[]" value="" class="ace ace-checkbox-2" type="checkbox" />
                                                                <span class="lbl">{{__('offers.others')}}</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div id="other_requirements_container" data-others="false" class="form-group" style="width: 300px">
                        
                                                        <textarea class="form-control" name="requirements[]" style="height: 110px"></textarea>
                                                    </div>
                                                </div>-->
                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary" name="job_offer" value="Job offer">{{__('offers.save')}}</button>

                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection