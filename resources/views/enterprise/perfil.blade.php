@extends('layouts.panelempresas')

@section('titulo_pantalla')

@endsection

@section('content')
<style>
    .tags{
        width: 100% !important;
    }
</style>
@if ($errors->any())
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-ban"></i></h4>
    @foreach ($errors->all() as $error)
    <p>{{ $error }}</p>
    @endforeach
</div>
@endif

@if(Session::has('alert-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
</div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <i class="icon-note"></i> Editar Perfil
            </div>
            <div class="card-body">
                <form method="POST" novalidate action="{{route('usuarios.update', $user->pk) }}"  accept-charset="UTF-8" class="form-horizontal" id="userForm" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{method_field('PATCH')}}
                    <div  class="row">
                        <div id="izquierda" class="col-md-6">

                            <div class="panel panel-default">
                                <div class="panel-heading">Datos de Acceso </div>
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">Tipo de Usuario</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select name="tiporole" id="tiporole">
                                                    <option {{$user->role_id==2?'selected':''}}  value="2">Empresa</option>

                                                </select>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Correo_electronico')}}</label>
                                        <div class="col-sm-6">
                                            <input type="email" name="email" id="email" placeholder="{{__('frontend.Correo_electronico')}}" class="col-xs-10 col-sm-5" value="{{$user->email}}"  required/>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Contrasenya')}}</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="password" id="password" placeholder="{{__('frontend.Contrasenya')}}" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>
<!--                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Repita Contraseña</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="rpassword" id="rpassword" placeholder="Repita Contraseña" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>-->


                                </div>
                            </div>

                            <div id="panel_datosempresariales" class="panel panel-default" >
                                <div class="panel-heading">Datos de Empresa</div>
                                <div class="panel-body">

                                    <div class="form-group required">

                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Nombre_de_la_Empresa')}} </label>

                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="name" id="name" placeholder="{{__('frontend.Nombre_de_la_Empresa')}}" value="{{$user->name}}"  required/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Denominacion_Social')}} </label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="last_name" id="last_name" placeholder="{{__('frontend.Denominacion_Social')}}" value="{{$user->surname}}" required/>
                                            </div>
                                        </div>
                                    </div>
<!--                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">Fecha de Constitución</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input class="form-control date-picker" id="birth" name="birth" type="text" data-date-format="dd/mm/YYYY" value="{{date('d/m/Y', strtotime($user->birth))}}"/>
                                            </div>	
                                        </div>

                                    </div>-->

                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.NIF_CIF')}} </label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="cif" id="cif" placeholder="{{__('frontend.NIF_CIF')}}" value="{{$user->cif}}" required />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>




                        </div>


                        <div id="derecha" class="col-md-6">

                            <div id="panel_direccion" class="panel panel-default">
                                <div class="panel-heading"> Dirección  </div>
                                <div class="panel-body">

                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right"> {{__('frontend.Provincia')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group" style="width:100%">
                                                <select class="" required name="province" id="province">
                                                    <option value="">Selecione...</option>
                                                    @foreach($provinces as $province)
                                                    <option {{$user->province_fk===$province->pk?'selected':''}} value="{{$province->pk}}">{{$province->name}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="input-group">
                                                    <input required style="margin-top: 5px;display: none;" class="form-control" id="other_province" name="other_province" type="text" placeholder="Especifique Provincia..."/>
                                                </div>
                                            </div>	
                                        </div>

                                    </div>

                                    <div class="form-group required" style="width:100%">
                                        <label id="labelconcello" class="col-sm-4 control-label no-padding-right">{{__('frontend.Ayuntamiento')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select class="" name="concello_chosen" id="concello_chosen" data-placeholder="Seleccione...">
                                                    <option value="">Seleccione...</option>
                                                    @foreach($concellos as $concello)
                                                    <option {{$user->concello_fk==$concello->id?'selected':''}} value="{{$concello->id}}">{{$concello->name}}</option>
                                                    @endforeach
                                                    <option value="otro">Otro</option>
                                                </select>


                                                <input required style="margin-top: 5px;display: none;" class="form-control" id="other_concello" name="other_concello" type="text" placeholder="Especifique..."/>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Direccion')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="address" id="address" placeholder="{{__('frontend.Direccion')}}" value="{{$user->address}}" required/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Codigo_Postal')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="number" name="postal_code" id="postal_code" placeholder="{{__('frontend.Codigo_Postal')}}" value="{{$user->zip}}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Telefono')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="number" name="phone" id="phone" maxlength=9 placeholder=" {{__('frontend.Telefono')}}"  value="{{$user->telephone}}" required/>
                                            </div>
                                        </div>
                                    </div>	
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1">{{__('frontend.Otro_Telefono')}}</label>
                                        <div class="col-sm-8">
                                            <input type="number" name="other_phone" id="other_phone" maxlength=9 placeholder="{{__('frontend.Otro_Telefono')}}"  value="{{$user->other_phone}}"/>
                                        </div>
                                    </div>



                                </div>
                            </div>

                            <div id="panel_personadecontacto" class="panel panel-default" >
                                <div class="panel-heading"> {{__('frontend.Persona_de_contacto')}} </div>
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Persona_de_contacto')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="contacto" id="contacto" placeholder="{{__('frontend.Persona_de_contacto')}}" value="{{$user->contacto}}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Cargo')}} </label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="cargocontacto" id="cargocontacto" placeholder="{{__('frontend.Cargo_Persona_de_contacto')}}" value="{{$user->cargocontacto}}" />
                                            </div>
                                        </div>
                                    </div>




                                </div>
                            </div>
                            <div id="panel_logo" class="panel panel-default" >
                                <div class="panel-heading"> {{__('Logo')}}</div>
                                <div class="panel-body">
                            <div class="form-group">
                                        <span class="col-sm-4 control-label no-padding-right">Logo Empresa</span>
                                        <div class="col-sm-4">
                                            
                                            <img id="imgSalida_e" width="120px" height="140px" src="{{asset('/public/storage/fotos/'. $user->foto)}}"/>
                                            <input id="foto_perfil_e" type="file" name="foto_perfil_e" accept="image/*" >
                                            <a class="removeimg_e" id ="removeimg_e" href="" title='Borrar' ><span class='glyphicon glyphicon-remove' style='color:red; '></span></a>
                                             <input type="hidden" name="foto_user_e" id="foto_user" value ="{{ $user->foto }}" />
                                        </div>
                                    </div>
                                    </div>
                            </div>
                            
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-success" name="Guardar">{{__('frontend.Guardar')}}</button> 
                        </div>
                    </div>
            </div>
            </form>						

        </div>
    </div>
</div>
</div>
@endsection
