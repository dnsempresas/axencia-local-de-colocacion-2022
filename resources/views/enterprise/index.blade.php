@extends('layouts.app')

@section('title_pestaña')

@endsection

@section('titulo_pantalla')

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item active">{{__('Empresas')}}</li>
    <!-- Breadcrumb Menu-->
</ol>
@endsection
@section('content')
@if(Session::has('alert-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
</div>
@endif
@if(Session::has('alert-warning'))
<div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-exclamation-triangle"></i> {{ Session::get('alert-warning') }}</h4>
</div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">{{__('Empresas')}}</div>
            <div class="card-body">

                <div class="toolbar">
                    <div class="btn-group">
                        <a class="btn btn-block btn-outline-primary" href="empresas/create"><i class="fa fa-plus-circle" aria-hidden="true"> </i> {{__('users.add')}}</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 form-horizontal">
                            <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('Id')}} </label>
                            <div class="col-sm-9">
                                <input type="text" id="id" placeholder="{{__('id')}}" class="col-xs-10 col-sm-5">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('frontend.Nombre_de_la_Empresa')}} </label>
                            <div class="col-sm-9">
                                <input type="text" id="name" placeholder="{{__('frontend.Nombre_de_la_Empresa')}}" class="col-xs-10 col-sm-5">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('Denominación Social')}} </label>
                            <div class="col-sm-9">
                                <input type="text" id="surname" placeholder="{{__('Denominación social')}}" class="col-xs-10 col-sm-5">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('NIF')}} </label>
                            <div class="col-sm-9">
                                <input type="text" name="cif" id="cif" placeholder="{{__('NIF')}}" class="col-xs-10 col-sm-5" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('users.direccion')}} </label>
                            <div class="col-sm-9">
                                <input type="text" name="address" id="address" placeholder="{{__('users.direccion')}}" class="col-xs-10 col-sm-5" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">{{__('frontend.Provincia')}}</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <select class="" required name="province" id="province" required>
                                        <option value="">{{__('frontend.Seleccione')}}...</option>
                                        @foreach($provinces as $province)
                                        <option  value="{{$province->pk}}" >{{$province->name}}</option>
                                        @endforeach
                                    </select>
                                  
                                </div>	
                            </div>

                        </div>

                    
                   </div>
                    <div class="col-sm-6  form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">{{__('frontend.Ayuntamiento')}}</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <select class=""  name="concello_chosen" id="concello_chosen" data-placeholder="{{__('frontend.Seleccione_Ayuntamiento')}}..." required>
                                        <option value="">     {{__('frontend.Seleccione')}}...</option>
                                        <option value="otro">{{__('frontend.Otro')}}</option>
                                    </select>

                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('users.email')}} </label>
                            <div class="col-sm-9">
                                <input type="text" name="email" id="email" placeholder="{{__('users.email')}}" class="col-xs-10 col-sm-5" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('frontend.Telefono')}} </label>
                            <div class="col-sm-9">
                                <input type="text" name="phone" id="phone" placeholder="{{__('frontend.Telefono')}}" class="col-xs-10 col-sm-5" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('Contacto')}} </label>
                            <div class="col-sm-9">
                                <input type="text" name="contacto" id="contacto" placeholder="{{__('Contacto')}}" class="col-xs-10 col-sm-5" />
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <a id="reset-filter-e" class="btn btn-sm btn-clear" style="float: right;margin-bottom: 15px;margin-top: 15px; margin-left: 10px">
                            Limpiar Filtros
                        </a>
                        <a id="enterprise-filter" class="btn btn-sm btn-success" style="float: right;margin-bottom: 15px;margin-top: 15px;">
                            {{__('users.filter')}}
                        </a>
                    </div>
                </div>
                
                                  
                        
                        
                <table id="enterprise-table" class="table table-striped table-bordered table-hover dataTable no-footer">
                    <thead class="thead-light">
                        <tr>
                            <th style ="width:5%">{{__('ID')}}</th>
                            <th style ="width:8%">{{__('Nome')}}</th>
                            <th style ="width:8%">{{__('Razón Social')}}</th>
                            <th style ="width:5%">{{__('users.email')}}</th>
                            <th style ="width:4%">{{__('Cif')}}</th>
                            <th style ="width:4%">{{__('Teléfono')}}</th>
                            <th style ="width:5%">{{__('users.direccion')}}</th>
                            <th style ="width:8%">{{__('Contacto')}}</th>
                            <th style ="width:12%"></th>
                            <th>{{__('province')}}</th>
                            <th>{{__('users.ayuntamiento')}}</th>
                            
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /.col-->
</div>

<div class="modal modal-md" tabindex="-1" role="dialog" id="modalcambiarpassword" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6>Cambiar {{__('users.contrasena')}}</h6>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 form-horizontal">
                        <form method="post" id="passwordForm" action="">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('users.email')}} </label>
                                <div class="col-sm-6">

                                    <input type="hidden" name="inputemailtochange" id="inputemailtochange" value="" />
                                     <input type="hidden" name="user_id" id="user_id" value="" />
                                    <span id="spanemailtochange" style="margin-top: 10px">  </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('users.contrasena')}} </label>
                                <div class="col-sm-6">
                                    <input type="password" name="password1" id="password1" placeholder="{{__('users.nuevacontrasena')}}" autocomplete="off" autofocus="autofocus" min="6" max="10"  />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('users.repitacontrasena')}} </label>
                                <div class="col-sm-6">
                                    <input type="password" name="password2" id="password2" placeholder="{{__('users.repitacontrasena')}}" autocomplete="off"  min="8" max="12"  />
                                </div>
                            </div>
                            <input id = "cambiarpassword" type="submit" class="col-sm-4 btn btn-primary btn-load btn-md" data-loading-text="Cambiando contrasinal..." value="Cambiar Contrasinal">
                        </form>
                    </div><!--/col-sm-6-->
                </div><!--/row-->
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('users.cerrar')}}</button>
            </div>
        </div>
    </div>
</div>
<!-- /.row-->
@endsection
