@extends('layouts.frontend')
@section('title', 'Se solicita ' . $oferta->requested_job)

@section('contenido')
<!--contenido-->
<section class="contenido">
    <?php
    $mostrar = 1;
    if ($oferta->limitar_inscripcion > 0) {
        $postulados = $oferta->postulados()->get()->count();
        if ($postulados >= $oferta->maximo_inscripciones) {
            $mostrar = 0;
        }
    }
    if ($mostrar == 1) {
    ?>

    <div class="container">

        <div class="row ">
            <div class="col-md-8">
                <h1 class="title color-primary">{{__('frontend.Ofertas_de')}} <strong>{{__('frontend.EMPLEO')}}</strong></h1>
            </div>
            <div class="col-md-4 ">
                <div class="breadcrumbs">
                    <span>  </span><a href="{{url('/ofertas')}}">{{__('frontend.Volver_a_Ofertas')}}</a>
                </div>
            </div>
            <div class=clearfix></div>
            <div class="col-md-8 content-oferta">


                <article class="">
                    <div class="text-conte">
                        <h1>{{$oferta->requested_job}}</h1>
                        <p>@if ($oferta->concello) {{  $oferta->concello->name }} @endif</p>
                    </div>
                    <span class="date b-silver"><i><strong>{{__('frontend.FECHA_DE_PUBLICACION')}} </strong> {{$oferta->getFechaLetras()}} </i></span><br>

                    @if ($oferta->tipoOferta->id > 1 && $oferta->pertenece_fie==1 )
                    <span class="title"><strong>{{__('frontend.Tipo_de_Oferta')}}:</strong> 
                        @endif     
                        @if ($oferta->tipoOferta->id > 1)
                        {{$oferta->tipoOferta->nome}} 
                        @endif
                        @if ($oferta->tipoOferta->id > 1 && $oferta->pertenece_fie==1 )
                        </br>
                        @endif

                        @if ($oferta->pertenece_fie==1)
                        TALENTIA SUMMIT 2020
                        @endif</span>


                    <div class="description-o">
                        @if (($oferta->rformacion!='') or ($oferta->rexperiencia!='') or ($oferta->rotros != '') )
                        
                        <h2>{{__('frontend.REQUISITOS')}}: </h2>
                        @if ($oferta->rformacion!='')
                        <p><strong>{{__('Formación')}}:</strong> {{$oferta->rformacion}}  <br>
                        @endif
                        @if ($oferta->rexperiencia!='')   
                            <strong>{{__('Experiencia')}}:</strong> {{$oferta->rexperiencia}} </p>
                        @endif
                        @if ($oferta->rotros != '')   
                            <strong>{{__('Outros')}}:</strong> {{$oferta->rotros}} </p>
                        @endif
                        @endif

                        <h2>{{mb_strtoupper(__('frontend.Descripción'))}}: </h2>
                        <p>{{$oferta->tasks}}</p> 
                    </div>
                    <div align="left">
                        @if (Auth::check())
                        <?php
                        //$postulado = $oferta->postulados()
                        $postulado = $oferta->postulados()->Where('user.pk', '=', Auth::user()->pk)->get();
                        if (($postulado->count() > 0)) {
//                            var_dump($postulado[0]->pivot->created);
//                            die();
                            ?>
                            </br>
                        <?php } else { ?>
                            <button class="btn btn-naranja postularse" data-id="{{$oferta->id}}">INSCRÍBETE AQUÍ</button>
<?php } ?>
                        @else 
                        <button class="btn btn-naranja postularse" data-id="{{$oferta->id}}">INSCRÍBETE AQUÍ</button>
                        @endif
                    </div>


                </article>


            </div>


            <div class="col-md-4 sidebar">
                @if ($oferta->imagen != '')
                <img class="img-responsive" src="/public/offers/{{$oferta->imagen}}">
                @endif
                <br>
                <div class="filtro">
                    <h3>{{__('frontend.OTRAS_CARACTERISTICAS')}}: </h3>
                    <div class="otras-c">
                        @if ( $oferta->salary != '' )
                        <strong> Salario: </strong> {{$oferta->salary}}

                        @endif <br>
                        <strong>{{__('frontend.Contrato')}}:</strong> {{$oferta->contract->nome}} <br>
                        <strong>{{__('frontend.Numero_de_vacantes')}}:</strong> {{$oferta->vacancy}}  <br>
                        @if ( $oferta->getDueDate() != '' ) 
                        <strong>{{__('Prazo de Presentacion de C.V ata')}}: </strong> {{date('d/m/Y', strtotime($oferta->due_date))}} <br>
                        @endif 
                        @if ( $oferta->hours != '' )
                        <strong>{{__('Horario')}}: </strong> {{$oferta->hours}} <br>
                        @endif 

                    </div>
                </div>
                <br>
                <div align="center">

                    @if (Auth::check())
                    <?php
                    //$postulado = $oferta->postulados()
                    $postulado = $oferta->postulados()->Where('user.pk', '=', Auth::user()->pk)->get();
                    if ($postulado->count() > 0) {
//                            var_dump($postulado[0]->pivot->created);
//                            die();
                        ?>
                        <h3 class='btn-naranja'>{{__('frontend.POSTULADO_EL')}} {{date('d/m/Y', strtotime($postulado[0]->pivot->created))}} <h3>

                        <button class="btn btn-default retirarOferta" data-id="{{$oferta->id}}">{{__('frontend.RETIRAR_POSTULACION')}}</button>
                            <?php } else { ?>
                                <button class="btn btn-naranja postularse" data-id="{{$oferta->id}}">{{__('frontend.INSCRIBETE_AQUI')}}</button>
<?php } ?>
                            @else 
                            <button class="btn btn-naranja postularse" data-id="{{$oferta->id}}">{{__('frontend.INSCRIBETE_AQUI')}}</button>
                            @endif

                </div>
            </div>
        </div>
<?php } ?>
    </div>
</section><!-- /contenido -->
@endsection


