@extends('layouts.frontend')
@section('title', $curso->nombre)
@section('contenido')

<!--contenido-->
<section class="contenido">
    <div class="container">
        <div class="row ">
            <div class="col-md-8">
                <h1 class="title color-primary"> <strong> {{__('frontend.Formacion')}} </strong></h1>
            </div>
            <div class="col-md-4 ">
                <div class="breadcrumbs">
                    <span>  </span><a href="{{url('/formacion')}}"> {{__('frontend.Volver_a_Cursos')}}</a>
                </div>
            </div>
            <div class=clearfix></div>
            <div class="col-md-8 content-oferta">

                <article class="">
                    <div class="text-conte">
                        <h1>{{$curso->nombre}}</h1>
                        <p>{{$curso->lugar_celebracion}} </p>
                    </div>
                    <span class="date b-silver"><i><strong>{{__('frontend.DataInicio')}} </strong> {{$curso->getFechaLetras()}} </i></span><br>
                    @if ($curso->tipoCurso->id == 2)
                    <span class="title"><strong>{{__('frontend.Tipo_de_Curso')}}</strong> </span>
                    {{$curso->tipoCurso->nome}} 
                    @endif
                    @if ($curso->tipoCurso->id == 2 && $curso->pertenece_fie==1) 
                    </br>
                    @endif

                    @if ($curso->pertenece_fie==1)
                    <span class="title">{{__('frontend.Perteneceafie')}}</span>
                    @endif


                    <div class="description-o">
                        @if ($curso->requerimiento)
                        <h2>{{__('frontend.REQUISITOS')}}: </h2>
                        <p>{{$curso->requerimiento}}   </p>
                        @endif
                        <br>
                        <p> <strong>{{__('frontend.CONTENIDO')}}:</strong>  {!! $curso->contenido !!}  </p>


                    </div>
                    <div align="left">
                        @if (Auth::check())
                        <?php
                        //$postulado = $oferta->postulados()
                        $postulado = $curso->users()->Where('pk', '=', Auth::user()->pk)->get();
                        if (($postulado->count() > 0)) {
//                            var_dump($postulado[0]->pivot->created);
//                            die();
                            ?>
                            </br>
                        <?php } else { ?>
                            <button class="btn btn-naranja inscribirme" data-id="{{$curso->id}}">{{__('frontend.INSCRIBETE_AQUI')}}</button>
                        <?php } ?>
                        @else 
                        <button class="btn btn-naranja inscribirme" data-id="{{$curso->id}}">{{__('frontend.INSCRIBETE_AQUI')}}</button>
                        @endif
                    </div>


                </article>


            </div>


            <div class="col-md-4 sidebar">
                @if ($curso->imagen != '')
                <img class="img-responsive" src="/public/cursos/{{$curso->imagen}}">
                @endif
                <br>
                
                <div class="filtro">
                    <h3>{{__('frontend.OTRAS_CARACTERISTICAS')}}: </h3>
                    <div class="otras-c">
                         
                        @if ( $curso->observaciones != '' )
                         {{$curso->observaciones}} <br>
                        @endif 
                        
                       
                        @if ( $curso->precio != '' )
                        <strong>Precio</strong>:{{$curso->precio}}
                        @endif <br>
                        <strong>{{__('Prazas totales')}}</strong>: {{$curso->plazas_total}}  <br>
                         @if ( $curso->duracion_horas != '' )
                        <strong>{{__('Duración')}}</strong>: {{ $curso->duracion_horas}} Horas <br>
                        @endif 
                        
                        @if ( $curso->hora_inicio != '' )
                        <strong>{{__('Horario')}}</strong>: de {{$curso->hora_inicio}} ata {{$curso->hora_fin}} <br>
                        @endif 

                    </div>
                </div>
                <br>
                <div align="center">

                    @if (Auth::check())
                    <?php
                    //$postulado = $oferta->postulados()
                    $postulado = $curso->users()->Where('pk', '=', Auth::user()->pk)->get();
                    if ($postulado->count() > 0) {
//                            var_dump($postulado[0]->pivot->created);
//                            die();
                        ?>
                        <h3 class='btn-naranja'>Inscrito el {{date('d/m/Y', strtotime($postulado[0]->pivot->fechainscripcion))}} <h3>
                            <?php } else { ?>
                                <button class="btn btn-naranja inscribirme" data-id="{{$curso->id}}">{{__('frontend.INSCRIBETE_AQUI')}}</button>
                            <?php } ?>
                            @else 
                            <button class="btn btn-naranja inscribirme" data-id="{{$curso->id}}">{{__('frontend.INSCRIBETE_AQUI')}}</button>
                            @endif

                            </div>
                            </div>
                            </div>
                            </div>
                            </div><!-- /contenido -->
                            @endsection