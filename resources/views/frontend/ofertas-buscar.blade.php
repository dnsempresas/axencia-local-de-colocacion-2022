@extends('layouts.frontend')
@section('contenido')
@section('title', 'Ofertas de Emprego')
<a id="inicio"></a>
<section class="contenido">
    <div class="container">
        <div class="row ">
            <div class="col-md-12">
                <h1 class="title color-primary"><strong>{{__('backend.Oferta_de_empleo')}}</strong></h1>

            </div>
            <div class="col-md-12 content-oferta">
                 @if (count($ofertas) > 0)
                <label class="control-label no-padding-right">{{__('frontend.Cantidad_por_pagina')}} </label>

                    <select id="pageSize" class="page_limit pgination-select" onchange="">  
                        <option value="10" @if($pageSize == 10) selected @endif>10</option>
                        <option value="30" @if($pageSize == 30) selected @endif>30</option>
                        <option value="50" @if($pageSize == 50) selected @endif>50</option>
                    </select> 

                    {{__('frontend.Presentando')}}  {{ $ofertas->firstItem() }} - {{ $ofertas->lastItem() }} {{__('frontend.de')}} {{ $ofertas->total() }}
                 
                <ul class="ofertas" style="column-count: 2;">
                
                    @foreach($ofertas as $oferta)
                    <?php
                    $mostrar = 1;
                    if ($oferta->limitar_inscripcion > 0) {
                        $postulados = $oferta->postulados()->get()->count();
                        if ($postulados >= $oferta->maximo_inscripciones) {
                            $mostrar = 0;
                        }
                    }
                    if ($mostrar == 1) {
                        ?>

                        <li class="item">
                            <!--<a class="oferta-ficha" href="/oferta-ficha/{{$oferta->id}}">-->
                            <a class="oferta-ficha" href="<?php if ($oferta->tipogestion == 1)
                        echo $oferta->urltercero;
                    else
                        echo $oferta->getSlug();
                    ?>" target="<?php if ($oferta->tipogestion == 1)
                        echo 'blank_';
                    else
                        echo '';
                    ?>">
                                <article class="feature">
                                    <div class="col-md-12">
                                        @if ($oferta->imagen != '')
                                        <div class="col-md-3">
                                            <img class="img-responsive" src="/public/offers/{{$oferta->imagen}}">
                                        </div>
                                        <div class="col-md-9">
                                            <p class="date"><i>{{$oferta->getFechaLetras()}}</i></p>
                                            @if ($oferta->tipoOferta->id > 1)
                                            <span class="title">{{$oferta->tipoOferta->nome}} </span>
                                            @endif
                                            @if ($oferta->tipoOferta->id > 1 && $oferta->pertenece_fie==1 )
                                            </br>
                                            @endif
                                            @if ($oferta->pertenece_fie==1)
                                            <span class="title">{{__('backend.perteneceafie')}}</span>
                                            @endif
                                            <div class="text-conte">
                                                <h2>{{$oferta->requested_job}}</h2>
                                                <p>@if ($oferta->concello) {{  $oferta->concello->name }} @endif </p>
                                            </div>
                                        </div>
                                        @else
                                        <div class="col-md-12">
                                            <p class="date"><i>{{$oferta->getFechaLetras()}}</i></p>
                                            @if ($oferta->tipoOferta->id > 1)
                                            <span class="title">{{$oferta->tipoOferta->nome}} </span>
                                            @endif
                                            @if ($oferta->tipoOferta->id > 1 && $oferta->pertenece_fie==1 )
                                            </br>
                                            @endif
                                            @if ($oferta->pertenece_fie==1)
                                            <span class="title">{{__('backend.perteneceafie')}}</span>
                                            @endif
                                            <div class="text-conte">
                                                <h2>{{$oferta->requested_job}}</h2>
                                                <p>@if ($oferta->concello) {{  $oferta->concello->name }} @endif </p>
                                            </div>
                                        </div>

                                        @endif
                                    </div>
                                </article>
                            </a>
                        </li>
<?php }; ?>
                    @endforeach

                   

                </ul>
                     @else
                    <h3>
                        Non se atoparon resultados. Pregase que o intente de novo usando outro filtro
                    </h3>

                    @endif

                <!--                //$ofertas->links()-->
             
                
                {{ $ofertas->appends(['buscarOferta' => $filtro])->links() }}
            </div>


            
        </div>
    </div>
</section><!-- /contenido -->
@endsection