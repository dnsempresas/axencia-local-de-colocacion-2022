
<div class="row">
    <div class="col-md-12">
        <h3 class="border-silver-top"><strong>{{__('frontend.Registrar_Perfil_de_Usuario')}}</strong> </h3>

        <div class="card">
            <div class="card-header">
                <i class="icon-note"></i>
            </div>

            @if ($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i></h4>
                @foreach ($errors->all() as $error)
                <p>{{ $error }}</p>
                @endforeach
            </div>
            @endif
            @if(Session::has('alert-success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
            </div>
            @endif

            <div class="card-body">
                <form method="POST" novalidate action="{{route('usuarios.store') }}"  accept-charset="UTF-8" class="form-horizontal" id="userForm" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div  class="row">
                        <div id="izquierda" class="col-md-6">

                            <div class="panel panel-default">
                                <div class="panel-heading">{{__('frontend.Datos_de_Acceso')}} </div>
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right">{{__('frontend.Tipo_de_Usuario')}}</label>
                                        <div class="col-sm-8" style="margin-top:10px">
                                            <span > 
                                               {{__('frontend.Demandante_de_empleo')}}
                                                <input type="hidden" name="tiporole" id="tiporole" value="1"/>
                                            </span>

                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1">{{__('frontend.Correo_electronico')}}</label>
                                        <div class="col-sm-6">
                                            <input type="email" name="email" id="email" placeholder="{{__('frontend.Correo_electronico')}}" class="col-xs-10 col-sm-5" value="{{old('email')}}"  />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1">{{__('frontend.Contrasenya')}}</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="password" id="password" placeholder="{{__('frontend.Contrasenya')}}" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>
<!--                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1">{{__('frontend.Repita_Contrasenya')}}</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="rpassword" id="rpassword" placeholder="{{__('frontend.Repita_Contrasenya')}}" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>-->


                                </div>
                            </div>



                            <div id="panel_datospersonales" class="panel panel-default" >
                                <div class="panel-heading">{{__('frontend.Datos_Personales')}} </div>
                                <div class="panel-body">

                                    <div class="form-group">

                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Nombre')}} </label>

                                        <div class="col-sm-8">
                                            <input type="text" name="name" id="name" placeholder="{{__('frontend.Nombre')}}"  value="{{old('name')}}"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Apellidos')}} </label>
                                        <div class="col-sm-8">
                                            <input type="text" name="last_name" id="last_name" placeholder="{{__('frontend.Apellidos')}}" value="{{old('last_name')}}" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.DNI-NIE')}} </label>
                                        <div class="col-sm-8">
                                            <input type="text" name="cif" id="cif" placeholder="{{__('frontend.DNI-NIE')}}" value="{{old('cif')}}"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">{{__('frontend.Genero')}}</label>
                                        <div class="col-sm-8">


                                                <select required name="genero" id="genero">
                                                    <option value="">{{__('frontend.Seleccione')}}...</option>
                                                    @foreach($generos as $genero)
                                                    <option {{old('genero')==$genero->id?'selected':''}}  value="{{$genero->id}}">{{ session('lang')=='es' ?  $genero->nombre :  $genero->nome }} </option>
                                                    @endforeach
                                                </select>
                                                <div class="input-group">
                                                    <input required style="margin-top: 5px; display: {{old('genero')==4?'block':'none'}};" class="form-control" id="other_genero" name="other_genero" type="text" placeholder="Especifique Genero..." value="{{old('other_genero')}}"/>
                                                </div>


                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">{{__('frontend.Fecha_de_Nacimiento')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input class="form-control date-picker" id="birth" name="birth" type="text" data-date-format="dd/mm/YYYY" value="{{date('d-m-Y', strtotime(old('birth')))}}"/>
                                            </div>	
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <span class="col-sm-3 control-label no-padding-right">Foto de Perfil</span>
                                        <div class="col-sm-4">
                                            <img id="imgSalida" width="120px" height="140px" src="" />
                                            <input id="foto_perfil" type="file" name="foto_perfil" accept="image/x-png,image/gif,image/jpeg" style="color:gray !important; border: 0">
                                            <a class="removeimg" id ="removeimg" href="" title='Borrar'style="display: none" ><span class='glyphicon glyphicon-remove' style='color:red; '></span></a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-8 control-label no-padding-right">{{__('frontend.Discapacidad_reconocida_igual_o_superior_al_33')}}</label>


                                        <div class="radio">
                                            <label>
                                                <input name="disability" type="checkbox" class="ace" <?php if (old('disability') == 1) echo 'checked'; ?> />
                                                <span class="lbl"></span>
                                            </label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            
                        </div>


                        <div id="derecha" class="col-md-6">
                            <div id="panel_direccion" class="panel panel-default">
                                <div class="panel-heading"> {{__('frontend.Direccion')}}  </div>
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">{{__('frontend.Provincia')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select class="" required name="province" id="province" required>
                                                    <option selected value="">{{__('frontend.Seleccione')}}...</option>
                                                    @foreach($provinces as $province)
                                                    <option  value="{{$province->pk}}" {{ (old('province') == $province->id ) ? 'selected' : '' }} >{{$province->name}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="input-group">
                                                    <input style="margin-top: 5px;display: none;" class="form-control" id="other_province" name="other_province" type="text" placeholder="Especifique Provincia..."/>
                                                </div>
                                            </div>	
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">{{__('frontend.Ayuntamiento')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select class=""  name="concello_chosen" id="concello_chosen" data-placeholder="{{__('frontend.Seleccione_Ayuntamiento')}}..." required>
                                                    <option selected value="">     {{__('frontend.Seleccione')}}...</option>
                                                    <option value="otro">{{__('frontend.Otro')}}</option>
                                                </select>

                                                <input  style="margin-top: 5px;display: none;" class="form-control" id="other_concello" name="other_concello" type="text" placeholder="{{__('frontend.Especifique_Ayuntamiento')}}..."/>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Direccion')}}</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="address" id="address" placeholder="{{__('frontend.Direccion')}}" value="{{old('address')}}" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Codigo_Postal')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="number" name="postal_code" id="postal_code" placeholder="{{__('frontend.Codigo_Postal')}}" value="{{old('postal_code')}}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1">{{__('frontend.Telefono')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="number" name="phone" id="phone" maxlength=9 placeholder="{{__('frontend.Telefono')}}"  value="{{old('phone')}}"/>
                                            </div>
                                        </div>
                                    </div>	
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1">{{__('frontend.Otro_Telefono')}}</label>
                                        <div class="col-sm-8">
                                            <input type="number" name="other_phone" id="other_phone" maxlength=9 placeholder="{{__('frontend.Otro_Telefono')}}"  value="{{old('other_phone')}}"/>
                                        </div>
                                    </div>



                                </div>
                            </div>
<!--                            <div id="panel_datosprofesionales" class="panel panel-default" >
                                <div class="panel-heading"> Datos Profesionales </div>
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">Estudios</label>
                                        <div class="col-sm-8">
                                                <select name="studies" id="studies">
                                                    <option value="">Selecione...</option>

                                                    @foreach($grades as $grade)
                                                    <option value="{{$grade->id}}">{{ session('lang')=='es' ?  $grade->description :  $grade->nome }}</option>
                                                    @endforeach
                                                </select>
                                        </div>

                                    </div>

                                    <div class="form-group" id="specialtyC">
                                        <label class="col-sm-4 control-label no-padding-right">Especialidad</label>
                                        <div class="col-sm-8">
                                                <select class="chosen-select" name="specialty" id="specialty" data-placeholder="Seleccione..."></select>
                                            <select class="chosen-select" name="specialty2[]" id="specialty2" multiple data-placeholder="" >
                                            </select>


                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">Otros Cursos:</label>
                                        <div class="col-sm-8">
                                            <textarea name="others_courses" id="others_courses">{{old('other_courses')}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label id="labelpuestos" class="col-sm-4 control-label no-padding-right">Sin experiencia profesional</label>
                                        <div class="col-sm-8">
                                            <input class="ace" type="checkbox" id="no_experience" name="no_experience" style="margin-top: 10px;" /> 
                                            <span class="lbl"></span>

                                        </div>

                                    </div>
                                    <div class="form-group" id="divpuestos">
                                        <label id="labelpuestos" class="col-sm-4 control-label no-padding-right">Puestos de Trabajo</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select  required class="chosen-select" name="job_posts[]" id="job_posts" multiple="" data-placeholder="Puestos de trabajo en que tienes experiencia profesional">
                                                    @foreach($jobPosts as $jobPost)
                                                    <option value="{{$jobPost->id}}">{{ session('lang')=='es' ?  $jobPost->nombre :  $jobPost->nome }}</option>
                                                    @endforeach
                                                </select>

                                            </div>

                                        </div>

                                    </div>

                                    <div id="divprestaciones" class="form-group">
                                        <label id="labelprestaciones" class="col-sm-4 control-label no-padding-right">Percibe prestación o subsidio por desempleo</label>
                                        <div class="col-sm-8">
                                            <input class="ace" type="checkbox" id="prestacion" name="prestacion" style="margin-top: 10px;" value="{{old('prestacion')}}"/> 
                                            <span class="lbl"></span>

                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">Idiomas</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="languages" id="languages" placeholder="Ingles avanzado..."  value="{{old('languages')}}" />
                                            <select name="langs" id="langs">
                                                    @foreach($langs as $lang)
                                                            <option value="{{$lang->id}}">{{$lang->name}}</option>
                                                    @endforeach
                                            </select>
                                            <select name="lang_level" id="lang_level" style="margin-top: 5px;">
                                                    <option value="native">Nativo</option>
                                                    <option value="fluid">Avanzado</option>
                                                    <option value="intermediate">Intermedio</option>
                                                    <option value="basic">Basico</option>
                                            </select>

                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">Informatica</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="computer_skills" id="computer_skills" placeholder="Microsoft Access avanzado..." value="{{old('computer_skills')}}" />
                                            <select name="computer" id="computer">
                                                    <option value="access">Microsoft Access</option>
                                                    <option value="excel">Microsoft Excel</option>
                                                    <option value="word">Microsoft Word</option>
                                                    <option value="powerpoint">Microsoft PowerPoint</option>
                                                    <option value="photoshop">Microsoft PhotoShop</option>
                                                    <option value="dreamweaver">Microsoft Dreamweaver</option>
                                                    <option value="corel_draw">Corel Draw</option>
                                            </select>
                                            <select name="computer_level" id="computer_level" style="margin-top: 5px;">
                                                    <option value="fluid">Avanzado</option>
                                                    <option value="intermediate">Intermedio</option>
                                                    <option value="basic">Basico</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">Permisos de Conducir</label>
                                        <div class="col-sm-8">
                                                <select class="chosen-select" name="driving_permit[]" id="driving_permit" multiple data-placeholder="Seleccione permisos de conducir...">
                                                    @foreach($permits as $permit)
                                                    <option data-img-src="/public/permisosconducir/{{$permit->icono}}" value="{{$permit->id}}">{{$permit->description}}</option>

                                                    @endforeach

                                                </select>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <label class="col-sm-4 control-label no-padding-right" for="file-input">CV Principal</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" id="cvprincipal" type="file" name="cvprincipal"  accept="application/pdf,application/msword,
                                                   application/vnd.openxmlformats-officedocument.wordprocessingml.document" >
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-4 control-label no-padding-right" for="file-input">CV Secundario</label>
                                        <div class="col-sm-8 ">
                                            <input class="form-control"  id="cvsecundario" type="file" name="cvsecundario" accept="application/pdf,application/msword,
                                                   application/vnd.openxmlformats-officedocument.wordprocessingml.document">
                                        </div>
                                    </div>


                                </div>
                            </div>


                            <div id="panel_intereseprofesionales" class="panel panel-default">
                                <div class="panel-heading"> Intereses Profesionales </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">Sectores de Interes</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select class="chosen-select" name="sectors[]" id="sectors" multiple data-placeholder="Seleccione sectores...">
                                                    @foreach($sectors as $sector)
                                                    <option value="{{$sector->id}}">{{ session('lang')=='es' ?  $sector->nombre :  $sector->nome }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group" id="divpuestos">
                                        <label id="labelpuestos" class="col-sm-4 control-label no-padding-right">Puestos de Interes</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select  class="chosen-select" name="positions[]" id="positions" multiple="" data-placeholder="Puestos en que te gustaría trabajar">
                                                    @foreach($jobPosts as $jobPost)
                                                    <option value="{{$jobPost->id}}">{{ session('lang')=='es' ?  $jobPost->nombre :  $jobPost->nome }}</option>
                                                    @endforeach
                                                </select>

                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </div>-->

                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success" name="Guardar">{{__('frontend.Guardar')}}</button> 
                            </div>
                        </div>
                    </div>
                </form>						

            </div>
        </div>
    </div>
</div>
