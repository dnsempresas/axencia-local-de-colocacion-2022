@extends('layouts.frontend')
@section('title', 'Axencia')
@section('contenido')


<!--contenido-->
<section class="contenido">
    <div class="container">
        <div class="row ">
            @if ($noticia)
            <div class="col-md-12">
                <h1 class="title color-primary">{{$noticia->titulo}}</h1>
            </div>
            <div class="col-md-4 ">
                <div class="breadcrumbs">
                    <span>  </span><a href=""></a>
                </div>
            </div>
            <div class=clearfix></div>
            <div class="col-md-12 content-oferta">

                <article class="">
                    <div class="text-conte">
                       
                    </div>

                    <div class="description-o">

                        @if ($noticia->imagen_destacada != '')
                        <div class="col-md-12 img-des">
                            <img class="" src="/public/noticias/{{$noticia->imagen_destacada}}">
                        </div>
                        @endif
                        <p>{{$noticia->entradilla}}</p> 
                    </div>
                    <div class="description-o">

                        {!!$noticia->noticia!!}
                    </div>


                </article>


            </div>
            @endif
        </div>
    </div>
</div><!-- /contenido -->
@endsection