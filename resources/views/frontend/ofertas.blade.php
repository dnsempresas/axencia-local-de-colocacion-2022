@extends('layouts.frontend')
@section('contenido')
@section('title', 'Ofertas de Emprego')
<a id="inicio"></a>
<section class="contenido">
    <div class="container">
        <div class="row ">
            <div class="col-md-12">
                <h1 class="title color-primary"><strong>{{__('backend.Oferta_de_empleo')}}</strong></h1>

            </div>
            <div class="col-md-8 content-oferta">
                <ul class="ofertas">
                    @if (count($ofertas) > 0)

                    <label class="control-label no-padding-right">{{__('frontend.Cantidad_por_pagina')}} </label>

                    <select id="pageSize" class="page_limit pgination-select" onchange="">  
                        <option value="10" @if($pageSize == 10) selected @endif>10</option>
                        <option value="30" @if($pageSize == 30) selected @endif>30</option>
                        <option value="50" @if($pageSize == 50) selected @endif>50</option>
                    </select> 

                    {{__('frontend.Presentando')}}  {{ $ofertas->firstItem() }} - {{ $ofertas->lastItem() }} {{__('frontend.de')}} {{ $ofertas->total() }} 
                    @foreach($ofertas as $oferta)
                    <?php
                    $mostrar = 1;
                    if ($oferta->limitar_inscripcion > 0) {
                        $postulados = $oferta->postulados()->get()->count();
                        if ($postulados >= $oferta->maximo_inscripciones) {
                            $mostrar = 0;
                        }
                    }
                    if ($mostrar == 1) {
                        ?>

                        <li class="item">
                            <!--<a class="oferta-ficha" href="/oferta-ficha/{{$oferta->id}}">-->
                            <a class="oferta-ficha" href="<?php if ($oferta->tipogestion == 1)
                        echo $oferta->urltercero;
                    else
                        echo $oferta->getSlug();
                    ?>" target="<?php if ($oferta->tipogestion == 1)
                        echo 'blank_';
                    else
                        echo '';
                    ?>">
                                <article class="feature">
                                    <div class="col-md-12">
                                        @if ($oferta->imagen != '')
                                        <div class="col-md-3">
                                            <img class="img-responsive" src="/public/offers/{{$oferta->imagen}}">
                                        </div>
                                        <div class="col-md-9">
                                            <p class="date"><i>{{$oferta->getFechaLetras()}}</i></p>
                                            @if ($oferta->tipoOferta->id > 1)
                                            <span class="title">{{$oferta->tipoOferta->nome}} </span>
                                            @endif
                                            @if ($oferta->tipoOferta->id > 1 && $oferta->pertenece_fie==1 )
                                            </br>
                                            @endif
                                            @if ($oferta->pertenece_fie==1)
                                            <span class="title">{{__('backend.perteneceafie')}}</span>
                                            @endif
                                            <div class="text-conte">
                                                <h2>{{mb_strtoupper($oferta->requested_job,'UTF-8')}}</h2>
                                                <p>@if ($oferta->concello) {{  $oferta->concello->name }} @endif </p>
                                            </div>
                                        </div>
                                        @else
                                        <div class="col-md-12">
                                            <p class="date"><i>{{$oferta->getFechaLetras()}}</i></p>
                                            @if ($oferta->tipoOferta->id > 1)
                                            <span class="title">{{$oferta->tipoOferta->nome}} </span>
                                            @endif
                                            @if ($oferta->tipoOferta->id > 1 && $oferta->pertenece_fie==1 )
                                            </br>
                                            @endif
                                            @if ($oferta->pertenece_fie==1)
                                            <span class="title">{{__('backend.perteneceafie')}}</span>
                                            @endif
                                            <div class="text-conte">
                                                <h2>{{mb_strtoupper($oferta->requested_job,'UTF-8')}}</h2>
                                                <p>@if ($oferta->concello) {{  $oferta->concello->name }} @endif </p>
                                            </div>
                                        </div>

                                        @endif
                                    </div>
                                </article>
                            </a>
                        </li>
<?php }; ?>
                    @endforeach
                 </ul>
                
                {{ $ofertas->appends(['provincias' => $filtro])->links() }}
                    @else
                    <h3>
                        Non se atoparon resultados. Pregase que o intente de novo usando outro filtro
                    </h3>

                    @endif
            </div>


            <div class="col-md-4 ">
                <div class="filtro">
                    <h3>{{__('backend.Filtrar_las_ofertas')}}</h3>
                   
                    <form method="GET" action="/ofertas"  accept-charset="UTF-8" >
                         <h4>Sectores</h4>
                         
                          <div class="form-group">
                                        <div class="col-sm-12">
                                            <select class="chosen-select" name="sectors[]" id="sectors" multiple data-placeholder="Seleccione sectores...">
                                                @foreach($SectoresOfertas as $sector)
                                                <option <?php
                                                //$usersectores = explode('/', $user->sector);
                                                if (in_array($sector->id, $sectoresSelected)) {
                                                    echo 'selected';
                                                }
                                                ?> value="{{$sector->id}}">{{$sector->nome}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                         
                         
                         <h4>Provincias</h4>
                        <input type="radio" name="provincias" id="provincias" value="todas"  checked >  Todas <br>
                        <input type="radio" name="provincias" id="provincias" value="15" <?php if ($filtro == 15) echo "checked"; ?> > A Coruña <br>
                        <input type="radio" name="provincias" id="provincias" value="27" <?php if ($filtro == 27) echo "checked"; ?>> Lugo <br>
                        <input type="radio" name="provincias" id="provincias" value="32" <?php if ($filtro == 32) echo "checked"; ?>> Ourense <br>
                        <input type="radio" name="provincias" id="provincias" value="36" <?php if ($filtro == 36) echo "checked"; ?>>  Pontevedra <br><br> 
                        <input type="radio" name="provincias" id="provincias" value="otras" <?php if ($filtro == "otras") echo "checked"; ?>>  Outras <br><br>
                        
                        <div align="center">	
                            <button type="submit" id="filtrar_ofertas" class="btn btn-default">FILTRAR</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section><!-- /contenido -->
@endsection