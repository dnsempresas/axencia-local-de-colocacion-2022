@extends('layouts.panelempresas')

@section('titulo_pantalla')

@endsection

@section('content')
@if ($errors->any())
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">Ã—</button>
    <h4><i class="icon fa fa-ban"></i>{{__('offers.errors')}}</h4>
    @foreach ($errors->all() as $error)
    <p>{{ $error }}</p>
    @endforeach
</div>
@endif

@if(Session::has('alert-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
    <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
</div>
@endif
<div class="row">
    <div class="col-md-12">
                <h3 class="border-silver-top"><strong>{{__('frontend.Editar_Perfil')}}</strong> </h3>
        <div class="card">
            <div class="card-header">
                <i class="icon-note"></i></div>
            @if (isset($anchor))
            <input type="hidden" id="anchor" value="{{ $anchor }}">
            @endif

            <div class="card-body">
                <form method="POST"  action="{{route('usuarios.update', $user->pk) }}"  accept-charset="UTF-8" class="form-horizontal" id="enterpriseForm" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{method_field('PATCH')}}
                    <div  class="row">
                        <div id="izquierda" class="col-md-6">

                            <div class="panel panel-default">
                                <div class="panel-heading">{{__('frontend.Datos_de_Acceso')}} </div>
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right">{{__('frontend.Tipo_de_Usuario')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select name="tiporole" id="tiporole">

                                                    <option selected value="2">Empresa</option>

                                                </select>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="form-group required">
                                      <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Correo_electronico')}}</label>
                                        <div class="col-sm-6">
                                            <input type="email" name="email" id="email" placeholder="{{__('frontend.Correo_electronico')}}" class="col-xs-10 col-sm-5" value="{{$user->email}}" required />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1">{{__('frontend.Contrasenya')}}</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="password" id="password" placeholder="{{__('frontend.Contrasenya')}}" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1">{{__('frontend.Repita_Contrasenya')}}</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="rpassword" id="rpassword" placeholder="{{__('frontend.Repita_Contrasenya')}}" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>


                                </div>
                            </div>

                            <div id="panel_datosempresariales" class="panel panel-default"  >
                                <div class="panel-heading">Datos de Empresa</div>
                                <div class="panel-body">

                                    <div class="form-group required">

                                         <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Nombre_de_la_Empresa')}} </label>

                                        <div class="col-sm-8">
                                            <input type="text" name="name" id="name" placeholder="{{__('frontend.Nombre_de_la_Empresa')}}" value="{{$user->name}}" class="col-xs-10 col-sm-5" required/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Denominacion_Social')}}  </label>
                                        <div class="col-sm-8">
                                            <input type="text" name="last_name" id="last_name" placeholder="{{__('frontend.Denominacion_Social')}} " value="{{$user->surname}}" class="col-xs-10 col-sm-5" required/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.NIF_CIF')}} </label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="cif" id="cif" placeholder="{{__('frontend.NIF_CIF')}}" value="{{$user->cif}}" class="col-xs-10 col-sm-5" required />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>


                        <div id="derecha" class="col-md-6">

                            <div id="panel_direccion" class="panel panel-default">
                                <div class="panel-heading"> Dirección  </div>
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">{{__('frontend.Provincia')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group" style="width:100%">
                                                <select class="chosen_select" required name="province" id="province">
                                                    @foreach($provinces as $province)
                                                    <option {{$user->province_fk===$province->pk?'selected':''}} value="{{$province->pk}}">{{$province->name}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="input-group">
                                                    <input required style="margin-top: 5px;display: none;" class="form-control" id="other_province" name="other_province" type="text" placeholder="Especifique Provincia..."/>
                                                </div>
                                            </div>	
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label id="labelconcello" class="col-sm-4 control-label no-padding-right">{{__('frontend.Ayuntamiento')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group" style="width:100%">
                                                <select class="chosen_select" required name="concello_chosen" id="concello_chosen" data-placeholder="Seleccione...">
                                                    <option value="">Seleccione...</option>
                                                    @foreach($concellos as $concello)
                                                    <option {{$user_concello===$concello->id?'selected':''}} value="{{$concello->id}}">{{$concello->name}}</option>
                                                    @endforeach
                                                    <option value="otro">Otro</option>
                                                </select>

                                                <input required style="margin-top: 5px;display: none;" class="form-control" id="other_concello" name="other_concello" type="text" placeholder="Especifique..."/>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Direccion')}}</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="address" id="address" placeholder="{{__('frontend.Direccion')}}" value="{{$user->address}}" required/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Codigo_Postal')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="number" name="postal_code" id="postal_code" placeholder="{{__('frontend.Codigo_Postal')}}" value="{{$user->zip}}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1">{{__('frontend.Telefono')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="number" name="phone" id="phone" maxlength=9 placeholder="{{__('frontend.Telefono')}}"  value="{{$user->telephone}}" required/>
                                            </div>
                                        </div>
                                    </div>	
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1">{{__('frontend.Otro_telefono')}}</label>
                                        <div class="col-sm-8">
                                            <input type="number" name="other_phone" id="other_phone" maxlength=9 placeholder="{{__('frontend.Otro_telefono')}}"  value="{{$user->other_phone}}"/>
                                        </div>
                                    </div>



                                </div>
                            </div>

                            <div id="panel_personadecontacto" class="panel panel-default" >
                                <div class="panel-heading"> {{__('frontend.Persona_de_contacto')}}</div>
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Persona_de_contacto')}} </label>
                                        <div class="col-sm-8">
                                            <input type="text" name="contacto" id="contacto" placeholder="Persona de contacto" value="{{$user->contacto}}" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Cargo')}} </label>
                                        <div class="col-sm-8">
                                            <input type="text" name="cargocontacto" id="cargocontacto" placeholder="{{__('frontend.Cargo_Persona_de_contacto')}}" value="{{$user->cargocontacto}}" />
                                        </div>
                                    </div>




                                </div>
                            </div>
                            
                            <div id="panel_logo" class="panel panel-default" >
                                <div class="panel-heading"> {{__('Logo')}}</div>
                                <div class="panel-body">
                            <div class="form-group">
                                        <span class="col-sm-4 control-label no-padding-right">Logo Empresa</span>
                                        <div class="col-sm-4">
                                            
                                            <img id="imgSalida" width="120px" height="140px" src="{{asset('/public/storage/fotos/'. $user->foto)}}"/>
                                            <input id="foto_perfil" type="file" name="foto_perfil" accept="image/*" >
                                            <a class="removeimg" id ="removeimg" href="" title='Borrar' ><span class='glyphicon glyphicon-remove' style='color:red; '></span></a>

                                          
                                             <input type="hidden" name="foto_user" id="foto_user" value ="{{ $user->foto }}" />
                                        </div>
                                    </div>
                                    </div>
                            </div>


                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success" name="Guardar">{{__('frontend.Guardar')}}</button> 
                            </div>
                        </div>
                    </div>
                </form>						

            </div>
        </div>
    </div>
</div>
@endsection