@extends('layouts.frontend')
@section('contenido')
@section('title', 'Oferta de Emprego')

<!--contenido-->
<section class="contenido">
    <div class="container">
        <div class="row ">
            <div class="col-md-8">
                <h1 class="title color-primary">Ofertas de <strong>EMPREGO</strong></h1>
            </div>
            <div class="col-md-4 ">
                <div class="breadcrumbs">
                    <span> < </span><a href="ofertas.php">Volver a Ofertas</a>
                </div>
            </div>
            <div class=clearfix></div>
            <div class="col-md-8 content-oferta">

                <article class="">
                    <div class="text-conte">
                        <h1>Técnico de mantemento con experiencia (reparación de maquinaria industrial)</h1>
                        <p>Santiago de Compostela </p>
                    </div>
                    <span class="date b-silver"><i><strong>DATA DE PUBLICACIÓN</strong>7 de septiembre de 2018</i></span><br>
                    <span class="title"><strong>Tipo de 0ferta:</strong> Oferta do FIE</span>
                    <div class="description-o">
                        <h2>REQUISITOS: </h2>
                        <p><strong>ESTUDIOS MÍNIMOS:</strong> sin estudios<br>
                            <strong>EXPERIENCIA MÍNIMA:</strong> al menos 3 años</p>

                        <h2>DESCRIPCIÓN: </h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> 

                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>

                        <p><i><strong>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</strong></i></p> 
                    </div>
                    <div align="left">
                        <button class="btn btn-naranja">INSCRÍBETE AQUÍ</button>
                    </div>


                </article>


            </div>


            <div class="col-md-4 sidebar">
                <img class="img-responsive" src="img/cocinero.jpg">
                <br>
                <div class="filtro">
                    <h3>OUTRAS CARACTERÍSTICAS: </h3>
                    <div class="otras-c">
                        <strong>Tipo de salario:</strong> no definido<br>
                        <strong>Contrato:</strong> Indefinido<br>
                        <strong>Nivel:</strong> encargado<br>
                        <strong>Persoal a cargo:</strong> 5 persoas<br>
                        <strong>Tipo de xornada:</strong> 8 horas<br>
                        <strong>Número de vacantes:</strong> 2<br>
                    </div>
                </div>
                <br>
                <div align="center">
                    <button class="btn btn-naranja">INSCRÍBETE AQUÍ</button>
                </div>
            </div>
        </div>
    </div>
</div><!-- /contenido -->
@endsection