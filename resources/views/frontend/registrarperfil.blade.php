@extends('layouts.frontend')
@section('contenido')
@section('title', 'Registrar Perfil')

<link rel="stylesheet" href="{{asset('assets/css/ace.min.css')}}" class="ace-main-stylesheet" id="main-ace-style" />
<link rel="stylesheet" href="{{asset('assets/css/ace-skins.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/ace-rtl.min.css')}}" />

<link rel="stylesheet" href="{{asset('assets/css/chosen.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-timepicker.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/daterangepicker.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/ImageSelect.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-datetimepicker.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/style.css')}}">


<section class="contenido">
    <div class="container">
        <div class="row">




            @if( $tipo =='2' )
            @include('frontend.empresacreate')
            @endif
            @if( $tipo  =='1' )
            @include('frontend.usuariocreate')
            @endif



        </div>
    </div>
</section><!-- /contenido -->


@endsection
<script>
$(document).ready(function () {

    $('html, body').animate({scrollTop: 494}, 600);
    });

</script>