@extends('layouts.frontend')
@section('title', 'Actualidade')
@section('contenido')
<a id="inicio"></a>
<section class="contenido">
    <div class="container">
        <div class="row ">
            <div class="col-md-12">
                <h1 class="title color-primary"><strong>{{__('backend.Actualidade')}}</strong></h1>

            </div>
            <div class="col-md-8 content-oferta">
                <ul class="ofertas noticias">
                    @if (count($actualidades) > 0)

                    <label class="control-label no-padding-right">{{__('frontend.Cantidad_por_pagina')}} </label>

                    <select id="pageSize" class="page_limit pgination-select" onchange="">  
                        <option value="10" @if($pageSize == 10) selected @endif>10</option>
                        <option value="30" @if($pageSize == 30) selected @endif>30</option>
                        <option value="50" @if($pageSize == 50) selected @endif>50</option>
                    </select> 

                    {{__('frontend.Presentando')}} {{ $actualidades->firstItem() }} - {{ $actualidades->lastItem() }} {{__('frontend.de')}} {{ $actualidades->total() }} 
                    <div class="clearfix"></div>
                    @foreach($actualidades as $actualidade)
                    <li class="item">
                        <a class="oferta-ficha" href="{{$actualidade->getSlug()}}">
                            <article class="feature">
                                <div class="col-md-12">

                                    @if ($actualidade->imagen_destacada != '')
                                    <div class="">
                                        <img class="img-responsive" src="/public/noticias/{{$actualidade->imagen_destacada}}">
                                    </div>
                                    @endif

                                    <div class="c">
                                        <p class="date"><i>{{$actualidade->getFechaLetras()}}</i></p>

                                    </div>
                                    @if($actualidade->titulo)
                                    <h2>{{$actualidade->titulo}}</h2> 
                                    @endif
<!--                                    @if($actualidade->entradilla)
                                    <h3>{{$actualidade->entradilla}}</h3> 
                                    @endif-->
                                    @if($actualidade->antetitulo)                                   
                                    <p>{{$actualidade->antetitulo}}</p> 
                                    @endif

                                    <div class="">

                                        {!! substr(strip_tags($actualidade->noticia),0,400)!!}[...]
                                    </div>
                                </div>


                            </article>
                        </a>
                    </li>
                    @endforeach

                    @endif

                </ul>
                <div class="clearfix"></div>

                {{ $actualidades->appends(['ano' => $ano])->links() }}


            </div>


            <div class="col-md-4 ">
                <div class="search">
                    <form method="GET" action="{{url('/actualidade')}}"  accept-charset="UTF-8" >
                        <input type="text" name="search" id = "search" placeholder="{{__('frontend.Buscar')}}">
                        <button type="submit" class="btn btn-default">{{__('frontend.Buscar')}}</button>
                    </form>
                    <div class="clearfix"></div>    
                </div>
                <div class="filter filtro">
                    <h3>{{__('frontend.Filtrar_oferta_por_anyo')}}</h3>
                    <ul>

                        @foreach($anos as $ano)
                        <li>
                            <a href="{{url('actualidade?ano='.$ano)}}">{{$ano}}</a>
                        </li>
                        @endforeach


                    </ul>
                </div>
            </div>
        </div>
    </div>
</section><!-- /contenido -->
@endsection