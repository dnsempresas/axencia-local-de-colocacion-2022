
<div id="crearuser" class="row">
    <div class="col-md-12">
        <h3 class="border-silver-top"><strong>{{__('frontend.Registrar_Perfil_de_Usuario')}}</strong> </h3>

        <div class="card">
            <div class="card-header">
                <i class="icon-note"></i>
            </div>

            @if ($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i></h4>
                @foreach ($errors->all() as $error)
                <p>{{ $error }}</p>
                @endforeach
            </div>
            @endif
            @if(Session::has('alert-success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
            </div>
            @endif

            <div class="card-body">
                <form method="POST" novalidate action="{{route('usuarios.store') }}"  accept-charset="UTF-8" class="form-horizontal" id="userForm" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div  class="row">
                        <div id="izquierda" class="col-md-6">

                            <div class="panel panel-default">
                                <div class="panel-heading">{{__('frontend.Datos_de_Acceso')}} </div>
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right">{{__('frontend.Tipo_de_Usuario')}}</label>
                                        <div class="col-sm-8" style="margin-top:10px">
                                            <span > 
                                                {{__('frontend.Demandante_de_empleo')}}
                                                <input type="hidden" name="tiporole" id="tiporole" value="1"/>
                                            </span>

                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1">{{__('frontend.Correo_electronico')}}</label>
                                        <div class="col-sm-6">
                                            <input type="email" name="email" id="email" placeholder="{{__('frontend.Correo_electronico')}}" class="col-xs-10 col-sm-5" value="{{old('email')}}"  />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1">{{__('frontend.Contrasenya')}}</label>
                                        <div class="col-sm-6">
                                            <input required type="password" name="password" id="password" placeholder="{{__('frontend.Contrasenya')}}" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>
                                    <!--                                    <div class="form-group">
                                                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">{{__('frontend.Repita_Contrasenya')}}</label>
                                                                            <div class="col-sm-6">
                                                                                <input type="password" name="rpassword" id="rpassword" placeholder="{{__('frontend.Repita_Contrasenya')}}" class="col-xs-10 col-sm-5" />
                                                                            </div>
                                                                        </div>-->


                                </div>
                            </div>



                            <div id="panel_datospersonales" class="panel panel-default" >
                                <div class="panel-heading">{{__('frontend.Datos_Personales')}} </div>
                                <div class="panel-body">

                                    <div class="form-group">

                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Nombre')}} </label>

                                        <div class="col-sm-8">
                                            <input type="text" name="name" id="name" placeholder="{{__('frontend.Nombre')}}"  value="{{old('name')}}"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Apellidos')}} </label>
                                        <div class="col-sm-8">
                                            <input type="text" name="last_name" id="last_name" placeholder="{{__('frontend.Apellidos')}}" value="{{old('last_name')}}" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.DNI-NIE')}} </label>
                                        <div class="col-sm-8">
                                            <input type="text" name="cif" id="cif" placeholder="{{__('frontend.DNI-NIE')}}" value="{{old('cif')}}"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">{{__('frontend.Genero')}}</label>
                                        <div class="col-sm-8">


                                            <select required name="genero" id="genero">
                                                <option value="">{{__('frontend.Seleccione')}}...</option>
                                                @foreach($generos as $genero)
                                                <option {{old('genero')==$genero->id?'selected':''}}  value="{{$genero->id}}">{{ session('lang')=='es' ?  $genero->nombre :  $genero->nome }} </option>
                                                @endforeach
                                            </select>
                                            <div class="input-group">
                                                <input required style="margin-top: 5px; display: {{old('genero')==4?'block':'none'}};" class="form-control" id="other_genero" name="other_genero" type="text" placeholder="Especifique Genero..." value="{{old('other_genero')}}"/>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">{{__('frontend.Fecha_de_Nacimiento')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input class="form-control date-picker" id="birth" name="birth" type="text" data-date-format="dd/mm/YYYY" value="{{date('d-m-Y', strtotime(old('birth')))}}"/>
                                            </div>	
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <span class="col-sm-3 control-label no-padding-right">Foto de Perfil</span>
                                        <div class="col-sm-4">
                                            <img id="imgSalida" width="120px" height="140px" src="" />
                                            <input id="foto_perfil" type="file" name="foto_perfil" accept="image/x-png,image/gif,image/jpeg" style="color:gray !important; border: 0">
                                            <a class="removeimg" id ="removeimg" href="" title='Borrar'style="display: none" ><span class='glyphicon glyphicon-remove' style='color:red; '></span></a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-8 control-label no-padding-right">{{__('frontend.Discapacidad_reconocida_igual_o_superior_al_33')}}</label>


                                        <div class="radio">
                                            <label>
                                                <input name="disability" type="checkbox" class="ace" <?php if (old('disability') == 1) echo 'checked'; ?> />
                                                <span class="lbl"></span>
                                            </label>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>


                        <div id="derecha" class="col-md-6">
                            <div id="panel_direccion" class="panel panel-default">
                                <div class="panel-heading"> {{__('frontend.Direccion')}}  </div>
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">{{__('frontend.Provincia')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group" style="width: 100%">
                                                <select name="province" id="province" required>
                                                    <option selected="selected" value="">{{__('frontend.Seleccione')}}...</option>';


                                                    @foreach($provinces as $province)
                                                    <option  value="{{$province->pk}}" <?php if ((old('province') == $province->pk) || $province->pk==15 ) echo "selected='selected'"; ?> >{{$province->name}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="input-group">
                                                    <input style="margin-top: 5px;display: none;" class="form-control" id="other_province" name="other_province" type="text" placeholder="Especifique Provincia..."/>
                                                </div>
                                            </div>	
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label id="labelconcello" class="col-sm-4 control-label no-padding-right">{{__('frontend.Ayuntamiento')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group" style="width: 100%">
                                                <select   name="concello_chosen" id="concello_chosen" data-placeholder="Seleccione..." required>
                                                    <option selected value="">     {{__('frontend.Seleccione')}}...</option>
                                                    <option value="otro">{{__('frontend.Otro')}}</option>
                                                </select>

                                                <input  style="margin-top: 5px;display: none;" class="form-control" id="other_concello" name="other_concello" type="text" placeholder="{{__('Especifique')}}..."/>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Direccion')}}</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="address" id="address" placeholder="{{__('frontend.Direccion')}}" value="{{old('address')}}" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Codigo_Postal')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="number" name="postal_code" id="postal_code" placeholder="{{__('frontend.Codigo_Postal')}}" value="{{old('postal_code')}}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1">{{__('frontend.Telefono')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="number" name="phone" id="phone" maxlength=9 placeholder="{{__('frontend.Telefono')}}"  value="{{old('phone')}}"/>
                                            </div>
                                        </div>
                                    </div>	
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1">{{__('frontend.Otro_Telefono')}}</label>
                                        <div class="col-sm-8">
                                            <input type="number" name="other_phone" id="other_phone" maxlength=9 placeholder="{{__('frontend.Otro_Telefono')}}"  value="{{old('other_phone')}}"/>
                                        </div>
                                    </div>



                                </div>
                            </div>
                            <div id="panel_politica" class="panel panel-default">
                                <div class="panel-heading"> {{__('Politica de Privacidad y Proteccion de Datos')}}  </div>
                                <div class="panel-body">

                                    @include('privacidad/privacidad')

                                    <div class="form-group">
                                        <div class="radio">
                                            <label>
                                                <input id="aceptapolitica" name="aceptapolitica" type="checkbox" class="ace"  />
                                                <span class="lbl popup-button"> Acepto a política de protección de datos </span>
                                            </label>
                                        </div>
                                    </div>



                                </div>
                            </div>




                        </div>

                        <div id="divGuardar" class="col-md-12">
                            <div class="col-md-1">
                                <div class="form-group">
                                    <button id="guardar" disabled="disabled"  type="submit" class="btn btn-success" name="Guardar">{{__('frontend.Guardar')}}</button> 
                                </div>
                            </div>



                        </div>
                    </div>
                </form>						

            </div>
        </div>
    </div>
</div>
