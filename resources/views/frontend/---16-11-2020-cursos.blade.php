@extends('layouts.frontend')
@section('contenido')
@section('title', 'Formación')
<a id="inicio"></a>
<section class="contenido">
    <div class="container">
        <div class="row ">
            <div class="col-md-12">
                <h1 class="title color-primary"><strong>{{__('Formación')}}</strong></h1>

            </div>
            <div class="col-md-8 content-oferta">
                <ul class="ofertas">
                    @if (count($cursos) > 0)
                    
                    <label class="control-label no-padding-right">{{__('frontend.Cantidad_por_pagina')}} </label>

                    <select id="pageSize" class="page_limit pgination-select" onchange="">  
                        <option value="10" @if($pageSize == 10) selected @endif>10</option>
                        <option value="30" @if($pageSize == 30) selected @endif>30</option>
                        <option value="50" @if($pageSize == 50) selected @endif>50</option>
                    </select> 
                    
                     Presentando {{ $cursos->firstItem() }} - {{ $cursos->lastItem() }} de {{ $cursos->total() }} 

                    @foreach($cursos as $curso)
                    <li class="item">
                        <a class="oferta-ficha" href="{{$curso->getSlug()}}">
                            <article class="feature">
                                <div class="col-md-12">
                                    @if ($curso->imagen != '')
                                    <div class="col-md-3">
                                        <img class="img-responsive" src="/public/cursos/{{$curso->imagen}}">
                                    </div>
                                    <div class="col-md-9">
                                        <p class="date"><i>{{$curso->getFechaLetras()}}</i></p>
                                        @if ($curso->tipoCurso->id > 1)
                                        <span class="title">{{$curso->tipoCurso->nome}} </span>
                                        @endif
                                        @if ($curso->tipoCurso->id > 1 && $curso->pertenece_fie==1 )
                                        </br>
                                        @endif
                                        @if ($curso->pertenece_fie==1)
                                        <span class="title">{{__('frontend.Perteneceafie')}}</span>
                                        @endif
                                        <div class="text-conte">
                                            <h2>{{$curso->nombre}}</h2>
                                            <p>{{$curso->lugar_celebracion}} </p>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-md-12">
                                        <p class="date"><i>{{$curso->getFechaLetras()}}</i></p>
                                        @if ($curso->tipoCurso->id > 1)
                                        <span class="title">{{$curso->tipoCurso->nome}} </span>
                                        @endif
                                        @if ($curso->tipoCurso->id > 1 && $curso->pertenece_fie==1 )
                                        </br>
                                        @endif
                                        @if ($curso->pertenece_fie==1)
                                        <span class="title">{{__('frontend.Perteneceafie')}}</span>
                                        @endif
                                        <div class="text-conte">
                                            <h2>{{$curso->nombre}}</h2>
                                            <p>{{$curso->lugar_celebracion}} </p>
                                        </div>
                                    </div>

                                    @endif
                                </div>
                            </article>
                        </a>
                    </li>
                    @endforeach
                    
                    @else 
                    
                     <img class="img-responsive" src="{{asset('assets/front/img/nohaycursos.jpg')}}">

                    @endif

                </ul>

                {{ $cursos->links() }}
            </div>


            <div class="col-md-4 ">
                 <div class="p-libres">
                        <button class="btn btn-naranja">{{__('frontend.Cursos_con_plazas_libres')}}</button>
                 </div>
                <div class="filtro f-cursos">
                     <form method="GET" action="/formacion"  accept-charset="UTF-8" >
                    <h3>{{__('backend.Filtrar_los_cursos')}}</h3>
                    <h4>{{__('frontend.Selecciona_fecha_de_inicio_del_curso')}}</h4>
                    <div class="fechas">
                    <label> {{__('frontend.Desde')}}:</label><input id="fecha_inicio" name="fecha_inicio" type="text">   
                    </div> 
                    <div class="fechas">
                    <label> {{__('frontend.Hasta')}}::</label><input id="fecha_fin" name="fecha_fin" type="text">   
                    </div>             
                    <div align="center">	
                        <button class="btn btn-default">{{__('frontend.Filtrar')}}</button>
                    </div>
                     </form>
                </div>
        </div>
    </div>
    </div>
</section><!-- /contenido -->
@endsection