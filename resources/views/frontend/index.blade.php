@extends('layouts.frontend')
@section('title', 'Axencia Local de Colocación')
@section('contenido')
@if(Session::has('alert-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
</div>
@endif
@if(Session::has('alert-danger'))
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i> {{ Session::get('alert-danger') }}</h4>
</div>
@endif
<section class="contenido">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h3 class="border-red-top"><strong>{{__('backend.Oferta')}}</strong> {{__('backend.deEmprego')}}</h3>


                @if (count($ofertas) > 0)

                @foreach($ofertas as $oferta)
                
                 <?php
                    $mostrar = 1;
                    if ($oferta->limitar_inscripcion > 0) {
                        $postulados = $oferta->postulados()->get()->count();
                        if ($postulados >= $oferta->maximo_inscripciones) {
                            $mostrar = 0;
                        }
                    }
                    if ($mostrar == 1) {
                        ?>

                <article >
                   
                    <a class="oferta-ficha" href="<?php if ($oferta->tipogestion==1) echo $oferta->urltercero; else echo $oferta->getSlug(); ?>" target="<?php if ($oferta->tipogestion==1) echo 'blank_'; else echo ''; ?>">
                    
                        <div class="content content-red">
                            @if ($oferta->imagen<>'')
                            <img class="img-responsive" src="/public/offers/{{$oferta->imagen}}">
                            </br>
                            @endif
                            <p class="date">{{$oferta->getFechaLetras()}}</p>
                            </br>
                            @if ($oferta->tipoOferta->id > 1)
                            <span class="title">{{$oferta->tipoOferta->nome}} </span>
                            @endif
                            @if ($oferta->tipoOferta->id > 1 && $oferta->pertenece_fie==1 )
                            </br>
                            @endif

                            @if ($oferta->pertenece_fie==1)
                            <span class="title">{{__('backend.perteneceafie')}}</span>
                            @endif
                            <h4>{{$oferta->requested_job}}</h4>
                            <p> @if ($oferta->concello) {{  $oferta->concello->name }} @endif </p>
                        </div>
                    </a>
                </article>
                    <?php } ?>



                @endforeach
                <a class="btn btn-primary" href="{{url('/ofertas')}}">Ver mas ofertas... </a>

                @else
                <div>
                    <h1>{{__('frontend.No_se_han_publicado_ofertas_de_empleo')}} </h1>
                </div>
                @endif




            </div>
            <div class="col-md-4">
                <h3 class="border-silver-top"><strong>{{__('frontend.FORMACION')}}</strong> </h3>

                @if (count($cursos) > 0)

                @foreach($cursos as $curso)

                <a href="{{$curso->getSlug()}}"><article >

                        @if ($curso->imagen<>'')
                        <img class="img-responsive" src="/public/cursos/{{$curso->imagen}}">
                        @endif



                        <div class="content content-silver">
                            <p class="date">{{$curso->getFechaLetras()}}</p>
                            @if ($curso->tipoCurso->id > 1)
                            <span class="title">{{$curso->tipoCurso->nome}} </span>
                            @endif
                            @if ($curso->tipoCurso->id == 2 && $curso->pertenece_fie==1) 
                            </br>
                            @endif

                            @if ($curso->pertenece_fie==1)
                            <span class="title">{{__('frontend.Perteneceafie')}}</span>
                            @endif
                            <h4>{{$curso->nombre}}</h4>
                            <p>{{$curso->lugar_celebracion}}</p>

                            
                            
                        </div>




                    </article></a>




                @endforeach
                <a class="btn btn-primary" href="{{url('/formacion')}}">Ver mas formación... </a>

                @else
                <div>
					<a href="https://www.cextec.com/ciclo-de-xornadas/bricomart"><img class="img-responsive" src="{{asset('assets/front/img/nohaycursos.jpg')}}"></a>
                   <!-- <a href="assets/front/img/integrado.png" width="900" height="1011" target="_blank"><img class="img-responsive" src="{{asset('assets/front/img/nohaycursos.jpg')}}"></a>-->
                <!--   <h1> {{__('frontend.No_se_han_publicado_cursos_de_formacion')}} </h1>-->
                    
                </div>
                @endif



            </div>

            <div class="col-md-4 ">


                <a href="https://xulio2019.gal/#whatsapp" target="_blank">						
                    <img class="" style="width: auto; height: auto; margin-bottom:10px" src="{{asset('assets/front/img/whatsapp-ma.png')}}">
                </a>

                <article >
                    <div class="img-des">
                        <a href="{{url('/boletins')}}" style="margin-top:10px">
                            <img class="img-responsive" src="{{asset('assets/front/img/oferta.jpg')}}"></a>
                            <div class="text-info">
                               <a href="{{url('/boletins')}}" style="margin-top:10px"> <span class="title">{{strtoupper(__('backend.Boletines'))}} </br>{{strtoupper(__('backend.deEmprego'))}} </span>
                                <img class="img-responsive" src="{{asset('assets/front/img/icono-descargas.png')}}" width="40"></a>
                            </div>
                    </div>
                  <!--    <div class="content content-gold">
                        <p class="date"> {{__('backend.Descargaboletin')}}:</p>
                      <form>
                            <input type="text" class="l-g" placeholder="{{__('backend.EscribeEmail')}}">
                            <button class="btn btn-gold">{{__('frontend.Suscribir')}} </button>
                        </form>
                    </div>-->
                </article>

                <article >
                    <a href="https://www.ponteenmarcha.gal/" target="_blank">						
                       </a> <img class="img-responsive" src="{{asset('assets/front/img/ponteenmarcha.png')}}">
                    </a>
                </article>
                
                @if (count($banners) > 0)

                @foreach($banners as $banner)
                
                @if($banner->enlace<>'')

                <a href="{{$banner->enlace}} " target="_blank">
                    <article >

                        @if ($banner->imagen_destacada <>'')
                        <img class="img-responsive" src="/public/banners/{{$banner->imagen_destacada}}">
                        @endif


                    </article></a>
                @else
                
                    <article>

                        @if ($banner->imagen_destacada <>'')
                        <img class="img-responsive" src="/public/banners/{{$banner->imagen_destacada}}">
                        @endif


                    </article>
                
                @endif

                @endforeach
               
                @else
               
                @endif
                
                
            </div>



        </div>


    </div>

</section><!-- /contenido -->
<!-- Default bootstrap modal example -->




@endsection
