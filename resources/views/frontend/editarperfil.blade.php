@extends('layouts.frontend')
@section('contenido')

<link rel="stylesheet" href="{{asset('assets/css/ace.min.css')}}" class="ace-main-stylesheet" id="main-ace-style" />
<link rel="stylesheet" href="{{asset('assets/css/ace-skins.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/ace-rtl.min.css')}}" />

<link rel="stylesheet" href="{{asset('assets/css/chosen.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-timepicker.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/daterangepicker.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/ImageSelect.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-datetimepicker.min.css')}}" />


<section class="contenido">
    <div class="container">
        <div class="row">



            @if (Auth::check())
            @if( Auth::user()->role_id =='2' )
            @include('frontend.empresaedit')
            @endif
            @if( Auth::user()->role_id =='1' or Auth::user()->role_id =='3' )
            @include('frontend.usuarioedit')
            @endif
            @endif


        </div>
    </div>
</section><!-- /contenido -->

@endsection
