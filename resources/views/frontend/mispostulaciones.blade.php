@extends('layouts.frontend')
@section('contenido')
@section('title', 'Mis Inscriciones a Cursos')

<section class="contenido">
    <div class="container">
        <div class="row ">
            <div class="col-md-12">
                <h1 class="title color-primary"><strong>{{__('backend.MisPostulaciones')}}</strong></h1>

            </div>
            <div class="col-md-10 content-oferta">
                <ul class="ofertas">
                    @if (count($ofertas) > 0)


                    <label class="control-label no-padding-right">{{__('frontend.Mostrando_Ofertas_por_pagina')}} </label>

                    <select id="pageSize" class="page_limit pgination-select" onchange=""> {{__('frontend.Mostrar_Por_Pagina')}}: 
                        <option value="10" @if($pageSize == 10) selected @endif>10</option>
                        <option value="30" @if($pageSize == 30) selected @endif>30</option>
                        <option value="50" @if($pageSize == 50) selected @endif>50</option>
                    </select>

                    @foreach($ofertas as $oferta)
                    <li class="item">
                        <a class="oferta-ficha" href="{{url('/oferta-ficha/'.$oferta->id)}}">
                            <article class="feature">
                                <div class="col-md-12">
                                    
                                    <div class="col-md-12">
                                        <p class="date"><i>{{$oferta->getFechaLetras()}}</i></p>
                                        @if ($oferta->tipoOferta->id > 1)
                                        <span class="title">{{$oferta->tipoOferta->nome}} </span>
                                        @endif
                                        @if ($oferta->tipoOferta->id > 1 && $oferta->pertenece_fie==1 )
                                        </br>
                                        @endif
                                        @if ($oferta->pertenece_fie==1)
                                        <span class="title">{{__('backend.perteneceafie')}}</span>
                                        @endif
                                        <div class="text-conte">
                                            <h2>{{$oferta->requested_job}}</h2>
                                            <p>{{$oferta->locality}} </p>
                                            <h3>INSCRITO EL {{date('d/m/Y', strtotime($oferta->pivot->created))}} <h3>
                                        </div>
                                    </div>

                                </div>
                            </article>
                        </a>
                    </li>
                    @endforeach

                    @endif


                </ul>

                {{ $ofertas->links() }}
            </div>
            
        </div>
    </div>
</section><!-- /contenido -->
@endsection