@extends('layouts.frontend')
@section('contenido')
@section('title', 'Boletins')
<a id="inicio"></a>
<section class="contenido">
    <div class="container">
        <div class="row ">
            <div class="col-md-12">
                <h1 class="title color-primary"><strong>{{__('Boletines')}}</strong></h1>

            </div>
            <div class="col-md-8 content-oferta">
                <ul class="ofertas">
                    @if (count($boletines) > 0)
                    
                    <label class="control-label no-padding-right">{{__('frontend.Cantidad_por_pagina')}} </label>

                    <select id="pageSize" class="page_limit pgination-select" onchange="">  
                        <option value="10" @if($pageSize == 10) selected @endif>10</option>
                        <option value="30" @if($pageSize == 30) selected @endif>30</option>
                        <option value="50" @if($pageSize == 50) selected @endif>50</option>
                    </select> 
                    
                    {{__('frontend.Presentando')}}  {{ $boletines->firstItem() }} - {{ $boletines->lastItem() }} {{__('frontend.de')}} {{ $boletines->total() }} 

                    @foreach($boletines as $boletin)
                    <li class="item">
                        <a class="oferta-ficha" href="{{url('/public'.$boletin->link)}}">
                            <article class="feature">
                                <div class="col-md-12">
                                    
                                    <div class="col-md-12">
                                        <h3>{{ session('lang')=='es' ?  $boletin->nombre :  $boletin->nome }}</h3>
                                        
                                    </div>

                                   
                                </div>
                            </article>
                        </a>
                    </li>
                    @endforeach

                    @endif

                </ul>

                {{ $boletines->links() }}
            </div>



        </div>
    </div>
</section><!-- /contenido -->
@endsection