@extends('layouts.frontend')
@section('contenido')
@section('title', 'Mis Inscriciones a Ofertas')
<section class="contenido">
    <div class="container">
        <div class="row ">

            <div class="col-md-12 content-oferta" >

                <div class="col-md-12 content-oferta" style="height:300px;  overflow-y : scroll; ">
                    <div class="col-md-12">

                    </div>
                    @if (count($ofertas) > 0)
                    <div class=" col-sm-12 form-group" >
                        <div class="col-sm-4 custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="checkFiltrarFecha" checked>
                            <label class="custom-control-label" for="checkFiltrarFecha">{{__('Filtrar por Data de Inscripción')}}</label>
                        </div>

                        <div class="col-sm-6" id ="fitrofechas" >
                            <span class="col-sm-6 input-icon">
                                <input style="color:black" type="text"  id="date_min" placeholder="{{__('backend.Desde')}}"/>
                            </span>

                            <span class="col-sm-6 input-icon input-icon-right">
                                <input style="color:black"  type="text"  id="date_max" placeholder="{{__('backend.Hasta')}}"/>
                            </span>
                        </div>
                        <div class="col-sm-2" >
                            <span class="col-sm-4 input-icon input-icon-right">
                                <button id="generarCertificadoOfertas" type="button" class="btn btn-primary" title="Certificado de inscrición na Axencia coas ofertas en que se inscribiu">{{__('Certificado')}}</button>

                            </span>
                        </div>

                    </div>
                    <div>
                        <h3 class=" color-primary" ><strong>{{__('Inscripciones Ofertas')}}</strong></h3>

                    </div>
                    <table id="usersoffersf" data-user="{{Auth::user()->pk}}" class="table table-striped table-bordered table-hover">
                        <thead class="thead-light">

                            <tr>
                                <th ></th>
                                <th style ="width:30%">{{__('Nome Oferta')}}</th>
                                <th style ="width:30%">{{__('Data Incripcion')}}</th>
                                <th style ="width:30%">{{__('Data Inicio')}}</th>
                                <th style ="width:30%"> <a id="certificadotodas" href="" title="Certificado de inscrición" onclick=""><span class='glyphicon glyphicon-check'></span></a>  </th>
    <!--                            <th style ="width:10%">{{__('Estatus')}}</th>-->

                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ofertas as $oferta)


                            <tr data-user="{{Auth::user()->pk}}" id="{{$oferta->id}}">
                                 <td> </td>
                                <td><b><a href="{{$oferta->getSlug()}}" target="_blank"> {{$oferta->requested_job }} </b></td>
                                <td>{{date('d/m/Y H:m:s', strtotime($oferta->pivot->created))}} </td>
                                <td>{{date('d/m/Y H:m:s', strtotime($oferta->start_date))}} </td>
                                <td><a href="{{url('user/' . Auth::user()->pk . '/certificadooferta/'.$oferta->id)}}" target="_blank" title="Certificado de inscrición"><span class='glyphicon glyphicon-check'></span></a> </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                    @else 
                    <th colspan="3"><h3 class="title color-primary" ><strong>{{__('Sen rexistro de ofertas')}}</strong></h3></th>
                    @endif 

                </div>



                <div class="col-md-12 content-oferta" style="height:300px;  overflow-y : scroll; margin-top: 30px">
                    <div class="col-md-12">


                    </div>
                    @if (count($cursos) > 0)
                    <table id="" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead class="thead-light">
                            <tr>
                                <th colspan="3"><h3 class="title color-primary" ><strong>{{__('Inscripciones Cursos')}}</strong></h3></th>

                            </tr>
                            <tr>
                                <th style ="width:30%">{{__('Nome Curso')}}</th>
                                <th style ="width:30%">{{__('Data Incripcion')}}</th>
                                <th style ="width:30%">{{__('Data Inicio')}}</th>
    <!--                            <th style ="width:10%">{{__('Estatus')}}</th>-->

                            </tr>
                        </thead>
                        <tbody>
                            @foreach($cursos as $curso)
                            <tr>
                                <td><b><a href="{{url('/curso-ficha/'.$curso->id)}}" target="_blank"> {{$curso->nombre }} </b></td>
                                <td>{{date('d/m/Y H:m:s', strtotime($curso->pivot->fechainscripcion))}} </td>
                                <td>{{date('d/m/Y H:m:s', strtotime($curso->fecha_inicio))}} </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                    @else 
                    <th colspan="3"><h3 class="title color-primary" ><strong>{{__('Sen rexistro de curso')}}</strong></h3></th>
                    @endif 

                </div>


            </div>
        </div>
    </div>

    <div class="modal " tabindex="-1" role="dialog" id="modalofertasinscitas" aria-hidden="true" >
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Inscripciones realizadas por el usuario</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                </div>

                <div class="col-md-12" style ="padding-bottom: 10px"> 


                </div>


                <div class="modal-body" id="bodymodalofertasinscitas">

                </div>
                <div class="modal-footer">
                    <button id="generarCertificadoOfertas" type="button" class="btn btn-primary" data-dismiss="modal">{{__('Certificado')}}</button>

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('users.cerrar')}}</button>
                </div>
            </div>
        </div>
    </div>



</section><!-- /contenido -->
@endsection