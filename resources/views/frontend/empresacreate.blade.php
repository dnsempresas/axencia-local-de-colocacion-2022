
<div id="crearuser" class="row">
    <div class="col-md-12">
        <h3 class="border-silver-top"><strong>{{__('frontend.Registrar_Perfil_de_Usuario')}}</strong> </h3>

        <div class="card">
            <div class="card-header">
                <i class="icon-note"></i>
            </div>

            @if ($errors->any())
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i></h4>
                @foreach ($errors->all() as $error)
                <p>{{ $error }}</p>
                @endforeach
            </div>
            @endif
            @if(Session::has('alert-success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
            </div>
            @endif

            <div class="card-body">
                <form method="POST"  action="{{route('usuarios.store') }}"  accept-charset="UTF-8" class="form-horizontal" id="enterpriseForm" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div  class="row">
                        <div id="izquierda" class="col-md-6">

                            <div class="panel panel-default">
                                <div class="panel-heading">{{__('frontend.Datos_de_Acceso')}} </div>
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right">{{__('frontend.Tipo_de_usuario')}}</label>
                                        <div class="col-sm-8" style="margin-top:10px">
                                            <span > 
                                                {{__('frontend.Empresa')}}
                                                <input type="hidden" name="tiporole" id="tiporole" value="2"/>
                                            </span>

                                        </div>

                                    </div>


                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Correo_electronico')}}</label>
                                        <div class="col-sm-6">
                                            <input type="email" name="email" id="email" placeholder="{{__('frontend.Correo_electronico')}}" class="col-xs-10 col-sm-5" value="{{old('email')}}"  required />
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Contrasenya')}}</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="password" id="password" placeholder="{{__('frontend.Contrasenya')}}" class="col-xs-10 col-sm-5" required/>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Repita_Contrasenya')}}</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="rpassword" id="rpassword" placeholder="{{__('frontend.Repita_Contrasenya')}}" class="col-xs-10 col-sm-5" required/>
                                        </div>
                                    </div>


                                </div>
                            </div>

                            <div id="panel_datosempresariales" class="panel panel-default"  >
                                <div class="panel-heading">Datos de Empresa</div>
                                <div class="panel-body">

                                    <div class="form-group required">

                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Nombre_de_la_Empresa')}} </label>

                                        <div class="col-sm-8 ">
                                            <div class="input-group col-sm-12">
                                                <input type="text" name="name" id="name" placeholder="{{__('frontend.Nombre_de_la_Empresa')}}" value="{{old('name')}}"  required/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Denominacion_Social')}} </label>
                                        <div class="col-sm-8">
                                            <div class="input-group col-sm-12">
                                                <input type="text" name="last_name" id="last_name" placeholder="{{__('frontend.Denominacion_Social')}}" value="{{old('last_name')}}" required/>
                                            </div>
                                        </div>
                                    </div>
<!--                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">{{__('frontend.Fecha_de_Constitucion')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input class="form-control date-picker" id="birth" name="birth" type="text" data-date-format="dd/mm/YYYY" value="{{date('d/m/Y', strtotime(old('birth')))}}"/>
                                            </div>	
                                        </div>

                                    </div>-->

                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.NIF_CIF')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" name="cif" id="cif" placeholder="{{__('frontend.NIF_CIF')}}" value="" required />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>




                        </div>
                        <div id="derecha" class="col-md-6">




                            <div id="panel_direccion" class="panel panel-default">
                                <div class="panel-heading"> {{__('frontend.Direccion')}}  </div>
                                <div class="panel-body">

                                     <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right"> {{__('frontend.Provincia')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group" style="width:100%">
                                                <select class="" required name="province" id="province" required>
                                                    <option selected value="">{{__('frontend.Seleccione')}}...</option>
                                                    @foreach($provinces as $province)
                                                    <option  value="{{$province->pk}}" <?php if ((old('province') == $province->pk) || $province->pk==15 ) echo "selected='selected'"; ?>>{{$province->name}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="input-group">
                                                    <input style="margin-top: 5px;display: none;" class="form-control" id="other_province" name="other_province" type="text" placeholder="Especifique Provincia..."/>
                                                </div>
                                            </div>	
                                        </div>

                                    </div>

                                    <div class="form-group required">
                                        <label id="labelconcello" class="col-sm-4 control-label no-padding-right"> {{__('frontend.Ayuntamiento')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group" style="width:100%">
                                                <select class=""  name="concello_chosen" id="concello_chosen" data-placeholder="{{__('frontend.Seleccione_Ayuntamiento')}}..." required>
                                                    <option selected value="">     {{__('frontend.Seleccione')}}...</option>
                                                    <option value="otro">{{__('frontend.Otro')}}</option>
                                                </select>

                                                <input  style="margin-top: 5px;display: none;" class="form-control" id="other_concello" name="other_concello" type="text" placeholder="{{__('frontend.Especifique_Ayuntamiento')}}..."/>
                                            </div>
                                        </div>
                                    </div>




                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Direccion')}}</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="address" id="address" placeholder="{{__('frontend.Direccion')}}" value="{{old('address')}}" required/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Codigo_Postal')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="number" name="postal_code" id="postal_code" placeholder="{{__('frontend.Codigo_Postal')}}" value="{{old('postal_code')}}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Telefono')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="number" name="phone" id="phone" maxlength=9 placeholder="{{__('frontend.Telefono')}}"  value="{{old('phone')}}" required/>
                                            </div>
                                        </div>
                                    </div>	
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1">{{__('frontend.Otro_Telefono')}}</label>
                                        <div class="col-sm-8">
                                            <input type="number" name="other_phone" id="other_phone" maxlength=9 placeholder="{{__('frontend.Otro_Telefono')}}"  value="{{old('other_phone')}}"/>
                                        </div>
                                    </div>



                                </div>
                            </div>

                            <div id="panel_personadecontacto" class="panel panel-default" >
                                <div class="panel-heading"> {{__('frontend.Persona_de_contacto')}} </div>
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> {{__('frontend.Persona_de_contacto')}}</label>
                                        <div class="col-sm-8">
                                            <div class="input-group col-sm-12">
                                                <input type="text" name="contacto" id="contacto" placeholder="{{__('frontend.Persona_de_contacto')}}" value="{{old('contacto')}}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right" for="form-field-1">{{__('frontend.Cargo')}} </label>
                                        <div class="col-sm-8">
                                            <div class="input-group col-sm-12">
                                                <input type="text" name="cargocontacto" id="cargocontacto" placeholder="{{__('frontend.Cargo_Persona_de_contacto')}}" value="{{old('cargocontacto')}}" />
                                            </div>
                                        </div>
                                    </div>




                                </div>
                            </div>
                            
                            <div id="panel_logo" class="panel panel-default">
                                <div class="panel-heading"> {{__('Logo')}}  </div>
                                <div class="panel-body">
                            
                            <div class="form-group">
                                        <span class="col-sm-3 control-label no-padding-right">Logo</span>
                                        <div class="col-sm-4">
                                            <img id="imgSalida" width="120px" height="140px" src="" />
                                            <input id="foto_perfil" type="file" name="foto_perfil" accept="image/*"  >
                                            <a class="removeimg" id ="removeimg" href="" title='Borrar'style="display: none" ><span class='glyphicon glyphicon-remove' style='color:red; '></span></a>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            


                        </div>
                          <div class="col-md-12">
                        <div id="panel_politica" class="panel panel-default">
                                <div class="panel-heading"> {{__('Politica de Privacidad y Proteccion de Datos')}}  </div>
                                <div class="panel-body">

                                    @include('privacidad/privacidad')

                                    <div class="form-group">
                                        <div class="radio">
                                            <label>
                                                <input id="aceptapolitica" name="aceptapolitica" type="checkbox" class="ace" />
                                                <span class="lbl popup-button"> Acepto a política de protección de datos </span>
                                            </label>
                                        </div>
                                    </div>



                                </div>
                            </div>
                        
                         </div>

                    <div id="divGuardar"  class="col-md-12">
                        <div class="form-group">
                            <button id="guardar" disabled="disabled"  type="submit" class="btn btn-success" name="Guardar">{{__('frontend.Guardar')}}</button> 
                        </div>
                    </div>
            </div>
            </form>						

        </div>
    </div>
</div>
</div>
 
