@extends('layouts.frontend')
@section('title', 'Error Interno')
@section('robots','noindex')
@section('contenido')
<a id="inicio"></a>
<section class="contenido">
    <div class="container">
        <div class="row ">
            <div class="col-md-12">
                <h1 class="title color-primary"><strong>{{__('Error Interno (500)')}}</strong></h1>

            </div>
        </div>
    </div>
</section><!-- /contenido -->
@endsection