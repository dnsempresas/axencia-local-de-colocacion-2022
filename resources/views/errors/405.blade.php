@extends('layouts.frontend')
@section('robots','noindex')
@section('title', 'Pagina no encontrada')
@section('contenido')
<a id="inicio"></a>
<section class="contenido">
    <div class="container">
        <div class="row ">
            <div class="col-md-12">
                <h1 class="title color-primary"><strong>{{__('frontend.Nofound')}}</strong></h1>

            </div>
        </div>
    </div>
</section><!-- /contenido -->
@endsection