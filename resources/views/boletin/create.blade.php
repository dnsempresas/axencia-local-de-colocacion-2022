@extends('layouts.app')

@section('titulo_pantalla')

@endsection

@section('content')
@if ($errors->any())
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-ban"></i>{{__('boletin.errors')}}</h4>
    @foreach ($errors->all() as $error)
    <p>{{ $error }}</p>
    @endforeach
</div>
@endif
@if(Session::has('alert-danger'))
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-danger"></i> {{ Session::get('alert-danger') }}</h4>
</div>
@endif
@if(Session::has('alert-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
</div>
@endif
<form style="padding: 30px;" id="boletinForm" method="POST" action="{{url('/panel/boletines')}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <div>
                        {{__('boletin.new_boletin_label')}}
                    </div>
                </div>
                <div class="card-body">
                    <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                        <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('boletin.name')}}</legend>
                        <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                            <div role="group" class="input-group">								
                                <input type="text" class="form-control" name="nombre" required>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                        <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('boletin.name_gl')}}</legend>
                        <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                            <div role="group" class="input-group">								
                                <input type="text" class="form-control" name="nome" required>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                        <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('boletin.Boletin')}}</legend>
                        <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                            <div role="group" class="input-group">								
                               <input id="boletin" type="file" name="boletin" accept="application/pdf,image/jpeg" required>
                            </div>
                        </div>
                    </fieldset>
                    
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary custom-btn" name="signup" value="Sign up">{{__('boletin.save')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>	
</div>
@endsection
