﻿<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-48530224-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-48530224-1');
        </script>
        <base href="./">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <meta name="description" content="Axencia Local de Colocación">
        <meta name="robots" content="@yield('robots','index')">
        <meta name="description" content="@yield('description','Axencia Local de Colocación')">
        <meta name="author" content="Dnsempresas">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title') - Axencia Local de Colocación</title>

        <!---->


        <link rel="stylesheet" href="{{asset('assets/css/chosen.min.css')}}" />

        <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/bootstrap.min.css')}}">

        <link rel="stylesheet" href="{{asset('assets/front/css/font-awesome.min.css')}}">

        <link rel="stylesheet" href="{{asset('assets/css/bootstrap-datepicker3.min.css')}}" />

        <link href="{{asset('assets/css/sellang.css')}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/style.css')}}">


        <style>

            .bg-dark{
                background-color: rgb(119, 119, 119) !important;
            }

            .form-group input[type=email], .form-group input[type=url], .form-group input[type=search], .form-group input[type=tel], .form-group input[type=color], .form-group input[type=text], .form-group input[type=password], .form-group input[type=datetime], .form-group input[type=datetime-local], .form-group input[type=date], .form-group input[type=month], .form-group input[type=time], .form-group input[type=week], .form-group input[type=number], .form-group select, .form-group textarea{
                width: 95%;
                max-width: 95%;
            }

            .card {
                margin-bottom: 1.5rem;
                margin-top: 1.5rem;
            }

            .card-body {
                -ms-flex: 1 1 auto;
                flex: 1 1 auto;
                padding: 2.25rem;
            }

            .popup{
                display: none;
                width: 150px;
                height: 100px;
                background-color: yellow;
            }
            .popup-button:hover .popup{
                display: block;
            }


        </style>

        <link href="{{asset('assets/css/responsive.dataTables.css')}}" rel="stylesheet" />
        <link href="{{asset('assets/css/buttons.dataTables.min.css')}}" rel="stylesheet" />
        <link href="{{asset('assets/css/select.dataTables.min.css')}}" rel="stylesheet" />
        <link href="{{asset('assets/css/jquery.dataTables.min.css')}}" rel="stylesheet" />


    </head>
    <body>


        <header>
            <div class="header-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-xs-6" style="padding-top: 10px">

                            <div class="lang navbar-brand" style="height: 25px; padding: 2px 12px;">
                                <li class="dropdown " style="list-style:none;">
                                    <?php
                                    if (!empty(session('lang'))) {
                                        if (session('lang') == 'gl') {
                                            ?>
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <img id="imgidiomaselect" src="{{asset('assets/images/gl_22.png')}}" alt="gl" title="Gallego"></a> 
                                        <?php } else { ?>
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <img id="imgidiomaselect" src="{{asset('assets/images/es_22.png')}}" alt="es" title="Español"></a> 
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <img id="imgidiomaselect" src="{{asset('assets/images/gl_22.png')}}" alt="gl" title="Gallego"></a> 
                                    <?php } ?>


                                    <ul class="dropdown-menu" role="menu">
                                        <li class="idiomaselected" data-img="{{asset('assets/images/es_22.png')}}"><a href="{{ url('lang', ['es']) }}"> <img  src="{{asset('assets/images/es_22.png')}}" alt="es" title="Español"></a></li>
                                        <li class="idiomaselected" data-img="{{asset('assets/images/gl_22.png')}}"><a href="{{ url('lang', ['gl']) }}"> <img  src="{{asset('assets/images/gl_22.png')}}" alt="gl" title="Galego"></a></li>
                                    </ul>
                                </li>
                            </div>

                            <a href="tel" class="tel"><i class="fa fa-phone"></i> <span>981 543 060</span></a>
                            <a href="mailto:axencialocaldecolocacion@santiagodecompostela.org" target="top" class="email"><i class="fa fa-envelope"></i> <span>axencialocaldecolocacion@santiagodecompostela.org</span></a>
                            <span class="location" data-toggle="modal" data-target="#Mlocation" style="cursor:pointer"><i class="fi flaticon-marcador-de-posicion"></i></span>
                            <!-- Modal -->
                            <div id="Mlocation" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d11693.666100444138!2d-8.516573!3d42.885158!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xac240e9b947de9ac!2sCERSIA+Empresa!5e0!3m2!1ses!2sve!4v1557810376383!5m2!1ses!2sve" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">{{__('frontend.Cerrar')}}</button>
                                        </div>
                                    </div>

                                </div>
                            </div>



                        </div>
                        <div class="col-md-4 col-xs-6 redes-top" style="padding-top: 10px">
                            <ul class="social ">
                                <li><a href="https://www.facebook.com/AxenciaLocaldeColocacion/"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://twitter.com/EmpregoSantiago"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/axencia-local-de-colocaci%C3%B3n-de-santiago-de-compostela/"><i class="fa fa-linkedin"></i></a></li>
                                <!--<li><a href="#"><i class="fa fa-google-plus"></i></a></li>-->
                                <li><a href="https://www.youtube.com/channel/UCdsjyUzIWwMfNX735vto7ew"><i class="fa fa-youtube"></i></a></li>
                                <!--<<li><a href="#" class="b-active"><i class="fa fa-podcast"></i></a></li>-->
                                <div class="clearfix"></div>



                            </ul>
                        </div>

                        @if (Auth::check())
                        <div class="col-md-4 col-xs-4 login" >
                            <ul class="nav navbar-nav navbar-right">     
                                <li class="dropdown">
                                    <a style="z-index:1000" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-hover="dropdown">

                                        @if(Auth::user()->foto)
                                        <img style="height: 35px; margin: 0 10px; border-radius: 50em;" class="img-avatar" src="{{asset('/public/storage/fotos/'. Auth::user()->foto)}}" alt="{{Auth::user()->email}}"> 
                                        @else
                                        <img style="height: 35px; margin: 0 10px; border-radius: 50em;" class="img-avatar" src="<?php
                                        if (Auth::user()->genero_fk == 1) {
                                            echo asset('assets/images/mujer.png');
                                        } else {
                                            if (Auth::user()->genero_fk == 2) {
                                                echo asset('assets/images/hombre.png');
                                            } else {
                                                echo asset('assets/images/otro.png');
                                            }
                                        }
                                        ?>" alt="{{Auth::user()->email}}"> 
                                        @endif
                                        <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu dropdown-menu-right dropdown-yellow">

                                        @if( Auth::user()->role_id =='1' )
                                        <li class="dropdown-item" style="color: #fff; padding: 10px; border-bottom: solid 1px #FFF;"> Mi perfil {{ Auth::user()->name }}
                                        <li class="dropdown-divider"></li>
                                            <ul style="list-style: none; padding-left: 0px; margin-left: 0px; padding-top: 5px; border-bottom: solid 1px #fff">
                                                <li style="padding:5px 20px;"><a href="{{url('/editarperfil')}}">{{__('backend.MiPerfil')}}</a></li>
                                                <!--<li><a href="{{url('/user/postulaciones')}}">{{__('backend.MisPostulaciones')}}</a></li>-->
                                                <li style="padding:5px 20px;"><a href="{{url('/user/misinscripciones', Auth::user()->pk)}}">{{__('backend.MisPostulaciones')}}</a></li>
                                                <li style="padding:5px 20px;"><a href="{{url('user/' . Auth::user()->pk . '/certificado')}}" target="_blank">{{__('backend.CertificadoInscripcion')}}</a></li>
                                                <!--<li><a id ='getCertificadoOferta' class='getcertificadoOfertas' href="{{url('user/' . Auth::user()->pk . '/certificadoOfertas')}}" target='_blank' title='Certificado de inscrición na Axencia coas ofertas en que se inscribiu' data-value='{{ Auth::user()->pk }}' >{{__('Certificado de inscrición na Axencia coas ofertas')}}</a></li>-->


                                            </ul>
                                        </li>
                                        @elseif( Auth::user()->role_id =='3' )
                                        <li class="dropdown-item"><a href="{{url('/admin')}}">Ir al Panel</a></li>


                                        @elseif( Auth::user()->role_id =='2' )
                                        <li class="dropdown-item"><a href="{{url('/empresas/perfil')}}">{{__('backend.editMiPerfil')}} </a></li>
                                        <li class="dropdown-item"><a href="{{url('/empresas/ofertas')}}">Mis Ofertas Publicadas</a></li>


                                        @endif

                                        @if( Auth::user()->role_id =='1' )

                                        <li class="dropdown-item" id="darsedebaja"><a href="">{{__('backend.DarseBaja')}}</a></li>
                                        @endif


                                        <li class="dropdown-item"><a href="{{url('/user/logout')}}">{{__('backend.PecharSesion')}}</a></li>


                                    </ul>
                                </li>

                            </ul>
                        </div>
                        @endif

                    </div>			
                </div>
            </div>
            <div class="branding">
                <div class="container">
                    <div class="row">
                        <!-- Static navbar -->
                        <nav class="navbar navbar-default">
                            <div class="container-fluid">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand" style="margin-left: -25px;" href="{{url('/')}}"><img class="logo" src="{{asset('assets/front/img/logo-axencia-local-colocacion.png')}}"></a>
                                </div>
                                <div id="navbar" class="navbar-collapse collapse">
                                    <ul class="nav navbar-nav navbar-right">
                                        <li ><a href="{{url('/axencia')}}" >{{__('backend.Agencia')}}</a></li>

                                        <li><a href="{{url('/ofertas')}}">Ofertas</a></li>




                                        <li><a  href="{{url('/formacion')}}">Formación</a></li>
                                        <li><a  href="{{url('/actualidade')}}">{{__('backend.Actualidade')}}</a></li>
                                        <li class="dropdown">
                                            <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-hover="dropdown">Programas <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <!--                                                <li><a href="#">Foro 2018</a>
                                                                                                    <ul>
                                                                                                        <li><a href="#">Presentación</a></li>
                                                                                                        <li><a href="#">Programas</a></li>
                                                                                                        <li><a href="#">Datas e horarios</a></li>
                                                                                                    </ul>
                                                                                                </li>-->
                                                <li><a href="{{url('/talentia')}}">TALENTIA 2018</a></li>
                                                <li><a href="{{url('/pactoporempleo')}}">PACTO POLO EMPREGO</a></li>
                                                <li><a href="{{url('/recursos')}}">Recursos</a></li>
                                            </ul>
                                        </li>

                                        <li>
                                            <form id="frmBucarOferta" method="get" action="{{url('ofertas/buscar')}}" accept-charset="UTF-8" class="form-inline">
                                               <!--<div class="input-group">
                                                    {{ csrf_field() }}
                                                    <input required  type="text" class="form-control" placeholder="Buscar ofertas..." aria-label="Buscar ofertas..."/>
                                                    <button type="submit" id="btnBuscarOferta" class="input-group-addon">
                                                        <i class="glyphicon glyphicon-search"></i>
                                                    </button>
                                                </div>-->
                                               {{ csrf_field() }}
                                                <input required name="buscarOferta" id="buscarOferta" class="form-control mr-sm-2" type="text" placeholder="Buscar Oferta..." aria-label="Buscar ofertas...">
                                                <button class="btn btn-outline-success my-2 my-sm-0" id="btnBuscarOferta" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                            </form>
                                        </li>


                                    </ul>




                                </div><!--/.nav-collapse -->
                            </div><!--/.container-fluid -->
                        </nav>
                    </div>
                </div>
            </div>	
        </header><!--/.header -->

        <!-- slider -->
        <section class="slider">
            <!-- Carousel 100% Fullscreen -->
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>

                </ol>
                <div class="carousel-inner">

                    <div title="Talentia Summit" class="item bg  active" onclick="window.open('https://www.cextec.com/ciclo-de-xornadas/bricomart', 'blank');" style="background-image: url({{asset('assets/front/img/slides/axencia-4.jpg')}}); background-position: center top; cursor:pointer;">
                        <div class="container">
                        </div>
                    </div>


                    <div title="Talentia Summit" class="item bg" onclick="window.open('https://www.talentiasummit.com/gl', 'blank');" style="cursor:pointer; ">
                        <img src="assets/front/img/slides/axencia2.jpg" class="img-responsive" alt="" style="height: inherit; width: 100%; padding-top: 55px">
                        <div class="container">
                        </div>
                    </div>


                    <div class="item bg">
                        <img src="assets/front/img/slides/slide1.png" class="img-responsive" alt="" style="height: inherit; width: 100%; padding-top: 55px">
                        <div class="container">
                            <div class="carousel-caption">
                                <h1>{{__('backend.Si_buscas_trabajo')}} </br> {{__('backend.PodemosAyudarte')}}.</h1>
                                <p><a class="btn btn-lg btn-primary" href="/axencia" role="button"> {{__('backend.Conocelaagencia')}} </a></p>
                            </div>
                        </div>
                    </div>


                    <div title="" class="item bg" >
                        <img src="assets/front/img/slides/slide_empresas.jpg" class="img-responsive" alt="" style="height: inherit; width: 100%; padding-top: 55px">
                        <div class="container">
                            <div class="carousel-caption">
                                <h1>{{__('backend.Se_tes_unha_oferta')}} </br> {{__('backend.PodemosAyudarte')}}.</h1>
                            </div>
                        </div>

                        <!--                    <div class="item bg " style="background-image: url({{asset('assets/front/img/slides/slide1.png')}}); background-position: center top;">
                                                <div class="container">
                                                    <div class="carousel-caption">
                                                        <h1>{{__('backend.Si_buscas_trabajo')}} </br> {{__('backend.PodemosAyudarte')}}.</h1>
                                                        <p><a class="btn btn-lg btn-primary" href="/axencia" role="button"> {{__('backend.Conocelaagencia')}} </a></p>
                                                    </div>
                                                </div>
                                            </div>
                        -->


                    </div>
                </div>
                <!-- /end Carousel 100% Fullscreen -->
        </section><!-- /slider -->
        <!--login-->

        <section class="login">

            <div class="container">
                @if (!Auth::check())
                <div class="row" id="barra_login">
                    <div class="col-md-8 form-login">
                        <div class="col-md-4 no-padding-md" ><label>{{__('backend.ya_eres_usuario')}}:</label></div>
                        <div class="col-md-3" >
                            <input id = "front_user_login" class="l-g" type="text" placeholder="Usuario/a">
                        </div>
                        <div class="col-md-3 ">
                            <input id="front_user_pass" class="l-g" type="password" placeholder="Contrasinal">
                        </div>
                        <div class="col-md-2">				
                            <button id = "blogin" class="btn btn-primary">Entrar</button>
                        </div>
                    </div>
                    <div class=" col-md-4 form-singin">
                        <div class="col-md-12 no-padding-md" >
                            <label> {{__('backend.Eres_nuevo')}}</label>
                            <button class="btn btn-default" id="bregistrarperfil">INSCRÍBETE AQUÍ</button>
                        </div>
                    </div>
                </div>
                @endif





            </div>


            <a id="irAnclaContenido" href="">
                <div class="arrow">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </a>

        </section><!-- / login-->

        <!-- Marketing messaging and featurettes
        ================================================== -->
        <!-- Wrap the rest of the page in another container to center all the content. -->

        <div class="contenido" id="Contenido">

            @yield('contenido')

        </div><!-- /.container -->
        <!-- pre-footer-->
        <section class="pre-footer">
            <div class="container">
                <div class="row">
                    <div class=col-md-4>
                        <ul class="m-footer">
                            <li><a href="{{url('/axencia')}}" >{{__('backend.Agencia')}}</a></li>
                            <li><a href="{{url('/ofertas')}}">Ofertas</a></li>
                            <li><a href="{{url('/formacion')}}">Formacion</a></li>

                        </ul>
                        <ul class="m-footer">
                            <li><a href="{{url('/accesibilidad')}}">{{__('Accesibilidade')}}</a></li>
                            <li><a href="{{url('/avisolegal')}}">Aviso Legal</a></li>
                            <!--                            <li><a href="#">Contacto</a></li>												-->
                        </ul>					
                    </div>
                    <div class=col-md-4>
                        <p><strong>{{__('frontend.Contacto')}}</strong></br>
                            Edificio Administrativo CERSIA</br>
                            Rúa Alcalde Raimundo López Pol, s/n, </br>
                            Santiago de Compostela</br></br>		

                            <a href="#"><strong>Tlf. 981 543 060</strong></a></br>
                            <strong>Fax 981 542 407</strong>
                        </p>

                        <ul class="social">

                            <li><a href="https://www.facebook.com/AxenciaLocaldeColocacion/"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/EmpregoSantiago"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.linkedin.com/company/axencia-local-de-colocaci%C3%B3n-de-santiago-de-compostela/"><i class="fa fa-linkedin"></i></a></li>
                            <!--<li><a href="#"><i class="fa fa-google-plus"></i></a></li>-->
                            <li><a href="https://www.youtube.com/channel/UCdsjyUzIWwMfNX735vto7ew"><i class="fa fa-youtube"></i></a></li>
<!--                            <li><a href="#" class=""><i class="fa fa-podcast"></i></a></li>-->
                        </ul>
                    </div>
                    <a href="http://www.santiagodecompostela.org/">
                        <div class="col-md-4 log">
                            <img src="{{asset('assets/front/img/logo-concello-santiago-compostela.png')}}">
                        </div>
                    </a>
                </div>
            </div>
        </section><!-- /prefooter-->
        <footer>
            <p>© 2019. Concello de Santiago. Concellaría de Igualdade, Desenvolvemento Económico e Turismo</p>
        </footer>

        <div class="modal " tabindex="-1" role="dialog" id="modalconsolidar" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Se han detectado varios perfiles para <span id="name"></span> <span id="surname"></span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row mensaje" style="margin-left:10px">
                            <h6> Debe seleccionar un sólo perfil válido, los demas perfiles quedarán inhabilitados de forma definitiva</h6>
                            <h6> Todo el historial quedará consolidado en el perfil elegido</h6>
                        </div>
                        <div class="row lista" style="margin-left:10px" >


                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="consolidar" type="button" class="btn btn-primary">Guardar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal " tabindex="-1" role="dialog" id="modalseltipo" aria-hidden="true">
            <div class="modal-dialog " role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Tipo de usuario</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row" style="margin-left:10px">
                            <h6> Debe seleccionar un tipo de usuario</h6>
                        </div>
                        <div class="row " style="margin-left:10px" >

                            <form method="GET" novalidate action="/registrarperfil"  accept-charset="UTF-8" class="form-horizontal" id="userRegitrar" >
                                {{ csrf_field() }}

                                <div  class="row">
                                    <div id="izquierda" class="col-md-11">

                                        <div class="panel panel-default">
                                            <div class="panel-heading">Datos de Acceso </div>
                                            <div class="panel-body">

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right">Tipo de Usuario</label>
                                                    <div class="col-sm-8" style="margin-top:10px">
                                                        <select name="tipo" id="tipo">
                                                            <option  value="1">Demandante de emprego</option>

                                                            <option  value="2">Empresa</option>

                                                        </select>

                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <button  type="submit" class="btn btn-primary">Aceptar</button>
                            </form>



                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal modal-md" tabindex="-1" role="dialog" id="modalloginincorrecto" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6>Inicio de Sesión</h6>
                    </div>
                    <div class="modal-body">
                        <div class="row" style="margin-left:10px">

                            {{__('frontend.loginincorrecto')}}... <br>
                            {{__('frontend.loginincorrecto2')}} <br>
                        </div>
                        <div class="row " style="margin-right:10px; text-align: right" >
                            {{__('frontend.Olvidosucontrasena2')}} <a  href="{{ url('password/reset')}}" style="color:#337ab7; text-decoration: underline"> ¿{{__('frontend.Olvidosucontrasena')}}? </a>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal modal-md" tabindex="-1" role="dialog" id="modaldarsedebaja" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>{{__('frontend.darbaja')}}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" style="margin-left:10px; margin-right:10px">
                            <span> Advertencia: </span>
                            <span> {{__('frontend.advertencia')}} </span> <br>
                            <span> {{__('frontend.nodeshacer')}} </span> <br>
                            <span> {{__('frontend.estaseguro')}} </span>

                        </div>
                        <div class="row" style="margin-left:10px; margin-right:10px">
                            <div class="">
                                <input type="checkbox" id="checkdarbaja" name="checkdarbaja" style="margin-top: 10px;" unchecked /> 
                                <span class="lbl"> {{__('frontend.siquiero')}} </span>

                            </div>
                        </div>
                        <div  id="divdardebaja" class="row" style="margin-left:10px" >
                            <span id="botondardebaja" class="btn btn-primary"> Aceptar </span>
                        </div>


                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>



        <div class="modal modal-md" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="modalpostularse">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Confirmar Inscrición</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            @if (Auth::check())

                            @if (Auth::user()->cvprincipal!='')


                            <label class="col-sm-12 control-label no-padding-right">{{__('Seleccione un Curriculum')}}</label>
                            <div class="col-sm-12">
                                <select name="curriculo" id="curriculo" required style="margin-right: 0;margin-left: 0;width: 100%;">

                                    <option  value="{{Auth::user()->cvprincipal}}">{{Auth::user()->cvprincipal}}</option>

                                    @if (Auth::user()->cvsecundario!='')
                                    <option  value="{{Auth::user()->cvsecundario}}">{{Auth::user()->cvsecundario}}</option>
                                    @endif


                                </select>
                            </div>
                            @else 
                            <div class="col-sm-12"> Debe subir un curriculo </div>
                            @endif

                            @else 
                            <div class="form-group"> Debe Iniciar Sesión

                            </div>

                            @endif

                        </div>
                    </div>
                    <div class="modal-footer">
                        @if (Auth::check())
                        @if (!empty($oferta))
                        @if (Auth::user()->cvprincipal!='') 
                        <button type="button" class="btn btn-default" id="modal-btn-si" data-id="{{$oferta->id}}">Inscribirme</button>
                        @endif
                        @endif
                        @endif
                        <button type="button" class="btn btn-secondary" id="modal-btn-no" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal modal-md" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="modalretiraroferta">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Confirmar Retiro Oferta</h4>
                    </div>
                    <div class="modal-body">
                        <p>{{__('frontend.retirar_oferta')}}</p>
                    </div>
                    <div class="modal-footer">
                        @if (!empty($oferta))
                        <a class="btn btn-default" href="{{route('desinscribir',$oferta->id)}}">Confirmar Retiro</a>
                        @endif
                       <button type="button" class="btn btn-secondary" id="modal-btn-no" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>


        <script>
            var assets = "{{ URL::asset('/assets/') }}";

        </script>

        <script type="text/javascript" src="{{asset('assets/front/js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/front/js/bootstrap.min.js')}}"></script> 
        <script type="text/javascript" src="{{asset('assets/front/js/main.js')}}"></script>

 <!--<script type="text/javascript" src="{{asset('assets/js/bootstrap-tag.min.js')}}"></script>-->
        <script src="{{asset('assets/js/jquery.validate.min.js')}}"></script>
        <script src="{{asset('assets/js/jquery-additional-methods.min.js')}}"></script>



        <script src="{{asset('assets/js/chosen.jquery.min.js')}}"></script>
        <script src="{{asset('assets/js/spinbox.min.js')}}"></script>
        <script src="{{asset('assets/js/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{asset('assets/js/bootstrap-datepicker.gl.min.js')}}"></script>
        <script src="{{asset('assets/js/bootstrap-timepicker.min.js')}}"></script>
        <script src="{{asset('assets/js/moment.min.js')}}"></script>
        <script src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
        <script src="{{asset('assets/js/bootstrap-datetimepicker.min.js')}}"></script>
        <script src="{{asset('assets/js/ImageSelect.jquery.js')}}"></script>    

        <script src="{{asset('assets/js/autosize.min.js')}}"></script>
        <script src="{{asset('assets/js/jquery.inputlimiter.min.js')}}"></script>
        <script src="{{asset('assets/js/jquery.maskedinput.min.js')}}"></script>
        <script src="{{asset('assets/js/bootstrap-tag.min.js')}}"></script>



        <script src="{{asset('assets/js/ace-elements.min.js')}}"></script>
        <script src="{{asset('assets/js/ace.min.js')}}"></script>
        <script src="{{ asset('assets/js/custom-validators.js') }}"></script>

        <script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>

        <script src="{{ asset('assets/js/users.js') }}"></script>

        <script src="{{asset('assets/front/js/frontend.js')}}"></script>

        <script >

            jQuery(document).ready(function ($) {

              

               $('#irAnclaContenido').click(function (event) {
                    event.preventDefault();
                    $('html, body').animate({scrollTop: 494}, 600);
                });




            });
            
              

        </script>



    </body>
</html>