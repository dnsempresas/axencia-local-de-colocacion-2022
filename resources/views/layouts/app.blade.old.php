<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v2.0.0
* @link https://coreui.io
* Copyright (c) 2018 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->

<html lang="en">
    <head>
        <base href="./">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <meta name="description" content="Axencia local de colocacion">
        <meta name="author" content="DenmaSoft Inc">
        <meta name="keyword" content="Axencia, Colocacion, Santiago">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>Axencia Local de Colocacion</title>


        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}" />


        <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}" />

        <!--        <link rel="stylesheet" href="{{asset('assets/css/icomoon.style.css')}}" />-->

        <!--        <link rel="stylesheet" href="{{asset('assets/fonts/panel/flaticon.css')}}" />
                <link href="{{ asset('public/node_modules/@coreui/icons/css/coreui-icons.min.css') }}" rel="stylesheet">
                <link href="{{ asset('public/node_modules/flag-icon-css/css/flag-icon.min.css') }}" rel="stylesheet">
                
                <link href="{{ asset('public/node_modules/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
                <link href="{{ asset('public/node_modules/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">-->

        <link href="{{ asset('src/css/style.css') }}" rel="stylesheet">

        <link rel="stylesheet" href="{{asset('assets/css/ace.min.css')}}" class="ace-main-stylesheet" id="main-ace-style" />
        <link rel="stylesheet" href="{{asset('assets/css/ace-skins.min.css')}}" />
        <link rel="stylesheet" href="{{asset('assets/css/ace-rtl.min.css')}}" />




        <link href="{{ asset('src/vendors/pace-progress/css/pace.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('assets/css/chosen.min.css')}}" />
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap-datepicker3.min.css')}}" />
        <!--<link rel="stylesheet" href="{{asset('assets/css/jquery.inputpicker.css')}}" />-->
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap-timepicker.min.css')}}" />
        <link rel="stylesheet" href="{{asset('assets/css/daterangepicker.min.css')}}" />
        <link rel="stylesheet" href="{{asset('assets/css/ImageSelect.css')}}" />
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap-datetimepicker.min.css')}}" />
        <link rel="stylesheet" href="{{asset('assets/css/fonts.googleapis.com.css')}}" />





        <link href="{{asset('assets/css/responsive.dataTables.css')}}" rel="stylesheet" />
        <link href="{{asset('assets/css/buttons.dataTables.min.css')}}" rel="stylesheet" />
        <link href="{{asset('assets/css/select.dataTables.min.css')}}" rel="stylesheet" />
        <link href="{{asset('assets/css/jquery.dataTables.min.css')}}" rel="stylesheet" />


        <style>
            body{
                font-size: 14px !important;
            }
            .form-group input[type=email], .form-group input[type=url], .form-group input[type=search], .form-group input[type=tel], .form-group input[type=color], .form-group input[type=text], .form-group input[type=password], .form-group input[type=datetime], .form-group input[type=datetime-local], .form-group input[type=date], .form-group input[type=month], .form-group input[type=time], .form-group input[type=week], .form-group input[type=number], .form-group select, .form-group textarea{
                width: 100%;
                max-width: 100%;
            }
            .btn{
                font-size: 14px;
            }
            .help-block {
                display: block;
                margin-top: 5px;
                margin-bottom: 10px;
                color: #D68273;
            }
            .table{
                width: 100% !important;
            }
            .dataTables_wrapper{
                display: block !important;
            }

            .pagination {
                display: inline-block !important;
            }
            .card {
                margin-bottom: 1.5rem;
                margin-top: 1.5rem;
            }
            select.form-control:not([size]):not([multiple]) {
                height: 25px;
                font-size: 12px;
            }
            .toolbar{
                margin-bottom: 1.5rem;
            }
            .card-body {
                -ms-flex: 1 1 auto;
                flex: 1 1 auto;
                padding: 2.25rem;
            }
            legend{
                border-bottom: none;
            }
            .custom-btn{
                float: right;
                margin-right: 12px;
            }
            #contract_type{
                margin-right: 12px;
                margin-left: 12px;
                width: 96%;
            }

            .table-scrooll-fija{
                display: block;
                overflow-x: auto; 
            }

            .table-scrooll-fija thead tr,.table-scrooll-fija tbody tr{
                position: relative;
            }
            .table-scrooll-fija tr td:last-child,.table-scrooll-fija tr th:last-child{
                position: absolute;
                background-color: white;
                z-index: 100;
                width: 150px;
                right:22px;
                height: inherit;
            }

            .ult-col{
                padding-right: 150px!important;
            }
            .form-group.required .control-label:before {
                content:"*" !important;
                color:red !important;
            }
            .app-body{
                margin-top: 0px;
            } 

            .dataTables_scrollHeadInner{
                width:max-content !important;
            }
            .dataTables_scrollHeadInner table{
                width:max-content !important;
            }
            .chosen-container {
                font-size: 14px !important;
            }




            @media only screen and (max-width:991px){
                .navbar .navbar-nav .dropdown-menu{
                    position:initial
                }
            }
        </style>
    </head>
    <body class="app pace-done">
        <header class="">


            <nav class="navbar navbar-expand-lg navbar-light bg-light">

                <div class="lang navbar-brand">
                    <li class="dropdown " style="list-style:none;">
                        <?php
                        if (!empty(session('lang'))) {
                            if (session('lang') == 'gl') {
                                ?>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <img id="imgidiomaselect" src="{{asset('assets/images/gl_22.png')}}" alt="gl" title="Gallego"></a> 
                            <?php } else { ?>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <img id="imgidiomaselect" src="{{asset('assets/images/es_22.png')}}" alt="es" title="Español"></a> 
                                <?php
                            }
                        } else {
                            ?>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <img id="imgidiomaselect" src="{{asset('assets/images/gl_22.png')}}" alt="gl" title="Gallego"></a> 
                        <?php } ?>


                        <ul class="dropdown-menu" role="menu">
                            <li class="idiomaselected" data-img="{{asset('assets/images/es_22.png')}}"><a href="{{ url('lang', ['es']) }}"> <img  src="{{asset('assets/images/es_22.png')}}" alt="es" title="Español"></a></li>
                            <li class="idiomaselected" data-img="{{asset('assets/images/gl_22.png')}}"><a href="{{ url('lang', ['gl']) }}"> <img  src="{{asset('assets/images/gl_22.png')}}" alt="gl" title="Gallego"></a></li>
                        </ul>
                    </li>
                </div>

                <a class="navbar-brand" href="{{url('/')}}" style="width: 300px !important">
                    <img class="navbar-brand-full" src="{{ asset('assets/images/logo-gl.png') }}" width="100%" height="100%" alt="Agencia local de colocacion">
                </a>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>



                <div class="collapse navbar-collapse" id="navbarTogglerDemo02">

                    <ul class="nav navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="/panel/admin">{{__('menu.dashboard')}}</a>
                        </li>

                        <li class="nav-item ">
                            <a class="nav-link" href="/panel/empresas">{{__('Empresas')}}</a>
                        </li>



                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">{{__('backend.Ofertas')}}</a>
                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-caret dropdown-close">
                                <li><a  href="/panel/ofertas">{{__('backend.Ofertas')}}</a></li>
                                <li><a  href="/panel/contratos">{{__('Tipo de Contratos')}}</a></li>
                                <li><a  href="/panel/disponibilidades">{{__('Tipos Disponibilidad')}}</a></li>
                                
                                 <li><a  href="{{url('panel/admin/ofertas/sectores')}}">{{__('Sectores')}}</a></li>
                                 
                                <!--<li><a  href="{{url('panel/admin/ofertas/seleccion')}}">{{__('Proceso de Selección')}}</a></li>-->



                            </ul>
                        </li>






                        <li class="nav-item px-2">
                            <a class="nav-link" href="/panel/usuarios">{{__('Usuarios')}}</a>
                        </li>

                        <li class="nav-item px-2 dropdown-modal">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                {{__('backend.Estudios')}}
                            </a>
                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a  href="/panel/estudios">{{__('backend.Estudios')}}</a>
                                </li>
                                <li>
                                    <a href="/panel/especialidades">{{__('backend.Especialidades')}}</a>
                                </li>
                                <li class="divider"></li>
                            </ul>
                        </li>

                        <li class="nav-item px-2 dropdown-modal">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                {{__('Formación')}}
                            </a>
                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a  href="/panel/cursos">{{__('Cursos')}}</a>
                                </li>
                                <li>
                                    <a href="/panel/tipocursos">{{__('Tipos de Cursos')}}</a>
                                </li>
                                <li class="divider"></li>
                            </ul>
                        </li>

                        <li class="nav-item px-2 dropdown-modal">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                {{__('backend.Noticias')}}
                            </a>
                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a  href="/panel/noticias">{{__('backend.Noticias')}}</a>
                                </li>
                                <li>
                                    <a href="/panel/categorias">{{__('backend.CategoriasNews')}}</a>
                                </li>
                                <li class="divider"></li>
                            </ul>
                        </li>


                        <li class="nav-item px-2 dropdown-modal">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                {{__('backend.Otros')}}
                            </a>
                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">

                                <li>
                                    <a href="/panel/puestos">{{__('backend.Puestos')}}</a>
                                </li>
                                <li>
                                    <a  href="/panel/sectores">{{__('Sectores')}}</a>
                                </li>
                                <li>
                                    <a href="/panel/permisos-conducir">{{__('backend.Permisos_de_Conducir')}}</a>
                                </li>
                                <li>
                                    <a href="/panel/generos">{{__('backend.Generos')}}</a>
                                </li>

                                <li>
                                    <a href="/panel/auditorias">{{__('Auditorias')}}</a>
                                </li>
                                <li>
                                    <a href="/panel/boletines">{{__('Boletíns')}}</a>
                                </li>
                                <!--                                <li>
                                                                    <a href="/panel/boletines2">{{__('Boletíns2')}}</a>
                                                                </li>-->
                                <li>
                                    <a href="/panel/banners">{{__('Banners')}}</a>
                                </li>
                                
<!--                                 <li>
                                    <a href="{{url('/panel/sepe-estadisticas')}}">{{__('Estadísticas SEPE')}}</a>
                                </li>-->


                                <li class="divider"></li>


                            </ul>
                        </li>


                        <li class="nav-item px-2 dropdown-modal">
                            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-bell-o"></i>
                                <span id="cantidadnotificaciones" class="badge badge-pill badge-danger">...</span>
                            </a>
                            <div id= "listNotificaciones" class="dropdown-menu dropdown-menu-right">


                            </div>
                        </li>

                    </ul>


                    <ul class="nav navbar-nav ml-auto navbar-brand">



                        <li class="nav-item dropdown">
                            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">

                                @if(Auth::user()->foto)
                                <img style="height: 35px; margin: 0 10px; border-radius: 50em;" class="img-avatar" src="{{asset('/public/storage/fotos/'. Auth::user()->foto)}}" alt="{{Auth::user()->email}}"> 
                                @else
                                <img style="height: 35px; margin: 0 10px; border-radius: 50em;" class="img-avatar" src="<?php
                                if (Auth::user()->genero_fk == 1) {
                                    echo asset('assets/images/mujer.png');
                                } else {
                                    if (Auth::user()->genero_fk == 2) {
                                        echo asset('assets/images/hombre.png');
                                    } else {
                                        echo asset('assets/images/otro.png');
                                    }
                                }
                                ?>" alt="{{Auth::user()->email}}"> 
                                @endif
                                <span class="caret"></span></a>



                            <div class=" dropdown-menu dropdown-menu-right">

                                <a class="dropdown-item" href="{{ url('user/logout') }}">
                                    <i class="fa fa-lock"></i> Pechar sesión 
                                </a>
                            </div>
                        </li>
                    </ul>

                </div>
            </nav>

        </header>
        <div class="app-body">

            <main class="main">
                <!-- Breadcrumb-->
                @yield('breadcrumb')

                <div class="container-fluid">
                    <div class="animated fadeIn">
                        <!-- /.row-->
                        @yield('content')
                    </div>
                </div>
            </main>

        </div>
        <footer class="app-footer">
            <!--<div>
              <a href="https://coreui.io">CoreUI</a>
              <span>&copy; 2018 creativeLabs.</span>
            </div>-->
            <div class="ml-auto">
                <span>Powered by</span>
                <a href="https://coreui.io">DnsEmpresas</a>
            </div>
        </footer>

    </body>


<!--    <script src="{{asset('node_modules/jquery/dist/jquery.min.js')}}"></script>-->
    <script src="{{asset('assets/js/jquery-2.1.4.min.js')}}"></script>
    <script src="{{asset('public/node_modules/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('public/node_modules/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('public/node_modules/pace-progress/pace.min.js')}}"></script>
    <script src="{{asset('public/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js')}}"></script>
    <script src="{{asset('public/node_modules/@coreui/coreui/dist/js/coreui.min.js')}}"></script>
    <!-- Plugins and scripts required by this view-->
    <script src="{{asset('public/node_modules/chart.js/dist/Chart.min.js')}}"></script>
    <script src="{{asset('public/node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/js/custom-tooltips.min.js')}}"></script>
    <script src="{{asset('public/dist/js/main.js')}}"></script>

    <!--     CoreUI and necessary plugins
    -->
    <script src="{{asset('assets/js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery-additional-methods.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>

    <script src="{{ asset('assets/vendor/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/vendor/ckfinder/ckfinder.js') }}"></script>
    <script src="{{asset('assets/js/chosen.jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/spinbox.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap-datepicker.gl.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap-timepicker.min.js')}}"></script>

    <!--
    
        <script src="{{asset('assets/js/chart.min.js')}}"></script>-->

    <script src="{{asset('assets/js/moment.min.js')}}"></script>


    <script src="{{asset('assets/js/daterangepicker.min.js')}}"></script>



    <script src="{{asset('assets/js/jquery.inputpicker.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap-datetimepicker.min.js')}}"></script>
    <script src="{{asset('assets/js/ImageSelect.jquery.js')}}"></script>
    <script src="{{asset('assets/js/autosize.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.inputlimiter.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.maskedinput.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap-tag.min.js')}}"></script>

    <script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<!--     <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>-->

    <script src="{{asset('assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
<!--    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>-->



    <script src="{{asset('assets/js/dataTables.buttons.min.js')}}"></script>
<!--    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>-->
    <script src="{{asset('assets/js/dataTables.select.min.js')}}"></script>
    <script src="{{asset('assets/js/input.js')}}"></script>

    <script src="{{asset('assets/js/datetime-moment.js')}}"></script>

<!--<script src="{{asset('assets/js/date-eu.js')}}"></script>-->







    <script src="{{asset('assets/js/buttons.flash.min.js')}}"></script>

    <script src="{{asset('assets/js/jszip.min.js')}}"></script>

    <script src="{{asset('assets/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/js/vfs_fonts.js')}}"></script>

    <script src="{{asset('assets/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/js/buttons.print.min.js')}}"></script>


    <!-- ace scripts -->
    <script src="{{asset('assets/js/ace-elements.min.js')}}"></script>
    <script src="{{asset('assets/js/ace.min.js')}}"></script>
    <script src="{{ asset('assets/js/custom-validators.js') }}"></script>


    <script src="{{ asset('assets/js/inscripciones.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/postulaciones.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/curso.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/users.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/offers.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/puesto.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/sector.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/enterprise.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/contract.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/newsletter.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/news.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/grade.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/specialty.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/drivingPermit.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/auditorias.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/boletines.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/banners.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>

    <script>
// "global" vars, built using blade
var assets = "{{ URL::asset('/assets/') }}";

var url1 = "{{url('/chart1')}}";
var ResumenGeneros = new Array();
var totalConcellos = new Array();
var nameConcellos = new Array();
var TotalUsuarios = 0;
var TotalUsuariosHoy = 0;
var totalOfertasPublicadas = 0;
var totalOfertaContrato = new Array();
var nametotalOfertaContrato = new Array();
var coloresdinamicos = new Array();
var totalCursosPublicados = 0;
var totalCursosTipo = new Array();
var nametotalCursosTipo = new Array();

var totalNoticiasPublicadas = 0;
var totalNoticiasCategoria = new Array();
var nameNoticiasCategoria = new Array();


function load_resumen() {
    $.ajax({
        url: "{{url('panel/admin/chart1')}}",
        method: 'POST',
        type: 'JSON',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        beforeSend: function () {},
        success: function (response) {


            $('#usuarios_registrados').html(response.totalUsuarios[0].total);

            $('#totalUsuariosHoy').html(response.totalUsuariosHoy[0].total);
            $('#totalEmpresas').html(response.totalEmpresas[0].total);

            response.totalGeneros.forEach(function (data) {
                ResumenGeneros.push(data.total);

            });
            response.totalConcellos.forEach(function (data) {
                totalConcellos.push(data.total);
                nameConcellos.push(data.name);

            });






            var pieChart = new Chart($('#canvas-usuarios'), {
                type: 'pie',
                data: {
                    labels: ['Masculino', 'Femenino', 'Otros'],
                    datasets: [{
                            data: ResumenGeneros,
                            backgroundColor: ['#FF6384', '#36A2EB', '#FFCE56'],
                            hoverBackgroundColor: ['#FF6384', '#36A2EB', '#FFCE56']
                        }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: true,
                        position: 'left'
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                var total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
                                    return previousValue + currentValue;
                                });
                                var currentValue = dataset.data[tooltipItem.index];
                                var percentage = Math.floor(((currentValue / total) * 100) + 0.5);
                                return percentage + "%";
                            }
                        }
                    }
                }
            }); // eslint-disable-next-line no-unused-vars


            var pieChart2 = new Chart($('#canvas-usuarios-concello'), {
                type: 'pie',
                data: {
                    labels: nameConcellos,
                    datasets: [{
                            data: totalConcellos,
                            backgroundColor: ['#6610f2', '#36A2EB', '#FFCE56', '#4dbd74', '#f86c6b'],
                            hoverBackgroundColor: ['#6610f2', '#36A2EB', '#FFCE56', '#4dbd74', '#f86c6b']
                        }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: true,
                        position: 'left'
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                var dataset = data.datasets[tooltipItem.datasetIndex];

                                var total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
                                    return previousValue + currentValue;
                                });
                                var currentValue = dataset.data[tooltipItem.index];

                                var percentage = Math.floor(((currentValue / total) * 100) + 0.5);
                                return percentage + "%";
                            }
                        }
                    }
                }
            });




            // *************************** OFERTAS

            $('#totalOfertasPublicadas').html(response.totalOfertasPublicadas[0].total);
            $('#totalOfertasNuevas').html(response.totalOfertasNuevas[0].total);
            coloresdinamicos = [];

            response.totalOfertaContrato.forEach(function (data) {
                totalOfertaContrato.push(data.total);
                nametotalOfertaContrato.push(data.name);
                color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                while ($.inArray(color, coloresdinamicos) > -1) {
                    color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                }
                coloresdinamicos.push(color);
            });
            var pieChart3 = new Chart($('#totalOfertaContrato'), {
                type: 'pie',
                data: {
                    labels: nametotalOfertaContrato,
                    datasets: [{
                            data: totalOfertaContrato,
                            backgroundColor: coloresdinamicos,
                            hoverBackgroundColor: coloresdinamicos
                        }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: true,
                        position: 'bottom'
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                var total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
                                    return previousValue + currentValue;
                                });
                                var currentValue = dataset.data[tooltipItem.index];
                                var percentage = Math.floor(((currentValue / total) * 100) + 0.5);
                                return percentage + "%";
                            }
                        }
                    }

                }
            });
            // *************************** FORMACION ***************************************************************

            $('#totalCursosPublicados').html(response.totalCursosPublicados[0].total);
            $('#totalCursosNuevos').html(response.totalCursosNuevos[0].total);
            coloresdinamicos = [];

            response.totalCursosTipo.forEach(function (data) {
                totalCursosTipo.push(data.total);
                nametotalCursosTipo.push(data.name);
                color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                while ($.inArray(color, coloresdinamicos) > -1) {
                    color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                }
                coloresdinamicos.push(color);
            });
            var pieChart4 = new Chart($('#totalCursosTipo'), {
                type: 'pie',
                data: {
                    labels: nametotalCursosTipo,
                    datasets: [{
                            data: totalCursosTipo,
                            backgroundColor: coloresdinamicos,
                            hoverBackgroundColor: coloresdinamicos
                        }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: true,
                        position: 'bottom'
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                var total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
                                    return previousValue + currentValue;
                                });
                                var currentValue = dataset.data[tooltipItem.index];
                                var percentage = Math.floor(((currentValue / total) * 100) + 0.5);
                                return percentage + "%";
                            }
                        }
                    }

                }
            });

            // *************************** NOTICIAS  ***************************************************************

            $('#totalNoticiasPublicadas').html(response.totalNoticiasPublicadas[0].total);
            $('#totalNoticiasNuevas').html(response.totalNoticiasNuevas[0].total);
            coloresdinamicos = [];

            response.totalNoticiasCategoria.forEach(function (data) {
                totalNoticiasCategoria.push(data.total);
                nameNoticiasCategoria.push(data.name);
                color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                while ($.inArray(color, coloresdinamicos) > -1) {
                    color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                }
                coloresdinamicos.push(color);
            });
            var pieChart5 = new Chart($('#totalNoticiasCategoria'), {
                type: 'pie',
                data: {
                    labels: nameNoticiasCategoria,
                    datasets: [{
                            data: totalNoticiasCategoria,
                            backgroundColor: coloresdinamicos,
                            hoverBackgroundColor: coloresdinamicos
                        }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: true,
                        position: 'left'
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                var total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
                                    return previousValue + currentValue;
                                });
                                var currentValue = dataset.data[tooltipItem.index];
                                var currentlabel = data.labels[tooltipItem.index];
                                var percentage = Math.floor(((currentValue / total) * 100) + 0.5);
                                return currentlabel + ' ' + percentage + "%";
                            }
                        }
                    }

                }
            });
        }
    });

}


function load_notificacion() {
    $.ajax({
        url: '/admin/notificaciones',
        method: 'POST',
        type: 'JSON',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        beforeSend: function () {},
        success: function (response) {

            $('#listNotificaciones').html('');

            $('#cantidadnotificaciones').text(response.cantidad);
            var iconread = '';
            var color = 'red';
            var link = ''
            var id;
            $.each(JSON.parse(response.lista), function (idx, obj) {
                if (obj.leido == '1') {
                    iconread = 'glyphicon glyphicon-remove';
                    color = 'blue';
                } else {
                    iconread = 'glyphicon glyphicon-check';
                    color = 'red';
                }
                link = obj.ruta;
                id = obj.id;


                //$('#listNotificaciones').append('<a data-id="' + id + '" class="dropdown-item notificacion" href="#" ><span class="' + iconread + '" style=" color:' + color + '"></span> ' + obj.mensaje + '<a>');
                $('#listNotificaciones').append('<a id="notificacion" data-id="' + id + '" class="dropdown-item notificacion" href=' + link + '><span class="' + iconread + '" style=" color:' + color + '"></span> ' + obj.mensaje + '<a>');

            });

        }
    });

}

jQuery(document).ready(function ($) {

    load_notificacion();
    load_resumen();
    var intevalo = setInterval('load_notificacion()', 180000);



});





    </script>

<!--<script>
$.datepicker.regional['es'] = {
closeText: 'Cerrar',
prevText: '< Ant',
nextText: 'Sig >',
currentText: 'Hoy',
monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
weekHeader: 'Sm',
dateFormat: 'dd/mm/yy',
firstDay: 1,
isRTL: false,
showMonthAfterYear: false,
yearSuffix: ''
};
$.datepicker.setDefaults($.datepicker.regional['es']);

</script>-->
</html>