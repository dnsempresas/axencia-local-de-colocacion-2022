<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v2.0.0
* @link https://coreui.io
* Copyright (c) 2018 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->

<html lang="en">
    <head>
        <base href="./">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <meta name="description" content="Axencia local de colocacion">
        <meta name="author" content="DenmaSoft Inc">
        <meta name="keyword" content="Axencia, Colocacion, Santiago">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>Axencia Local de Colocacion</title>
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}" />
        <link rel="stylesheet" href="{{asset('assets/font-awesome/4.5.0/css/font-awesome.min.css')}}" />
        <link href="{{ asset('node_modules/@coreui/icons/css/coreui-icons.min.css') }}" rel="stylesheet">
        <link href="{{ asset('node_modules/flag-icon-css/css/flag-icon.min.css') }}" rel="stylesheet">
        <link href="{{ asset('node_modules/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('node_modules/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">
        <link href="{{ asset('src/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('src/vendors/pace-progress/css/pace.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('assets/css/chosen.min.css')}}" />
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap-datepicker3.min.css')}}" />
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap-timepicker.min.css')}}" />
        <link rel="stylesheet" href="{{asset('assets/css/daterangepicker.min.css')}}" />
        <link rel="stylesheet" href="{{asset('assets/css/ImageSelect.css')}}" />
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap-datetimepicker.min.css')}}" />
        <link rel="stylesheet" href="{{asset('assets/css/fonts.googleapis.com.css')}}" />
        <link href="{{asset('assets/css/responsive.dataTables.min.css')}}" rel="stylesheet" />
        <link href="{{asset('assets/css/buttons.dataTables.min.css')}}" rel="stylesheet" />

        <link rel="stylesheet" href="{{asset('assets/css/ace.min.css')}}" class="ace-main-stylesheet" id="main-ace-style" />


        <link rel="stylesheet" href="{{asset('assets/css/ace-skins.min.css')}}" />
        <link rel="stylesheet" href="{{asset('assets/css/ace-rtl.min.css')}}" />

        <style>
            body{
                font-size: 14px !important;
            }
            .form-group input[type=email], .form-group input[type=url], .form-group input[type=search], .form-group input[type=tel], .form-group input[type=color], .form-group input[type=text], .form-group input[type=password], .form-group input[type=datetime], .form-group input[type=datetime-local], .form-group input[type=date], .form-group input[type=month], .form-group input[type=time], .form-group input[type=week], .form-group input[type=number], .form-group select, .form-group textarea{
                width: 100%;
                max-width: 100%;
            }
            .btn{
                font-size: 14px;
            }
            .help-block {
                display: block;
                margin-top: 5px;
                margin-bottom: 10px;
                color: #D68273;
            }
            .table{
                width: 100% !important;
            }
            .dataTables_wrapper{
                display: block !important;
            }

            .pagination {
                display: inline-block !important;
            }
            .card {
                margin-bottom: 1.5rem;
                margin-top: 1.5rem;
            }
            select.form-control:not([size]):not([multiple]) {
                height: 25px;
                font-size: 12px;
            }
            .toolbar{
                margin-bottom: 1.5rem;
            }
            .card-body {
                -ms-flex: 1 1 auto;
                flex: 1 1 auto;
                padding: 2.25rem;
            }
            legend{
                border-bottom: none;
            }
            .custom-btn{
                float: right;
                margin-right: 12px;
            }
            #contract_type{
                margin-right: 12px;
                margin-left: 12px;
                width: 96%;
            }

            .form-group.required .control-label:before {
                content:"*" !important;
                color:red !important;
            }
        </style>
    </head>
    <body class="app header-fixed sidebar-fixed aside-menu-fixed pace-done">
        <header class="app-header navbar">
            <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="#" style="width: 300px !important">
                <img class="navbar-brand-full" src="{{ asset('assets/images/logo-gl.png') }}" width="100%" height="100%" alt="Agencia local de colocacion">
                <img class="navbar-brand-minimized" src="{{ asset('assets/images/logo-gl.png') }}" width="30" height="30" alt="Agencia local de colocacion">
            </a>
            <!--<button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
              <span class="navbar-toggler-icon"></span>
            </button>-->
            <ul class="nav navbar-nav d-md-down-none">

                <li class="nav-item px-3">
                    <a class="nav-link" href="{{url('/empresas/perfil')}}">{{__('Mi perfil')}}</a>
                </li>

                <li class="nav-item px-3">
                    <a class="nav-link" href="{{url('/empresas/ofertas')}}">{{__('Ofertas')}}</a>
                </li>
                <!--                <li class="nav-item px-3">
                                    <a class="nav-link" href="/empresas/cursos">{{__('Cursos')}}</a>
                                </li>
                                <li class="nav-item px-3">
                                    <a class="nav-link" href="/empresas/noticias">{{__('Noticias')}}</a>
                                </li>-->

            </ul>
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item d-md-down-none">
                    <!--                    <a class="nav-link" href="#">
                                            <i class="icon-bell"></i>
                                            <span class="badge badge-pill badge-danger">5</span>
                                        </a>-->
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        
                        @if(Auth::user()->foto)
                                <img style="height: 35px; margin: 0 10px; border-radius: 50em;" class="img-avatar" src="{{asset('/public/storage/fotos/'. Auth::user()->foto)}}" alt="{{Auth::user()->email}}"> 
                                @else


                        <img style="height: 35px; margin: 0 10px; border-radius: 50em;" class="img-avatar" src="<?php
                        echo asset('assets/images/otro.png');
                        ?>" alt="{{Auth::user()->email}}"> 
                        @endif

                        <span class="caret"></span></a>
                    <div class="dropdown-menu dropdown-menu-right">

                        <a class="dropdown-item" href="{{ url('user/logout') }}">
                            <i class="fa fa-lock"></i> Cerrar Sesión 
                        </a>
                    </div>
                </li>
            </ul>
            <button class="navbar-toggler aside-menu-toggler d-lg-none" type="button" data-toggle="aside-menu-show">
                <span class="navbar-toggler-icon"></span>
            </button>
        </header>
        <div class="app-body">
            <div class="sidebar">
                <nav class="sidebar-nav">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('cursos')}}">
                                <i class="nav-icon icon-speedometer"></i> Cursos
                                <span class="badge badge-primary">Nuevo</span>
                            </a>
                        </li>

                    </ul>
                </nav>
                <button class="sidebar-minimizer brand-minimizer" type="button"></button>
            </div>
            <main class="main">
                <!-- Breadcrumb-->
                @yield('breadcrumb')

                <div class="container-fluid">
                    <div class="animated fadeIn">
                        <!-- /.row-->
                        @yield('content')
                    </div>
                </div>
            </main>

        </div>
        <footer class="app-footer">
            <!--<div>
              <a href="https://coreui.io">CoreUI</a>
              <span>&copy; 2018 creativeLabs.</span>
            </div>-->
            <div class="ml-auto">
                <span>Powered by</span>
                <a href="https://coreui.io">DnsEmpresas</a>
            </div>
        </footer>

    </body>
    <!-- CoreUI and necessary plugins-->
    <script src="{{asset('assets/js/jquery-2.1.4.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery-additional-methods.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/chosen.jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/spinbox.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap-timepicker.min.js')}}"></script>
    <script src="{{asset('assets/js/moment.min.js')}}"></script>
    <script src="{{asset('assets/js/chart.min.js')}}"></script>
    <script src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap-datetimepicker.min.js')}}"></script>
    <script src="{{asset('assets/js/ImageSelect.jquery.js')}}"></script>
    <script src="{{asset('assets/js/autosize.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.inputlimiter.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.maskedinput.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap-tag.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/dataTables.buttons.min.js')}}"></script>

    <script src="{{asset('assets/js/buttons.flash.min.js')}}"></script>

    <script src="{{asset('assets/js/jszip.min.js')}}"></script>

    <script src="{{asset('assets/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/js/vfs_fonts.js')}}"></script>

    <script src="{{asset('assets/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/js/buttons.print.min.js')}}"></script>


    <!-- ace scripts -->
    <script src="{{asset('assets/js/ace-elements.min.js')}}"></script>
    <script src="{{asset('assets/js/ace.min.js')}}"></script>
    <script src="{{ asset('assets/js/custom-validators.js') }}"></script>
    <script src="{{ asset('assets/js/offers.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/users.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/puesto.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/sector.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/enterprise.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/contract.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/newsletter.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/news.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/grade.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/specialty.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/drivingPermit.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>
    <script src="{{ asset('assets/js/auditorias.js?v=3d0429209bc2ee9724da4c101a2b6588') }}"></script>

    <script>
// "global" vars, built using blade
var assets = "{{ URL::asset('/assets/') }}";

var totalOfertasPublicadas = 0;
var totalOfertas = 0;
var totalOfertaContrato = new Array();
var nametotalOfertaContrato = new Array();
var coloresdinamicos = new Array();

function load_resumen() {
    $.ajax({
        url: "{{url('/empresas/chart1')}}",
        method: 'POST',
        type: 'JSON',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        beforeSend: function () {},
        success: function (response) {

            // *************************** OFERTAS
            $('#totalOfertas').html(response.totalOfertas[0].total);

            $('#totalOfertasPublicadas').html(response.totalOfertasPublicadas[0].total);
            $('#totalOfertasNuevas').html(response.totalOfertasNuevas[0].total);
            coloresdinamicos = [];

            response.totalOfertaContrato.forEach(function (data) {
                totalOfertaContrato.push(data.total);
                nametotalOfertaContrato.push(data.name);
                color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                while ($.inArray(color, coloresdinamicos) > -1) {
                    color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                }
                coloresdinamicos.push(color);
            });
            var pieChart3 = new Chart($('#totalOfertaContrato'), {
                type: 'pie',
                data: {
                    labels: nametotalOfertaContrato,
                    datasets: [{
                            data: totalOfertaContrato,
                            backgroundColor: coloresdinamicos,
                            hoverBackgroundColor: coloresdinamicos
                        }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: true,
                        position: 'bottom'
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                var total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
                                    return previousValue + currentValue;
                                });
                                var currentValue = dataset.data[tooltipItem.index];
                                var percentage = Math.floor(((currentValue / total) * 100) + 0.5);
                                return percentage + "%";
                            }
                        }
                    }

                }
            });



        }
    });

}




jQuery(document).ready(function ($) {

    load_resumen();

});





    </script>

</html>