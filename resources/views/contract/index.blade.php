@extends('layouts.app')

@section('title_pestaña')
| Elegir Evento
@endsection

@section('titulo_pantalla')

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item active">Tipo de Contratos</li>
    <!-- Breadcrumb Menu-->
</ol>
@endsection

@section('content')
@if(Session::has('alert-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
</div>
@endif
@if(Session::has('alert-warning'))
<div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-exclamation-triangle"></i> {{ Session::get('alert-warning') }}</h4>
</div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">{{__('contract.contracts')}}

            </div>
            <div class="card-body">
                <div class="toolbar">
                    <div class="btn-group">
                        <a class="btn btn-block btn-outline-primary" href="{{ url('/panel/contratos/create') }}"><i class="fa fa-plus-circle" aria-hidden="true"> </i> {{__('contract.add')}}</a>
                    </div>
                </div>
                <table id="contract-table" class="table table-striped table-bordered table-hover dataTable no-footer">
                    <thead class="thead-light">
                        <tr>
                            <th>{{__('contract.name')}}</th>
                            <th>{{__('contract.name_gl')}}</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($contracts as $contract)
                        <tr>
                            <td>{{ $contract->nombre }}</td>
                            <td>{{ $contract->nome }}</td>
                            <td style="text-align:center; vertical-align:middle;">
                                <a href='{{url('/panel/contratos/'.$contract->id.'/edit')}}' title='Edit' ><span class='glyphicon glyphicon-edit'></span></a>
                                &emsp;<a id='remove_link' href='{{url('/panel/admin/contratos/'.$contract->id.'/remove')}}' title='Delete' ><span class='glyphicon glyphicon-remove' style='color:red'></span></a>
                            </td>
                        </tr>
                        @endforeach	
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /.col-->
</div>
<!-- /.row-->
@endsection
