<html lang="en"><head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
        <meta name="author" content="Łukasz Holeczek">
        <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
        <title>Axencia Local de Colocacion</title>

        <link href="{{ asset('/public/dist/vendors/@coreui/icons/css/coreui-icons.min.css') }}" rel="stylesheet">
        <link href="{{ asset('/public/dist/vendors/flag-icon-css/css/flag-icon.min.css') }}" rel="stylesheet">
        <link href="{{ asset('/public/dist/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('/public/dist/vendors/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">
        <!-- Main styles for this application-->
        <link href="{{ asset('/public/dist/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('/public/dist/vendors/pace-progress/css/pace.min.css') }}" rel="stylesheet">


    <body class="app flex-row align-items-center  pace-done" cz-shortcut-listen="true"><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
                <div class="pace-progress-inner"></div>
            </div>
            <div class="pace-activity"></div></div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-5">
                    <div class="card-group">
                        <div class="card p-4">
                             <div class="card-header">{{__('frontend.Solicituddecambiodecontrasinal')}}</div>
                            <div class="card-body">

                               <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-6 control-label">Email</label>

                                    <div class="col-md-12">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{__('frontend.Enviarcontrasena')}}  
                                        </button>
                                    </div>
                                </div>
                            </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <script src="{{ asset('/public/dist/vendors/jquery/js/jquery.min.js') }}"></script>
        <script src="{{ asset('/public/dist/vendors/popper.js/js/popper.min.js') }}"></script>
        <script src="{{ asset('/public/dist/vendors/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('/public/dist/vendors/pace-progress/js/pace.min.js') }}"></script>
        <script src="{{ asset('/public/dist/vendors/perfect-scrollbar/js/perfect-scrollbar.min.js') }}"></script>
        <script src="{{ asset('/public/dist/vendors/@coreui/coreui/js/coreui.min.js') }}"></script>
        <!-- Plugins and scripts required by this view-->
        <script src="{{ asset('/public/dist/vendors/chart.js/js/Chart.min.js') }}"></script>
        <script src="{{ asset('/public/dist/vendors/@coreui/coreui-plugin-chartjs-custom-tooltips/js/custom-tooltips.min.js') }}"></script>
        <script src="{{ asset('/public/dist/js/main.js') }}"></script>
        <script>
$('#ui-view').ajaxLoad();
$(document).ajaxComplete(function () {
    Pace.restart()
});
        </script>


        <div id="naptha_container0932014_0707"></div></body></html>
