@extends('layouts.app')

@section('titulo_pantalla')

@endsection

@section('content')
@if ($errors->any())
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-ban"></i>Precauccion</h4>
    @foreach ($errors->all() as $error)
    <p>{{ $error }}</p>
    @endforeach
</div>
@endif

@if(Session::has('alert-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
</div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <i class="icon-note"></i> Editar Curso de formación
            </div>
            <div class="card-body">
                <div class="row">
                    <form method="POST" action="{{route('cursos.update', $curso->id) }}"  accept-charset="UTF-8" class="form-group">
                        <div class="col-md-6">

                            {{ csrf_field() }}
                            {{method_field('PATCH')}}
                            <div class="form-group">
                                <label>Nombre</label>
                                <input type="text" class="form-control" name="nombre" value="{{ old('nombre')? old('nombre') : strtoupper($curso->nombre) }}" placeholder="Nombre del curso">
                            </div>
                            <div class="form-group">
                                <label>Lugar Celebracion</label>
                                <input type="text" class="form-control" name="lugar_celebracion"  placeholder="Lugar de celebracion">
                            </div>
                            <div class="form-group">
                                <label>Descripccion larga</label>
                                <input type="text" class="form-control" name="contenido" value="{{ old('contenido')? old('contenido') : strtoupper($curso->contenido) }}" placeholder="Descripccion larga">
                            </div>
                            <div class="form-group">
                                <label>Fecha Inicio</label>
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </span>
                                    <input type="text" name="fecha_inicio" value="{{ old('fecha_inicio')? old('fecha_inicio') : strtoupper($curso->fecha_inicio) }}" class="form-control" id="date">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Fecha Fin</label>
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </span>
                                    <input type="text" name="fecha_fin" value="{{ old('fecha_fin')? old('fecha_fin') : strtoupper($curso->fecha_fin) }}" class="form-control" id="date">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>hora Incio</label>
                                <input type="text" class="form-control" name="hora_inicio" value="{{ old('hora_inicio')? old('hora_inicio') : strtoupper($curso->hora_inicio) }}" placeholder="Hora inicio">
                            </div>			
                            <div class="form-group">
                                <label>Hora fin:</label>
                                <input type="text" class="form-control" name="hora_fin" value="{{ old('hora_fin')? old('hora_fin') : strtoupper($curso->hora_fin) }}" placeholder="Hora inicio">
                            </div>
                            <div class="form-group">
                                    <label>Duracion horas</label>
                                    <input type="text" class="form-control" name="duracion_horas" value="{{ old('duracion_horas')? old('duracion_horas') : strtoupper($curso->duracion_horas) }}" placeholder="Horas totales">
                            </div>
                            <div class="form-group">
                                <label>Plazas Totales</label>
                                <input type="text" class="form-control" name="plazas_total" value="{{ old('plazas_total')? old('plazas_total') : strtoupper($curso->plazas_total) }}" placeholder="Nombre del curso">
                            </div>
                            <!--<div class="form-group">
                                    <label>Plazas disponibles actualmente:</label>
                                    <input type="text" class="form-control" name="plazas_disponibles" value="{{ old('plazas_disponibles') }}" placeholder="Nombre del curso">
                            </div>-->
                            <div class="form-group">
                                <label>Mayores de:</label>
                                <input type="text" class="form-control" name="edades_recomendadas" value="{{ old('edades_recomendadas')? old('edades_recomendadas') : strtoupper($curso->edades_recomendadas) }}" placeholder="Nombre del curso">                   
                            </div>
                            <div class="form-group">
                                <label for="clasificacion_evento">Sector:</label>
                                <select id="sector" class="form-control" name="sector">
                                    <option value="0" selected="selected">Seleccione</option>
                                    @foreach($sectores as $sector)
                                    <option value="{{ $sector->id }}"  {{ (count($curso->sectores) > 0) ? ($curso->sectores[0]->id == $sector->id ) ? 'selected' : '' : '' }}>{{ $sector->nombre }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <span>Pertenece Fie</span><br>
                                <label class="switch switch-label switch-outline-danger">
                                    <input type="checkbox" name="pertenece_fie" value="true" class="switch-input" checked="{{ old('pertenece_fie') }}">
                                    <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                </label>
                            </div>								
                        </div>		
                        @foreach($puestos as $indexKey => $puesto)
                        <div class="col-md-6">								
                            <label class="switch switch-label switch-primary">
                                <input type="checkbox" name="puestos[]" value="{{$puesto->id}}" class="switch-input" {{ (count($curso->puestos->pluck('id')->toArray()) > 0) ? (in_array($puesto->id, $curso->puestos->pluck('id')->toArray()))? 'checked' : '' : ''}}>
                                <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>										
                            </label>
                            <span>{{ $puesto->nombre }}</span>
                        </div>
                        @endforeach
                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary" name="signup" value="Sign up">Guardar</button>
                            </div>
                        </div>
                    </form>			
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <i class="icon-note"></i> Nuevo Curso de formación
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <form method="POST" action="{{url('/panel/cursos')}}" accept-charset="UTF-8" class="form-group" enctype="multipart/form-data">
                            <div class="form-horizontal col-md-6">

                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('Título')}} </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="nombre" name="nombre" placeholder="{{__('Título')}}" class="col-xs-10 col-sm-5" value="{{ old('nombre')? old('nombre') : strtoupper($curso->nombre) }}"  required>
                                    </div>

                                </div>



                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('Descripcion larga')}} </label>
                                    <div class="col-sm-9">
                                        <textarea required="required"  class="ckeditor form-control" name="contenido" id="contenido" rows="4" cols="80" >"{{ old('contenido')? old('contenido') : strtoupper($curso->contenido) }}"</textarea>
                                    </div>
                                    <script>
                                        CKEDITOR.replace('contenido', {
                                            
                                            width: '100%',
                                            height: 150

                                        });

                                    </script>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('Lugar Celebracion')}} </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="lugar_celebracion" name="lugar_celebracion" placeholder="{{__('Lugar Celebracion')}}" class="col-xs-10 col-sm-5" value="{{ old('lugar_celebracion')? old('lugar_celebracion') : strtoupper($curso->lugar_celebracion) }}" required>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label ">{{__('Fecha')}}</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="fecha_inicio" name="fecha_inicio" required>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="fecha_fin" name="fecha_fin" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label ">{{__('Horario')}}</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="hora_inicio" name="hora_inicio" required>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="hora_fin" name="hora_fin" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">{{__('Total de Horas')}}</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="duracion_horas" name="duracion_horas" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">{{__('Total de Plazas')}}</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="plazas_total" name="plazas_total" required>
                                    </div>
                                </div>
                            </div>
                            <div class=" form-horizontal col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label ">{{__('Rango de Edades Recomendadas')}}</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="edad_min" name="edad_min" required>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="edad_max" name="edad_min" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">{{__('Sector al que aplica:')}}</label>
                                    <div class="col-sm-9">
                                        <select id="sector" class="form-control" name="sector">
                                            <option value="0" selected="selected">Seleccione</option>
                                            @foreach($sectores as $sector)
                                            <option value="{{ $sector->id }}"  {{ (old('sector') == $sector->id ) ? 'selected' : '' }}>{{ $sector->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span class="col-sm-4 control-label no-padding-right">Pertenece Fie</span>
                                    <div class="col-sm-8">
                                        <label class="switch switch-label switch-outline-danger">
                                            <input type="checkbox" name="pertenece_fie" value="true" class="switch-input" checked="">
                                            <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">{{__('Precio')}}</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="precio" name="precio" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">{{__('Requerimientos')}}</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" name="requerimientos" id="requerimientos" required></textarea>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">{{__('Observaciones')}}</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" name="requerimientos" id="requerimientos" required></textarea>
                                    </div>
                                </div>

                                <!--                        @foreach($puestos as $indexKey => $puesto)
                                                        <div class="col-md-4">								
                                                            <label class="switch switch-label switch-primary">
                                                                <input type="checkbox" name="puestos[]" value="{{$puesto->id}}" class="switch-input" {{ (old('puestos')) ? (in_array($puesto->id, old('puestos')))? 'checked' : '' : ''}}>
                                                                <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>										
                                                            </label>
                                                            <span>{{ $puesto->nombre }}</span>
                                                        </div>
                                                        @endforeach-->
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary" name="signup" value="Sign up">Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>			

                </div>

            </div>
        </div>
    </div>
</div>


@endsection
