@extends('layouts.app')

@section('title_pestaña')
| Elegir Evento
@endsection

@section('titulo_pantalla')

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">{{__('curso.Inscripciones')}} </li>
    <li class="breadcrumb-item active"> {{__('curso.Inscripciones')}} </li>
    <!-- Breadcrumb Menu-->
</ol>
@endsection

@section('content')
@if(Session::has('alert-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
</div>
@endif
@if(Session::has('alert-danger'))
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-danger"></i> {{ Session::get('alert-danger') }}</h4>
</div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">{{__('curso.Inscripciones')}}

            </div>
            <div class="card-body">
                <div class="toolbar">
                    <h3> CURSO:   {{$curso->nombre}} </h3> </br>
                    Data de Inicio: {{date('d/m/Y', strtotime($curso->fecha_inicio))}} </br>

                </div>


                <div class="row">


                    <div class="col-md-8">
                        <div class="input-group">
                            <input type="text" id="dni" class="form-control" placeholder="Inscribir usuario por DNI">
                            <span class="input-group-btn">
                                <button id="buscar-user" class="btn btn-default" type="button"> Buscar e Inscribir...</button>
                            </span>



                        </div><!-- /input-group -->
                    </div>
                    <div class="col-md-12">
                        <span id="Datos" class="">
                        </span>
                    </div>
                    <input id="idCurso"type="hidden" value="{{$curso->id}}">


                </div>
                </br>
                </br>

                <div class="row"> 
                    <div class="col-lg-12">
                        <table id="table-inscritos" class="table table-responsive-sm table-hover table-outline mb-0">
                            <thead class="thead-light">
                                <tr>
                                    <th>{{__('Data Inscripcion')}} </th>
                                    <th>{{__('DNI')}}</th>
                                    <th>{{__('Nombre')}}</th>
                                    <th >{{__('Apellidos')}}</th>
                                    <th>{{__('Email')}}</th>
                                    <th>{{__('Telefono')}} </th>
                                    
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($curso->users)
                                @foreach($curso->users as $inscrito)
                                <tr>
                                    <td>{{date('d/m/Y H:m:s', strtotime($inscrito->pivot->fechainscripcion))}}</td>
                                    <td><b>{{ $inscrito->cif }}</b><br></td>
                                    <td>{{ $inscrito->name }}</td>
                                    <td>{{ $inscrito->surname }}</td>
                                    <td>{{ $inscrito->email }}</td>
                                    <td>{{ $inscrito->telephone }}</td>

                                    <td style="text-align:center; vertical-align:middle;">							
                                        &emsp;<a id="remove_link" href="{{url('/panel/admin/cursos/'.$curso->id.'/retirar/'.$inscrito->pk)}}" title="Retirar" ><span class="glyphicon glyphicon-remove" style="color:red"></span></a>
                                    </td>
                                </tr>
                                @endforeach	
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.col-->
</div>
<!-- /.row-->
<div class="modal " tabindex="-1" role="dialog" id="modalincribir" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Inscribir en Curso de Formación</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row " style="margin-left:10px" >

                    <form action="/panel/admin/cursos/inscribir" method="POST" class="form-horizontal" id ="formoIncribir">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="col-sm-6 control-label no-padding-right" for="form-field-1">Inscribiendo en el Curso: </label>
                            <div class="col-sm-6">
                                <span id="datosdelcurso"> {{ $curso->nombre . ' '.$curso->localidad.' '. date('d/m/Y', strtotime($curso->fecha_inicio))  }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label no-padding-right" for="form-field-1">Datos del Usuario</label>
                            <div class="col-sm-6">
                                <span id="datosdelusuario"> </span>
                            </div>
                        </div>


                        <input name="idCurso" id="idCurso"type="hidden" value="{{$curso->id}}">
                        <input name = "idUser" id="idUser"type="hidden" value="">

                        <button  type="submit" class="btn btn-primary">Aceptar</button>
                    </form>

                </div>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

@endsection
