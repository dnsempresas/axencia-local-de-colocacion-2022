@extends('layouts.app')

@section('titulo_pantalla')

@endsection

@section('content')
@if ($errors->any())
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-ban"></i>Precauccion</h4>
    @foreach ($errors->all() as $error)
    <p>{{ $error }}</p>
    @endforeach
</div>
@endif

@if(Session::has('alert-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
</div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <i class="icon-note"></i> {{__('curso.editar_curso')}} 
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <form method="POST" action="{{route('cursos.update', $curso->id) }}"  accept-charset="UTF-8" class="form-group"enctype="multipart/form-data">
                            <div class="form-horizontal col-md-6">

                                {{ csrf_field() }}
                                {{method_field('PATCH')}}

                                <div class="form-group required">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('curso.titulo')}} </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="nombre" name="nombre" placeholder="{{__('curso.titulo')}}" class="col-xs-10 col-sm-5" required value="{{ old('nombre')? old('nombre') : strtoupper($curso->nombre) }}">
                                    </div>

                                </div>



                                <div class="form-group required">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('curso.descripcion_larga')}} </label>
                                    <div class="col-sm-9">
                                        <textarea required="required"  class="ckeditor form-control" name="contenido" id="contenido" rows="4" cols="50" >{{old('contenido')?old('contenido'):$curso->contenido }}</textarea>
                                    </div>
                                   
                                </div>


                                <div class="form-group required">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{__('curso.ubicacion')}} </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="lugar_celebracion" name="lugar_celebracion" placeholder="{{__('curso.ubicacion')}}" class="col-xs-10 col-sm-5" required value="{{ strtoupper($curso->lugar_celebracion) }}" >
                                    </div>

                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-3 control-label ">{{__('curso.fecha')}}</label>



                                    <div class="col-sm-3">
                                        <input type="text" class="fecha form-control" id="fecha_inicio" name="fecha_inicio" required placeholder="{{__('curso.desde')}}" data-date-format="dd/mm/YYYY" value="{{date('d-m-Y', strtotime($curso->fecha_inicio))}}">
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="fecha form-control" id="fecha_fin" name="fecha_fin" required  placeholder="{{__('curso.hasta')}}" data-date-format="dd/mm/YYYY" value="{{date('d-m-Y', strtotime($curso->fecha_fin))}}">
                                    </div>

                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-3 control-label ">{{__('Horario')}}</label>

                                    <div class="col-sm-3">
                                        <input type="time" class=" form-control" id="hora_inicio" name="hora_inicio" required value="{{$curso->hora_inicio}}" placeholder="{{__('curso.desde')}}">
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="time" class=" form-control" id="hora_fin" name="hora_fin" required value="{{$curso->hora_fin}}"  placeholder="{{__('curso.hasta')}}">
                                    </div>
                                </div>

                            </div>
                            <div class=" form-horizontal col-md-6">




                                <div class="form-group required">
                                    <label class="col-sm-3 control-label no-padding-right">{{__('curso.horas_toltales')}}</label>
                                    <div class="col-sm-3">

                                        <input type="number" class="form-control" id="duracion_horas" name="duracion_horas" required value="{{$curso->duracion_horas}}"  placeholder="{{__('curso.duracion')}}">
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-3 control-label no-padding-right">{{__('curso.plazas')}}</label>
                                    <div class="col-sm-3">
                                        <input type="number" class="form-control" id="plazas_total" name="plazas_total"  required  value="{{$curso->plazas_total}}">
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-3 control-label ">{{__('curso.edades_recomendadas')}} </label>

                                    <div class="col-sm-3">
                                        <input type="number" min="1" max="999" class="numero form-control" id="edad_min" name="edad_min" required value="{{$curso->edad_min}}" placeholder="{{__('curso.desde')}}">
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="number" min="1" max="999" class="numero form-control" id="edad_max" name="edad_max" value="{{$curso->edad_max}}" placeholder="{{__('curso.hasta')}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">{{__('curso.sectoraplica')}}:</label>
                                    <div class="col-sm-9">
                                        <select id="sector" class="form-control" name="sector">
                                            <option value="0" selected="selected">Seleccione</option>
                                            @foreach($sectores as $sector)
                                            <option value="{{ $sector->id }}"  {{$curso->sectores()->first() ? ( ($curso->sectores()->first()->id == $sector->id ) ? 'selected' : '') : '' }} > {{ session('lang')=='es' ?  $sector->nombre :  $sector->nome }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">{{__('Tipo de Curso')}}</label>
                                    <div class="col-sm-9">
                                        <select id="tipoCursos" class="form-control" name="tipoCursos">
                                           
                                            @foreach($tipoCursos as $tipocurso)
                                            <option value="{{ $tipocurso->id }}"  {{ ($curso->tipocurso  == $tipocurso->id ) ? 'selected' : '' }}> {{ session('lang')=='es' ?  $tipocurso->nombre :  $tipocurso->nome }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <span class="col-sm-4 control-label no-padding-right">{{__('curso.perteneceafie')}}</span>
                                    <div class="col-sm-8">
                                        <label class="switch switch-label switch-outline-success">
                                            <input type="checkbox" name="pertenece_fie" value="true" class="switch-input" <?php
                                                   if ($curso->pertenece_fie == 1) {
                                                       echo 'checked';
                                                   }
                                                   ?>>
                                             
                                            <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">{{__('curso.precio')}}</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="precio" name="precio" required value="{{$curso->precio}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">{{__('curso.requirimientos')}}</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" name="requerimiento" id="requerimiento" required>{{$curso->requerimiento}}</textarea>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">{{__('curso.observaciones')}}</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" name="observaciones" id="observaciones" required>{{$curso->observaciones}}</textarea>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">{{__('curso.imagenDestacada')}}</label>
                                    <div class="col-sm-9">
                                        <input id="imagen_destacada" type="file" name="imagen_destacada" accept="image/x-png,image/gif,image/jpeg" >
                                        @if ($curso->imagen !='')
                                            <a href="/public/curso/{{$curso->imagen}} ">{{ $curso->imagen}}</a>
                                            <a href='{{url('/curso/imagen/'.$curso->id.'/remove')}}' title='Edit' ><span class='glyphicon glyphicon-remove' style='color:red'></span></a>
                                        @endif
                                    </div>
                                </div>
                                 <div class="form-group required">
                                    <span class="col-sm-4 control-label no-padding-right">{{__('curso.activado')}}</span>
                                    <div class="col-sm-8">
                                        <label class="switch switch-label switch-outline-success">
                                            <input type="checkbox" name="estatus" value="true" class="switch-input" <?php
                                                   if ($curso->estatus == 1) {
                                                       echo 'checked';
                                                   }
                                                   ?>>
                                             
                                            <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                        </label>
                                    </div>
                                </div>
                                <!--                        @foreach($puestos as $indexKey => $puesto)
                                                        <div class="col-md-4">								
                                                            <label class="switch switch-label switch-primary">
                                                                <input type="checkbox" name="puestos[]" value="{{$puesto->id}}" class="switch-input" {{ (old('puestos')) ? (in_array($puesto->id, old('puestos')))? 'checked' : '' : ''}}>
                                                                <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>										
                                                            </label>
                                                            <span>{{ $puesto->nombre }}</span>
                                                        </div>
                                                        @endforeach-->
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary" name="signup" value="Sign up">{{__('curso.guardar')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>			

                </div>

            </div>
        </div>
    </div>
</div>

@endsection
