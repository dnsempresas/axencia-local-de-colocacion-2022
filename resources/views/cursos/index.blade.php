@extends('layouts.app')

@section('title_pestaña')
| Elegir Evento
@endsection

@section('titulo_pantalla')

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">{{__('curso.inicio')}} </li>
    <li class="breadcrumb-item active"> {{__('curso.modulo')}} </li>
    <!-- Breadcrumb Menu-->
</ol>
@endsection

@section('content')
@if(Session::has('alert-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
</div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">{{__('curso.modulo')}}

            </div>
            <div class="card-body">
                <div class="toolbar">
                    <div class="btn-group">
                        <a class="btn btn-block btn-outline-primary" href="{{ url('/panel/cursos/create') }}"><i class="fa fa-plus-circle" aria-hidden="true"> </i> {{__('curso.add')}}</a>
                    </div>
                </div>
                @if($cursos->count())
                <form action="/panel/admin/cursos/busquedaAvanzada" method="POST" class="form-horizontal" id ="formfiltros">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>{{__('curso.titulo')}}</label>
                                <input type="text" class="form-control" value="{{ old('nombre') }}" id="nombre" name="nombre">
                            </div>						
                        </div>
                        <div class="col-md-2">	
                            <div class="form-group">
                                <label>{{__('curso.ubicacion')}}</label>
                                <input type="text" class="form-control" name="ubicacion" id="ubicacion">
                            </div>	
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>{{__('curso.plazas_total')}}</label>
                                <input type="text" class="form-control" name="plazas" id="plazas" placeholder="Mayor a ..">
                            </div>					
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>{{__('curso.edad_min')}}</label>
                                <input type="text" class="form-control" id="edades_recomendadas" name="edades_recomendadas" placeholder="Mayor a ..">
                            </div>					
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>{{__('curso.disponibilidad')}} :</label><br>
                                <label class="switch switch-label switch-outline-danger">
                                    <input type="checkbox" id="plazas_disponibles" name="plazas_disponibles" value="true" class="switch-input">
                                    <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>									
                                </label>
                            </div>	
                        </div>


                        <div class="row">
                            <div class="col-sm-12">
                                <a id="reset-filter" class="btn btn-sm btn-clear" style="float: right;margin-bottom: 15px;margin-top: 15px; margin-left: 10px">
                                    {{__('curso.limpiar_filtros')}}
                                </a>
                                <button type="submit" id="user-filter" class="btn btn-sm btn-success" style="float: right;margin-bottom: 15px;margin-top: 15px;">
                                    {{__('curso.filtrar')}}
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
                <table id="cursos-table" class="table table-striped table-bordered table-hover dataTable no-footer">
                    <thead class="thead-light">
                        <tr>
                            <th style="width: 40%">{{__('curso.descripcion')}}</th>
                            <th style="width: 10%">{{__('curso.duracion')}}</th>
                            <th class="text-center">{{__('curso.Plazas')}}</th>
                            <th class="text-center" style="width: 10%">{{__('Cupos')}}</th>
                            <th style="width: 10%">{{__('curso.edades_recomendadas')}} </th>
                            <th style="width: 10%">{{__('curso.fecha_inicio')}} </th>
                            <th style="width: 10%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($cursos as $curso)
                        <tr>
                            <td><b>{{ $curso->nombre }}</b><br>
                                {{ $curso->lugar_celebracion }}</td>
                            <td>{{ $curso->duracion_horas }} Horas</td>
                            <td class="text-center">{{ $curso->plazas_total }}</td>
                            <td class="text-center">{{ $curso->plazas_disponibles }} Cupos</td>
                            <td>{{ $curso->edad_min }}-{{ $curso->edad_max}}</td>
                            <td>{{date('d-m-Y', strtotime($curso->fecha_inicio))}}</td>
                            <td style="text-align:center; vertical-align:middle;">							
                            &emsp;<a href="{{ url('/panel/cursos/'.$curso->id.'/edit') }}" title='Editar' ><span class='glyphicon glyphicon-edit'></span></a>	  
                            &emsp;<a id='remove_link' href='{{url('/panel/admin/cursos/'.$curso->id.'/remove')}}' title='Delete' ><span class='glyphicon glyphicon-remove' style='color:red'></span></a>
                            &emsp;<a href="{{ url('/panel/admin/cursos/'.$curso->id.'/inscripcion') }}" title='Inscripcion' ><span class='glyphicon  glyphicon-tasks'></span> </a></a>	
                            </td>


                        </tr>
                        @endforeach	
                    </tbody>
                </table>
                @else
                <p style="text-align:center;">{{ $mensaje_empty }}</p>
                @endif
            </div>
        </div>
    </div>
    <!-- /.col-->
</div>
<!-- /.row-->
<script>
  
</script>   
@endsection
