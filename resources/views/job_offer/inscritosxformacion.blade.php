<div class="col-sm-8 col-lg-8">
    <div class="card text-white bg-light">

        <div class="card-header">Contrataciones por formación 
            <div class="card-header-actions">

            </div>
        </div>
        <div class="card-body">
            <div class="chart-wrapper bg-white">
                <canvas id="totalInscritosFormacion"></canvas>
            </div>
        </div>
    </div>
</div>


<script src="{{asset('assets/js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('assets/js/chart.min.js')}}"></script>
<script src="{{asset('assets/js/chartjs-plugin-labels.min.js')}}"></script>
<!--<script src="{{asset('node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/js/custom-tooltips.min.js')}}"></script>-->


<script>



var totalInscritosFormacion = nameTotalInscritosFormacion = [];


function load_inscritosxformacion() {
    $.ajax({
        url: "{{url('/inscritosxformacion')}}"+"?ano="+{{$ano}},
        method: 'GET',
        type: 'JSON',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        beforeSend: function () {},
        success: function (response) {

            coloresdinamicos = [];
            totalInscritosFormacion = [];
            nameTotalInscritosFormacion = [];

//                Object.keys(response.totalInscritosEdad).forEach(function (key) {
//                    // do something with obj[key]
//                });



            $.each(response.totalInscritosFormacion, function (i, data) {
                console.log(data.titulacion, data.total);
                totalInscritosFormacion.push(data.total);
                nameTotalInscritosFormacion.push(data.titulacion);
                color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                while ($.inArray(color, coloresdinamicos) > -1) {
                    color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                }
                coloresdinamicos.push(color);
            });

//            alert(totalInscritosEdad);

            var pieChartTotalInscritosFormacion = new Chart($('#totalInscritosFormacion'), {
                type: 'pie',
                data: {
                    labels: nameTotalInscritosFormacion,
                    datasets: [{
                            label: 'Estudios',
                            data: totalInscritosFormacion,
                            backgroundColor: [ 'rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(54, 262, 200)', 'rgb(54, 162, 100)',],
                            hoverBackgroundColor: [ 'rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(54, 262, 200)', 'rgb(54, 162, 100)',],
                        }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: true,
                        position: 'left'
                    },
                    title: {
                        display: true,
                        text: 'Distribución total de los usuarios según la formación academica Año ' + {{$ano}}
                    },
                    plugins: {

                        labels: {
                            // render 'label', 'value', 'percentage', 'image' or custom function, default is 'percentage'
                            render: function (args) {
                                // args will be something like:
                                // { label: 'Label', value: 123, percentage: 50, index: 0, dataset: {...} }
                                return  args.value + ' ('+args.percentage+'%) '

                                // return object if it is image
                                // return { src: 'image.png', width: 16, height: 16 };
                            },
                            showActualPercentages: true,
                            

                        }
                    },

                    tooltips: {

                    }

                }
            });
        }
    });
}

jQuery(document).ready(function ($) {
    load_inscritosxformacion();
});


</script>