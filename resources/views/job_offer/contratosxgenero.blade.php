<div class="col-sm-8 col-lg-8">
    <div class="card text-white bg-light">

        <div class="card-header">Contrataciones por Genero
            <div class="card-header-actions">

            </div>
        </div>
        <div class="card-body">
            <div class="chart-wrapper bg-white">
                <canvas id="totalContratosGenero"></canvas>
            </div>
        </div>
    </div>
</div>


<script src="{{asset('assets/js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('assets/js/chart.min.js')}}"></script>
<script src="{{asset('assets/js/chartjs-plugin-labels.min.js')}}"></script>
<!--<script src="{{asset('node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/js/custom-tooltips.min.js')}}"></script>-->


<script>



var totalContratosGenero = nameTotalContratosGenero = [];


function load_contratosxgenero() {
    $.ajax({
        url: "{{url('/contratosxgenero')}}"+"?ano="+{{$ano}},
        method: 'GET',
        type: 'JSON',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        beforeSend: function () {},
        success: function (response) {

            coloresdinamicos = [];
            totalContratosGenero = [];
            nameTotalContratosGenero = [];

//                Object.keys(response.totalContratosEdad).forEach(function (key) {
//                    // do something with obj[key]
//                });



            $.each(response.totalContratosGenero, function (i, data) {
                console.log(data.name, data.total);
                totalContratosGenero.push(data.total);
                nameTotalContratosGenero.push(data.name);
                color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                while ($.inArray(color, coloresdinamicos) > -1) {
                    color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                }
                coloresdinamicos.push(color);
            });

//            alert(totalContratosEdad);

            var pieChartTotalContratosGenero = new Chart($('#totalContratosGenero'), {
                type: 'pie',
                data: {
                    labels: nameTotalContratosGenero,
                    datasets: [{
                            label: 'Generos',
                            data: totalContratosGenero,
                            backgroundColor: [ 'rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', ],
                            hoverBackgroundColor: [ 'rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', ],
                        }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: true,
                        position: 'left'
                    },
                    title: {
                        display: true,
                        text: 'Contrataciones por Genero Año ' + {{$ano}}
                    },
                    plugins: {
                        datalabels: {
                            display: true,
                            align: 'bottom',
                            backgroundColor: '#ccc',
                            borderRadius: 3,
                            font: {
                                size: 18,
                            }
                        },
                    },

                    tooltips: {

                    }

                }
            });
        }
    });
}

jQuery(document).ready(function ($) {
    load_contratosxgenero();
});


</script>