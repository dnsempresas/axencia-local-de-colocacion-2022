@extends('layouts.app')

@section('titulo_pantalla')

@endsection

@section('content')
@if ($errors->any())
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-ban"></i>{{__('offers.errors')}}</h4>
    @foreach ($errors->all() as $error)
    <p>{{ $error }}</p>
    @endforeach
</div>
@endif

@if(Session::has('alert-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
</div>
@endif


<div id="createoffer" class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <i class="icon-note"></i> {{ __('offers.new_offer_label') }}
            </div>
            <div class="card-body">
                <form style="padding: 30px;" id="offerForm" method="POST"  action="{{url('/panel/ofertas')}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group required">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-1">{{__('Empresa')}}</label>
                                <div class="col-sm-8">
                                    <select class="chosen-select" name="propietario" id="propietario" multiple="" required>
                                        @foreach($empresas as $empresa)
                                        <option value="{{$empresa->pk}}">Cod.: {{$empresa->pk}} - {{$empresa->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-4 control-label no-padding-right">{{ __('offers.requested_job') }}</label>
                                <div class="col-sm-8">
                                    <div class="input-group">

                                        <input type="text" class="form-control" name="requested_job" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="col-sm-4 control-label no-padding-right">{{__('Tipo de Gestión')}}</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <select name="tipogestion" id="tipogestion" required style="margin-right: 0;margin-left: 0;width: 100%;">
                                            <option selected value = "0" > Gestionada por la Axencia </option>
                                            <option value = "1" > Gestionada por terceros </option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group d-none" id="divUrl" >
                                <label class="col-sm-4 control-label no-padding-right">{{__('Url Externa')}}</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input type="url" class="form-control" id="urlTercero" name="urlTercero">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group required gestionada">
                                <label class="col-sm-4 control-label no-padding-right">{{ __('offers.vacancy') }}</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input type="number" class="form-control" name="vacancy" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group required gestionada">
                                <label class="col-sm-4 control-label no-padding-right">{{__('offers.tasks')}}</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <textarea class="form-control" name="tasks" required></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group gestionada">
                                <label class="col-sm-4 control-label no-padding-right">{{__('offers.due_date')}}</label>
                                <div class="col-sm-8">
                                    <input type="text"  class="form-control" id="due_date" name="due_date" >
                                </div>
                            </div>
                            <div class="form-group required gestionada">
                                <label class="col-sm-4 control-label no-padding-right">{{__('offers.contract_type')}}</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <select name="contract_type" id="contract_type" required style="margin-right: 0;margin-left: 0;width: 100%;">
                                            <!--                                       <option value="">{{__('seleccione')}}</option>-->
                                            @foreach($contracts as $contract)
                                            <option <?php if ($contract->id == "1") echo "selected='selected'"; ?> value="{{$contract->id}}">{{ session('lang')=='es' ?  $contract->nombre :  $contract->nome }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group required gestionada" id="divTiempo">
                                <label class="col-sm-4 control-label no-padding-right">{{__('offers.duration')}}</label>
                                <div class="col-sm-8">
                                    <div class="col-sm-6">
                                        <div class="input-group">
                                            <input type="number" class="form-control" name="duration" id ="duration" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6 gestionada">
                                        <select name="tiempo" id="tiempo" required style="margin-right: 0;margin-left: 0;width: 100%;">
                                            <option value="MESES" >MESES</option>
                                            <option value="AÑOS" >ANOS</option>
                                            <option value="DIAS" >DIAS</option>
                                        </select>
                                    </div>

                                </div>

                            </div>
                            <div class="form-group required gestionada">
                                <label class="col-sm-4 control-label no-padding-right">{{__('Xornada Laboral')}}</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <select name="disponibilidad" id="disponibilidad" required style="margin-right: 0;margin-left: 0;width: 100%;">
                                            <!--                                        <option value="">{{__('seleccione')}}</option>-->
                                            @foreach($disponibilidades as $disponibilidad)
                                            <option value="{{$disponibilidad->id}}">{{ session('lang')=='es' ?  $disponibilidad->nombre :  $disponibilidad->nome }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group required gestionada">
                                <label class="col-sm-4 control-label no-padding-right">{{__('Tipo de oferta')}}</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <select name="tipooferta" id="contract_type" required style="margin-right: 0;margin-left: 0;width: 100%;">
                                            <!--                                        <option value="">{{__('seleccione')}}</option>-->
                                            @foreach($tipoofertas as $tipooferta)
                                            <option value="{{$tipooferta->id}}">{{ session('lang')=='es' ?  $tipooferta->nombre :  $tipooferta->nome }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group required gestionada">
                                <label class="col-sm-4 control-label no-padding-right">{{__('offers.start_date')}}</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input required type="text" class="form-control" id="start_date" name="start_date">
                                    </div>
                                </div>
                            </div>



                        </div>

                        <div class="col-md-6">
                            <div class="form-group required ">
                                <label class="col-sm-4 control-label no-padding-right">Provincia</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <select class="provincejob " required name="province" id="provincejob">
                                            <option selected value="">Selecione...</option>
                                            @foreach($provinces as $province)
                                            <option value="{{$province->pk}}" <?php if ((old('province') == $province->pk) || $province->pk == 15) echo "selected='selected'"; ?> >{{$province->name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="input-group">
                                            <input required style="margin-top: 5px;display: none;" class="form-control" id="other_province" name="other_province" type="text" placeholder="Especifique Provincia..."/>
                                        </div>
                                    </div>	
                                </div>

                            </div>

                            <div class="form-group required">
                                <label id="labelconcello" class="col-sm-4 control-label no-padding-right">{{__('frontend.Ayuntamiento')}}</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <select required class="form-control" name="concello_chosen" id="concellojob" data-placeholder="Seleccione...">

                                        </select>

                                        <input required style="margin-top: 5px;display: none;" class="form-control" id="other_concello" name="other_concello" type="text" placeholder="Especifique..."/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group gestionada">
                                <label class="col-sm-4 control-label no-padding-right">{{__('offers.hours')}}</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="hours" >
                                </div>

                            </div>
                            <div class="form-group gestionada">
                                <label class="col-sm-4 control-label no-padding-right">{{__('offers.salary')}}</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="salary">
                                </div>
                            </div>
                            <div class="form-group gestionada">
                                <label class="col-sm-4 control-label no-padding-right">{{__('Requisitos-Formacion')}}</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <textarea class="form-control" name="rformacion" ></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group gestionada">
                                <label class="col-sm-4 control-label no-padding-right">{{__('Requisitos-Experiencia')}}</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="rexperiencia" ></textarea>
                                </div>
                            </div>
                            <div class="form-group gestionada">
                                <label class="col-sm-4 control-label no-padding-right">{{__('Requisitos-Outros:')}}</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="rotros" ></textarea>
                                </div>
                            </div>
                            <div class="form-group gestionada">
                                <label class="col-sm-4 control-label no-padding-right">{{__('Observaciones Internas:')}}</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="observaciones" ></textarea>
                                </div>
                            </div>
                            <div class="form-group ">
                                <span class="col-sm-4 control-label no-padding-right"> TALENTIA SUMMIT 2020 </span>
                                <div class="col-sm-8">
                                    <label class="switch switch-label switch-outline-danger">
                                        <input type="checkbox" name="pertenece_fie" class="switch-input" >
                                        <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="col-sm-4 control-label no-padding-right">Imaxe Destacada</span>
                                <div class="col-sm-8">
                                    <input id="imagen_destacada" type="file" name="imagen_destacada" accept="image/*" >
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-4 control-label no-padding-right">{{__('Estatus')}}</label>
                                <div class="col-sm-8">
                                    <select name="publicada" id="publicada" required style="margin-right: 0;margin-left: 0;width: 100%;">
                                        <option value="1"  >{{__('backend.Pendiente')}}</option>
                                        <option value="2"  >PUBLICADA</option>
                                        <option value="3"  >{{__('backend.Rechazada')}}</option>
                                        <option value="4"  >PAUSADA</option>
                                        <option value="5"  >{{__('backend.Cerrada')}}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group ">
                                <span class="col-sm-4 control-label no-padding-right"> Limitar Inscripciones </span>
                                <div class="col-sm-8">
                                    <div class="col-sm-3">
                                        <label class="switch switch-label switch-outline-danger">
                                            <input type="checkbox" id="limitar_inscripcion" name="limitar_inscripcion" class="switch-input" >
                                            <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                        </label>
                                    </div>
                                    <div id = "maximo_ins" class="col-sm-9" style="display:none">
                                        <div class="col-sm-4">
                                             <label class="control-label no-padding-right">{{__('Máximo')}}</label>
                                        </div>
                                        <div class="col-sm-8">
                                              <input type="number" class="form-control col-sm-8" name="maximo_inscripciones" id ="maximo_inscripciones" />
                                        </div>
                                            
                                             
                                             
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                        <label class="col-sm-4 control-label no-padding-right">Sector</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <select class="chosen-select" name="sector" id="sectors" data-placeholder="Seleccione sector...">
                                                    <option  value="-1">No Definido</option>
                                                    @foreach($sectores as $sector)
                                                    <option  value="{{$sector->id}}">{{$sector->nome}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                            </div>



                        </div>
                        <!--                        <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>{{__('offers.requirements')}}</label>
                                                        <div class="checkbox" style="width: 150px">
                                                            <label>
                                                                <input name="requirements[]" type="checkbox" value="training" class="ace ace-checkbox-2" />
                                                                <span class="lbl"> {{__('offers.training')}}</span>
                                                            </label>
                                                        </div>
                                                        <div class="checkbox" style="width: 150px">
                                                            <label>
                                                                <input name="requirements[]" class="ace ace-checkbox-2" value="experience" type="checkbox" />
                                                                <span class="lbl">{{__('offers.experience')}}</span>
                                                            </label>
                                                        </div>
                                                        <div class="checkbox" style="width: 150px">
                                                            <label>
                                                                <input id="others_check" name="requirements[]" value="" class="ace ace-checkbox-2" type="checkbox" />
                                                                <span class="lbl">{{__('offers.others')}}</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div id="other_requirements_container" data-others="false" class="form-group" style="width: 300px">
                        
                                                        <textarea class="form-control" name="requirements[]" style="height: 110px"></textarea>
                                                    </div>
                                                </div>-->
                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary" name="job_offer" value="Job offer">{{__('offers.save')}}</button>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection