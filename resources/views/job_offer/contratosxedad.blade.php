<div class="col-sm-8 col-lg-8">
    <div class="card text-white bg-light">

        <div class="card-header">Contrataciones por Edad  Año {{$ano}}

            </div>
        </div>
        <div class="card-body">
            <div class="chart-wrapper bg-white">
                <canvas id="totalContratosEdad"></canvas>
            </div>
        </div>
    </div>
</div>


<script src="{{asset('assets/js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('assets/js/chart.min.js')}}"></script>
<script src="{{asset('assets/js/chartjs-plugin-labels.min.js')}}"></script>
<!--<script src="{{asset('node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/js/custom-tooltips.min.js')}}"></script>-->


<script>



var totalContratosEdad = nameTotalContratosEdad = [];


function load_contratosxedad() {
    $.ajax({
        url: "{{url('/contratosxedad')}}"+"?ano="+{{$ano}},
        method: 'GET',
        type: 'JSON',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        beforeSend: function () {},
        success: function (response) {

            coloresdinamicos = [];
            totalContratosEdad = [];
            nameTotalContratosEdad = [];

//                Object.keys(response.totalContratosEdad).forEach(function (key) {
//                    // do something with obj[key]
//                });



            $.each(response.totalContratosEdad, function (key, value) {
                console.log(key, value)
                totalContratosEdad.push(value);
                nameTotalContratosEdad.push(key);
                color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                while ($.inArray(color, coloresdinamicos) > -1) {
                    color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                }
                coloresdinamicos.push(color);
            });

//            alert(totalContratosEdad);

            var pieChartTotalContratosEdad = new Chart($('#totalContratosEdad'), {
                type: 'pie',
                data: {
                    labels: nameTotalContratosEdad,
                    datasets: [{
                            label: 'Rango de Edades',
                            data: totalContratosEdad,
                            backgroundColor:[ 'rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(54, 262, 200)', 'rgb(54, 162, 100)',],
                            hoverBackgroundColor:[ 'rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(54, 262, 200)', 'rgb(54, 162, 100)',],
                        }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: true,
                        position: 'left'
                    },
                    title: {
                        display: true,
                        text: 'Contrataciones por Edad Año ' + {{$ano}}
                    },
                    plugins: {
                        datalabels: {
                            display: true,
                            align: 'bottom',
                            backgroundColor: '#ccc',
                            borderRadius: 3,
                            font: {
                                size: 18,
                            }
                        },
                    },

                    tooltips: {

                    }

                }
            });
        }
    });
}

jQuery(document).ready(function ($) {
    load_contratosxedad();
});


</script>