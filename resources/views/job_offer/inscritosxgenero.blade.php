<div class="col-sm-8 col-lg-8">
    <div class="card text-white bg-light">

        <div class="card-header">Total usuarios por Genero
            <div class="card-header-actions">

            </div>
        </div>
        <div class="card-body">
            <div class="chart-wrapper bg-white">
                <canvas id="inscritosGenero"></canvas>
            </div>
        </div>
    </div>
</div>


<script src="{{asset('assets/js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('assets/js/chart.min.js')}}"></script>
<script src="{{asset('assets/js/chartjs-plugin-labels.min.js')}}"></script>
<!--<script src="{{asset('node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/js/custom-tooltips.min.js')}}"></script>-->


<script>



var totalDistribucionaltasGenero = nameTotalDistribucionaltasGenero = [];


function load_inscritosxgenero() {
    $.ajax({
        url: "{{url('/inscritosxgenero')}}"+"?ano="+{{$ano}},
        method: 'GET',
        type: 'JSON',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        beforeSend: function () {},
        success: function (response) {

            coloresdinamicos = [];
            totalInscritosGenero = [];
            nameTotalInscritosGenero = [];

//                Object.keys(response.totalContratosEdad).forEach(function (key) {
//                    // do something with obj[key]
//                });



            $.each(response.inscritosGenero, function (i, data) {
                console.log(data.name, data.total);
                totalInscritosGenero.push(data.total);
                nameTotalInscritosGenero.push(data.name);
                color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                while ($.inArray(color, coloresdinamicos) > -1) {
                    color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                }
                coloresdinamicos.push(color);
            });

//            alert(totalContratosEdad);

            var pieChartDistribucionAltasGenero = new Chart($('#inscritosGenero'), {
                type: 'pie',
                data: {
                    labels: nameTotalInscritosGenero,
                    datasets: [{
                            label: 'Generos',
                            data: totalInscritosGenero,
                            backgroundColor: [ 'rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', ],
                            hoverBackgroundColor: [ 'rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', ],
                        }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: true,
                        position: 'left'
                    },
                    title: {
                        display: true,
                        text: 'Inscripción de demandantes. Evolución por genero  Año ' + {{$ano}}
                    },
                    plugins: {
                        datalabels: {
                            display: true,
                            align: 'bottom',
                            backgroundColor: '#ccc',
                            borderRadius: 3,
                            font: {
                                size: 18,
                            }
                        },
                    },

                    tooltips: {

                    }

                }
            });
        }
    });
}

jQuery(document).ready(function ($) {
    load_inscritosxgenero();
});


</script>