<div class="col-sm-8 col-lg-8">
    <div class="card text-white bg-light">

        <div class="card-header">Curriculos Enviados por Edad
            <div class="card-header-actions">

            </div>
        </div>
        <div class="card-body">
            <div class="chart-wrapper bg-white">
                <canvas id="totalCurriculosEdad"></canvas>
            </div>
        </div>
    </div>
</div>


<script src="{{asset('assets/js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('assets/js/chart.min.js')}}"></script>
<script src="{{asset('assets/js/chartjs-plugin-labels.min.js')}}"></script>
<!--<script src="{{asset('node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/js/custom-tooltips.min.js')}}"></script>-->


<script>



var totalCurriculosEdad = nameTotalCurriculosEdad = [];


function load_curriculosxedad() {
    $.ajax({
        url: "{{url('/curriculosxedad')}}"+"?ano="+{{$ano}},
        method: 'GET',
        type: 'JSON',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        beforeSend: function () {},
        success: function (response) {

            coloresdinamicos = [];
            totalCurriculosEdad = [];
            nameTotalCurriculosEdad = [];

//                Object.keys(response.totalCurriculosEdad).forEach(function (key) {
//                    // do something with obj[key]
//                });



            $.each(response.totalCurriculosEdad, function (key, value) {
                console.log(key, value)
                totalCurriculosEdad.push(value);
                nameTotalCurriculosEdad.push(key);
                color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                while ($.inArray(color, coloresdinamicos) > -1) {
                    color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                }
                coloresdinamicos.push(color);
            });

//            alert(totalCurriculosEdad);

            var pieChartTotalCurriculosEdad = new Chart($('#totalCurriculosEdad'), {
                type: 'pie',
                data: {
                    labels: nameTotalCurriculosEdad,
                    datasets: [{
                            label: 'Rango de Edades',
                            data: totalCurriculosEdad,
                            backgroundColor: [ 'rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(54, 262, 200)', 'rgb(54, 162, 100)',],
                            hoverBackgroundColor: [ 'rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(54, 262, 200)', 'rgb(54, 162, 100)',],
                        }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: true,
                        position: 'left'
                    },
                    title: {
                        display: true,
                        text: 'Curriculos enviados por Edad Año ' + {{$ano}}
                    },
                    plugins: {
                        datalabels: {
                            display: true,
                            align: 'bottom',
                            backgroundColor: '#ccc',
                            borderRadius: 3,
                            font: {
                                size: 18,
                            }
                        },
                    },

                    tooltips: {

                    }

                }
            });
        }
    });
}

jQuery(document).ready(function ($) {
    load_curriculosxedad();
});


</script>