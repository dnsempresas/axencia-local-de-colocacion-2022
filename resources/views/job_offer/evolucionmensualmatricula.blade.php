<div class="col-sm-8 col-lg-8">
    <div class="card text-white bg-light">

        <div class="card-header">Evolución mensual de la matrícula de los cursos de formación.
            <div class="card-header-actions">

            </div>
        </div>
        <div class="card-body">
            <div class="chart-wrapper bg-white">
                <canvas id="evolucionMensual"></canvas>
            </div>
        </div>
    </div>
</div>


<script src="{{asset('assets/js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('assets/js/chart.min.js')}}"></script>
<script src="{{asset('assets/js/chartjs-plugin-labels.min.js')}}"></script>
<!--<script src="{{asset('node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/js/custom-tooltips.min.js')}}"></script>-->


<script>



var totalEvolucionMensual = [];


function load_evolucionmensual() {
    $.ajax({
        url: "{{url('/evolucionmensualmatricula')}}"+"?ano="+{{$ano}},
        method: 'GET',
        type: 'JSON',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        beforeSend: function () {},
        success: function (response) {

            coloresdinamicos = [];
            totalMeses = [];
            nameMeses = ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC', ];

//                Object.keys(response.totalContratosEdad).forEach(function (key) {
//                    // do something with obj[key]
//                });


            $.each(response.evolucionMensualMatricula, function (i, data) {
                console.log(data.mes, data.total);
                totalMeses.push(data.total);

                color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                while ($.inArray(color, coloresdinamicos) > -1) {
                    color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                }
                coloresdinamicos.push(color);
            });

            console.log('QUI..' + totalMeses);

            var barChartEvolucionMensual = new Chart($('#evolucionMensual'), {
                type: 'bar',
                data: {
                    labels: nameMeses,
                    datasets: [{
                            label: '',
                            data: totalMeses,

                        }]
                },
                options: {
                    responsive: true,
                    "animation": {
      "duration": 1,
      "onComplete": function() {
        var chartInstance = this.chart,
          ctx = chartInstance.ctx;

        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
        ctx.textAlign = 'center';
        ctx.textBaseline = 'bottom';

        this.data.datasets.forEach(function(dataset, i) {
          var meta = chartInstance.controller.getDatasetMeta(i);
          meta.data.forEach(function(bar, index) {
            var data = dataset.data[index];
            ctx.fillText(data, bar._model.x, bar._model.y - 5);
          });
        });
      }
    },
                    legend: {
                        display: false,
                    },
                    title: {
                       display: true,
                        text: 'Evolución mensual de la matrícula de los cursos de formación. Año ' + {{$ano}},
                        position: 'bottom'
                    },

                    plugins: {
                        labels: false,
                        datalabels: {
                            display: true,
                            align: 'bottom',
                            anchor: 'end',
                            formatter: Math.round,
                            font: {
                                weight: 'bold'
                            }
                        },

                    },

                    tooltips: {

                    }


                }
            });
        }
    });
}

jQuery(document).ready(function ($) {
    load_evolucionmensual();
});


</script>