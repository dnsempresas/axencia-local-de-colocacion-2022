<div class="col-sm-8 col-lg-8">
    <div class="card text-white bg-light">

        <div class="card-header">CV enviados a empresas de personas con prestación según la edad
            <div class="card-header-actions">

            </div>
        </div>
        <div class="card-body">
            <div class="chart-wrapper bg-white">
                <canvas id="distribucionAltasEdad"></canvas>
            </div>
        </div>
    </div>
</div>


<script src="{{asset('assets/js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('assets/js/chart.min.js')}}"></script>
<script src="{{asset('assets/js/chartjs-plugin-labels.min.js')}}"></script>
<!--<script src="{{asset('node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/js/custom-tooltips.min.js')}}"></script>-->


<script>



var totalDistribucionAltasxEdad = nameDistribucionAltasxEdad = [];


function load_distribucionAltasxEdad() {
    $.ajax({
        url: "{{url('/distribucionaltasxedad')}}"+"?ano="+{{$ano}},
        method: 'GET',
        type: 'JSON',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        beforeSend: function () {},
        success: function (response) {

            coloresdinamicos = [];
            totalDistribucionAltasxEdad = [];
            namelDistribucionAltasxEdad = [];

//                Object.keys(response.totalCurriculosEdad).forEach(function (key) {
//                    // do something with obj[key]
//                });



            $.each(response.distribucionAltasPorEdad, function (key, value) {
                console.log(key, value)
                totalDistribucionAltasxEdad.push(value);
                namelDistribucionAltasxEdad.push(key);
                color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                while ($.inArray(color, coloresdinamicos) > -1) {
                    color = '#' + parseInt(Math.random() * 0xffffff).toString(16);

                }
                coloresdinamicos.push(color);
            });

//            alert(totalCurriculosEdad);

            var pieChartTotalDistribucionAltasxEdad = new Chart($('#distribucionAltasEdad'), {
                type: 'pie',
                data: {
                    labels: namelDistribucionAltasxEdad,
                    datasets: [{
                            label: 'Rango de Edades',
                            data: totalDistribucionAltasxEdad,
                            backgroundColor: [ 'rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(54, 262, 200)', 'rgb(54, 162, 100)',],
                            hoverBackgroundColor:[ 'rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(54, 262, 200)', 'rgb(54, 162, 100)',],
                        }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: true,
                        position: 'left'
                    },
                    title: {
                        display: true,
                        text: 'Evolución anual de la inscripción de demandantes. Por rangos de edad.  Año ' + {{$ano}}
                    },
                   plugins: {

                        labels: {
                            // render 'label', 'value', 'percentage', 'image' or custom function, default is 'percentage'
                            render: function (args) {
                                // args will be something like:
                                // { label: 'Label', value: 123, percentage: 50, index: 0, dataset: {...} }
                                return  args.value + ' ('+args.percentage+'%) '

                                // return object if it is image
                                // return { src: 'image.png', width: 16, height: 16 };
                            },
                            showActualPercentages: true,
                            

                        }
                    },

                    tooltips: {

                    }

                }
            });
        }
    });
}

jQuery(document).ready(function ($) {
    load_distribucionAltasxEdad();
});


</script>