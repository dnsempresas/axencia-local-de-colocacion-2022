@extends('layouts.app')


@section('titulo_pantalla')
<style>





</style>

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">{{__('Outros')}} </li>
    <li class="breadcrumb-item active"> {{__('Estadisticas SEPE')}} </li>
    <!-- Breadcrumb Menu-->
</ol>
@endsection


@section('content')
@if(Session::has('alert-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
</div>
@endif
@if(Session::has('alert-danger'))
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-danger"></i> {{ Session::get('alert-danger') }}</h4>
</div>
@endif

<div class="col-md-12">
    <div class="card">
        <div class="card-header">{{__('Estadísticas SEPE')}}

        </div>
        <div class="card-body">
            <div class="toolbar">
                <div class="row">
                    <div class="col-md-12">

                        <form>

                            <div class="form-group col-md-3">
                                <label class="col-sm-2 control-label no-padding-right" >{{__('Año')}}</label>
                                <div class="col-sm-8">
                                    <input type="number" value="{{$ano}}" name="ano">

                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <label class="col-sm-2 control-label no-padding-right" >{{__('Mes')}}</label>
                                <div class="col-sm-8">
                                    <select class="js-example-basic-multiple chosen-select" name="mes[]" multiple="multiple">
                                        <option <?php
                                        if (in_array("01", $mes)) {
                                            echo 'selected';
                                        }
                                        ?> value="01">Xaneiro</option>
                                        <option <?php
                                        if (in_array("02", $mes)) {
                                            echo 'selected';
                                        }
                                        ?> value="02">Febreiro</option>
                                        <option <?php
                                        if (in_array("03", $mes)) {
                                            echo 'selected';
                                        }
                                        ?> value="03">Marzo</option>
                                        <option <?php
                                        if (in_array("04", $mes)) {
                                            echo 'selected';
                                        }
                                        ?> value="04">Abril</option>
                                        <option <?php
                                        if (in_array("05", $mes)) {
                                            echo 'selected';
                                        }
                                        ?> value="05">Maio</option>
                                        <option <?php
                                        if (in_array("06", $mes)) {
                                            echo 'selected';
                                        }
                                        ?> value="06">Xuño</option>
                                        <option <?php
                                        if (in_array("07", $mes)) {
                                            echo 'selected';
                                        }
                                        ?> value="07">Xullo</option>
                                        <option <?php
                                        if (in_array("08", $mes)) {
                                            echo 'selected';
                                        }
                                        ?> value="08">Agosto</option>
                                        <option <?php
                                        if (in_array("09", $mes)) {
                                            echo 'selected';
                                        }
                                        ?> value="09">Setembro</option>
                                        <option <?php
                                        if (in_array("10", $mes)) {
                                            echo 'selected';
                                        }
                                        ?> value="10">Outubro</option>
                                        <option <?php
                                        if (in_array("11", $mes)) {
                                            echo 'selected';
                                        }
                                        ?> value="11">Novembro</option>
                                        <option <?php
                                        if (in_array("12", $mes)) {
                                            echo 'selected';
                                        }
                                        ?> value="12">Desembro</option>


                                    </select>

                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Actualizar</button>
                        </form>


                    </div>


                </div>
            </div>
        </div>


    </div>

    

    <div class="card">
        <div class="card-header">{{__('AGREGADOS')}}

        </div>
        <div class="card-body">
            <div class="toolbar">


                <div class="row card-header" style="margin-top:10px; padding-top: 10px"> 

                    <div class="col-md-3"> TOTAL PERSONAS: 
                        <span> {{$totalPersonas}} </span>

                    </div>
                    <div class="col-md-3"> TOTAL_NUEVAS_REGISTRADAS:
                        <span> {{$totalNuevasRegistradas}} </span>

                    </div>
                    <div class="col-md-3"> TOTAL_PERSONAS_PERCEPTORES:
                        <span> {{$totalPersonasPerceptores}} </span>

                    </div>
                    <div class="col-md-3"> TOTAL_PERSONAS_INSERCION:
                        <span> {{$totalPersonasInsercion}} </span>

                    </div>
                    <div class="col-md-3"> TOTAL_OFERTAS:
                        <span> {{$totalOfertas}} </span>

                    </div>
                    <div class="col-md-3"> TOTAL_OFERTAS_ENVIADAS:
                        <span> {{$totalOfertaEnviadas}} </span>

                    </div>
                    <div class="col-md-3"> TOTAL_OFERTAS_CUBIERTAS:
                        <span> {{$totalOfertasCubiertas}} </span>

                    </div>
                    <div class="col-md-3"> TOTAL_PUESTOS:
                        <span> {{$totalPuestos}} </span>

                    </div>
                    <div class="col-md-3"> TOTAL_PUESTOS_CUBIERTOS:
                        <span> {{$totalPuestosCubiertos}} </span>

                    </div>
                    <div class="col-md-3"> TOTAL_CONTRATOS:
                        <span> {{$totalContratos}} </span>

                    </div>
                    <div class="col-md-3"> TOTAL_CONTRATOS_INDEFINIDOS:
                        <span> {{$totalContratosIndefinidos}} </span>

                    </div>
                    <div class="col-md-3"> TOTAL_PERSONAS_COLOCADAS:
                        <span> {{$totalPersonasColocadas}} </span>

                    </div>


                </div>
                

                <div>
                    @if($UserStats->count())
                    <div class="row mr-auto" style="margin-top:10px; padding-top: 10px; "> 
                    <a class="btn btn-success" target="blank" href="{{url("/panel/descargarXml")}}"> Descargar XML</a>
                </div>

                    @php
                    // Inicio Creacion XML
                    $XML = new XMLWriter();

                    $XML->openUri("AXENCIA_XML_DEFINITIVO.xml");
                    $XML->setIndent(true);
                    $XML->setIndentString("\t");
                    $XML->startDocument('1.0', 'utf-8', 'yes');
                    // Inicio del nodo raíz
                    $XML->startElement("ENVIO_ENPI");
                    $XML->writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                    $XML->startElement("ENVIO_MENSUAL");

                    $XML->startElement("CODIGO_AGENCIA");
                    $XML->text('1200000008');
                    $XML->fullEndElement();

                    $XML->startElement("AÑO_MES_ENVIO");
                    if ( !( count($mes)==0 || count($mes)==12 ) ) {
                    $XML->text($ano.$mes[0]);
                    } else {$XML->text($ano.'99');}
                    $XML->fullEndElement();
                    
                    if ( !( count($mes)==0 || count($mes)==12 ) ) {

                    $XML->startElement("ACCIONES_REALIZADAS");

                    @endphp
                    
                    

                    @foreach($UserStats as $stat)
                    @php
                    
                    $XML->startElement("ACCION"); 

                    //Elemento ID_TRABAJADOR
                    $XML->startElement("ID_TRABAJADOR");
                    $XML->text($stat->ID_TRABAJADOR);
                    $XML->fullEndElement();

                    //Elemento NOMBRE_TRABAJADOR
                    $XML->startElement("NOMBRE_TRABAJADOR");
                    $XML->text($stat->NOMBRE_TRABAJADOR);
                    $XML->fullEndElement();

                    //Elemento APELLIDO1_TRABAJADOR
                    $XML->startElement("APELLIDO1_TRABAJADOR");
                    $XML->text($stat->APELLIDO1_TRABAJADOR);
                    $XML->fullEndElement();

                    //Elemento APELLIDO2_TRABAJADOR
                    $XML->startElement("APELLIDO2_TRABAJADOR");
                    $XML->text($stat->APELLIDO2_TRABAJADOR);
                    $XML->fullEndElement();

                    //Elemento FECHA_NACIMIENTO
                    $XML->startElement("FECHA_NACIMIENTO");
                    $XML->text($stat->FECHA_NACIMIENTO);
                    $XML->fullEndElement();

                    //Elemento SEXO_trabajador
                    $XML->startElement("SEXO_TRABAJADOR");
                    $XML->text($stat->SEXO_TRABAJADOR);
                    $XML->fullEndElement();

                    //Elemento NIVEL_FORMATIVO
                    $XML->startElement("NIVEL_FORMATIVO");
                    $XML->text($stat->NIVEL_FORMATIVO);
                    $XML->fullEndElement();

                    //Elemento DISCAPACIDAD
                    $XML->startElement("DISCAPACIDAD");
                    $XML->text($stat->DISCAPACIDAD);
                    $XML->fullEndElement();

                    //Elemento INMIGRANTE
                    $XML->startElement("INMIGRANTE");
                    $XML->text($stat->INMIGRANTE);
                    $XML->fullEndElement();

                    //Elemento COLOCACION
                    $XML->startElement("COLOCACION");
                    $XML->text($stat->COLOCACION);
                    $XML->fullEndElement();

                    if($stat->COLOCACION == 'S') {
                    //Elemento FECHA_COLOCACION
                    $XML->startElement("FECHA_COLOCACION");
                    $XML->text($stat->FECHA_COLOCACION);
                    $XML->fullEndElement();

                    //Elemento TIPO_CONTRATO
                    $XML->startElement("TIPO_CONTRATO");
                    $XML->text($stat->TIPO_CONTRATO);
                    $XML->fullEndElement();

                    //Elemento CIF_NIF_EMPRESA
                    $XML->startElement("CIF_NIF_EMPRESA");
                    $XML->text($stat->CIF_NIF_EMPRESA);
                    $XML->fullEndElement();

                    //Elemento RAZON_SOCIAL_EMPRESA
                    $XML->startElement("RAZON_SOCIAL_EMPRESA");
                    $XML->text($stat->RAZON_SOCIAL_EMPRESA);
                    $XML->fullEndElement();
                    }

                    @endphp
                    @php  $XML->fullEndElement (); // Fin elemento accion
                    @endphp
                    @endforeach
                    @php  $XML->fullEndElement (); // Fin elemento acciones realizadas
                    }
                    @endphp
                    @php
                     if ( ( count($mes)==0 || count($mes)==12 ) ) {

                    $XML->startElement("ACCIONES_REALIZADAS");
                    $XML->endElement();
                    }
                    @endphp


                    @php
                    //Nodo DATOS_AGREGADOS
                    $XML->startElement("DATOS_AGREGADOS");

                    $XML->startElement("TOTAL_PERSONAS");
                    $XML->text($totalPersonas);
                    $XML->fullEndElement();

                    $XML->startElement("TOTAL_NUEVAS_REGISTRADAS");
                    $XML->text($totalNuevasRegistradas);
                    $XML->fullEndElement();

                    $XML->startElement("TOTAL_PERSONAS_PERCEPTORES");
                    $XML->text($totalPersonasPerceptores);
                    $XML->fullEndElement();

                    $XML->startElement("TOTAL_PERSONAS_INSERCION");
                    $XML->text($totalPersonasInsercion);
                    $XML->fullEndElement();

                    $XML->startElement("TOTAL_OFERTAS");
                    $XML->text($totalOfertas);
                    $XML->fullEndElement();

                    $XML->startElement("TOTAL_OFERTAS_ENVIADAS");
                    $XML->text($totalOfertaEnviadas);
                    $XML->fullEndElement();

                    $XML->startElement("TOTAL_OFERTAS_CUBIERTAS");
                    $XML->text($totalOfertasCubiertas);
                    $XML->fullEndElement();

                    $XML->startElement("TOTAL_PUESTOS");
                    $XML->text($totalPuestos);
                    $XML->fullEndElement();

                    $XML->startElement("TOTAL_PUESTOS_CUBIERTOS");
                    $XML->text($totalPuestosCubiertos);
                    $XML->fullEndElement();

                    $XML->startElement("TOTAL_CONTRATOS");
                    $XML->text($totalContratos);
                    $XML->fullEndElement();

                    $XML->startElement("TOTAL_CONTRATOS_INDEFINIDOS");
                    $XML->text('1');
                    $XML->fullEndElement();

                    $XML->startElement("TOTAL_PERSONAS_COLOCADAS");
                    $XML->text($totalPersonasColocadas);
                    $XML->fullEndElement();

                    $XML->fullEndElement(); // Final datos_agregados


                    $XML->fullEndElement (); // Final Envio_mensual

                    $XML->endElement(); // Final del nodo raíz, "envio_enpi"
                    $XML->endDocument(); // Final del documento

                    @endphp

                    @else
                    <h2>No hay datos en la tabla, documento xml no generado</h2>
                    @endif

                </div>

            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">{{__('Graficas')}}

        </div>

        <div class="card-body">
            <div class="toolbar">

                <div class="col-sm-6 col-lg-6">
                    <select  id="grafico">

                        <option value="job_offer/contratosxedad"> Contrataciones por Edad</option>
                        <option value="job_offer/contratosxformacion"> Contrataciones por Formacion</option>
                        <option value="job_offer/contratosxgenero"> Contrataciones por Genero</option>
                        <option value="job_offer/curriculosxedad"> Curriculos Enviados por Edad</option>
                        <option value="job_offer/curriculosxformacion"> Curriculos Enviados por Formación</option>
                        <option value="job_offer/curriculosxgenero"> Curriculos Enviados por Genero</option>
                        <option value="job_offer/curriculosprestacionxedad"> Curriculos Enviados por Prestación y Edad</option>
                        <option value="job_offer/distribucionaltasxedad"> Distruibuición de Altas por Edad</option>
                        <option value="job_offer/distribucionaltasxformacion"> Distruibuición de Altas por Formación Académica</option>
                        <option value="job_offer/distribucionaltasxgenero"> Distruibuición de Altas por Genero</option>
                        <option value="job_offer/evolucionmensualaltas"> Evolución de Altas por Mes</option>
                        <option value="job_offer/evolucionmensualcontratos"> Evolución de Contrataciones por Mes</option>
                        <option value="job_offer/evolucionmensualcursos"> Evolución de Cursos por Mes</option> 
                        <option value="job_offer/evolucionmensualmatricula"> Evolución de Matricula Cursos por Mes</option>
                        <option value="job_offer/evolucionmensualpuestos"> Evolución de Puestos por Mes</option>
                        <option value="job_offer/inscritosxedad"> Inscritos por Edad</option>
                        <option value="job_offer/inscritosxformacion"> Inscritos por Formación</option>
                        <option value="job_offer/inscritosxgenero"> Inscritos por Genero</option>







                    </select>

                </div>
            </div>

            <div class="grafico">
                @include('job_offer/contratosxedad')
            </div>


        </div>
    </div>


</div>




<script src="{{asset('assets/js/jquery-2.1.4.min.js')}}"></script>
<script>

$(document).ready(function () {



    table = $('#table-inscritos-cv').DataTable({
        "scrollY": 300,
        "scrollX": true,
        autoWidth: true,

        language: {

            url: '/assets/js/datatable_' + $('#imgidiomaselect').attr('alt') + '.json'//Ubicacion del archivo con el json del idioma.
        },

        buttons: [
            {
                extend: 'excel',
                text: '<span class="fa fa-file-excel-o"></span> Exportar a Excel',
                exportOptions: {

                    modifier: {
                        search: 'applied',
                        page: 'all',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'csv',
                text: '<span class="glyphicon-export"></span> Exportar a Csv',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        page: 'all',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'pdf',
                text: '<span class="fa fa-file-pdf-o"></span> Exportar a Pdf',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        page: 'all',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'print',
                text: '<span class="fa fa-print bigger-110"></span> Imprimir',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        page: 'all',
                        order: 'applied'
                    }
                }
            }
        ],

    });



    $(document).on('change', '#grafico', function () {

        var ruta = $(this).val();
        // alert(ruta);


        $.ajax({
            url: "{{url('/rendergrafico')}}",
            method: 'POST',
            data: {'ruta': ruta, 'ano':"{{$ano}}"},
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            beforeSend: function () {},
            success: function (response) {

                $('.grafico').html(response);
                return true;
            }
        });
    });

});

</script>
@endsection