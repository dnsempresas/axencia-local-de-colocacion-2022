@extends('layouts.app')

@section('titulo_pantalla')

@endsection

@section('content')
@if ($errors->any())
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-ban"></i>{{__('grade.errors')}}</h4>
    @foreach ($errors->all() as $error)
    <p>{{ $error }}</p>
    @endforeach
</div>
@endif

@if(Session::has('alert-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> {{ Session::get('alert-success') }}</h4>
</div>
@endif
<form style="padding: 30px;" id="gradeForm" method="POST" action="{{route('estudios.update', $grade->id) }}" accept-charset="UTF-8" class="form-group">
    {{method_field('PATCH')}}
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <div>
                        {{__('Editar Estudios')}}
                    </div>
                </div>
                <div class="card-body">
                    <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_description_" class="b-form-group form-group">
                        <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Nombre')}}</legend>
                        <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                            <div role="group" class="input-group">								
                                <input type="text" class="form-control" name="description" required value="{{ $grade->description }}">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_nome_" class="b-form-group form-group">
                        <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Nome')}}</legend>
                        <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                            <div role="group" class="input-group">								
                                <input type="text" class="form-control" name="nome" required value="{{ $grade->nome }}">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_nome_" class="b-form-group form-group">
                        <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Nivel Formativo')}}</legend>
                        <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                            <div role="group" class="input-group">								
                                <select  id="nivelformativo" name="nivelformativo">

                                    <option  <?php if ($grade->nivelformativo == '00') echo 'selected'; ?> value="00"> Sin Estudios </option>
                                    <option <?php if ($grade->nivelformativo == '10') echo 'selected'; ?> value="10"> Estudios Primarios </option>
                                    <option  <?php if ($grade->nivelformativo == '20') echo 'selected'; ?> value="20"> Estudios Secundarios</option>
                                    <option  <?php if ($grade->nivelformativo == '30') echo 'selected'; ?> value="30"> Estudios PostSecundarios </option>


                                </select>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset id="" role="group" aria-labelledby="__BVID__232__BV_label_" aria-describedby="__BVID__232__BV_nome_" class="b-form-group form-group">
                        <legend id="__BVID__232__BV_label_" class="col-form-label pt-0">{{__('Titulacion')}}</legend>
                        <div role="group" aria-labelledby="__BVID__232__BV_label_" class="">
                            <div role="group" class="input-group">								
                                <select  id="titulacion" name="titulacion">

                                    <option <?php if ($grade->titulacion == 'Sin Estudios') echo 'selected'; ?> value="Sin Estudios">Sin Estudios</option>
                                    <option <?php if ($grade->titulacion == 'Diplomatura') echo 'selected'; ?> value="Diplomatura">Estudios Primarios</option>
                                    <option  <?php if ($grade->titulacion == 'Estudios Secundarios') echo 'selected'; ?> value="Estudios Secundarios">Estudios Secundarios</option>

                                    <option  <?php if ($grade->titulacion == 'Bachillerato') echo 'selected'; ?> value="Bachillerato">Bachillerato</option>

                                    <option  <?php if ($grade->titulacion == 'FP Ciclo Medio') echo 'selected'; ?> value="FP Ciclo Medio">FP Ciclo Medio</option>
                                    <option  <?php if ($grade->titulacion == 'FP Ciclo Superior') echo 'selected'; ?> value="FP Ciclo Superior">FP Ciclo Superior</option>
                                    <option  <?php if ($grade->titulacion == 'Diplomatura') echo 'selected'; ?> value="Diplomatura">Diplomatura</option>
                                    <option  <?php if ($grade->titulacion == 'Licenciatura') echo 'selected'; ?> value="Licenciatura">Licenciatura</option>
                                    <option  <?php if ($grade->titulacion == 'Master') echo 'selected'; ?> value="Master">Master</option>
                                    <option  <?php if ($grade->titulacion == 'Doctorado') echo 'selected'; ?> value="Doctorado">Doctorado</option>



                                </select>
                            </div>
                        </div>
                    </fieldset>

                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary custom-btn" name="signup" value="Sign up">{{__('grade.save')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
</div>
@endsection
