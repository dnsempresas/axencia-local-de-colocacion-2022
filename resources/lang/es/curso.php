<?php

return [

    'inicio'=>'Inicio',
    'modulo'=>'Curso',
    'titulo'=>'Título',
    'ubicacion'=>'Lugar de Celebración',
    'plazas_total'=>'Plazas Total',
    'edad_min'=>'Edad Mínima',
    'disponibilidad'=>'Disponibilidad',
    'limpiar_filtros'=>'Limpiar Filtros',
    'filtrar'=>'Filtrar',
    'descripcion'=>'Descripción',
    'duracion'=>'Duración(horas)',
    'plazas'=>'No. de Plazas',
    'edades_recomendadas'=>'Edad Recomendada',
    'agregar'=>'Agregar',
    'nuevo_curso' => 'Nuevo curso',
    'editar_curso' => 'Editar curso',
    'guardar'=>'Guadar',
    'errors'=>'Errores',
    'ver_mas_detalle'=> 'Ver mas Detalle',
    'descripcion_larga'=>'Descripción larga',
    'contenido'=>'Contenido',
    'fecha'=>'Fecha',
    'fecha_inicio'=>'Fecha Inicio',
    'fecha_fin'=>'Fecha Fin',
    'desde'=>'desde',
    'hasta'=>'hasta',
    'sectoraplica'=>'Sector al que aplica',
    'perteneceafie'=>'TALENTIA SUMMIT 2019',
    'precio'=>'Precio',
    'requirimientos'=>'Requirimientos',
    'observaciones'=>'Observaciones',
    'imagenDestacada'=>'Imagen destacada',
    'guardar'=>'Guardar',
    'activado'=>'ACTIVO',
    'Plazas'=>'Prazas',
    'horas_toltales'=>'Horas Totales'
    
    

];
