<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'categorias'=>'Categorias',
    'add'=>'Agregar',
    'new_categoria_label' => 'Nueva Categoria',
    'edit_categoria_label' => 'Editar categoria',
    'name' => 'Nombre',
    'name_gl' => 'Nome',
    'save'=>'Guardar',
    'errors'=>'Errores',

];
