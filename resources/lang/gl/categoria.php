<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'categorias'=>'Categorias',
    'add'=>'Engadir',
    'new_categoria_label' => 'Novo categoria o nova',
    'edit_categoria_label' => 'Editar categoria o nova',
    'name' => 'Nombre',
    'name_gl' => 'Nome',
    'save'=>'Gardar',
    'errors'=>'Errores',

];
