<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'boletines'=>'Boletíns',
    'Boletin'=>'Boletin',
    'add'=>'Engadir',
    'new_boletin_label' => 'Nuevo Boletin',
    'edit_boletin_label' => 'Editar Boletin',
    'name' => 'Nombre',
    'name_gl' => 'Nome',
    'save'=>'Gardar',
    'errors'=>'Errors',

];
