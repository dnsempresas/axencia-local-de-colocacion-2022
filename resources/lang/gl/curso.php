<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'cursos'=>'cursos',
    'add'=>'Engadir',
    'new_contract_label' => 'Novo curso',
    'edit_contract_label' => 'Editar curso',
    'name' => 'Nombre',
    'name_gl' => 'Nome',
    'save'=>'Gardar',
    'errors'=>'Errores',
    
    'inicio'=>'Inicio',
    'modulo'=>'Curso',
    'titulo'=>'Título',
    'ubicacion'=>'Lugar de Celebración',
    'plazas_total'=>'Prazas Total',
    'edad_min'=>'Idade Mínima',
    'disponibilidad'=>'Dispoñibilidade',
    'limpiar_filtros'=>'Limpar Filtros',
    'filtrar'=>'Filtrar',
    'descripcion'=>'Descrición',
    'duracion'=>'Duración (horas)',
    'plazas'=>'No. de Prazas',
    'edades_recomendadas'=>'Idade recomendada',
    'agregar'=>'Engadir',
    'nuevo_curso' => 'Novo curso',
    'editar_curso' => 'Editar curso',
    'guardar'=>'Gardar',
    'errors'=>'Erros',
    'ver_mas_detalle'=> 'Ver máis detalle',
    'descripcion_larga'=>'Descrición longa',
    'contenido'=>'Contido',
    'fecha'=>'Data',
    'fecha_inicio'=>'Data Inicio',
    'fecha_fin'=>'Data Fin',
    'desde'=>'dende',
    'hasta'=>'ata',
    'sectoraplica'=>'Sector o que aplica',
    'perteneceafie'=>'TALENTIA SUMMIT 2020',
    'precio'=>'Prezo',
    'requirimientos'=>'Requerimentos',
    'observaciones'=>'Observacións',
    'imagenDestacada'=>'Imaxe destacada',
    'activado'=>'ACTIVO',
    'Plazas'=>'Prazas',
    'horas_totales'=>'Horas Totais'

];