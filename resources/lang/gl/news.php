<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'list'=>'Novas',
    'add'=>'Engadir',
    'new_news_label' => 'Nova noticia',
    'edit_news_label' => 'Editar noticia',
    'title' => 'titulo',
    'before_title' => 'antetitulo',
    'entry'=>'entradilla',
    'body'=>'noticia',
    'publishedAt'=>'Fecha de Publicacion',
    'expiredAt'=>'Fecha de Expiracion',
    'save'=>'Gardar',
    'errors'=>'Errores',

];
