<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'offers'=>'Ofertas',
    'add'=>'Engadir',
    'new_offer_label' => 'Nova oferta de emprego',
    'edit_offer_label' => 'Editar oferta de emprego',
    'requested_job' => 'Ocupación solicitada',
    'vacancy' => 'Nº de postos',
    'tasks' => 'Tarefas a desenrolar',
    'due_date' => 'Prazo de presentación de C.V.',
    'contract_type' => 'Tipo de contrato',
    'start_date' => 'Data  prevista de incorporación',
    'duration' => 'Duración do contrato',
    'locality' => 'Concello',
    'hours'=>'Horario',
    
    'salary'=>'Salario',
    'requirements'=>'Requisitos',
    'training'=>'Formacion',
    'experience'=>'Experiencia',
    'others'=>'Otros',
    'save'=>'Gardar',
    'errors'=>'Errores',
    'filter'=>'filtrar',
    'disponibilidad'=>'Xornada Laboral'

];
