<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'disponibilidads'=>'Tipo de Jornada',
    'add'=>'Engadir',
    'new_contract_label' => 'Novo Tipo de Jornada',
    'edit_contract_label' => 'Editar Tipo de Jornada',
    'name' => 'Nombre',
    'name_gl' => 'Nome',
    'save'=>'Gardar',
    'errors'=>'Errores',

];
