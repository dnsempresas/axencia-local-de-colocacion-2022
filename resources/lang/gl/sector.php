<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'sectores'=>'Sectores',
    'add'=>'Engadir',
    'new_sector_label' => 'Novo sector de emprego',
    'edit_sector_label' => 'Editar sector de emprego',
    'name' => 'Nombre',
    'name_gl' => 'Nome',
    'save'=>'Guardar',
    'errors'=>'Errores',

];
