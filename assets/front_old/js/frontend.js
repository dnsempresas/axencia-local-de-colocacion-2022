/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(document).ready(function ($) {


//    /* Custom select design */
//    jQuery('.drop-down').append('<div class="button"></div>');
//    jQuery('.drop-down').append('<ul class="select-list"></ul>');
//    jQuery('.drop-down select option').each(function () {
//        var bg = jQuery(this).css('background-image');
//        jQuery('.select-list').append('<li class="clsAnchor"><span value="' + jQuery(this).val() + '" class="' + jQuery(this).attr('class') + '" style=background-image:' + bg + '>' + jQuery(this).text() + '</span></li>');
//    });
//    jQuery('.drop-down .button').html('<span style=background-image:' + jQuery('.drop-down select').find(':selected').css('background-image') + '>' + jQuery('.drop-down select').find(':selected').text() + '</span>' + '<a href="javascript:void(0);" class="select-list-link">&nbsp;&nbsp;</a>');
//    jQuery('.drop-down ul li').each(function () {
//        if (jQuery(this).find('span').text() == jQuery('.drop-down select').find(':selected').text()) {
//            jQuery(this).addClass('active');
//        }
//    });
//    jQuery('.drop-down .select-list span').on('click', function ()
//    {
//        var dd_text = jQuery(this).text();
//        var dd_img = jQuery(this).css('background-image');
//        var dd_val = jQuery(this).attr('value');
//        jQuery('.drop-down .button').html('<span style=background-image:' + dd_img + '>' + dd_text + '</span>' + '<a href="javascript:void(0);" class="select-list-link">&nbsp;&nbsp;</a>');
//        jQuery('.drop-down .select-list span').parent().removeClass('active');
//        jQuery(this).parent().addClass('active');
//        $('.drop-down select[name=options]').val(dd_val);
//        $('.drop-down .select-list li').slideUp();
//    });
//    jQuery('.drop-down .button').on('click', 'a.select-list-link', function ()
//    {
//        jQuery('.drop-down ul li').slideToggle();
//    });
//    /* End */
    jQuery('.idiomaselected').on('click', function ()
    {

        var img = $(this).data("img");
        jQuery('#imgidiomaselect').attr('src', img);

    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });



    $('#view-menu-mobile').on('click', function () {
        var x = document.getElementById("menu-mobile");
        if (x.style.display === "block") {
            x.style.display = "none";
        } else {
            x.style.display = "block";
        }
    }
    );


    $('#blogin').on('click', function () {
        var username = $('#front_user_login').val();
        var pswd = $('#front_user_pass').val();
        $.ajax({
            url: 'user/login',
            type: 'POST',
            data: {
                name: username,
                password: pswd
            },
            dataType: 'json',
            success: function (response) {
                if (response.message == 'Exitoso') {
                    alert('Bienvenido')
                    console.log(response.userdata);
                    if (response.userdata.role_id == 4) {
                        window.location.href = '/admin';
                    }
                } else {
                    alert('Login incorrecto, intente de nuevo')
                }
            }

        });
    }
    );
});