jQuery(document).ready(function ($) {

    $.mask.definitions['~'] = '[+-]';

    $('#birth').mask('99/99/9999');




    $('#fecha_inicio').datepicker({
        defaultDate: new Date(),
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',

    }).on('changeDate', function (selected) {

    }).on('clearDate', function (selected) {

    });
    $('#fecha_fin').datepicker({
        defaultDate: new Date(),
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'
    }).on('changeDate', function (selected) {

    }).on('clearDate', function (selected) {

    });


    $('#reset-filter').on('click', function () {


        $('#nombre').val('');
        $('#ubicacion').val('');
        $('#plazas').val('');
        $('#edades_recomendadas').val('');
        $('#plazas_disponibles').val('');



        // offer_table.search('').columns().search('').draw();
        $("#formfiltros").submit();

    });

    $('.time').timepicker({
        defaultTime: false,
        use24hours: true,
        timeFormat: 'HH:mm'
    });


    $('.fecha').mask('99/99/9999');


});
