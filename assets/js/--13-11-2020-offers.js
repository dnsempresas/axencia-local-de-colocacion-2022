jQuery(document).ready(function ($) {
    $('#due_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl',
//        startDate: new Date()
    });

    $('#start_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl',
        startDate: new Date()
    });
    $('#start_date_min').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl'
    }).on('changeDate', function (selected) {
        var startDate = new Date(selected.date.valueOf());
        $('#start_date_max').datepicker('setStartDate', startDate);
    }).on('clearDate', function (selected) {
        $('#start_date_max').datepicker('setStartDate', null);
    });
    $('#start_date_max').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl'
    }).on('changeDate', function (selected) {
        var endDate = new Date(selected.date.valueOf());
        $('#start_date_min').datepicker('setEndDate', endDate);
    }).on('clearDate', function (selected) {
        $('#start_date_min').datepicker('setEndDate', null);
    });
    
    $('#public_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl',
        startDate: new Date()
    });
    $('#public_date_min').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl'
    }).on('changeDate', function (selected) {
        var publicDate = new Date(selected.date.valueOf());
        $('#public_date_max').datepicker('setStartDate', publicDate);
    }).on('clearDate', function (selected) {
        $('#public_date_max').datepicker('setStartDate', null);
    });
    $('#public_date_max').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl'
    }).on('changeDate', function (selected) {
        var endDate = new Date(selected.date.valueOf());
        $('#public_date_min').datepicker('setEndDate', endDate);
    }).on('clearDate', function (selected) {
        $('#public_date_min').datepicker('setEndDate', null);
    });

    $('#due_date_min').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl'
    }).on('changeDate', function (selected) {
        var startDate = new Date(selected.date.valueOf());
        $('#due_date_max').datepicker('setStartDate', startDate);
    }).on('clearDate', function (selected) {
        $('#due_date_max').datepicker('setStartDate', null);
    });
    $('#due_date_max').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl'
    }).on('changeDate', function (selected) {
        var endDate = new Date(selected.date.valueOf());
        $('#due_date_min').datepicker('setEndDate', endDate);
    }).on('clearDate', function (selected) {
        $('#due_date_min').datepicker('setEndDate', null);
    });


    $('.dataTables_length').addClass('bs-select');



    var offer_table = $('#offer-table').DataTable({
        responsive: true,
        //pagingType: 'input',
        //"bPaginate": true,

        language: {

            url: '/assets/js/datatable_' + $('#imgidiomaselect').attr('alt') + '.json'//Ubicacion del archivo con el json del idioma.
        },
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: "/admin/offers"
        },
        "columns": [
            {data: 'id'},
            {data: 'propietario'},
            {data: 'requested_job'},
            {data: 'vacancy'},
            {data: 'tasks'},
            {data: 'created_at'},
            
            {data: 'contract_type'},
            {data: 'start_date'},
            {data: 'duration'},
            {data: 'locality'},
            {data: 'hours'},
            {data: 'salary'},
            {data: 'publicada'},
            {data: 'province'},
            {data: 'tipooferta'},
            {data: 'options'},
            {data: 'concello'},
            {data: 'due_date'},
            
            ],
        "columnDefs": [
            {
                "targets": [4],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [8],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [9],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [10],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [11],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [13],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [14],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [15],
                "visible": true,
                "searchable": false,
                "orderable": false,
            },
             {
                "targets": [16],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [17],
                "visible": false,
                "searchable": false
            },
        ],
        "bSort": true,
        "bFilter": true,

        "scrollX": true,

        dom: 'Bfrtip',
        order: [[5, 'desc']],
        buttons: [
            {
                extend: 'excel',
                text: '<span class="fa fa-file-excel-o"></span> Exportar a Excel',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,14,15,16,17], //Your Colume value those you want
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }

            },
            {
                extend: 'csv',
                text: '<span class="glyphicon-export"></span> Exportar a Csv',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'pdf',
                text: '<span class="fa fa-file-pdf-o"></span> Exportar a Pdf',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'print',
                text: '<span class="fa fa-print bigger-110"></span> Imprimir',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            }

        ],
    });

    $('#offer-filter').on('click', function () {
        offer_table.columns(0).search($('#propietario').val());
        offer_table.columns(1).search($('#requested_job').val());
        offer_table.columns(2).search($('#vacancy').val());
        offer_table.columns(3).search($('#tasks').val());
        offer_table.columns(4).search($('#public_date_min').val() + '-' + $('#public_date_max').val());
       
        offer_table.columns(5).search($('#contract_type').val());
        offer_table.columns(6).search($('#start_date_min').val() + '-' + $('#start_date_max').val());
        offer_table.columns(7).search($('#duration_min').val() + '-' + $('#duration_max').val());
        offer_table.columns(8).search($('#locality').val());
        offer_table.columns(10).search($('#salary_min').val() + '-' + $('#salary_max').val());
        offer_table.columns(11).search($('#publicada').val());
        offer_table.columns(12).search($('#provincejob').val());
        offer_table.columns(13).search($('#tipooferta').val());
        offer_table.columns(15).search($('#concellojob').val());
        offer_table.columns(16).search($('#id').val());
         offer_table.columns(17).search($('#due_date_min').val() + '-' + $('#due_date_max').val());
        
        offer_table.draw();
    });
    $('#reset-filter').on('click', function () {
        var pathname = window.location.pathname;

        if (pathname != '/empresas/ofertas')
        {

            $('#propietario').val('');
        }

        $('#requested_job').val('');
        $('#vacancy').val('');
        $('#tasks').val('');
        $('#due_date_min').val('');
        $('#due_date_max').val('');

        //$("option:selected").removeAttr("selected");

        $('#contract_type').val('');

        $('#contract_type').val('').trigger('chosen:updated');


        $('#start_date_min').val('');
        $('#start_date_max').val('');
        $('#public_date_min').val('');
        $('#public_date_max').val('');
        $('#duration_min').val('');
        $('#duration_max').val('');
        $('#locality').val('');
        $('#salary_min').val('');
        $('#salary_max').val('');
        $('#publicada').val('');
        $('#provincejob').val('');
        $('#concellojob').val('');
        $('#id').val('');

        //

        // offer_table.search('').columns().search('').draw();
        $('#offer-filter').trigger('click');

    });
    // $.validator.setDefaults({ignore: ":hidden:not(.chosen-select)"}); //for all select having class .chosen-select

    $.validator.setDefaults({ignore: ":hidden:not(select)"});

// validation of chosen on change
    if ($("select.chosen-select").length > 0) {
        $("select.chosen-select").each(function () {
            if ($(this).attr('required') !== undefined) {
                $(this).on("change", function () {
                    $(this).valid();
                });
            }
        });
    }
    ;

    $('#offerForm').validate({

        errorPlacement: function (error, element) {
            console.log("placement");
            if (element.is("select.chosen-select")) {
                console.log("placement for chosen");
                // placement for chosen
                element.next("div.chzn-container").append(error);
            } else {
                // standard placement
                error.insertAfter(element);
            }
        },
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            propietario: {required: true},
            requested_job: {required: true},
            vacancy: {required: true},
            tasks: {required: true},
//            due_date: {required: true},
            contract_type: {required: true},
            start_date: {required: true},

//            duration: {required: function (element) {
//                    if ($('#contract_type').val() == 1) {
//                        return true;
//                    } else {
//                        return false;
//                    }
//                },
//                    },
            provincejob: {required: true},
            province_chosen: {required: true},
            concellojob: {required: true},
            concello_chosen: {required: true},
            disponibilidad: {required: true},
            tipooferta: {required: true},

        },
        messages: {
            propietario: {
                required: 'Debe encher este campo'
            },
            requested_job: {
                required: 'Debe encher este campo'
            },
            vacancy: {
                required: 'Debe encher este campo'
            },
            tasks: {
                required: 'Debe encher este campo'
            },
//            due_date: {
//                required: 'Requerido'
//            },
            contract_type: {
                required: 'Debe encher este campo'
            },
            start_date: {
                required: 'Debe encher este campo'
            },
            duration: {
                required: 'Debe encher este campo',
                min: 'Mínimo valor debe ser 1'
            },

            provincejob: {
                required: 'Debe encher este campo'
            },
            province: {
                required: 'Debe encher este campo'
            },
            concello_chosen: {
                required: 'Debe encher este campo'
            },
            concellojob: {
                required: 'Debe encher este campo'
            },
            concellojob_chosen: {
                required: 'Debe encher este campo'
            },
            disponibilidad: {
                required: 'Debe encher este campo'
            },
            tipooferta: {
                required: 'Debe encher este campo'
            },

        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error');
            $(e).remove();
        },
        errorPlacement: function (error, element) {
            if (element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1)
                    controls.append(error);
                else
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            } else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            } else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            } else
                error.insertAfter(element.parent());
        },
        submitHandler: function (form) {
            form.submit();
        },
        invalidHandler: function (form) {}
    });

    $("#propietario").chosen({max_selected_options: 1});
    $("#provincia").chosen({max_selected_options: 1});

    $('#offer-filter').trigger('click');

    $('#contract_type').on('change', function (e) {
        e.preventDefault();
        var duracion = $(this).val();

        if (duracion == '1') { //es de tiempo determinado debe mostrar cuanto tiempo
            $('#divTiempo').show();
            $('#tiempo').attr('required', 'required');
            $('#duration').attr('required', 'required');
            $('#tiempo').val('MESES');

        } else { //es de tiempo indefinido ocultar tiempo
            $('#tiempo').val('');
            $('#duration').val('');
            $('#tiempo').removeAttr('required');
            $('#duration').removeAttr('required');
            $('#divTiempo').hide();
        }


    });

    $('#tipogestion').on('change', function (e) {
        e.preventDefault();
        var gestion = $(this).val();

        if (gestion == '1') {
            $('#divUrl').removeClass('d-none');
            $('#divUrl').show();
            $('#urlTercero').attr('required', 'required');
            $('.gestionada').hide();
            $('#urlTercero').focus();

        } else {
            $('#urlTercero').val('');
            $('#urlTercero').removeAttr('required');
            $('#divUrl').addClass('d-none');
            $('#divUrl').hide();
            $('#urlTercero').attr('required', 'required');
            $('.gestionada').show();
        }


    });

    $('#provincejob').on('change', function (e) {
        e.preventDefault();
        var $province = $(this).val();
        var $text = $("option:selected", this).text();

        $('#other_province').removeAttr('required');
        $('#other_concello').removeAttr('required');

        if ($text === 'Otra' || $text === 'Outra') {
            $('#other_province').show();
            $('#labelconcello').html('Concello');

            $('#concellojob').hide();
            $('#concellojob').chosen();
            $('#concellojob_chosen').val('otro').change();
            $('#concellojob').val('otro').change();
            $('#other_concello').show();
            $('#other_province').focus();

            $('#other_province').attr('required', 'required');
            $('#other_concello').attr('required', 'required');


            //return;
        } else if ($text === 'Extranjero' || $text === 'Extranjeiro') {
            $('#other_province').hide();
            //$('#concellojob').show();
            // $('#concellojob_chosen').show();
            $('#other_concello').hide();
            $('#other_concello').val('');

            $('#labelconcello').html('Pais');


        } else {
            $('#other_province').hide();
            $('#other_province').val('');
            //$('#hall').show();
            $('#concellojob').val('');
            //$('#concellojob_chosen').show();
            $('#other_concello').hide();
            $('#other_concello').val('');
            $('#labelconcello').html('Concello');
        }
        if ($province != '') {
            $.ajax({
                url: '/province/' + $province + '/concellos',
                method: 'GET',
                type: 'JSON',
                beforeSend: function () {},
                success: function (response) {

                    listItems = '';
                    listItems = '<option selected="selected" value="">Seleccione...</option>';
                    var descripcion = '';
                    for (var i = 0; i < response.length; i++) {

                        listItems += "<option value='" + response[i].id + "'>" + response[i].name + "</option>";
                    }
                    ;
                    listItems += '<option  value="otro">Outro</option>';

                    $('#concellojob').html(listItems);

                    $("#concellojob").trigger("chosen:updated");


                    if ($text === 'Otra' || $text === 'Outra') {
//                        $('#concello_chosen_chosen').val('otro').change();
//                        $('#concello_chosen').val('otro').change();
                        $('#concellojob_chosen').val('otro');
                        $('#concellojob').val('otro');
                        $('#concellojob option[value=otro').attr('selected', 'selected');
                        $('#other_province').focus();
                    }

                    if ($text === 'Extranjero' || $text === 'Extranjeiro') {
//                        $('#concellojob_chosen').val('otro').change();
//                        $('#concellojob').val('otro').change();
//                        $('#concellojob_chosen').val('otro');
//                        $('#concellojob').val('otro');
//                        $('#concellojob option[value=otro').attr('selected', 'selected');
//                        $('#other_concello').focus();

                    }
                    $('#concellojob option[value=otro').hide();
                    $('#concellojob').trigger("chosen:updated");




                }
            })
        } else {
            $('#concellojob').html('').trigger("chosen:updated");
            $('#concellojob').chosen();
        }
    });
    $('#concellojob').on('change', function (e) {
        e.preventDefault();
        var $province = $('#provincejob').val();
        var $text = $("option:selected", this).text();
        if ($text === 'Otro' || $text === 'Outro') {
            //$('#hall').hide();
            $('#concellojob').hide();
            $('#other_concello').show();
            $('#other_concello').focus();
            return;
        } else {
            $('#concellojob').trigger("chosen:updated");
            $('#other_concello').hide();
            // $('#concello_chosen_chosen').focus();
        }


    });





    $('#concellojob').chosen({
        width: "100%"
    });

    $('#provincejob').chosen({
        width: "100%"
    });

    $('#tipogestion').change();

    if ($("#createoffer").length > 0) {
        $("#provincejob").change();
    }
    ;

    $('#limitar_inscripcion').on('change', function (e) {

       if( $(this).is(':checked') )  {
            $('#maximo_ins').show();
        } else {  $('#maximo_ins').hide();}

    });

});
