jQuery(document).ready(function ($) {



    $('.chosen-select').chosen({allow_single_deselect: true});
    $('#publishedAt').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl'
    });
    $('#publishedAt_min').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl'
    }).on('changeDate', function (selected) {
        var startDate = new Date(selected.date.valueOf());
        $('#publishedAt_max').datepicker('setStartDate', startDate);
    }).on('clearDate', function (selected) {
        $('#publishedAt_max').datepicker('setStartDate', null);
    });
    $('#publishedAt_max').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl'
    }).on('changeDate', function (selected) {
        var endDate = new Date(selected.date.valueOf());
        $('#publishedAt_min').datepicker('setEndDate', endDate);
    }).on('clearDate', function (selected) {
        $('#publishedAt_min').datepicker('setEndDate', null);
    });
    var auditoria_table = $('#auditoria-table').DataTable({
        responsive: true,
        language: {

            url: '/assets/js/datatable_' + $('#imgidiomaselect').attr('alt') + '.json'//Ubicacion del archivo con el json del idioma.
        },
        "processing": true,
        "serverSide": true,
        "autowidth":false,
        
        "ajax": {
            url: "/admin/auditorys"
        },
        "columns": [
            {data: 'model_name'},
            {data: 'publishedAt'},
            {data: 'action'},
            {data: 'usuario'},
            {data: 'original_set'},
            {data: 'changed_set'}],
        "columnDefs": [
            {
                "targets": [0],
                "width": '10%',
                "max-width": "10%"
            },
            {
                "targets": [1],
                "width": '10%',
                "max-width": "10%"
            },
            {
                "targets": [2],
                "width": '30%',
                "max-width": "30%"
            },
             {
                "targets": [3],
                "width": '10%',
                "max-width": "10%"
            },
             {
                "targets": [4],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [5],
                "visible": false,
                "searchable": false
            }
        ],
        scrollY:        "300px",
        scrollX:        true,
        scrollCollapse: true,
        
        fixedColumns: true,
        "bSort": true,
        "bFilter": true,
        "aaSorting": [[0, "DESC"]],
        "iDisplayLength": 100,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                text: '<span class="fa fa-file-excel-o"></span> Exportar a Excel',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'csv',
                text: '<span class="glyphicon-export"></span> Exportar a Csv',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'pdf',
                text: '<span class="fa fa-file-pdf-o"></span> Exportar a Pdf',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'print',
                text: '<span class="fa fa-print bigger-110"></span> Imprimir',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            }

        ],
        
    });
    $('#auditoria-filter').on('click', function () {

        auditoria_table.columns(0).search($('#model_name').val());
        auditoria_table.columns(1).search($('#publishedAt_min').val() + '-' + $('#publishedAt_max').val());
        // auditoria_table.columns(2).search($('#usuario').val());
        auditoria_table.columns(2).search($('#action').val());
        auditoria_table.columns(3).search($('#usuario').val());
        auditoria_table.columns(4).search($('#cif').val());
        auditoria_table.draw();
    });
    $('#reset-filter').on('click', function () {

        $('#model_name').val('');
        $('#publishedAt_min').val('');
        $('#publishedAt_max').val('');
        // $('#usuario').val('');
        $('#action').val('');
        $('#usuario').val('');
        $('#cif').val('');
        auditoria_table.search('').columns().search('').draw();
    });

    $('#auditoriaForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            model_name: {required: true},
            publishedAt: {required: true},
            usuario: {required: true},
            action: {required: true},
            original_set: {required: true},
            changed_set: {required: true}
        },
        messages: {
            model_name: {
                required: 'Requerido'
            },
            publishedAt: {
                required: 'Requerido'
            }

        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error');
            $(e).remove();
        },
        errorPlacement: function (error, element) {
            if (element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1)
                    controls.append(error);
                else
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            } else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            } else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            } else
                error.insertAfter(element.parent());
        },
        submitHandler: function (form) {
            form.submit();
        },
        invalidHandler: function (form) {}
    });


    $(document).on('click', '.auditoriasdetalles', function (e) {

        e.preventDefault();

        var parentRow = $(this).closest("tr");

        var rowData = auditoria_table.row(parentRow).data();
        var id = rowData.id; // or whatever it is
        console.log(rowData);

        var json_originales = JSON.parse(rowData.original_set);
        var model_tabla = rowData.model_name;
        var usuario = rowData.usuario;
        var fecha = rowData.publishedAt;
        var accion = rowData.action;

        var json_modificados = '';
        if (rowData.changed_set != '') {
            var json_modificados = JSON.parse(rowData.changed_set);
        }

        console.log(json_originales);
        console.log(json_modificados);

        var originales = $('#originales');
        var modificados = $('#modificados');
        originales.html('');
        modificados.html('');


        var result = '';
        $.each(json_originales, function (k, v) {
            result += '<div class="col-md-4"> <label>' + k + '</label> </div> ' + '<div class="col-md-8"> <input class="form-control" value = "' + v + '" /></div>';
        });

        originales.html(result);

        if (json_modificados != '') {
            result = '';
            $.each(json_modificados, function (k, v) {
                result += '<div class="col-md-8"> <input class="form-control" value = "' + v + '" ></div>';
            });


            modificados.html(result);
        }

        $('#modulo').html('Modulo: ' + model_tabla);
        $('#usuario').html('Usuario: ' + usuario);
        $('#fecha').html('Fecha: ' + fecha);
        $('#accion').html('Acción: ' + accion);



        $('#modaldetalles').modal('toggle');



    });

});
