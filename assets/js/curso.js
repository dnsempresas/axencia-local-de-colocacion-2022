jQuery(document).ready(function ($) {

    $.mask.definitions['~'] = '[+-]';

    $('#birth').mask('99/99/9999');




    $('#fecha_inicio').datepicker({
        defaultDate: new Date(),
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl'

    }).on('changeDate', function (selected) {

    }).on('clearDate', function (selected) {

    });
    $('#fecha_fin').datepicker({
        defaultDate: new Date(),
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl'
    }).on('changeDate', function (selected) {

    }).on('clearDate', function (selected) {

    });


    $('#reset-filter').on('click', function () {


        $('#nombre').val('');
        $('#ubicacion').val('');
        $('#plazas').val('');
        $('#edades_recomendadas').val('');
        $('#plazas_disponibles').val('');



        // offer_table.search('').columns().search('').draw();
        $("#formfiltros").submit();

    });

    $('.time').timepicker({
        defaultTime: false,
        use24hours: true,
        timeFormat: 'HH:mm'
    });


    $('.fecha').mask('99/99/9999');

    var cursos_table = $('#cursos-table').DataTable({
        responsive: true,
        language: {

            url: '/assets/js/datatable_' + $('#imgidiomaselect').attr('alt') + '.json'//Ubicacion del archivo con el json del idioma.
        },
        "processing": true,

        "lengthMenu": [[100, -1], [100, "All"]],
        "iDisplayLength": 1000,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                text: '<span class="fa fa-file-excel-o"></span> Exportar a Excel',
                exportOptions: {

                    modifier: {
                        search: 'applied',
                        page: 'all',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'csv',
                text: '<span class="glyphicon-export"></span> Exportar a Csv',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        page: 'all',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'pdf',
                text: '<span class="fa fa-file-pdf-o"></span> Exportar a Pdf',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        page: 'all',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'print',
                text: '<span class="fa fa-print bigger-110"></span> Imprimir',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        page: 'all',
                        order: 'applied'
                    }
                }
            }
        ],

        "bSort": true,
        "bFilter": true

    });

    if ($('textarea.ckeditor#contenido').length > 0) {
        CKEDITOR.replace('contenido', {
            filebrowserBrowseUrl: '/filemanager/filemanager/dialog.php?type=2&lang=es&editor=ckeditor&fldr=',
            //filebrowserUploadUrl : '/filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
            filebrowserImageBrowseUrl: '/filemanager/filemanager/dialog.php?type=1&lang=es&editor=ckeditor&fldr='
        });
    }


});
