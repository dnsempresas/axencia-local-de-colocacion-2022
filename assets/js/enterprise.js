jQuery(document).ready(function ($) {

    $(document).on('click', '#remove_link', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        if (window.confirm("'Realmente desea eliminar este elemento?'")) {
            window.location = url;
        }
    });
    var enterprise_table = $('#enterprise-table').DataTable({
        responsive: true,
        language: {

            url: '/assets/js/datatable_' + $('#imgidiomaselect').attr('alt') + '.json'//Ubicacion del archivo con el json del idioma.
        },
        "processing": true,
        "serverSide": true,

        "lengthMenu": [[100, -1], [100, "All"]],
        "iDisplayLength": 1000,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                text: '<span class="fa fa-file-excel-o"></span> Exportar a Excel',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 8,9],
                    modifier: {
                        search: 'applied',
                        page: 'all',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'csv',
                text: '<span class="glyphicon-export"></span> Exportar a Csv',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        page: 'all',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'pdf',
                text: '<span class="fa fa-file-pdf-o"></span> Exportar a Pdf',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        page: 'all',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'print',
                text: '<span class="fa fa-print bigger-110"></span> Imprimir',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        page: 'all',
                        order: 'applied'
                    }
                }
            }
        ],
        "ajax": {
            url: "/admin/enterprises"
        },
        
        "columns": [
            {data: 'pk'},
            {data: 'name'},
            {data: 'surname'},

            {data: 'email'},
            {data: 'cif'},
            {data: 'telephone'},
            {data: 'address'},
            {data: 'contacto'},
            
            {data: 'options'},
            {data: 'province'},
            {data: 'concello'},
            ],
        "columnDefs": [
            {
                "targets": [8],
                "searchable": false,
                "width": '10%'
            },
            {
                "targets": [9],
                "visible": false,
                "searchable": false
            },{
                "targets": [10],
                "visible": false,
                "searchable": false
            }],

        'orderCellsTop': true,
        'autoWidth': false,
        'scrollX': true,
        'ordering': true,

        'fixedColumns': true,

        "bSort": true,
        "bFilter": true,
//        "aaSorting": [[0, "DESC"]],
        "iDisplayLength": 100
    });
    $('#enterprise-filter').on('click', function () {
        enterprise_table.columns(1).search($('#name').val());
        enterprise_table.columns(2).search($('#surname').val());
        enterprise_table.columns(3).search($('#email').val());
        enterprise_table.columns(4).search($('#cif').val());
        enterprise_table.columns(5).search($('#phone').val());
        enterprise_table.columns(6).search($('#contacto').val());
        enterprise_table.columns(7).search($('#address').val());
        enterprise_table.columns(9).search($('#province').val());
        enterprise_table.columns(10).search($('#concello_chosen').val());
        

        enterprise_table.draw();
    });
    $('#reset-filter-e').on('click', function () {
        $('#name').val('');
        $('#surname').val('');
        $('#email').val('');
        $('#cif').val('');
        $('#phone').val('');
        $('#contacto').val('');
        $('#province').val('');
        $('#concello_chosen').val('');
         $('#address').val('');
        enterprise_table.columns(1).search($('#name').val());
        enterprise_table.columns(2).search($('#surname').val());
        enterprise_table.columns(3).search($('#email').val());
        enterprise_table.columns(4).search($('#cif').val());
        enterprise_table.columns(5).search($('#phone').val());
        enterprise_table.columns(6).search($('#contacto').val());
        enterprise_table.columns(7).search($('#address').val());
        enterprise_table.columns(9).search($('#province').val());
        enterprise_table.columns(10).search($('#concello_chosen').val());
        
        //enterprise_table.draw('');
        enterprise_table.search('').columns().search('').draw();
        $('#province').trigger("chosen:updated");
        $('#concello_chosen').trigger("chosen:updated");
    });
  $.validator.setDefaults({ ignore: ":hidden:not(.chosen-select)" }); //for all select having class .chosen-select
   
    $('#enterpriseForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            name: {required: true},
            last_name: {required: true},
            email: {required: true, email: true},
            cif: {required: true, Dni: true},
            phone: {required: true},
            
            province: {required: true},
            concello_chosen: {required: true},
            address: {required: true},

        },
        messages: {
            name: {
                required: 'Debe encher este campo'
            },
            last_name: {
                required: 'Debe encher este campo'
            },
            email: {
                required: 'Debe encher este campo',
                Dni: 'Error'
            },
            cif: {
                required: 'Debe encher este campo'
            },
            phone: {
                required: 'Debe encher este campo'
                
            },
            
            province: {
                required: 'Debe encher este campo'
            },
            concello_chosen: {
                required: 'Debe encher este campo'
            },
            password: {
                required: 'Debe encher este campo'
                
            },
             address: {
                required: 'Debe encher este campo'
                
            },


        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error');
            $(e).remove();
        },
        errorPlacement: function (error, element) {
            if (element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1)
                    controls.append(error);
                else
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            } else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            } else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            } else
                error.insertAfter(element.parent());
        },
        submitHandler: function (form) {
            form.submit();
        },
        invalidHandler: function (form) {}
    });
});
