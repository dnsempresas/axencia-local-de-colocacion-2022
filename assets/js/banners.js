jQuery(document).ready(function ($) {
    var banners_table =
            $('#banners_table').DataTable({
        responsive: true,
        language: {

            url: '/assets/js/datatable_' + $('#imgidiomaselect').attr('alt') + '.json'//Ubicacion del archivo con el json del idioma.
        },
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: "/admin/banners"
        },
        "columns": [
            {data: 'id'},
            {data: 'titulo'},
            {data: 'enlace'},
            {data: 'fecha_publicacion'},
            {data: 'fecha_expiracion'},
            {data: 'options'}],
        "bSort": true,
        "bFilter": true,
        "aaSorting": [[0, "DESC"]],
        "iDisplayLength": 100,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                text: '<span class="fa fa-file-excel-o"></span> Exportar a Excel',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'csv',
                text: '<span class="glyphicon-export"></span> Exportar a Csv',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'pdf',
                text: '<span class="fa fa-file-pdf-o"></span> Exportar a Pdf',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'print',
                text: '<span class="fa fa-print bigger-110"></span> Imprimir',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            }

        ],
    });

    $('#banners-filter').on('click', function () {
        banners_table.columns(0).search($('#id').val());
        banners_table.columns(1).search($('#titulo').val());
        banners_table.columns(2).search($('#enlace').val());
        banners_table.columns(5).search($('#fecha_publicacion_min').val() + '-' + $('#fecha_publicacion_max').val());
        banners_table.columns(6).search($('#fecha_expiracion_min').val() + '-' + $('#fecha_expiracion_max').val());
        banners_table.draw();
    });
    $('#reset-filter').on('click', function () {
        $('#id').val('');
        $('#titulo').val('');
        $('#enlace').val('');

        $('#fecha_publicacion_min').val('');
        $('#fecha_publicacion_max').val('');
        $('#fecha_expiracion_min').val('');
        $('#fecha_expiracion_max').val('');
        banners_table.search('').columns().search('').draw();
    });

    $('#fecha_publicacion').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl'
    }).on('changeDate', function (selected) {

    }).on('clearDate', function (selected) {

    });
    $('#fecha_publicacion_min').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl',
        orientation: 'bottom'
    }).on('changeDate', function (selected) {

    }).on('clearDate', function (selected) {

    });
    $('#fecha_publicacion_max').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl',
        orientation: 'bottom'
    }).on('changeDate', function (selected) {

    }).on('clearDate', function (selected) {

    });
    $('#fecha_expiracion').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl',
    }).on('changeDate', function (selected) {

    }).on('clearDate', function (selected) {

    });

    $('#fecha_expiracion_min').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl',
        orientation: 'bottom'
    }).on('changeDate', function (selected) {

    }).on('clearDate', function (selected) {

    });
    $('#fecha_expiracion_max').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl',
        orientation: 'bottom'
    }).on('changeDate', function (selected) {

    }).on('clearDate', function (selected) {

    });

    $('#bannersForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            titulo: {required: true},
            fecha_publicacion: {required: true},
            fecha_expiracion: {required: true}
        },
        messages: {
           
            titulo: {
                required: 'Requerido'
            },
           
            fecha_publicacion: {
                required: 'Requerido'
            },
            fecha_expiracion: {
                required: 'Requerido'
            }
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error');
            $(e).remove();
        },
        errorPlacement: function (error, element) {
            if (element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1)
                    controls.append(error);
                else
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            } else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            } else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            } else
                error.insertAfter(element.parent());
        },
        submitHandler: function (form) {
            form.submit();
        },
        invalidHandler: function (form) {}
    });
});
