
jQuery(document).ready(function ($) {

    var selectedIds = [];
    var usersoffers;


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#userForm').on('submit', function () {
        if ($('#tiporole').val() == 2) {
            if (!($('#name_e').val() == null || $('#name_e').val() == undefined))
                $('#name').val($('#name_e').val());
            if ($('#last_name_e').val())
                $('#last_name').val($('#last_name_e').val());
            if ($('#cif_e').val())
                $('#cif').val($('#cif_e').val());


        } else {
            if ($('#tiporole').val() == 1) {

            }
        }
    });




    var listItems = '<option selected="selected" value="">Seleccione...</option>';
//   // $("#specialty2").prop('disabled', true).trigger("chosen:updated");;

    $("#specialty2").chosen().on('chosen:showing_dropdown', function () {
        $('.chosen-select').attr('disabled', true).trigger('chosen:updated');
        $('.chosen-select').attr('disabled', false).trigger('chosen:updated');
        $('.search-choice-close').hide();
    });
    //$('.search-choice-close').hide();
    $("#specialty2").chosen({disable_search: true, placeholder_text_single: ''}).trigger("chosen:updated");

//    $.validator.addMethod('filesize', function (value, element, arg) {
//        var minsize = 1000; // min 1kb
//        if ((value > minsize) && (value <= arg)) {
//            return true;
//        } else {
//            return false;
//        }
//    });
    //$.validator.setDefaults({ignore: ":hidden:not(.chosen-select)"}); //for all select having class .chosen-select

    $.validator.setDefaults({ignore: ":hidden:not(select)"});

    $('#userForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            name: {required: function (element) {
                    if ($('#tiporole').val() == 1) {
                        return true;
                    } else {
                        return false;
                    }
                }},
            last_name: {required: function (element) {
                    if ($('#tiporole').val() == 1) {
                        return true;
                    } else {
                        return false;
                    }
                }},
            cif: {required: function (element) {
                    if ($('#tiporole').val() == 1) {
                        return true;
                    } else {
                        return false;
                    }
                }, Dni: true},
            birth: {required: function (element) {
                    if ($('#tiporole').val() == 1) {
                        return true;
                    } else {
                        return false;
                    }
                }, validDate: true},
            address: {required: true},
//            postal_code: {required: true},
            phone: {required: true, phone: true},
            email: {required: true, email: true},
            province: {required: true},
            concello_chosen: {required: true},
            name_e: {required: function (element) {
                    if ($('#tiporole').val() == 2) {
                        return true;
                    } else {
                        return false;
                    }
                }},
            last_name_e: {required: function (element) {
                    if ($('#tiporole').val() == 2) {
                        return true;
                    } else {
                        return false;
                    }
                }},
            cif_e: {required: function (element) {
                    if ($('#tiporole').val() == 2) {
                        return true;
                    } else {
                        return false;
                    }
                }},
            cvprincipal: {
                required: function (element) {
                    if ($('#tiporole').val() == 1) {
                        if ($('#nombreCv1').text() == '') {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                }
//                filesize: 3000000   //max size 3000 kb
            },
//            job_post: {
//                required: function (element) {
//                    if ($('#tiporole').val() == 1) {
//                        if ($('#no_experiencie').checked) {
//                            return false;
//                        } else {
//                            return true;
//                        }
//                    } else {
//                        return false;
//                    }
//                }
//            },


        },
        messages: {
            name: {
                required: 'Debe encher este campo'
            },
            last_name: {
                required: 'Debe encher este campo'
            },
            cif: {
                required: 'Debe encher este campo',
                Dni: 'Erroneo'
            },
            birth: {
                required: 'Debe encher este campo',
                validDate: 'Error'
            },
            address: {
                required: 'Debe encher este campo'
            },
//            postal_code: {
//                required: 'Debe encher este campo'
//            },
            phone: {
                required: 'Debe encher este campo',
                phone: 'Erroneo'
            },
            email: {
                required: 'Debe encher este campo',
                email: 'Erroneo'
            },
            province: {
                required: 'Debe encher este campo'
            },
            concello_chosen: {
                required: 'Debe encher este campo'
            },
            job_posts: {
                required: 'Debe encher este campo'
            },
            job_posts_chosen: {
                required: 'Debe encher este campo'
            },
            name_e: {
                required: 'Debe encher este campo'
            },
            last_name_e: {
                required: 'Debe encher este campo'
            },
            cif_e: {
                required: 'Debe encher este campo',
                Dni: 'Erroneo'
            },
            cvprincipal: {
                required: 'Debe engadir o seu CV  para poder inscribirse nas ofertas de emprego',
//                filesize: 'Tamaño no permitido'
            },
            password: {
                required: 'Debe encher este campo',
//                filesize: 'Tamaño no permitido'
            },
            genero: {
                required: 'Debe encher este campo',
//                filesize: 'Tamaño no permitido'
            },

            specialty2: {
                required: 'Debe encher este campo',
//                filesize: 'Tamaño no permitido'
            },

            specialty2_chosen: {
                required: 'Debe encher este campo',
//                filesize: 'Tamaño no permitido'
            },

        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error');
            $(e).remove();
        },
        errorPlacement: function (error, element) {
            if (element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1)
                    controls.append(error);
                else
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            } else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            } else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            } else
                error.insertAfter(element.parent());
        },
        submitHandler: function (form) {
            form.submit();
        },
        invalidHandler: function (form) {}
    });

    $('.chosen-select').chosen({allow_single_deselect: true});

    var user_table = $('#users-table').DataTable({
        responsive: true,

        language: {

            url: '/assets/js/datatable_' + $('#imgidiomaselect').attr('alt') + '.json'//Ubicacion del archivo con el json del idioma.
        },
        "processing": true,
        "serverSide": true,
        "lengthMenu": [[100, -1], [100, "All"]],
        "iDisplayLength": 1000,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                text: '<span class="fa fa-file-excel-o"></span> Exportar a Excel',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                    modifier: {
                        search: 'applied',
                        page: 'all',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'csv',
                text: '<span class="glyphicon-export"></span> Exportar a Csv',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        page: 'all',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'pdf',
                text: '<span class="fa fa-file-pdf-o"></span> Exportar a Pdf',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        page: 'all',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'print',
                text: '<span class="fa fa-print bigger-110"></span> Imprimir',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        page: 'all',
                        order: 'applied'
                    }
                }
            }
        ],
        "ajax": {
            url: "/admin/users"
        },
        "columns": [
            {data: 'pk'},
            {data: 'name'},
            {data: 'surname'},
            {data: 'age'},
            {data: 'email'},
            {data: 'cif'},
            {data: 'telephone'},
            {data: 'address', 'defaultContent': " "},
            {data: 'options'},
            {data: 'province'},
            {data: 'concello'},
        ],
        "columnDefs": [
            {
                "targets": [9],
                "visible": false,
                "searchable": false
            }, {
                "targets": [10],
                "visible": false,
                "searchable": false
            }],
        "bSort": true,
        "bFilter": true

    });
    $('#user-filter').on('click', function () {
        
        user_table.columns(1).search($('#name').val());
        user_table.columns(2).search($('#surname').val());
        user_table.columns(3).search($('#cif').val());
        user_table.columns(4).search($('#email').val());
        user_table.columns(5).search($('#phone').val());
        user_table.columns(6).search($('#min-age').val() + '-' + $('#max-age').val());
        user_table.columns(7).search($('#address').val());
        user_table.columns(9).search($('#province').val());
        user_table.columns(10).search($('#concello_chosen').val());


        user_table.draw();
    });
    $('#reset-filter').on('click', function () {
        $('#name').val('');
        $('#surname').val('');
        $('#cif').val('');
        $('#email').val('');
        $('#phone').val('');
        $('#min-age').val('');
        $('#max-age').val('');
        $('#province').val('');
        $('#concello_chosen').val('');
        $('#address').val('');
        user_table.search('').columns().search('').draw();
        $('#province').trigger("chosen:updated");
        $('#concello_chosen').trigger("chosen:updated");
    });

    $('#studies').on('change', function (e) {
        e.preventDefault();
        var study = $(this).val();

        var Text = $("#studies option:selected").text();

        if (study === '-1' || study === '1' || study === '2' || study === '3') {

            $('#specialtyC').hide();
            $('#specialty').html('');
            $('#specialty2').html('');
            $('#specialty2').html('').chosen().trigger('chosen:updated');
            $("#specialty2").append('<option value="' + study + '" selected="selected" style="display:none"> ' + Text + '</option>');
            $('#specialty2').trigger('chosen:updated');

            return;
        } else {
            $('#specialtyC').show();
            $('#specialty').html('');
            if ($('#tiporole').val() == '1')
                $('#specialty2').attr('required', 'required');

        }
        $.ajax({
            url: '/estudios/' + study + '/especialidades',
            method: 'GET',
            type: 'JSON',
            beforeSend: function () {

            },
            success: function (response) {
                listItems = '';
                listItems = '<option selected="selected" value="-1">Seleccione...</option>';
                var descripcion = '';
                for (var i = 0; i < response.length; i++) {



                    if ($('#imgidiomaselect').attr('alt') === 'es') {
                        descripcion = response[i].description;
                    } else {
                        descripcion = response[i].nome;
                    }

                    listItems += "<option value='" + response[i].id + "'>" + descripcion + "</option>";
                }
                ;



                $('#specialtyC').show();
                $('#specialty').html(listItems);

                $("#specialty").trigger("chosen:updated");
                $('#specialty').focus();
            }
        })
    });

    $("#specialty").chosen().change(function () {
        var Value = $(this).val();
        //var optionText = $('#dropdownList option[value="'+optionValue+'"]').text();
        var Text = $("#specialty option:selected").text();

        console.log(Value, Text);

        $("#specialty2").append('<option value="' + Value + '" selected > ' + Text + '</option>');

        $('#specialty2').chosen().trigger('chosen:updated');


    });

    $('#province').on('change', function (e) {
        e.preventDefault();
        var $province = $(this).val();
        var $text = $("option:selected", this).text();
        $('#other_province').removeAttr('required');
        $('#other_concello').removeAttr('required');
        if ($text === 'Otra' || $text === 'Outra') {
            $('#other_province').show();
            $('#labelconcello').html('Concello');

            $('#concello_chosen').hide();
            $('#concello_chosen').chosen();
            $('#concello_chosen_chosen').val('otro').change();
            $('#concello_chosen').val('otro').change();
            $('#other_concello').show();
            $('#other_province').attr('required', 'required');
            $('#other_concello').attr('required', 'required');



            //return;
        } else if ($text === 'Extranjero' || $text === 'Extranjeiro') {
            $('#other_province').hide();
            //$('#concellojob').show();
            // $('#concellojob_chosen').show();
            $('#other_concello').hide();
            $('#other_concello').val('');

            $('#labelconcello').html('Pais');


        } else {
            $('#other_province').hide();
            $('#other_province').val('');
            //$('#hall').show();

            //$('#concellojob_chosen').show();
            $('#other_concello').hide();
            $('#other_concello').val('');
            $('#labelconcello').html('Concello');
            //$('#concello_chosen').show();
            //$('#concello_chosen').val('otro');
        }
        if ($province != '') {
            $.ajax({
                url: '/province/' + $province + '/concellos',
                method: 'GET',
                type: 'JSON',
                beforeSend: function () {},
                success: function (response) {

                    //$('#concello_chosen').html(response).trigger("chosen:updated");
                    //$('#concello_chosen').html(response).trigger("chosen:updated");



                    listItems = '';
                    listItems = '<option selected="selected" value="">Seleccione...</option>';

                    for (var i = 0; i < response.length; i++) {

                        listItems += "<option value='" + response[i].id + "'>" + response[i].name + "</option>";
                    }
                    ;
//                    listItems += '<option  value="otro">Outro</option>';

                    $('#concello_chosen').html(listItems);


                    //$('#concello_chosen').chosen();







                    if ($text === 'Otra' || $text === 'Outra') {
//                        $('#concello_chosen_chosen').val('otro').change();
//                        $('#concello_chosen').val('otro').change();
                        $('#concello_chosen_chosen').val('otro');
                        $('#concello_chosen').val('otro');
                        $("#concello_chosen option[value='otro'").attr('selected', 'selected');
                        $('#other_province').focus();
                    }

                    if ($text === 'Extranjero' || $text === 'Extranjeiro') {
//                        $('#concello_chosen_chosen').val('otro').change();
//                        $('#concello_chosen').val('otro').change();
//                        $('#concello_chosen_chosen').val('otro');
//                        $('#concello_chosen').val('otro');
//                        $('#concello_chosen option[value=otro').attr('selected', 'selected');
//                        $('#other_concello').focus();

                    }
                    $('#concello_chosen option[value=otro').hide();

                    $('#concello_chosen').trigger("chosen:updated");


                    //$('#concello_chosen').focus();

                }
            });
        } else {
            $('#concello_chosen').html('').trigger("chosen:updated");
            $('#concello_chosen').chosen();
        }
    });



    $('#concello_chosen').on('change', function (e) {
        e.preventDefault();
        var $province = $('#province').val();
        var $text = $("option:selected", this).text();
        if ($text === 'Otro' || $text === 'Outro') {
            //$('#hall').hide();
            $('#concello_chosen').hide();
            $('#other_concello').show();
            $('#other_concello').focus();
            return;
        } else {
            $('#concello_chosen').focus();
            $('#concello_chosen').trigger("chosen:updated");
            $('#other_concello').hide();

//            $('#concello_chosen').show();
//            $('#other_concello').hide();

        }


    });

    $('#tiporole').on('change', function (e) {

        var valor = $("option:selected", this).val();
        if (valor == 1 || valor == 3) {

            $('#panel_datosempresariales').hide();
            $('#panel_personadecontacto').hide();
            $('#panel_logo').hide();
            $('#panel_datospersonales').show();

            var panel_direccion = $('#panel_direccion').detach();
            panel_direccion.appendTo('#izquierda');


            if (valor == 1) {
                $('#panel_datosprofesionales').show();
                $('#panel_intereseprofesionales').show();

                //$('#job_posts').setAttribute('required', 'required');
                $('#cvprincipal').attr('required', 'required');
                // $('#specialty2').attr('required', 'required');

            } else {
                $('#panel_datosprofesionales').hide();
                $('#panel_intereseprofesionales').hide();

                $('#job_posts').removeAttr('required');
                $('#cvprincipal').removeAttr('required');
                $('#specialty2').removeAttr('required');
                $('#specialty2').html('').chosen().trigger('chosen:updated');


                var panel_direccion = $('#panel_direccion').detach();
                panel_direccion.appendTo('#izquierda');
//
//                var panel_direccion = $('#panel_direccion').detach();
//
//                panel_direccion.prependTo('#derecha');
            }
            $('#genero').attr('required', 'required');





        } else {
            $('#panel_datosempresariales').show();
            $('#panel_personadecontacto').show();
            $('#panel_logo').show();
            $('#panel_datospersonales').hide();

//            var panel_direccion = $('#panel_direccion').detach();
//
//            panel_direccion.prependTo('#derecha');
            var panel_direccion = $('#panel_direccion').detach();
            panel_direccion.appendTo('#izquierda');

            $('#panel_datosprofesionales').hide();
            $('#panel_intereseprofesionales').hide();

            $('#job_posts').removeAttr('required');
            $('#cvprincipal').removeAttr('required');
            $('#specialty2').removeAttr('required');
            $('#genero').removeAttr('required');
            $('#specialty2').html('').chosen().trigger('chosen:updated');

        }
        $('.chosen-container').css('width', '100%');

    });




    $('#genero').on('change', function (e) {
        e.preventDefault();
        var $text = $("option:selected", this).text().trim();

        var genero = $('#genero').val();

        if (genero == 1) {
            $('#imgSalida').attr('src', assets + '/images/mujer.png');

        } else {
            if (genero == 2) {
                $('#imgSalida').attr('src', assets + '/images/hombre.png');

            } else {
                $('#imgSalida').attr('src', assets + '/images/otro.png');
            }
        }

        if (($text === 'OTRO') || ($text === 'Otro') || ($text === 'Outro') || ($text === 'OUTRO')) {
            //$('#hall').hide();
            //$('#genero').hide();
            $('#other_genero').val('');
            $('#other_genero').show();
            $('#other_genero').focus();

            return;
        }
        $('#other_genero').val('');
        $('#other_genero').hide();


    });



    function addImage(e) {
        var file = e.target.files[0],
                imageType = /image.*/;

        if (!file.type.match(imageType))
            return;

        var reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
    }

    function addImage_e(e) {
        var file = e.target.files[0],
                imageType = /image.*/;

        if (!file.type.match(imageType))
            return;

        var reader = new FileReader();
        reader.onload = fileOnload_e;
        reader.readAsDataURL(file);
    }

    function fileOnload(e) {
        var result = e.target.result;
        $('#imgSalida').attr("src", result);
    }
    function fileOnload_e(e) {
        var result = e.target.result;
        $('#imgSalida_e').attr("src", result);
    }

    $('#no_experience').on('click', function (e) {

        if ($(this).prop("checked")) {
            $('#divpuestos').hide();
            $('#divprestaciones').hide();

            $('#job_posts').removeAttr('required');
            $('#job_posts').val('');
            $('#job_posts').trigger('chosen:updated');
        } else {
            $('#divpuestos').show();
            $('#divprestaciones').show();
            $('#job_posts').attr('required', 'required');
        }
        $('.chosen-container').css('width', '100%');
        $('.search-field .default').css('width', '100%');
        return;

    });
    $('#removeimg').on('click', function (e) {
        e.preventDefault();

        $('#imgSalida').attr('src', '');
        $('#foto_perfil').val('');
        $('#removeimg').hide();

        $('#foto_user').val('');


        return;

    });

    $('#removeimg_e').on('click', function (e) {
        e.preventDefault();

        $('#imgSalida_e').attr('src', '');
        $('#foto_perfil_e').val('');
        $('#removeimg_e').hide();

        $('#foto_user_e').val('');


        return;

    });

    $('#removeCV2').on('click', function (e) {
        e.preventDefault();

        $('#cv2').text('');
        $('#cvsecundario').val('');
        $('#removeCV2').hide();
        $('#cvsecundario_user').val('');

        return;

    });

    $('#cvsecundario').on('change', function (e) {
        var fileName = e.target.files[0].name;

        $('#cvsecundario_user').val(fileName);
        $('#cv2').text('');
        //$('#removeCV2').hide();
        $('#removeCV2').show();

    });



    $.mask.definitions['~'] = '[+-]';

    $('#birth').mask('99/99/9999');



    $('#birth').datepicker({
        defaultDate: new Date(),
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl'
    }).on('changeDate', function (selected) {

    }).on('clearDate', function (selected) {

    });


    var lang_input = $('#languages');
    lang_input.tag({placeholder: lang_input.attr('placeholder')});

    var computer_skills = $('#computer_skills');
    computer_skills.tag({placeholder: computer_skills.attr('placeholder')});

    $(document).on('click', '.resetpassword', function (e) {
        e.preventDefault();

        user_id = $(this).data('value');
        email = $(this).data('email');
        status = $(this).data('status');

        confirmed = $(this).data('confirmed');
        role_id = $(this).data('role_id');
if (status==1){
        if (confirmed == 1) {

            e.preventDefault();
            $('#inputemailtochange').val(email);
            $('#user_id').val(user_id);
            $('#spanemailtochange').text(email);
            $('#password1').val('');
            $('#password2').val('');

            $('#modalcambiarpassword').modal('show');
        } else {
            if (role_id == 1) {
                alert('El usuario no ha confirmado su email');
            }
        }
    } else {                
        alert('El usuario debe estar activo');}


    });





    $(document).on('click', '.mostrarincripciones', function (e) {
        e.preventDefault();
        id = $(this).data('value');


        $.ajax({
            url: '/user/resumeninscripciones',
            type: 'POST',
            data: {
                id: id
            },

            success: function (response) {

                $('#bodyresumeninscripciones').html(response);

                $('#modalresumeninscripciones').modal('show');

            },
            error: function () {
                alert('Un error ha ocurrido');
            }

        });
    });

    $(document).on('click', '.mostrarauditorias', function (e) {
        e.preventDefault();
        id = $(this).data('value');


        $.ajax({
            url: '/user/mostrarauditorias',
            type: 'POST',
            data: {
                id: id
            },

            success: function (response) {

                $('#bodymostrarauditorias').html(response);

                $('#modalmostrarauditorias').modal('show');

            },
            error: function () {
                alert('Un error ha ocurrido');
            }

        });
    });

    $(document).on('click', '.reenviarconfirmacion', function (e) {
        e.preventDefault();
        id = $(this).data('value');


        $.ajax({
            url: '/user/reenviarconfirmacion',
            type: 'POST',
            data: {
                id: id
            },

            success: function (response) {

                alert(response.message);

            },
            error: function () {
                alert('Un error ha ocurrido');
            }

        });
    });

//    $(document).on('click', '.dardebaja', function (e) {
//        e.preventDefault();
//        id = $(this).data('value');
//
//
//        $.ajax({
//            url: '/user/dardebaja',
//            type: 'POST',
//            data: {
//                id: id
//            },
//
//            success: function (response) {
//                if (response.message == 'Exitoso') {
//
//                    alert('Cambio se realizó exitosamente');
//                    user_table.ajax.reload();
//
//
//                } else {
//                    alert('Un error ha ocurrido');
//                }
//            },
//            error: function () {
//                alert('Un error ha ocurrido');
//            }
//
//        });
//    });

    var pass1 = $('[name=password1]');
    var pass2 = $('[name=password2]');
    var confirmacion = "";
    var longitud = "La contraseña debe estar formada entre 8-12 carácteres (ambos inclusive)";
    var negacion = "No coinciden las contraseñas";
    var vacio = "La contraseña no puede estar vacía";
    //oculto por defecto el elemento span
    var span = $('<span></span>').insertAfter(pass2);
    span.hide();
    //función que comprueba las dos contraseñas
    function coincidePassword() {
        var valor1 = pass1.val();
        var valor2 = pass2.val();
        //muestro el span
        span.show().removeClass();
        //condiciones dentro de la función
        if (valor1 != valor2) {
            span.text(negacion).addClass('negacion');
        }
        if (valor1.length == 0 || valor1 == "") {
            span.text(vacio).addClass('negacion');
        }
        if (valor1.length < 8 || valor1.length > 12) {
            span.text(longitud).addClass('negacion');
        }
        if (valor1.length != 0 && valor1 == valor2) {
            span.text(confirmacion).removeClass("negacion").addClass('confirmacion');
        }
    }
    //ejecuto la función al soltar la tecla
    pass2.keyup(function () {
        coincidePassword();
    });


    $(document).on('click', '#cambiarpassword', function (e) {

        e.preventDefault();
        var pass1 = $('[name=password1]');
        var pass2 = $('[name=password2]');
        var valor1 = pass1.val();
        var valor2 = pass2.val();
        //muestro el span
        span.show().removeClass();
        //condiciones dentro de la función
        if (valor1 != valor2) {
            span.text(negacion).addClass('negacion');
            return
        }
        if (valor1.length == 0 || valor1 == "") {
            span.text(vacio).addClass('negacion');
            return
        }
        if (valor1.length < 8 || valor1.length > 12) {
            span.text(longitud).addClass('negacion');
            return
        }
        if (valor1.length != 0 && valor1 == valor2) {
            span.text(confirmacion).removeClass("negacion").addClass('confirmacion');

        }
        var email = $('#inputemailtochange').val();
        var user_id = $('#user_id').val();

        $.ajax({
            url: '/user/resetpassword',
            type: 'POST',
            data: {
                email: email,
                password: valor1,
                user_id: user_id
            },
            dataType: 'json',
            success: function (response) {
                if (response.message == 'Exitoso') {

                    alert('Cambio se realizó exitosamente');


                } else {
                    alert('Un error ha ocurrido');
                }
            },
            error: function () {
                alert('Un error ha ocurrido');
            }

        });

    });



    $('#aceptapolitica').on('click', function (e) {

        if ($(this).prop("checked")) {
            $("#guardar").removeAttr('disabled');

        } else {
            $("#guardar").attr('disabled', 'true');
        }

        return;

    });



    $('#foto_perfil').change(function (e) {

        if (this.files[0].size > 2048000) {

            $(this).val('');
            alert("Tamaño de imagen excedido (Max.2MB)");
            return
        }
        var fileName = e.target.files[0].name;
        $('#foto_user').val(fileName);
        addImage(e);
        $('#removeimg').show();
    });
    $('#foto_perfil_e').change(function (e) {

        if (this.files[0].size > 2048000) {

            $(this).val('');
            alert("Tamaño de imagen excedido (Max.2MB)");
            return
        }
        var fileName = e.target.files[0].name;
        $('#foto_user_e').val(fileName);
        addImage_e(e);
        $('#removeimg_e').show();
    });


    $("select#tiporole").change();
    // $("select#province").change();


    $("#driving_permit").chosen({
        width: "100%"
                //html_template: '<img style="border:3px solid black;padding:0px;margin-right:4px" class="{class_name}" src="{url}">'
    });

    $('#concello_chosen').chosen({
        width: "100%"
    });

    $('#province').chosen({
        width: "100%"
    });



    if ($("#crearuser").length > 0) {
        $("#province").change();
    }
    ;

    $(document).on('click', '.dardebaja', function (e) {
        e.preventDefault();
        id = $(this).data('value');
        $('#botondardebajaadm').data('id', id);
        $('#modaldardebajaadm').modal('show');
    });
    $("#botondardebajaadm").attr("disabled", "disabled");


    $(document).on('click', '#botondardebajaadm', function (e) {
        e.preventDefault();
        var checked = $('#checkdardebajaadm').is(':checked');

        if (checked) {


            id = $(this).data('id');


            $.ajax({
                url: '/user/dardebaja',
                type: 'POST',
                data: {
                    id: id
                },

                success: function (response) {
                    if (response.message == 'Exitoso') {

                        alert('Perfil foi eliminado con éxito');
                        $('#modaldardebajaadm').modal('toggle');
                        location.reload();


                    } else {
                        alert('Un error ha ocurrido');
                    }
                },
                error: function () {
                    alert('Un error ha ocurrido');
                }

            });

        }

    });
    $("#checkdardebajaadm").click(function () {
        var checked = $(this).is(':checked');
        if (checked) {
            $('#botondardebajaadm').removeAttr('disabled');
        } else {
            $("#botondardebajaadm").attr("disabled", "disabled");
        }
    });


    $(document).on('click', '.getcertificadoOfertas', function (e) {
        e.preventDefault();
        id = $(this).data('value');


        $.ajax({
            url: '/user/' + id + '/resumenInscripcionesOfertas',
            type: 'GET',
            data: {
                id: id
            },

            success: function (response) {

                $('#bodymodalofertasinscitas').html(response);

                usersoffers = $('#usersoffers').DataTable({
                    language: {

                        url: '/assets/js/datatable_' + $('#imgidiomaselect').attr('alt') + '.json'//Ubicacion del archivo con el json del idioma.
                    },
                    scrollY: 300,
                    scrollX: true,
                    scrollCollapse: true,
                    paging: false,
                    searching: true,
                    columnDefs: [{
                            orderable: false,
                            className: 'select-checkbox',
                            targets: 0,
                            visible: false
                        }],
                    select: {
                        style: 'os',
                        selector: 'td:first-child'
                    },
                    dom: 'Bfrtip',
                    buttons: [
                        'selectAll',
                        'selectNone'
                    ],
                    order: [[1, 'asc']]

                });

                $('.dt-buttons').hide();
                $('#usersoffers_filter').hide();

                selectedIds = new Array();



                // Event listener to the two range filtering inputs to redraw on input
                $('#date_min, #date_max').change(function () {
                    usersoffers.draw();
                });


//                $('#date_min').datepicker({onSelect: function () {
//                        table.draw();
//                    }, changeMonth: true, changeYear: true});
//                $('#date_max').datepicker({onSelect: function () {
//                        table.draw();
//                    }, changeMonth: true, changeYear: true});



                usersoffers.on('select.dt', function (e, dt, type, indexes) {
//                    selectedIds.push(indexes[0]);
//                    console.log(selectedIds);

                    if (type === 'row') {
                        var node = dt.rows(indexes).nodes()[0];
                        var bid = $(node).attr("id");
                        selectedIds.push(bid);

                    }

                })

                usersoffers.on('deselect.dt', function (e, dt, type, indexes) {


                    if (type === 'row') {
                        var node = dt.rows(indexes).nodes()[0];
                        var bid = $(node).attr("id");
                        //selectedIds.splice(selectedIds.indexOf(bid), 1);
                        selectedIds.splice(selectedIds.indexOf(bid), 1);


                    }

//                    selectedIds.splice(selectedIds.indexOf(indexes[0]), 1);
//                    console.log(selectedIds);
                })




                $('#modalofertasinscitas').modal('show');

            },
            error: function () {
                alert('Un error ha ocurrido');
            }

        });
    });

    $('#date_min').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl'
    }).on('changeDate', function (selected) {
        var startDate = new Date(selected.date.valueOf());
        $('#date_max').datepicker('setStartDate', startDate);
    }).on('clearDate', function (selected) {
        $('#date_max').datepicker('setStartDate', null);
    });

    $('#date_max').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl'
    }).on('changeDate', function (selected) {
        var endDate = new Date(selected.date.valueOf());
        $('#date_min').datepicker('setEndDate', endDate);
    }).on('clearDate', function (selected) {
        $('#date_min').datepicker('setEndDate', null);
    });




    $('#checkFiltrarFecha').change(function () {

        if (!$('#checkFiltrarFecha').prop('checked')) {
            //var table = $('#usersoffers').DataTable();
            $('.dt-buttons').hide();
            $('#usersoffers_filter').hide();
            $('#usersoffersf_filter').hide();
            if (usersoffers)
                usersoffers.search('').columns().search('').draw();
            if (typeof usersoffersf !== 'undefined')
                usersoffersf.search('').columns().search('').draw();

            $('#date_min, #date_max').val('');
            $('#fitrofechas').hide();
        } else
            $('#fitrofechas').show();

    });




    $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {

                var min = $('#date_min').datepicker('getDate');
//                 $.datepicker.formatDate('dd/mm/yyyy', min);
                // var min = new Date($('#date_min').val());
                var max = $('#date_max').datepicker('getDate');
//                $.datepicker.formatDate('dd/mm/yyyy', max);

                // var max = $('#date_max').val();
                var filtrar_fecha = $('#checkFiltrarFecha').prop('checked');

                var dateParts = data[2].split(" ");
                dateParts = dateParts[0].split("/");

                var startDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);



                if (filtrar_fecha) {



                    if (min == null && max == null)
                        return true;
                    if (min == null && startDate <= max)
                        return true;
                    if (max == null && startDate >= min)
                        return true;
                    if (startDate <= max && startDate >= min)
                        return true;
                    return false;
                } else
                    return true;
            }
    );

//    $(document).on('click', '#generarCertificadoOfertas', function (e) {
//        e.preventDefault();
//        var id = $('#usersoffers').data('user');
//        var offers = selectedIds;
//
//        $.ajax({
//            url: '/user/generarCertificadoOfertas',
//            type: 'POST',
//            data: {
//                id: id,
//                offers: offers
//            },
//
//            success: function (response) {
//
//                var blob = new Blob([response]);
//                var link = document.createElement('a');
//                link.href = window.URL.createObjectURL(blob);
//                link.download = "certificado.pdf";
//                link.click();
//
//
//
//            },
//            error: function () {
//                alert('Un error ha ocurrido');
//            }
//
//        });
//    });

    $(document).on('click', '#generarCertificadoOfertas', function (e) {
        e.preventDefault();

        if (typeof usersoffersf !== 'undefined') {
            var id = $('#usersoffersf').data('user');
            var rows = $('#usersoffersf tr');
        } else {

            if (usersoffers) {
                var id = $('#usersoffers').data('user');
                var rows = $('#usersoffers tr');
            }

        }



        var offers = [];

        rows.each(function (indice, elemento) {
            if ($(this).attr('id'))
                offers.push($(this).attr('id'));

        });

        var link = document.createElement('a');
        link.href = '/user/generarCertificadoOfertas?user=' + id + '&&offers=' + offers;
        link.target = '_blank';
        // link.download = "certificado.pdf";
        link.click();

    });

    $(document).on('click', '#certificadotodas', function (e) {
        e.preventDefault();


        $('#generarCertificadoOfertas').click();

    });





});
