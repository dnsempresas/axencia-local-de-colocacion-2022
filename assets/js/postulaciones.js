jQuery(document).ready(function ($) {
     $.fn.dataTable.moment("DD/MM/YYYY HH:mm:ss");
     
    $('#table-inscritos-job').DataTable({
        responsive: true,
        language: {

            url: '/assets/js/datatable_' + $('#imgidiomaselect').attr('alt') + '.json'//Ubicacion del archivo con el json del idioma.
        },
        "bSort": true,
        "bFilter": true,
        "aaSorting": [[0, "DESC"]],
         "ordering" : true,
        "iDisplayLength": 100,
        dom: 'Bfrtip',
        
        
        buttons: [
            {
                extend: 'excel',
                text: '<span class="fa fa-file-excel-o"></span> Exportar a Excel',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'csv',
                text: '<span class="glyphicon-export"></span> Exportar a Csv',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'pdf',
                text: '<span class="fa fa-file-pdf-o"></span> Exportar a Pdf',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'print',
                text: '<span class="fa fa-print bigger-110"></span> Imprimir',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            }

        ],
    });

    $('#buscar-user-job').on('click', function (e) {
        e.preventDefault();
        idUser = $('#dni').val();
        idOferta = $('#idOferta').val();
        if (idUser!=''){
        $.ajax({
            url: '/ofertas/buscardatos',
            data: {'idUser': idUser, 'idOferta': idOferta},
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            method: 'POST',
            type: 'JSON',
            beforeSend: function () {

            },
            success: function (response) {
                if (response.message == 'Exitoso') {
                    $('#Datos').text(response.usuario[0].name + ' ' + response.usuario[0].surname);
                    $('#idUser').val(response.usuario[0].pk);

                    $('#datosdelusuario').text(response.usuario[0].cif + ' ' + response.usuario[0].name + ' ' + response.usuario[0].surname);
                    
                    var curriculo =
                            ' Seleccione Curriculo: <select required name="curriculo" id="curriculo" required style="margin-right: 0;margin-left: 0;width: 100%;">'; 
                   
                   if (response.usuario[0].cvprincipal != null && response.usuario[0].cvprincipal.length > 1)  
                         curriculo = curriculo  +   '<option  value="' + response.usuario[0].cvprincipal + '">' + response.usuario[0].cvprincipal + '</option>';

                     if (response.usuario[0].cvsecundario != null && response.usuario[0].cvsecundario.length > 1) 
                        curriculo = curriculo + '<option  value="' + response.usuario[0].cvsecundario + '">' + response.usuario[0].cvsecundario + '</option>';

                    curriculo = curriculo + '</select>';
                    
                    $('#curriculos').html (curriculo);

                    $('#modalincribir').modal('show');

                } else {
                    if (response.message == 'Ya se encuentra inscrito') {

                        alert(response.message);

                    } else {
                        alert(response.message);
                    }
                }
            }
        })
    }
    });



});
