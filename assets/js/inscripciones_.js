jQuery(document).ready(function ($) {
    $('#table-inscritos').DataTable({
        responsive: true,
        language: {

            url: '/assets/js/datatable_' + $('#imgidiomaselect').attr('alt') + '.json'//Ubicacion del archivo con el json del idioma.
        },
        "bSort": true,
        "bFilter": true,
        "aaSorting": [[0, "DESC"]],
        "iDisplayLength": 100,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                text: '<span class="fa fa-file-excel-o"></span> Exportar a Excel',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'csv',
                text: '<span class="glyphicon-export"></span> Exportar a Csv',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'pdf',
                text: '<span class="fa fa-file-pdf-o"></span> Exportar a Pdf',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'print',
                text: '<span class="fa fa-print bigger-110"></span> Imprimir',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            }

        ],
    });

    $('#buscar-user').on('click', function (e) {
        e.preventDefault();
        idUser = $('#dni').val();
        idCurso = $('#idCurso').val();
        $.ajax({
            url: '/cursos/buscardatos',
            data: {'idUser': idUser, 'idCurso': idCurso},
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            method: 'POST',
            type: 'JSON',
            beforeSend: function () {

            },
            success: function (response) {
                if (response.message == 'Exitoso') {
                    $('#Datos').text(response.usuario[0].name + ' ' + response.usuario[0].surname);
                    $('#idUser').val(response.usuario[0].pk);
                    
                      $('#datosdelusuario').text(response.usuario[0].cif+  ' '+response.usuario[0].name + ' ' + response.usuario[0].surname);
                    
                    $('#modalincribir').modal('show');
                    
                } else {
                    if (response.message == 'Ya se encuentra inscrito') {

                        alert(response.message);

                    } else {
                        alert(response.message);
                    }
                }
            }
        })
    });



});
