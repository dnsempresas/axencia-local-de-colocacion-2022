$(document).ready(function ($) {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });




    $(document).on('click', '#remove_link_oferta_ext', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        if (window.confirm("'Realmente desea eliminar este elemento?'")) {

            id_boletin = $(this).data('boletin');
            id_oferta = $(this).data('oferta');


            $.ajax({
                url: '/panel/admin/boletines/ofertaexterna/remove',
                type: 'POST',
                data: {
                    id_boletin: id_boletin,
                    id_oferta: id_oferta
                },

                success: function (response) {

                    location.reload();

                },
                error: function () {
                    alert('Un error ha ocurrido');
                }

            });
        }
    });


    function appendLeadingZeroes(n) {
        if (n <= 9) {
            return "0" + n;
        }
        return n
    }






    $(document).on('click', '#edit_link_oferta_ext', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        var oferta = $(this).data('objeto');

        $('#epublicada_en').val(oferta.publicada_en);
        $('#edenominacion_da_oferta').val(oferta.denominacion_da_oferta);

        let current_datetime = new Date(oferta.data_da_oferta)
        let formatted_date = current_datetime.getDate() + "/" + appendLeadingZeroes(current_datetime.getMonth() + 1) + "/" + current_datetime.getFullYear()
        console.log(formatted_date);

        $('#edatadaoferta').val(formatted_date);
        $('#elocalidade').val(oferta.localidade);
        $('#en_postos').val(oferta.n_postos);
        $('#edescricion').val(oferta.descricion);
        $('#erequisitos').val(oferta.requisitos);
        $('#etipo_contrato').val(oferta.tipo_contrato);
        $('#eotros_datos').val(oferta.otros_datos);
        $('#econtacto').val(oferta.contacto);
        $('#elink_publicacion').val(oferta.link_publicacion);
        $('#id_oferta').val(oferta.id);


        $('#Editmodalofertasexternas').modal('show');

    });

    $('#table-ofertasinternas').DataTable({
        responsive: true,
        language: {

            url: '/assets/js/datatable_' + $('#imgidiomaselect').attr('alt') + '.json'//Ubicacion del archivo con el json del idioma.
        },
        "bSort": true,
        "bFilter": true,
        "aaSorting": [[2, "DESC"]],
        "iDisplayLength": 100,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                text: '<span class="fa fa-file-excel-o"></span> Exportar a Excel',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'csv',
                text: '<span class="glyphicon-export"></span> Exportar a Csv',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'pdf',
                text: '<span class="fa fa-file-pdf-o"></span> Exportar a Pdf',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'print',
                text: '<span class="fa fa-print bigger-110"></span> Imprimir',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            }

        ],
    });

    $(document).on('click', '#buscar-job', function (e) {

        $('#modalofertas').modal('show');


    });
    $(document).on('click', '#buscar-job-externas', function (e) {

        $('#modalofertasexternas').modal('show');


    });
    $(document).on('click', '#guardarofertasenboletin', function (e) {

        e.preventDefault();



        var url = $(this).attr('href');
        if (window.confirm("'Realmente agregar estos elementos al boletin?'")) {



            var cellsSelected = tabla_ofertasinternas.rows({selected: true}).data();


            var ofertasseleccionadas = [];
            // Iterate over all selected checkboxes
            $.each(cellsSelected, function (index, rowId) {
                // Create a hidden element

                ofertasseleccionadas.push(rowId[0]);


            });




            $.ajax({
                url: '/panel/admin/boletines/guardarofertasinternas',
                type: 'POST',
                data: {
                    ofertas_selected: ofertasseleccionadas,
                    id_boletin: id_boletin
                },

                success: function (response) {

                    location.reload();

                },
                error: function () {
                    alert('Un error ha ocurrido');
                }

            });
        }




    });


    var id_boletin = $('#id_boletin').val();
//    $(document).on('click', '#buscar-job', function (e) {
//        e.preventDefault();
//        var id_boletin = $('#id_boletin').val();
//        $.ajax({
//            url: '/panel/admin/boletines/listarofertasinternas',
//            type: 'POST',
//            async: false,
//            data: {
//                id_boletin: id_boletin
//
//            },
//
//            success: function (response) {
//                var result = '';
//                var json_ofertas = response.ofertas;
//                $.each(json_ofertas, function (k, v) {
//
//
//                    result += '<tr> \n\
//                        <td>' + v.requested_job + '</td> \n\
//                        <td>' + v.concello + '</td> \n\
//                        <td>' + v.created_at + '</td>\n\</tr>';
//
//                });
//
//                $('#tabla-ofertasinternas').DataTable({
//                    responsive: true,
//                    language: {
//
//                        url: '/assets/js/datatable_' + $('#imgidiomaselect').attr('alt') + '.json'//Ubicacion del archivo con el json del idioma.
//                    },
//                    "bSort": true,
//                    "bFilter": true,
//                    "paging": false // false to disable pagination (or any other option)
//                });
//                $('.dataTables_length').addClass('bs-select');
//
//
//
//                $('#tbodyofertas').html(result);
//
//                $('#modalofertas').modal('show');
//
//            },
//            error: function () {
//                alert('Un error ha ocurrido');
//            }
//
//        });
//
//
//
//    });

    var tabla_ofertasinternas = $('#tabla_ofertasinternas').DataTable({
        responsive: true,
        language: {

            url: '/assets/js/datatable_' + $('#imgidiomaselect').attr('alt') + '.json'//Ubicacion del archivo con el json del idioma.
        },
        "bSort": true,
        "bFilter": true,
        "aaSorting": [[2, "DESC"]],
        select: true,

        //"serverSide": true,
        "paging": true,
        "pageLength": 10,
        rowId: 'id',
        "columnDefs": [{
                orderable: false,
                className: 'select-checkbox',
                targets: 4
            }]



//        "ajax": {
//            url: "/panel/admin/boletines/listarofertasinternas",
//            data: {id_boletin :id_boletin},
//            type: 'POST'
//        },
//        "columns": [
//            {data: 'id'},
//            {data: 'requested_job'},
//            {data: 'created_at'},
//            {data: 'concello'},
//            {data: 'options'}],


    });
    var tabla_ofertasinternas = $('#tabla-ofertasexternas').DataTable({
        responsive: true,
        language: {

            url: '/assets/js/datatable_' + $('#imgidiomaselect').attr('alt') + '.json'//Ubicacion del archivo con el json del idioma.
        },
        "bSort": true,
        "bFilter": true,
        "aaSorting": [[2, "DESC"]],
        select: true,

        //"serverSide": true,
        "paging": true,
        "pageLength": 10,
        rowId: 'id'

    });

    $('#datadaoferta').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl',
        startDate: new Date()
    });
    $('#edatadaoferta').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        language: 'gl',
        startDate: new Date()
    });


});
