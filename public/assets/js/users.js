jQuery(document).ready(function ($) {



    $('#userForm').on('submit', function () {
        if ($('#tiporole').val() == 2) {
            if (!($('#name_e').val() == null || $('#name_e').val() == undefined))
                $('#name').val($('#name_e').val());
            if ($('#last_name_e').val())
                $('#last_name').val($('#last_name_e').val());
            if ($('#cif_e').val())
                $('#cif').val($('#cif_e').val());


        } else {
            if ($('#tiporole').val() == 1) {

            }
        }
    });
    $("#driving_permit").chosen({
        width: "100%"
                //html_template: '<img style="border:3px solid black;padding:0px;margin-right:4px" class="{class_name}" src="{url}">'
    });

    var listItems = '<option selected="selected" value="0">Seleccione...</option>';
//   // $("#specialty2").prop('disabled', true).trigger("chosen:updated");;

    $("#specialty2").chosen().on('chosen:showing_dropdown', function () {
        $('.chosen-select').attr('disabled', true).trigger('chosen:updated');
        $('.chosen-select').attr('disabled', false).trigger('chosen:updated');
        $('.search-choice-close').hide();
    });
    //$('.search-choice-close').hide();
    $("#specialty2").chosen({disable_search: true, placeholder_text_single: ''}).trigger("chosen:updated");

//    $.validator.addMethod('filesize', function (value, element, arg) {
//        var minsize = 1000; // min 1kb
//        if ((value > minsize) && (value <= arg)) {
//            return true;
//        } else {
//            return false;
//        }
//    });

    $('#userForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            name: {required: function (element) {
                    if ($('#tiporole').val() == 1) {
                        return true;
                    } else {
                        return false;
                    }
                }},
            last_name: {required: function (element) {
                    if ($('#tiporole').val() == 1) {
                        return true;
                    } else {
                        return false;
                    }
                }},
            cif: {required: function (element) {
                    if ($('#tiporole').val() == 1) {
                        return true;
                    } else {
                        return false;
                    }
                }, Dni: true},
            birth: {required: true, validDate: true},
            address: {required: true},
            postal_code: {required: true},
            phone: {required: true, phone: true},
            email: {required: true, email: true},
            province: {required: true},
            concello_chosen: {required: true},
            name_e: {required: function (element) {
                    if ($('#tiporole').val() == 2) {
                        return true;
                    } else {
                        return false;
                    }
                }},
            last_name_e: {required: function (element) {
                    if ($('#tiporole').val() == 2) {
                        return true;
                    } else {
                        return false;
                    }
                }},
            cif_e: {required: function (element) {
                    if ($('#tiporole').val() == 2) {
                        return true;
                    } else {
                        return false;
                    }
                }},
            cvprincipal: {
                required: function (element) {
                    if ($('#tiporole').val() == 1) {
                        if ($('#nombreCv1').text() == '') {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                },
//                filesize: 3000000   //max size 3000 kb
            }
        },
        messages: {
            name: {
                required: 'Requerido'
            },
            last_name: {
                required: 'Requerido'
            },
            cif: {
                required: 'Requerido',
                Dni: 'Erroneo'
            },
            birth: {
                required: 'Requerido',
                validDate: 'Error'
            },
            address: {
                required: 'Requerido'
            },
            postal_code: {
                required: 'Requerido'
            },
            phone: {
                required: 'Requerido',
                phone: 'Erroneo'
            },
            email: {
                required: 'Requerido',
                email: 'Erroneo'
            },
            province: {
                required: 'Requerido'
            },
            concello_chosen: {
                required: 'Requerido'
            },
            job_posts: {
                required: 'Requerido'
            },
            name_e: {
                required: 'Requerido'
            },
            last_name_e: {
                required: 'Requerido'
            },
            cif_e: {
                required: 'Requerido',
                Dni: 'Erroneo'
            },
            cvprincipal: {
                required: 'Requerido',
//                filesize: 'Tamaño no permitido'
            },
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error');
            $(e).remove();
        },
        errorPlacement: function (error, element) {
            if (element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1)
                    controls.append(error);
                else
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            } else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            } else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            } else
                error.insertAfter(element.parent());
        },
        submitHandler: function (form) {
            form.submit();
        },
        invalidHandler: function (form) {}
    });

    $('.chosen-select').chosen({allow_single_deselect: true});

    var user_table = $('#users-table').DataTable({
        responsive: true,
        language: {

            url: '/assets/js/datatable_' + $('#imgidiomaselect').attr('alt') + '.json'//Ubicacion del archivo con el json del idioma.
        },
        "processing": true,
        "serverSide": true,
        "lengthMenu": [[100, -1], [100, "All"]],
        "iDisplayLength": 1000,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                text: '<span class="fa fa-file-excel-o"></span> Exportar a Excel',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        page: 'all',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'csv',
                text: '<span class="glyphicon-export"></span> Exportar a Csv',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        page: 'all',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'pdf',
                text: '<span class="fa fa-file-pdf-o"></span> Exportar a Pdf',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        page: 'all',
                        order: 'applied'
                    }
                }
            },
            {
                extend: 'print',
                text: '<span class="fa fa-print bigger-110"></span> Imprimir',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        page: 'all',
                        order: 'applied'
                    }
                }
            }
        ],
        "ajax": {
            url: "/admin/users"
        },
        "columns": [
            {data: 'name'},
            {data: 'surname'},
            {data: 'age'},
            {data: 'email'},
            {data: 'cif'},
            {data: 'telephone'},
            {data: 'options'}],
        "bSort": true,
        "bFilter": true,
        "aaSorting": [[0, "ASC"]]
    });
    $('#user-filter').on('click', function () {
        user_table.columns(0).search($('#name').val());
        user_table.columns(1).search($('#surname').val());
        user_table.columns(2).search($('#cif').val());
        user_table.columns(3).search($('#email').val());
        user_table.columns(4).search($('#phone').val());
        user_table.columns(5).search($('#min-age').val() + '-' + $('#max-age').val());
        user_table.draw();
    });
    $('#reset-filter').on('click', function () {
        $('#name').val('');
        $('#surname').val('');
        $('#cif').val('');
        $('#email').val('');
        $('#phone').val('');
        $('#min-age').val('');
        $('#max-age').val();
        user_table.search('').columns().search('').draw();
    });

    $('#studies').on('change', function (e) {
        e.preventDefault();
        var study = $(this).val();

        var Text = $("#studies option:selected").text();

        if (study === '1' || study === '2' || study === '3') {

            $('#specialtyC').hide();
            $('#specialty').html('');
            $('#specialty2').html('');
            $('#specialty2').html('').chosen().trigger('chosen:updated');
            $("#specialty2").append('<option value="' + study + '" selected="selected" style="display:none"> ' + Text + '</option>');
            $('#specialty2').trigger('chosen:updated');

            return;
        } else {
            $('#specialtyC').show();
            $('#specialty').html('');

        }
        $.ajax({
            url: '/estudios/' + study + '/especialidades',
            method: 'GET',
            type: 'JSON',
            beforeSend: function () {

            },
            success: function (response) {
                listItems = '';
                listItems = '<option selected="selected" value="-1">Seleccione...</option>';

                for (var i = 0; i < response.length; i++) {
                    listItems += "<option value='" + response[i].id + "'>" + response[i].description + "</option>";
                }

                $('#specialtyC').show();
                $('#specialty').html(listItems);

                $("#specialty").trigger("chosen:updated");
                $('#specialty').focus();
            }
        })
    });

    $("#specialty").chosen().change(function () {
        var Value = $(this).val();
        //var optionText = $('#dropdownList option[value="'+optionValue+'"]').text();
        var Text = $("#specialty option:selected").text();

        console.log(Value, Text);

        $("#specialty2").append('<option value="' + Value + '" selected > ' + Text + '</option>');

        $('#specialty2').chosen().trigger('chosen:updated');


    });


    $('#province').on('change', function (e) {
        e.preventDefault();
        var $province = $(this).val();
        var $text = $("option:selected", this).text();
        if ($text === 'Otra') {
            $('#other_province').show();
            $('#other_province').focus();
            //$('#hall').hide();
            $('#concello_chosen').hide();
            $('#other_concello').show();
            return;
        } else {
            $('#other_province').hide();
            $('#other_province').val('');
            //$('#hall').show();
            $('#concello_chosen').val('');
            $('#concello_chosen').show();
            $('#other_concello').hide();
            $('#other_concello').val('');
        }
        $.ajax({
            url: '/province/' + $province + '/concellos',
            method: 'GET',
            type: 'JSON',
            beforeSend: function () {},
            success: function (response) {

                $('#concello_chosen').html(response).trigger("chosen:updated");
                $('#concello_chosen').focus();
            }
        })
    });

    $('#concello_chosen').on('change', function (e) {
        e.preventDefault();
        var $province = $('#province').val();
        var $text = $("option:selected", this).text();
        if ($text === 'Otro') {
            //$('#hall').hide();
            $('#concello_chosen').hide();
            $('#other_concello').show();
            $('#other_concello').focus();
            return;
        }


    });

    $('#tiporole').on('change', function (e) {

        var valor = $("option:selected", this).val();
        if (valor == 1) {

            $('#panel_datosempresariales').hide();
            $('#panel_personadecontacto').hide();


            $('#panel_datospersonales').show();

            var panel_direccion = $('#panel_direccion').detach();
            panel_direccion.appendTo('#izquierda');

            $('#panel_datosprofesionales').show();
            $('#panel_intereseprofesionales').show();




        } else {
            $('#panel_datosempresariales').show();
            $('#panel_personadecontacto').show();
            $('#panel_datospersonales').hide();

            var panel_direccion = $('#panel_direccion').detach();

            panel_direccion.prependTo('#derecha');

            $('#panel_datosprofesionales').hide();
            $('#panel_intereseprofesionales').hide();
        }

    });




    $('#genero').on('change', function (e) {
        e.preventDefault();
        var $text = $("option:selected", this).text();
        if (($text === 'OTRO') || ($text === 'Otro')) {
            //$('#hall').hide();
            //$('#genero').hide();
            $('#other_genero').val('');
            $('#other_genero').show();
            $('#other_genero').focus();
            return;
        }
        $('#other_genero').val('');
        $('#other_genero').hide();


    });
    $('#no_experience').on('click', function (e) {

        if ($(this).prop("checked")) {
            $('#divpuestos').hide();
            $('#divprestaciones').hide();

            $('#job_posts').removeAttr('required');
        } else {
            $('#divpuestos').show();
            $('#divprestaciones').show();
            $('#job_posts').setAttribute('required', 'required');
        }

        return;

    });


    var lang_input = $('#languages');
    lang_input.tag({placeholder: lang_input.attr('placeholder')});
    var computer_skills = $('#computer_skills');
    computer_skills.tag({placeholder: computer_skills.attr('placeholder')});
    $.mask.definitions['~'] = '[+-]';
    $('#birth').mask('99/99/9999');



    $('#birth').datepicker({
        defaultDate: new Date(),
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'
    }).on('changeDate', function (selected) {

    }).on('clearDate', function (selected) {

    });
});
