jQuery(document).ready(function($){
  var user_table = $('#users-table').DataTable({
    responsive: true,
    "processing": true,
    "serverSide": true,
    "ajax": {
      url: "/admin/users"
    },
    "columns": [
      { data: 'surname' },
      { data: 'name' },
      { data: 'age' },
      { data: 'email' },
      { data: 'cif' },
      { data: 'telephone' },
      { data: 'options' }],
    "bSort" : true,
    "bFilter": true,
    "aaSorting": [[ 0, "DESC" ]],
    "iDisplayLength": 100
  });
  $('#user-filter').on('click',function () {
    user_table.columns(0).search($('#name').val());
    user_table.columns(1).search($('#surname').val());
    user_table.columns(2).search($('#dni').val());
    user_table.columns(3).search($('#email').val());
    user_table.columns(4).search($('#phone').val());
    user_table.columns(5).search($('#min-age').val()+'-'+$('#max-age').val());
    user_table.draw();
  });

});
