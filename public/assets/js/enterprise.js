jQuery(document).ready(function ($) {

    $(document).on('click', '#remove_link', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        if (window.confirm("'Realmente desea eliminar este elemento?'")) {
            window.location = url;
        }
    });
    var enterprise_table = $('#enterprise-table').DataTable({
        responsive: true,
        language: {

            url: '/assets/js/datatable_' + $('#imgidiomaselect').attr('alt') + '.json'//Ubicacion del archivo con el json del idioma.
        },
        "processing": true,
        "serverSide": true,
        'scroller':    true,
        "ajax": {
            url: "/admin/enterprises"
        },
        "columns": [
             {data: 'name'},
            {data: 'surname'},
            
            {data: 'email'},
            {data: 'cif'},
            {data: 'telephone'},
            {data: 'contacto'},
            {data: 'options'}],
           
        'orderCellsTop': true,
        'autoWidth': false,
        'scrollX': true,
        'ordering': true,
        
        'fixedColumns': true,

        "bSort": true,
        "bFilter": true,
        "aaSorting": [[0, "DESC"]],
        "iDisplayLength": 100
    });
    $('#enterprise-filter').on('click', function () {
        enterprise_table.columns(0).search($('#name').val());
        enterprise_table.columns(1).search($('#surname').val());
        enterprise_table.columns(2).search($('#email').val());
        enterprise_table.columns(3).search($('#cif').val());
        enterprise_table.columns(4).search($('#telephone').val());
         enterprise_table.columns(5).search($('#contacto').val());
        
        enterprise_table.draw();
    });
     $('#reset-filter-e').on('click', function () {
        $('#name').val('');
        $('#surname').val('');
        $('#email').val('');
        $('#cif').val('');
        $('#phone').val('');
        $('#contacto').val('');
        enterprise_table.columns(0).search($('#name').val());
        enterprise_table.columns(1).search($('#surname').val());
        enterprise_table.columns(2).search($('#email').val());
        enterprise_table.columns(3).search($('#cif').val());
        enterprise_table.columns(4).search($('#telephone').val());
        enterprise_table.columns(5).search($('#contacto').val());
        //enterprise_table.draw('');
        enterprise_table.search('').columns().search('').draw();
    });

    $('#enterpriseForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            name: {required: true},
            surname: {required: true},
            email: {required: true, email: true},
            cif: {required: true,Dni: true},
            telephone: {required: true},
            contacto: {required: true},

        },
        messages: {
            name: {
                required: 'Requerido'
            },
            surname: {
                required: 'Requerido'
            },
            email: {
                required: 'Requerido',
                Dni: 'Error'
            },
            cif: {
                required: 'Requerido'
            },
            telephone: {
                required: 'Requerido',
                email: 'Error'
            },
             contacto: {
                required: 'Requerido',
                email: 'Error'
            },
            
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error');
            $(e).remove();
        },
        errorPlacement: function (error, element) {
            if (element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1)
                    controls.append(error);
                else
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            } else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            } else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            } else
                error.insertAfter(element.parent());
        },
        submitHandler: function (form) {
            form.submit();
        },
        invalidHandler: function (form) {}
    });
});
