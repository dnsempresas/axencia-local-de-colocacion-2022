<?php

use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\Tags\Url;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', 'FrontController@index');
Route::get('/home', 'FrontController@index');

Route::get('/panel', 'DashboardController@index');
Route::get('/admin', 'DashboardController@index');
Route::post('/admin/notificaciones', 'DashboardController@notificaciones');

Route::post('/admin/desmarcarnotificaciones', 'DashboardController@desmarcarnotificaciones');

Route::get('/admin/users', 'UsersController@listUsers');
Route::get('/admin/enterprises', 'EnterpriseController@listEnterprises');
Route::get('/admin/offers', 'JobOfferController@listOffers');
Route::get('/admin/news', 'NoticiaController@listNews');
Route::get('/admin/newsletters', 'NewsletterController@listNewsletters');
Route::get('/admin/studies/{id}/specialties', 'StudyController@specialties');
Route::get('/admin/province/{id}/concellos', 'ProvinceController@concellos');
Route::get('/admin/auditorys', 'AuditoriasController@listAuditorias');
Route::get('/admin/banners', 'BannerController@listBanners');
/* Route::get('/', function () {
  return redirect()->route('panel/index');
  }); */
Route::group(['prefix' => 'panel', /* 'middleware' => 'auth' */], function() {

    Route::post('/admin/ofertas/busquedaAvanzada', 'JobOfferController@index');
    Route::get('/admin/ofertas/{oferta_id}/inscripcion', 'JobOfferController@inscripcion');
    Route::post('/admin/ofertas/{oferta_id}/inscribirme', 'JobOfferController@inscribirme');
    Route::post('/admin/ofertas/inscribir', 'JobOfferController@confirmarInscripcion');
    //Route::post('/admin/ofertas/oferta_id', 'JobOfferController@confirmarInscripcion');
    Route::get('/admin/ofertas/{idOferta}/retirar/{idUser}', 'JobOfferController@retirarInscripcion');
    
    Route::get('/admin/ofertas/inscritos', 'JobOfferController@inscritos');

    Route::post('/admin/cursos/busquedaAvanzada', 'CursoController@index');
    Route::get('/admin/cursos/{curso_id}/inscripcion', 'CursoController@inscripcion');
    Route::post('/admin/cursos/{curso_id}/inscribirme', 'CursoController@inscribirme');
    Route::post('/admin/cursos/inscribir', 'CursoController@confirmarInscripcion');
    Route::get('/admin/cursos/{idCurso}/retirar/{idUser}', 'CursoController@retirarInscripcion');
    Route::resource('admin', 'DashboardController');


    Route::resource('boletin-ofertas', 'NewsletterController');
    Route::resource('auditorias', 'AuditoriasController');
    Route::get('/admin/empresas/{id}/remove', 'EnterpriseController@remove');
    Route::get('/admin/boletin-ofertas/{id}/remove', 'NewsletterController@remove');
    Route::get('/admin/ofertas/{id}/remove', 'JobOfferController@remove');
    Route::get('/admin/contratos/{id}/remove', 'ContractController@remove');
    Route::get('/admin/noticias/{id}/remove', 'NoticiaController@remove');
    Route::get('/admin/estudios/{id}/remove', 'EstudioController@remove');
    Route::get('/admin/especialidades/{id}/remove', 'SpecialtyController@remove');
    Route::get('/admin/permisos-conducir/{id}/remove', 'DrivingPermitController@remove');
    Route::get('/admin/usuarios/{id}/remove', 'UsersController@remove');
    Route::get('/admin/puestos/{id}/remove', 'PuestoController@remove');
    Route::get('/admin/sectores/{id}/remove', 'SectorController@remove');
    Route::get('/admin/boletines/{id}/remove', 'BoletinController@remove');
    Route::get('/admin/cursos/{id}/remove', 'CursoController@remove');
    Route::get('/admin/categorias/{id}/remove', 'CategoriaController@remove');
    Route::get('/admin/tipocursos/{id}/remove', 'TipoCursoController@remove');
    Route::get('/admin/disponibilidades/{id}/remove', 'DisponibilidadController@remove');
    Route::resource('sectores', 'SectorController');
    Route::get('/admin/banners/{id}/remove', 'BannerController@remove');



    //Route::get('/admin/boletines/{id}/edit2', 'BoletinController@edit2');

    Route::resource('boletines', 'BoletinController');

    Route::get('boletines2', 'BoletinController@index2');

    Route::get('/admin/boletines/{id}/generar', 'BoletinController@generar_boletin');
    Route::post('/admin/boletines/{id}/remove2', 'BoletinController@remove2');

    Route::get('/admin/boletines/{id}/edit2', 'BoletinController@edit2');

    Route::get('/admin/boletines/create2', 'BoletinController@create2');

    Route::post('/admin/boletines/store2', 'BoletinController@store2');
    Route::post('/admin/boletines/update2', 'BoletinController@update2');

    Route::resource('generos', 'GeneroController');
    Route::resource('categorias', 'CategoriaController');
    Route::resource('disponibilidades', 'DisponibilidadController');
    Route::resource('tipocursos', 'TipoCursoController');
    Route::resource('puestos', 'PuestoController');
    Route::resource('cursos', 'CursoController');
    Route::resource('contratos', 'ContractController');
    Route::resource('noticias', 'NoticiaController');
    Route::resource('estudios', 'EstudioController');
    Route::resource('especialidades', 'SpecialtyController');
    Route::resource('permisos-conducir', 'DrivingPermitController');
    Route::resource('ofertas', 'JobOfferController');
    Route::resource('usuarios', 'UsersController');
    Route::resource('banners', 'BannerController');
    
  
    
    Route::post('/admin/chart1', 'DashboardController@chart1');




    //Route::get('/admin', 'HomeController@index')->name('admin');

    Route::get('/admin/noticias/{id}/remove/{img}', 'NoticiaController@removeimgd')->name('noticias.removeimgd');
    Route::get('/admin/noticias/{id}/remove1/{img}', 'NoticiaController@removeimg1')->name('noticias.removeimg1');
    Route::get('/admin/noticias/{id}/remove2/{img}', 'NoticiaController@removeimg2')->name('noticias.removeimg2');

    Route::get('/admin/banners/{id}/remove/{img}', 'BannerController@removeimgd')->name('banners.removeimgd');

    Route::get('/admin/ofertas/{id}/remove/{img}', 'JobOfferController@removeimgd')->name('ofertas.removeimgd');

    Route::post('/admin/chart1', 'DashboardController@chart1');

    Route::get('/admin/boletines/{id}/ofertasinternas', 'BoletinController@ofertasinternas');
    Route::post('/admin/boletines/ofertainterna/remove', 'BoletinController@ofertasinternas_remove');
    Route::post('/admin/boletines/listarofertasinternas', 'BoletinController@ofertasinternas_listar');
    Route::post('/admin/boletines/guardarofertasinternas', 'BoletinController@ofertasinternas_guardar');
    Route::post('/admin/boletines/updateofertasinternas', 'BoletinController@ofertasinternas_update');


    Route::get('/admin/boletines/{id}/ofertasexternas', 'BoletinController@ofertasexternas');
    Route::post('/admin/boletines/ofertaexterna/remove', 'BoletinController@ofertasexternas_remove');
    Route::post('/admin/boletines/listarofertasexternas', 'BoletinController@ofertasexternas_listar');
    Route::post('/admin/boletines/guardarofertasexternas', 'BoletinController@ofertasexternas_guardar');
    Route::post('/admin/boletines/updateofertasexternas', 'BoletinController@ofertasexternas_update');


    Route::get('/admin/ofertas/seleccion', 'JobOfferController@reclutamiento');
    Route::post('/admin/ofertas/actualizar_campo', 'JobOfferController@actualizarCampo');
    
    Route::post('/admin/ofertas/actualizar_campo', 'JobOfferController@actualizarCampo');
    
     Route::post('/admin/ofertas/actualizar_rematado', 'JobOfferController@actualizarRematado');
    
     Route::get('/sepe-estadisticas', 'JobOfferController@sepeEstadisticas');
    
      Route::get('/descargarXml', 'JobOfferController@descargarXml');
      
      
      
      Route::get('/admin/ofertas/sectores', 'SectorOfertaController@index');
      
      Route::get('/admin/ofertas/sectores/create', 'SectorOfertaController@create');
      
       Route::post('/admin/ofertas/sectores/store', 'SectorOfertaController@store');
       
       Route::get('/admin/ofertas/sectores/{id}/edit', 'SectorOfertaController@edit');
       
       Route::post('/admin/ofertas/sectores/update', 'SectorOfertaController@update');
       
       Route::get('/admin/ofertas/sectores/{id}/remove', 'SectorOfertaController@remove');
       
});


Route::get('/estudios/{id}/especialidades', 'EstudioController@especialidades');
Route::get('/province/{id}/concellos', 'ProvinceController@concellos');
Route::get('/bajarcv/{filename}/', 'UsersController@downloadcv');
Route::get("/getFoto/{nombre}", 'UsersController@descargarFoto');

Route::post('user/login', 'UsersController@login');

Route::post('user/darsedebaja', 'UsersController@darsedebaja');

Route::post('user/obtenercertificado', 'UsersController@obtenerCertificado');

Route::post('user/dardebaja', 'UsersController@dardebaja');


Route::get('user/logout', 'UsersController@logout');

Route::post('user/resetpassword', 'UsersController@resetpassword');

Route::post('cursos/buscardatos', 'CursoController@buscardatos');
Route::post('ofertas/buscardatos', 'JobOfferController@buscardatos');

Route::get('registrarperfil', 'FrontController@registrarPerfil');
Route::get('editarperfil', 'FrontController@editarPerfil');

Route::get('/ofertas', 'FrontController@ofertas');
//Route::get('oferta-ficha/{id}', 'FrontController@ofertaFicha');
Route::get('ofertas/{id}/{slug}', 'FrontController@ofertaFicha');

Route::get('ofertas/buscar', 'FrontController@buscarOfertas');

Route::get('ofertasempresa/{slug}', 'FrontController@ofertasEmpresa');

Route::get('formacion', 'FrontController@cursos');
Route::get('curso-ficha/{id}', 'FrontController@cursoFicha');
Route::get('formacion/{id}/{slug}', 'FrontController@cursoFicha');

Route::get('actualidade', 'FrontController@actualidade');
//Route::get('actualidade-ficha/{id}', 'FrontController@actualidadeFicha');
Route::get('actualidade/{id}/{slug}', 'FrontController@actualidadeFicha');

Route::get('pactoporempleo', 'FrontController@pactoporempleo');
Route::get('talentia', 'FrontController@talentia');
Route::get('axencia', 'FrontController@axencia');
Route::get('recursos', 'FrontController@recursos');

Route::get('accesibilidad', 'FrontController@accesibilidad');
Route::get('avisolegal', 'FrontController@avisolegal');
Route::get('boletins', 'FrontController@boletines');

Route::post('user/consolidar', 'UsersController@consolidar');

Route::post('user/postularme', 'UsersController@postularme');
Route::post('user/inscribirme', 'UsersController@inscribirme');
//Route::get('user/postulaciones', 'UsersController@postulaciones');
Route::get('user/misinscripciones/{user_id}', 'UsersController@misInscripciones');
Route::post('user/resumeninscripciones', 'UsersController@resumenInscripciones');

Route::post('user/mostrarauditorias', 'UsersController@mostrarauditorias');

Route::post('user/reenviarconfirmacion', 'UsersController@reenviarconfirmacion');

Route::get('user/{id}/certificado', 'UsersController@certificado');
Route::get('user/{id}/certificadooferta/{jobOffer}', 'UsersController@certificadoOferta');
Route::get('user/{id}/certificadoOfertas', 'UsersController@certificadoOfertas');
Route::get('user/{id}/resumenInscripcionesOfertas', 'UsersController@resumenInscripcionesOfertas');

Route::get('user/generarCertificadoOfertas', 'UsersController@generarCertificadoOfertas');



//Route::get('consolidarperfil/{id}', 'FrontController@consolidadperfil');
//Route::get('consolidarperfil/{id}', 'FrontController@consolidadperfil');

Route::resource('/panel/empresas', 'EnterpriseController');

Route::get('/empresas', 'EnterpriseController@panel');

Route::group(['prefix' => 'empresas', /* 'middleware' => 'auth' */], function() {

    Route::get('ofertas', 'EnterpriseController@ofertasEmpresa');
    Route::get('ofertas/create', 'EnterpriseController@ofertasEmpresaCreate');
    Route::get('ofertas/{id}/edit', 'EnterpriseController@ofertasEmpresaEdit');

    Route::get('ofertas/{id}/remove', 'EnterpriseController@ofertasEmpresaRemove');

    Route::get('/perfil', 'EnterpriseController@perfilEmpresa');
    Route::post('/chart1', 'EnterpriseController@chart1');
});

Route::get('verificar/usuario/{code}', 'UsersController@verify');

Route::get('404', ['as' => '404', 'uses' => 'FrontController@errorCode404']);

Route::get('405', ['as' => '405', 'uses' => 'FrontController@errorCode405']);

//Route::get('500', ['as' => '405', 'uses' => 'FrontController@errorCode500']);

Route::get('sitemap/generate', function () {
    SitemapGenerator::create('https://www.axencialocaldecolocacion.org')->writeToFile('sitemap.xml');
});

Auth::routes();


//Route::post('/login', 'UsersController@login');
//Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['web']], function () {

    Route::get('lang/{lang}', function ($lang) {
        session(['lang' => $lang]);
        return \Redirect::back();
    })->where([
        'lang' => 'gl|es'
    ]);
});
