<?php
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Genero extends Model {

    protected $table = 'generos';
    protected $fillable = [
        'nombre',
        'nome',
        'abriviatura',
        
        
    ];
    protected $visible = [
        'nombre',
        'nome',
        'abriviatura',
    ];

}