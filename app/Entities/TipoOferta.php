<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class TipoOferta extends Model
{
    protected $table = 'tiposdeoferta';
    protected $fillable = [
        'nombre',
        'nome'
    ];
     protected $visible = [
        'nombre',
        'nome'
    ];
   
    public function ofertas()
    {
        return $this->belongsToMany('JobOfeer', 'tipooferta');
    }
    
}
