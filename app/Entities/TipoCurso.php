<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class TipoCurso extends Model
{
    protected $table = 'tiposdecurso';
    protected $fillable = [
        'nombre',
        'nome'
    ];
     protected $visible = [
        'nombre',
        'nome'
    ];
   
    public function cursos()
    {
        return $this->belongsToMany('Curso', 'tipocurso');
    }
    
}
