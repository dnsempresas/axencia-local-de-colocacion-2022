<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Support\Str as Str;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model {

    protected $table = 'banner';
    protected $primaryKey = 'id';

    /**
     * @var bool
     */
    public $timestamps = false;

    public function getSlug() {
        
       return   url('publicidad/'.$this->id.'/'. Str::Slug($this->titulo));
       
    }

    protected $fillable = [
        'titulo',
        'enlace',
        'fecha_publicacion',
        'fecha_expiracion',
        'imagen_destacada',
        

    ];
    protected $visible = [
        'id',
        'titulo',
        'enlace',
        'fecha_publicacion',
        'fecha_expiracion',
        'imagen_destacada',
        
    ];

    public function getFecha_publicacion() {
        return Carbon::createFromFormat('Y-m-d', $this->fecha_publicacion)->format('d/m/Y');
        //return($this->due_date);
    }

    public function getFecha_expiracion() {
        return Carbon::createFromFormat('Y-m-d', $this->fecha_expiracion)->format('d/m/Y');
        //return($this->due_date);
    }

   

    public function getFechaLetras() {
        $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $fecha = Carbon::parse($this->fecha_publicacion);
        $mes = $meses[($fecha->format('n')) - 1];
        return $fecha->format('d') . ' de ' . $mes . ' de ' . $fecha->format('Y');
    }

}
