<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class NotificacionesAdmin extends Model {

    protected $table = 'admin_notificaciones';
    protected $primaryKey = 'id';
    protected $fillable = [
        'mensaje',
        'tabla',
        'tabla_id',
        'ruta',
        'leido',
        'procesado',
    ];
  

}
