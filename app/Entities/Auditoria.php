<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Auditoria extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'logauditory';
    protected $primaryKey = 'id';

    /**
     * @var bool
     */
    public $timestamps = false;

    public function getPublishedAt(){
        return Carbon::createFromFormat('Y-m-d H:i:s',$this->publishedAt)->format('d/m/Y');
    }


    protected $fillable = [
        'model_name',
        'user_id',
        'action',
        'original_set',
        'changed_set'
       
    ];
    
     public function usuario()
    {
        return $this->belongsTo('App\Entities\User', 'pk', 'user_id');
        
        

    }
    
    
    
}
