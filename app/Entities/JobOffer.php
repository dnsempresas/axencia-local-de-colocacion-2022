<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str as Str;

class JobOffer extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'job_offer';
    protected $primaryKey = 'id';

    /**
     * @var bool
     */
    public $timestamps = false;

    public function getDueDate() {
        
        if ($this->due_date)
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->due_date)->format('d/m/Y');
        else return '';      //return($this->due_date);
    }

    public function getStartDate() {
        
           if ($this->start_date)
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->start_date)->format('d/m/Y');
        else return '';      //return($this->due_date);
         
       
    }

    public function getRequirements() {
        return unserialize($this->requirements);
    }

    public function contract() {
        return $this->hasOne(Contract::class, 'id', 'contract_type');
    }

    public function getFechaLetras() {
        
        if (session('lang')=='es')
        $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        else
          $meses = array("Xaneiro", "Febreiro", "Marzo", "Abril", "Maio", "Xuño", "Xullo", "Agosto", "Setembro", "Outubro", "Novembro", "Decembro");
        
            
        //$fecha = Carbon::parse($this->due_date);
        $fecha = Carbon::parse($this->created_at);
        $mes = $meses[($fecha->format('n')) - 1];
        return $fecha->format('d') . ' de ' . $mes . ' de ' . $fecha->format('Y');
    }
    
     public function getSlug() {
         
        return url('/ofertas/' . $this->id . '/' . Str::Slug($this->requested_job));
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'requested_job',
        'vacancy',
        'tasks',
        'due_date',
        'contract_type',
        'start_date',
        'duration',
        'tiempo',
        'locality',
        'hours',
        'salary',
        'rexperiencia',
        'rformacion',
        'rotros',
        'tipooferta',
        'tipogestion',
        'pertenece_fie',
        'created_at',
        'updated_at',
        'propietario_fk',
        'imagen',
        'updated_by',
        'created_by',
        'publicada',
        'province_fk',
        'concello_fk',
        'disponibilidad_fk',
        'expediente_rematado',
        'seleccion_rematada',
        'sector_fk',
    ];
    protected $visible = [
        'id',
        'requested_job',
        'vacancy',
        'tasks',
        'due_date',
        'contract_type',
        'start_date',
        'duration',
        'tiempo',
        'locality',
        'hours',
        'salary',
        'rexperiencia',
        'rformacion',
        'rotros',
        'tipooferta',
        'tipogestion',
        'pertenece_fie',
        'created_at',
        'updated_at',
        'propietario_fk',
         'imagen',
        'updated_by',
        'created_by',
        'publicada',
        'province_fk',
        'concello_fk',
        'disponibilidad_fk',
        'expediente_rematado',
        'seleccion_rematada',
        'sector_fk',
    ];

    public function tipooferta() {
        return $this->belongsTo(TipoOferta::class, 'tipooferta');
    }
    
      public function disponibilidad() {
        return $this->belongsTo(Disponibilidad::class, 'disponibilidad_fk');
    }
    public function propietario() {
        return $this->hasOne(User::class, 'pk', 'propietario_fk');
    }

    public function concello() {
        return $this->belongsTo(Concello::class, 'concello_fk');
    }
    
    public function province() {
        return $this->belongsTo(Province::class, 'province_fk');
    }
    
     public function sector() {
        return $this->belongsTo(SectorOferta::class, 'sector_fk');
    }
    
     public function postulados() {
         return $this->belongsToMany(User::class,'cv','job_fk', 'user_fk')->withPivot(['created','curriculo', 'cv_pasa','cv_no_pasa','cv_enviado',
             'entrevistado','hora_entrevista','contratado','data_contrato','tipo_contrato','empresa','cif_empresa', 'pk'])->orderBy('pivot_created','desc');
    }
    
    
}
