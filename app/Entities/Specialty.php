<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;

class Specialty extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'specialty';
    protected $primaryKey = 'id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description',
        'nome',
        'grade_id',
        'type'
    ];
    protected $visible = [
        'id',
        'description',
        'nome',
        'grade_id',
        'type'
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Study()
    {
        return $this->belongsTo(Grade::class,'grade_id','id');
        
    }
}
