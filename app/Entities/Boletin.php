<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Boletin extends Model
{
    protected $table = 'boletines';
    protected $fillable = [
        'nombre',
        'nome',
        'link'
    ];
     protected $visible = [
        'nombre',
        'nome',
        'link'
    ];
    
    
}
