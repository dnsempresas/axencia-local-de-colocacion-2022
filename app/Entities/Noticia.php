<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Support\Str as Str;
use Illuminate\Database\Eloquent\Model;

class Noticia extends Model {

    protected $table = 'noticia';
    protected $primaryKey = 'id';

    /**
     * @var bool
     */
    public $timestamps = false;

    public function getSlug() {
        
       return   url('actualidade/'.$this->id.'/'. Str::Slug($this->titulo));
       
    }

    protected $fillable = [
        'titulo',
        'antetitulo',
        'entradilla',
        'noticia',
        'fecha_publicacion',
        'fecha_expiracion',
        'imagen_destacada',
        'imagen1',
        'imagen2',
        'categoria_fk'
    ];
    protected $visible = [
        'id',
        'titulo',
        'antetitulo',
        'entradilla',
        'noticia',
        'fecha_publicacion',
        'fecha_expiracion',
        'imagen_destacada',
        'imagen1',
        'imagen2',
        'categoria_fk'
    ];

    public function getFecha_publicacion() {
        return Carbon::createFromFormat('Y-m-d', $this->fecha_publicacion)->format('d/m/Y');
        //return($this->due_date);
    }

    public function getFecha_expiracion() {
        return Carbon::createFromFormat('Y-m-d', $this->fecha_expiracion)->format('d/m/Y');
        //return($this->due_date);
    }

    public function categoria() {
        return $this->belongsTo('App\Entities\Categoria', 'categoria_fk');
    }

    public function getFechaLetras() {
        $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $fecha = Carbon::parse($this->fecha_publicacion);
        $mes = $meses[($fecha->format('n')) - 1];
        return $fecha->format('d') . ' de ' . $mes . ' de ' . $fecha->format('Y');
    }

}
