<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'category';
    protected $fillable = [
        'nombre',
        'nome'
    ];
     protected $visible = [
        'nombre',
        'nome'
    ];

    public function noticias()
    {
        return $this->hasMany('App\Entities\Noticia', 'categoria_fk');
    }
    
}
