<?php //

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Notifications\MyResetPassword;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use App\Scopes\FechaBajaScope;

class User extends Model implements \Illuminate\Contracts\Auth\Authenticatable, \Illuminate\Contracts\Auth\CanResetPassword{

    use Authenticatable,CanResetPassword,
        Notifiable;
    
     protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new FechaBajaScope);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
//        'lang', 
//        'name',
//        'pass',
//        'surname',
//        'sex',
//        'birth',
//        'email',
//        'alias',
//        'company',
//        'nif',
//        'sector',
//        'telephone',
//        'other_phone',
//        'driving_permit',
//        'mobile',
//        'address',
//        'zip',
//        'city',
//        'country_fk',
//        'province_fk',
//        'grade',
//        'prestacion',
//        'country',
//        'cif',
//        'degree',
//        'languages',
//        'situation',
//        'experience',
//        'disability',
//        'role_id',
//        'status',
//        'city_hall',
//        'computer_skills',
//        'other_courses',
//        'specialty',
//        'study',
//        'job_posts',
//        'posts'
        'password',
        'lang',
        'name',
        'surname',
        'sex',
        'genero',
        'otro_genero',
        'birth',
        'email',
        'alias',
        'company',
        'nif',
        'sector',
        'telephone',
        'other_phone',
        'mobile',
        'address',
        'zip',
        'city',
        'country_fk',
        'province_fk',
        'concello_fk',
        'grade',
        'specialty',
        'other_courses',
        'prestacion',
        'country',
        'cif',
        'degree',
        'computer_skills',
        'languages',
        'situation',
        'experience',
        'disability',
        'updated_at',
        'created_at',
        'role_id',
        'status',
        'city_hall',
        'driving_permit',
        'position',
        'cvprincipal',
        'cvsecundario',
        'contacto',
        'cargocontacto',
        'alta',
        'fecha_alta',
        'perfilcompletado',
        'fechacompletado',
        'foto',
    ];
    protected $table = 'user';
    protected $primaryKey = 'pk';

    public function getFullNameAttribute() {
        return ucwords($this->name . ' ' . $this->surname);
    }

    public function getFullNameEmailAttribute() {
        return ucwords($this->email . ': ' . $this->name . ' ' . $this->surname);
    }

    public function getBirth() {
        return Carbon::createFromFormat('Y-m-d', $this->birth)->format('d/m/Y');
        //return($this->due_date);
    }

    public function setPassAttribute($value) {
        if (!empty($value)) {

            $this->attributes['pass'] = \Hash::make($value);
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country() {
        return $this->belongsTo(Country::class, 'country_fk');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function province() {
        return $this->belongsTo(Province::class, 'province_fk');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function concello() {
        return $this->belongsTo(Concello::class, 'concello_fk');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role() {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function auditorias() {
        return $this->hasMany('App\Entities\Auditoria', 'user_id', 'pk');
    }

    public function ofertasPublicadas() {
        return $this->hasMany('App\Entities\JobOffer', 'propietario_fk', 'pk');
    }

    public function postulaciones() {
        return $this->belongsToMany(JobOffer::class, 'cv', 'user_fk', 'job_fk')->withPivot(['created', 'curriculo']);
    }

    public function cursos() {
        return $this->belongsToMany(Curso::class, 'curso_user', 'user_id', 'curso_id')->withPivot('fechainscripcion');
    }
    
     public function experiencias() {
        return $this->belongsToMany(Puesto::class, 'user_experiencia', 'user_id', 'puesto_id');
    }
    
     public function puestosInteres() {
        return $this->belongsToMany(Puesto::class, 'user_puestos_interes', 'user_id', 'puesto_id');
    }
    
      public function sectoresInteres() {
        return $this->belongsToMany(Sector::class, 'user_sectores_interes', 'user_id', 'sector_id');
    }
    
      public function especialidades() {
        return $this->belongsToMany(Specialty::class, 'user_especialidad', 'user_id', 'especialidad_id');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'pass', 'remember_token',
    ];

    public function authorizeRoles($roles) {
        if ($this->hasAnyRole($roles)) {
            return true;
        }
        abort(401, 'Esta acción no está autorizada.');
    }

    public function hasAnyRole($roles) {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }

    public function hasRole($role) {
        if ($this->role()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }
    
public function sendPasswordResetNotification($token)
{
    $this->notify(new MyResetPassword($token));
}

 public function getFechaLetras() {
        
        if (session('lang')=='es')
        $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        else
          $meses = array("Xaneiro", "Febreiro", "Marzo", "Abril", "Maio", "Xuño", "Xullo", "Agosto", "Setembro", "Outubro", "Novembro", "Decembro");
        
            
        //$fecha = Carbon::parse($this->due_date);
        $fecha = Carbon::parse($this->created_at);
        $mes = $meses[($fecha->format('n')) - 1];
        return $fecha->format('d') . ' de ' . $mes . ' de ' . $fecha->format('Y');
    }
    

   
}
