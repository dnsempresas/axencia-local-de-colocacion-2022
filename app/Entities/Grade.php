<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'grade';
    protected $primaryKey = 'id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description',
        'nome',
        'nivelformativo',
        'titulacion'
    ];
    protected $visible = [
        'description',
        'nome',
        'nivelformativo',
        'titulacion'
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function Specialties()
    {
        return $this->hasMany(Specialty::class, 'grade_id', 'id');
    }
}
