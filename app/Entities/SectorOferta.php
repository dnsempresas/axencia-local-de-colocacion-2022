<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SectorOferta extends Model
{
    protected $table = 'sectores_ofertas';
    protected $fillable = [
        'nombre',
        'nome'
    ];
     protected $visible = [
        'nombre',
        'nome'
    ];
    public function ofertas()
    {
        return $this->hasMany(JobOffer::class, 'sector_fk');
    }
   
    
}
