<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    protected $table = 'sectores';
    protected $fillable = [
        'nombre',
        'nome'
    ];
     protected $visible = [
        'nombre',
        'nome'
    ];
    public function users()
    {
        return $this->belongsToMany('User', 'user_sector');
    }
    public function cursos()
    {
        return $this->belongsToMany('Curso', 'sector_id');
    }
    
}
