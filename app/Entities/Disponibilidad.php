<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Disponibilidad extends Model
{
    protected $table = 'disponibilidad';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nombre',
        'nome'
    ];
     protected $visible = [
        'nombre',
        'nome'
    ];
    
    public function joboffer()
    {
        return $this->hasMany('App\Entities\JobOffer', 'disponibilidad_fk', 'id');
        
        
    }
    
    
    
}
