<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Str as Str;

class Curso extends Model {
    
     public function getSlug() {
       
        
         return url('formacion/'.$this->id.'/'. Str::Slug($this->nombre));
                 
    }

    protected $visible = [
        'nombre',
        'lugar_celebracion',
        'fecha_inicio',
        'fecha_fin',
        'hora_inicio',
        'hora_fin',
        'edad_min',
        'edad_max',
        'duracion_horas',
        'plazas_total',
        //'plazas_disponibles',
        'edades_recomendadas',
        'pertenece_fie',
        'contenido',
        'created_at',
        'updated_at',
        'tipocurso',
        'propietario',
        'created_by',
        'updated_by',
        'imagen'
    ];

    public function sectores() {
        return $this->belongsToMany('App\Entities\Sector', 'curso_sector', 'curso_id', 'sector_id');
    }

    public function puestos() {
        return $this->belongsToMany('App\Entities\Puesto', 'curso_puesto', 'curso_id', 'puesto_id');
    }

    public function users() {
        return $this->belongsToMany('App\Entities\User', 'curso_user', 'curso_id', 'user_id')->withPivot('fechainscripcion')->orderBy('pivot_fechainscripcion','desc');;
        
    }
    
    

    public function getFechaLetras() {
        $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $fecha = Carbon::parse($this->fecha_inicio);
        $mes = $meses[($fecha->format('n')) - 1];
        return $fecha->format('d') . ' de ' . $mes . ' de ' . $fecha->format('Y');
    }

    public function tipocurso() {
        return $this->belongsTo(TipoCurso::class, 'tipocurso');
    }

}
