<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    protected $table = 'contract';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nombre',
        'nome'
    ];
     protected $visible = [
        'nombre',
        'nome'
    ];
    
    public function joboffer()
    {
        return $this->hasMany('App\Entities\JobOffer', 'contract_type', 'id');
        
        
    }
    
    
    
}
