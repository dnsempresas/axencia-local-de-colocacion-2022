<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MyResetPassword extends Notification {

    use Queueable;
    
    private $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token) {
        //
        $this->token=$token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable) {
        return (new MailMessage)
                        ->subject('Recuperar contraseña')
                        ->greeting('Ola')
                        ->line('Estas recibindo este correo porque fixeches una solicitude de recuperación de contrasinal para a tua conta')
                        ->action('Recuperar contraseña', route('password.reset', $this->token))
                        ->line('Si non realizaches esta solicitude non se require ninguna outra acción.')
                        ->salutation('Saudos, Axencia Local de Colocación');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable) {
        return [
                //
        ];
    }

}
