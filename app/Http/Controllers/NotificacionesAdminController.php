<?php

namespace App\Http\Controllers;

use Validator;
use App\Entities\NotificacionesAdmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificacionesAdminController extends Controller {
 public function __construct() {
//         $this->middleware('auth');
//        $this->middleware('admin', ['only' => ['index','create','edit','remove']]);
   }
  
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Genero  $genero
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
      
    }

    public function update(Request $request, $id) {
    }

   public function remove(Request $request, $id) {
      
    }

   public function show(Genero $genero) {
        
    }

}
