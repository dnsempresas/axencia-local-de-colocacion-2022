<?php

namespace App\Http\Controllers;

use Validator;
use App\Entities\Boletin;
use App\Entities\JobOffer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use Codedge\Fpdf\Facades\Fpdf;

//use Meneses\LaravelMpdf\Facades\LaravelMpdf;

class BoletinController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('admin', ['only' => ['index', 'create', 'edit', 'remove']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $boletines = Boletin::orderBy('created_at', 'DESC')->get();
        $message = "No hay boletines registrados";
        return view('boletin.index')->with(['boletines' => $boletines, 'message' => $message]);
    }

    public function index2(Request $request) {
        $boletines = Boletin::all();
        $message = "No hay boletines registrados";
        return view('boletin.index2')->with(['boletines' => $boletines, 'message' => $message]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('boletin.create')->with([]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $boletin = new Boletin();
        $boletin->nombre = $request->input('nombre');
        $boletin->nome = $request->input('nome');
        $boletinfile = $request->file('boletin');

        if (($boletin->nome == '') || ($boletin->nombre == '') || !($boletinfile)) {
            $request->session()->flash('alert-danger', 'Debe especifar todos los campos solicitados');
            return redirect()->route('boletines.create')->withInput();
        }



        $destinationPath = public_path('boletines');

        if ($boletinfile) {
            $imagen_str = strtolower($boletinfile->getClientOriginalName());

            $boletinfile->move($destinationPath, $imagen_str);

            $boletin->link = '/boletines/' . $imagen_str;
        }

        $editados = json_encode($boletin->getDirty());


        $boletin->save();

        \App\Entities\Auditoria::create([
            'model_name' => 'Boletin',
            'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
            'action' => 'Creó el registro Boletin ' . $boletin->id . ' ' . $editados,
            'original_set' => $boletin->toJson(),
            'changed_set' => ''
        ]);


        $request->session()->flash('alert-success', 'Boletin creado correctamente');
        return redirect()->route('boletines.index')->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Boletin  $boletin
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $boletin = Boletin::findOrFail($id);
        return view('boletin.edit')->with(['boletin' => $boletin]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Boletin  $boletin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        //$values = array('nombre', 'nome', 'link);
        $boletin = Boletin::find($id);
        $original = $boletin->toJson();

        $boletin->nombre = $request->input('nombre');
        $boletin->nome = $request->input('nome');

        $boletinfile = $request->file('boletin');

        if (($boletin->nome == '') || ($boletin->nombre == '')) {
            $request->session()->flash('alert-danger', 'Debe especifar todos los campos solicitados');
            return redirect()->route('boletines.create')->withInput();
        }



        $destinationPath = public_path('boletines');

        if ($boletinfile) {

            $image_path = public_path("{$boletin->link}");

            if (File::exists($image_path)) {
                File::delete($image_path);
                //unlink($image_path);
            }


            $imagen_str = strtolower($boletinfile->getClientOriginalName());

            $boletinfile->move($destinationPath, $imagen_str);

            $boletin->link = '/boletines/' . $imagen_str;
        }


        $editados = json_encode($boletin->getDirty());
        $boletin->update();


        if ($original != $boletin->toJson()) {
            \App\Entities\Auditoria::create([
                'model_name' => 'Boletin',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Actualizó el registro ' . $boletin->id . ' ' . $editados,
                'original_set' => $original,
                'changed_set' => $boletin->toJson()
            ]);
        }

        $request->session()->flash('alert-success', 'Boletin editado correctamente');
        return redirect()->route('boletines.index')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Boletin  $boletin
     * @return \Illuminate\Http\Response
     */
    public function remove(Request $request, $id) {
        $boletin = Boletin::find($id);
        $original = $boletin->toJson();

        $image_path = public_path("{$boletin->link}");

        if (File::exists($image_path)) {
            File::delete($image_path);
            //unlink($image_path);
        }



        $boletin->delete();
        \App\Entities\Auditoria::create([
            'model_name' => 'Boletin',
            'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
            'action' => 'Eliminó el registro Boletin ' . $id,
            'original_set' => $original,
            'changed_set' => ''
        ]);


        $request->session()->flash('alert-success', 'Boletin eliminado correctamente');
        return redirect()->route('boletines.index')->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Boletin  $boletin
     * @return \Illuminate\Http\Response
     */
    public function show(Boletin $boletin) {
        
    }

    public function ofertasinternas($id) {

        $boletin = Boletin::find($id);
        $ofertasboletin = $boletin->ofertasInternas()->orderBy('created_at', 'DESC')->get();

        $offersboletin = $boletin->ofertasInternas()->pluck('id_oferta')->toArray();

        $offers = DB::table('job_offer')
                        ->select('job_offer.id', 'job_offer.requested_job', 'job_offer.created_at', 'concello.name as concello')
                        ->join('concello', 'concello.id', '=', 'job_offer.concello_fk')
                        ->whereNotIn('job_offer.id', $offersboletin)
                        ->orderBy('created_at', 'DESC')->get();

        return view('boletin.ofertasinternas')->with(['boletin' => $boletin, 'ofertasboletin' => $ofertasboletin, 'ofertas' => $offers]);
    }

    public function ofertasinternas_remove(Request $request) {
        $id_boletin = $request['id_boletin'];
        $id_oferta = $request['id_oferta'];

        $boletin = Boletin::find($id_boletin);
        $offers = $boletin->ofertasInternas()->where('id_oferta', '=', $id_oferta)->detach($id_oferta);
        //return view('boletin.ofertasinternas')->with(['boletin' => $boletin, 'ofertas' => $boletin->ofertasInternas()->orderBy('created_at', 'DESC')->paginate(25)]);
    }

    public function ofertasinternas_guardar(Request $request) {
        $id_boletin = $request['id_boletin'];
        $ofertas = $request['ofertas_selected'];


        $boletin = Boletin::find($id_boletin);
        $boletin->ofertasInternas()->attach($ofertas);
        //return view('boletin.ofertasinternas')->with(['boletin' => $boletin, 'ofertas' => $boletin->ofertasInternas()->orderBy('created_at', 'DESC')->paginate(25)]);
    }

    public function ofertasinternas_listar(Request $request) {

        $id_boletin = $request['id_boletin'];


//
        $boletin = Boletin::find($id_boletin);
        $offersboletin = $boletin->ofertasInternas()->pluck('id_oferta')->toArray();

//        $offers = DB::table('job_offer')
//                        ->select('job_offer.id', 'job_offer.requested_job', 'job_offer.created_at', 'concello.name as concello')
//                        ->join('concello', 'concello.id', '=', 'job_offer.concello_fk')
//                        ->whereNotIn('job_offer.id', $offersboletin)
//                        ->orderBy('created_at', 'DESC')->paginate(25);
//
//        return response()->json(['ofertas' => $offers], 200);
        //return view('boletin.ofertasinternas')->with(['boletin' => $boletin, 'ofertas' => $boletin->ofertasInternas()->orderBy('created_at', 'DESC')->paginate(25)]);


        $draw = $request->get('draw');
        $fields = array('job_offer.id', 'job_offer.requested_job', 'job_offer.created_at', 'concello.name as concello');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->input('search.value');
        $colums = $request->get('columns');
        $query = array();
        $params = [];


        if (empty($search) && empty($query)) {
            $total_ofertas = JobOffer::whereNotIn('job_offer.id', $offersboletin)
                            ->join('concello', 'concello.id', '=', 'job_offer.concello_fk')->count();
            //$ofertas = JobOffer::orderBy('created_at', 'DESC')->skip($start)->take($length)->get($fields);





            $ofertas = DB::table('job_offer')
                            ->select('job_offer.id', 'job_offer.requested_job', 'job_offer.created_at', 'concello.name as concello')
                            ->join('concello', 'concello.id', '=', 'job_offer.concello_fk')
                            ->whereNotIn('job_offer.id', $offersboletin)
                            ->orderBy('created_at', 'DESC')->skip($start)->take($length)->get($fields);
        } else if (!empty($query)) {

            $query = implode(' AND ', $query);
            $total_ofertas = JobOffer::whereRaw($query, $params)
                            ->whereNotIn('job_offer.id', $offersboletin)
                            ->join('concello', 'concello.id', '=', 'job_offer.concello_fk')->count();

            $ofertas = JobOffer::whereRaw($query, $params)
                            ->whereNotIn('job_offer.id', $offersboletin)
                            ->join('concello', 'concello.id', '=', 'job_offer.concello_fk')
                            ->offset($start)->limit($length)
                            ->orderBy('created_at', 'DESC')->get($fields);
        } else {
            $total_ofertas = JobOffer::where('job_offer.id', 'LIKE', "%{$search}%")
                    ->orWhere('requested_job', 'LIKE', "%{$search}%")
                    ->orWhere('concello.name', 'LIKE', "%{$search}%")
                    ->whereNotIn('job_offer.id', $offersboletin)
                    ->join('concello', 'concello.id', '=', 'job_offer.concello_fk')
                    ->count();
            $ofertas = JobOffer::where('job_offer.id', 'LIKE', "%{$search}%")
                    ->orWhere('requested_job', 'LIKE', "%{$search}%")
                    ->orWhere('concello.name', 'LIKE', "%{$search}%")
                    ->whereNotIn('job_offer.id', $offersboletin)
                    ->join('concello', 'concello.id', '=', 'job_offer.concello_fk')
                    ->offset($start)
                    ->limit($length)
                    ->orderBy('created_at', 'DESC')
                    ->get($fields);
        }

        $data = array();

        foreach ($ofertas as $oferta) {
            $nestedData['id'] = $oferta->id;
            $nestedData['requested_job'] = $oferta->requested_job;
            $nestedData['created_at'] = $oferta->created_at;
            $nestedData['concello'] = $oferta->concello;

            //$nestedData['options']= "<input name='ofertas_selected[]' type='checkbox' value=".$oferta->id.">";



            $data[] = $nestedData;
        }

        $data2 = array(
            'draw' => $draw,
            'recordsTotal' => $total_ofertas,
            'recordsFiltered' => $total_ofertas,
            'data' => $data,
        );
        echo json_encode($data2);
    }

    // OFERFTAS EXTERNAS

    public function ofertasexternas($id) {

        $boletin = Boletin::find($id);
        $ofertasboletin = $boletin->ofertasExternas()->orderBy('created_at', 'DESC')->get();

        $offersboletin = $boletin->ofertasExternas()->pluck('id_oferta')->toArray();

//        $offers = DB::table('job_offer')
//                        ->select('job_offer.id', 'job_offer.requested_job', 'job_offer.created_at', 'concello.name as concello')
//                        ->join('concello', 'concello.id', '=', 'job_offer.concello_fk')
//                        ->whereNotIn('job_offer.id', $offersboletin)
//                        ->orderBy('created_at', 'DESC')->get();

        return view('boletin.ofertasexternas')->with(['boletin' => $boletin, 'ofertasboletin' => $ofertasboletin,]);
    }

    public function ofertasexternas_remove(Request $request) {
        $id_boletin = $request['id_boletin'];
        $id_oferta = $request['id_oferta'];

        $boletin = Boletin::find($id_boletin);
        $offers = $boletin->ofertasExternas()->where('id_oferta', '=', $id_oferta)->detach($id_oferta);
        //return view('boletin.ofertasexternas')->with(['boletin' => $boletin, 'ofertas' => $boletin->ofertasExternas()->orderBy('created_at', 'DESC')->paginate(25)]);
    }

    public function ofertasexternas_guardar(Request $request) {

        $ofertaExt = new \App\Entities\OfertaExterna;
        $ofertaExt->denominacion_da_oferta = $request['denominacion_da_oferta'];


        $ofertaExt->data_da_oferta = Carbon::createFromFormat('d/m/Y', $request['data_da_oferta'])->format('Y-m-d');
        $ofertaExt->localidade = $request['localidade'];
        $ofertaExt->n_postos = $request['n_postos'];
        $ofertaExt->descricion = $request['descricion'];
        $ofertaExt->requisitos = $request['requisitos'];
        $ofertaExt->tipo_contrato = $request['tipo_contrato'];
        $ofertaExt->otros_datos = $request['otros_datos'];
        $ofertaExt->contacto = $request['contacto'];
        $ofertaExt->publicada_en = $request['publicada_en'];
        $ofertaExt->link_publicacion = $request['link_publicacion'];
//        $ofertaExt->created_at = $request['denomeninaciondaoferta'];
//        $ofertaExt->updated_at = $request['denomeninaciondaoferta'];
        $ofertaExt->save();


        $id_boletin = $request['id_boletin'];
        $ofertas = $ofertaExt->id;



        $boletin = Boletin::find($id_boletin);
        $boletin->ofertasExternas()->attach($ofertas);

        return Redirect::back();

        //return view('boletin.ofertasexternas')->with(['boletin' => $boletin, 'ofertasboletin' => $boletin->ofertasExternas()->get()]);
        //return view('boletin.ofertasexternas')->with(['boletin' => $boletin, 'ofertas' => $boletin->ofertasExternas()->orderBy('created_at', 'DESC')->paginate(25)]);
    }

    public function ofertasexternas_update(Request $request) {

        $id = $request['id_oferta'];

        $ofertaExt = \App\Entities\OfertaExterna::find($id);

        $ofertaExt->denominacion_da_oferta = $request['denominacion_da_oferta'];


        $ofertaExt->data_da_oferta = Carbon::createFromFormat('d/m/Y', $request['data_da_oferta'])->format('Y-m-d');
        $ofertaExt->localidade = $request['localidade'];
        $ofertaExt->n_postos = $request['n_postos'];
        $ofertaExt->descricion = $request['descricion'];
        $ofertaExt->requisitos = $request['requisitos'];
        $ofertaExt->tipo_contrato = $request['tipo_contrato'];
        $ofertaExt->otros_datos = $request['otros_datos'];
        $ofertaExt->contacto = $request['contacto'];
        $ofertaExt->publicada_en = $request['publicada_en'];
        $ofertaExt->link_publicacion = $request['link_publicacion'];
//        $ofertaExt->created_at = $request['denomeninaciondaoferta'];
//        $ofertaExt->updated_at = $request['denomeninaciondaoferta'];
        $ofertaExt->update();




        return Redirect::back();

        //return view('boletin.ofertasexternas')->with(['boletin' => $boletin, 'ofertasboletin' => $boletin->ofertasExternas()->get()]);
        //return view('boletin.ofertasexternas')->with(['boletin' => $boletin, 'ofertas' => $boletin->ofertasExternas()->orderBy('created_at', 'DESC')->paginate(25)]);
    }

    public function ofertasexternas_listar(Request $request) {

        $id_boletin = $request['id_boletin'];


//
        $boletin = Boletin::find($id_boletin);
        $offersboletin = $boletin->ofertasExternas()->pluck('id_oferta')->toArray();

//        $offers = DB::table('job_offer')
//                        ->select('job_offer.id', 'job_offer.requested_job', 'job_offer.created_at', 'concello.name as concello')
//                        ->join('concello', 'concello.id', '=', 'job_offer.concello_fk')
//                        ->whereNotIn('job_offer.id', $offersboletin)
//                        ->orderBy('created_at', 'DESC')->paginate(25);
//
//        return response()->json(['ofertas' => $offers], 200);
        //return view('boletin.ofertasexternas')->with(['boletin' => $boletin, 'ofertas' => $boletin->ofertasExternas()->orderBy('created_at', 'DESC')->paginate(25)]);


        $draw = $request->get('draw');
        $fields = array('job_offer.id', 'job_offer.requested_job', 'job_offer.created_at', 'concello.name as concello');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->input('search.value');
        $colums = $request->get('columns');
        $query = array();
        $params = [];


        if (empty($search) && empty($query)) {
            $total_ofertas = JobOffer::whereNotIn('job_offer.id', $offersboletin)
                            ->join('concello', 'concello.id', '=', 'job_offer.concello_fk')->count();
            //$ofertas = JobOffer::orderBy('created_at', 'DESC')->skip($start)->take($length)->get($fields);





            $ofertas = DB::table('job_offer')
                            ->select('job_offer.id', 'job_offer.requested_job', 'job_offer.created_at', 'concello.name as concello')
                            ->join('concello', 'concello.id', '=', 'job_offer.concello_fk')
                            ->whereNotIn('job_offer.id', $offersboletin)
                            ->orderBy('created_at', 'DESC')->skip($start)->take($length)->get($fields);
        } else if (!empty($query)) {

            $query = implode(' AND ', $query);
            $total_ofertas = JobOffer::whereRaw($query, $params)
                            ->whereNotIn('job_offer.id', $offersboletin)
                            ->join('concello', 'concello.id', '=', 'job_offer.concello_fk')->count();

            $ofertas = JobOffer::whereRaw($query, $params)
                            ->whereNotIn('job_offer.id', $offersboletin)
                            ->join('concello', 'concello.id', '=', 'job_offer.concello_fk')
                            ->offset($start)->limit($length)
                            ->orderBy('created_at', 'DESC')->get($fields);
        } else {
            $total_ofertas = JobOffer::where('job_offer.id', 'LIKE', "%{$search}%")
                    ->orWhere('requested_job', 'LIKE', "%{$search}%")
                    ->orWhere('concello.name', 'LIKE', "%{$search}%")
                    ->whereNotIn('job_offer.id', $offersboletin)
                    ->join('concello', 'concello.id', '=', 'job_offer.concello_fk')
                    ->count();
            $ofertas = JobOffer::where('job_offer.id', 'LIKE', "%{$search}%")
                    ->orWhere('requested_job', 'LIKE', "%{$search}%")
                    ->orWhere('concello.name', 'LIKE', "%{$search}%")
                    ->whereNotIn('job_offer.id', $offersboletin)
                    ->join('concello', 'concello.id', '=', 'job_offer.concello_fk')
                    ->offset($start)
                    ->limit($length)
                    ->orderBy('created_at', 'DESC')
                    ->get($fields);
        }

        $data = array();

        foreach ($ofertas as $oferta) {
            $nestedData['id'] = $oferta->id;
            $nestedData['requested_job'] = $oferta->requested_job;
            $nestedData['created_at'] = $oferta->created_at;
            $nestedData['concello'] = $oferta->concello;

            //$nestedData['options']= "<input name='ofertas_selected[]' type='checkbox' value=".$oferta->id.">";



            $data[] = $nestedData;
        }

        $data2 = array(
            'draw' => $draw,
            'recordsTotal' => $total_ofertas,
            'recordsFiltered' => $total_ofertas,
            'data' => $data,
        );
        echo json_encode($data2);
    }

    public function generar_boletin(Request $request, $id) {

        $lineasPorColumna = 70;
        $letrasPorLineas = 100;
        $columnasMaximas = 4;
        $cuentaLinea = 5;
        $posicioColumna = 10;
        $anchoColumna = 64;



        $boletin = Boletin::find($id);
        $ofertasboletin = $boletin->ofertasExternas()->orderBy('created_at', 'DESC')->get();

        $offersboletin = $boletin->ofertasExternas()->pluck('id_oferta')->toArray();

        Fpdf::AddPage('L', 'Letter');
        Fpdf::SetFont('Arial', 'B', 6);

        foreach ($ofertasboletin as $oferta) {
            if ($cuentaLinea <= $lineasPorColumna) {
                Fpdf::SetXY($posicioColumna, ($cuentaLinea));
                Fpdf::Cell($anchoColumna, 5, 'Oferta' . $oferta->denominacion_da_oferta, 1, 1, 'L');
                $cuentaLinea = $cuentaLinea + 5;
                if ($oferta->publicada_en != '') {


                    Fpdf::SetXY($posicioColumna, ($cuentaLinea));
                    Fpdf::Cell($anchoColumna, 5, 'Publicada en:' . $oferta->publicada_en, 1, 1, 'L');
                    $cuentaLinea = $cuentaLinea + 5;
                }
            } else {
                $cuentaLinea = 5;
                $posicioColumna = $posicioColumna + $anchoColumna;
                if ($posicioColumna > ($columnasMaximas * $anchoColumna)) {
                    //salto de pagina
                    Fpdf::AddPage();
                    $cuentaLinea = 5;
                    $posicioColumna = 10;
                }
            }
        }



        Fpdf::Output();
        exit;



        //$view = View('boletin.boletinTemplate', compact(['boletin'], ['ofertasboletin']));
//          $pdf = \App::make('dompdf.wrapper');
//          $pdf->loadHTML($view->render());
//        return $pdf->stream();
//        return $view->render();
//
//        $pdf = \App::make('snappy.pdf.wrapper');
//        $pdf->loadHTML($view->render());
//        return $pdf->inline();
//$pdf = \App::make('mpdf.wrapper');
//$pdf = LaravelMpdf::loadHTML($view->render());
//// 
// return $pdf->stream();
//         
    }

}
