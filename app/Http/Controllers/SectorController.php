<?php

namespace App\Http\Controllers;
use Validator;
use App\Entities\Sector;

use Illuminate\Http\Request;

class SectorController extends Controller
{
     public function __construct() {
         $this->middleware('auth');
        $this->middleware('admin', ['only' => ['index','create','edit','remove']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sectores = Sector::all();
        $message = "No hay sectores registradas";
        return view('sector.index')->with(['sectores' => $sectores, 'message' => $message]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sector.create')->with([]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sector = new Sector();
        $sector->nombre = $request->input('nombre');
        $sector->nome = $request->input('nome');
        $editados = json_encode($sector->getDirty());
        $sector->save();
        
         \App\Entities\Auditoria::create([
                'model_name' => 'Sector',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Creó el registro '. $sector->id . ' '. $editados,
                'original_set' => $sector->toJson(),
                'changed_set' => ''
            ]);
        
        
        $request->session()->flash('alert-success', 'Sector creado correctamente');
        return redirect()->route('sectores.index')->withInput();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sector = Sector::findOrFail($id);
        return view('sector.edit')->with(['sector' => $sector]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $values = array('nombre','nome');
        $sector= Sector::find($id);
        $original = $sector->toJson();
        $editados = json_encode($sector->getDirty());
        $sector->update($request->all($values));
        
        if($original != json_encode($request->all($values))){
        \App\Entities\Auditoria::create([
                'model_name' => 'Sector',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Actualizó el registro ' .$sector->id . ' '. $editados,
                'original_set' => $original,
                'changed_set' => json_encode($request->all($values))
            ]);
        }
        
        $request->session()->flash('alert-success', 'Sector editado correctamente');
        return redirect()->route('sectores.index')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function remove(Request $request, $id)
    {
        $sector= Sector::find($id);
        $original= $sector->toJson();
        $sector->delete();
        \App\Entities\Auditoria::create([
                'model_name' => 'Sector',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Eliminar el registro',
                'original_set' => $original,
                'changed_set' => ''
            ]);
        
        
        $request->session()->flash('alert-success', 'Sector eliminado correctamente');
        return redirect()->route('sectores.index')->withInput();
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function show(Sector  $sector)
    {}
}
