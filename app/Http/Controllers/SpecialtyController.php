<?php

namespace App\Http\Controllers;
use Validator;
use App\Entities\Specialty;
use App\Entities\Grade;

use Illuminate\Http\Request;

class SpecialtyController extends Controller
{
     public function __construct() {
        $this->middleware('auth');
        $this->middleware('admin', ['only' => ['index','create','edit','remove']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $specialties = Specialty::all();
        $message = 'No hay especialidades registradas';
        return view('specialty.index')->with(['specialties' => $specialties, 'message' => $message]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $grades = Grade::all();
        return view('specialty.create')->with(['grades'=>$grades]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $specialty = new Specialty();
        $specialty->description = $request->input('description');
         $specialty->nome = $request->input('nome');
        $specialty->grade_id = $request->input('grade_id');
        $editados = json_encode($specialty->getDirty());
        $specialty->save();
         \App\Entities\Auditoria::create([
                'model_name' => 'Especialidades',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Creó el registro '. $specialty->id . ' '. $editados,
                'original_set' => $specialty->toJson(),
                'changed_set' => ''
            ]);
         
        $request->session()->flash('alert-success', 'Especialidad creado correctamente');
        return redirect()->route('especialidades.index')->withInput();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $specialty = Specialty::findOrFail($id);
        $grades = Grade::all();
        return view('specialty.edit')->with(['specialty' => $specialty,'grades'=>$grades]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $values = array('description','nome','grade_id');
        $especialidad= Specialty::find($id);
        $original = $especialidad->toJson();
        $editados = json_encode($request->all($values));
        
        $especialidad->update($request->all($values));
        
        if($original != json_encode($request->all($values))){
        \App\Entities\Auditoria::create([
                'model_name' => 'Especialidades',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Actualizó el registro '. $especialidad->id . ' '. $editados,
                'original_set' => $original,
                'changed_set' => json_encode($request->all($values))
            ]);
        }
        
        $request->session()->flash('alert-success', 'Especialidades editado correctamente');
        return redirect()->route('especialidades.index')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Specialty::destroy($id);
        $request->session()->flash('alert-success', 'Especialidades eliminado correctamente');
        return redirect()->route('especialidades.index')->withInput();
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function show(Contract  $puesto)
    {}
    public function remove(Request $request, $id)
    {
        $specialty= Specialty::find($id);
        $original = $specialty->toJson();
        $specialty->delete();
        
         \App\Entities\Auditoria::create([
                'model_name' => 'Especialidades',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Eliminar el registro',
                'original_set' => $original,
                'changed_set' => ''
            ]);
         
         
        $request->session()->flash('alert-success', 'Especialidad eliminada correctamente');
        return redirect()->route('especialidades.index');
    }
}
