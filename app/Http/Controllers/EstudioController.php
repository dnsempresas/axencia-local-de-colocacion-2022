<?php

namespace App\Http\Controllers;

use Validator;
use App\Entities\Grade;
use Illuminate\Http\Request;

class EstudioController extends Controller {

    public function __construct() {
         $this->middleware('auth', ['only' => ['index','create','edit','remove']]);
        $this->middleware('admin', ['only' => ['index','create','edit','remove']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $estudios = Grade::all();
        $message = 'No hay estudios registradas';
        return view('grade.index')->with(['estudios' => $estudios, 'message' => $message]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('grade.create')->with([]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $grade = new Grade();
        $grade->description = $request->input('description');
        
        $grade->nome = $request->input('nome');
        $grade->nivelformativo = $request->input('nivelformativo');
        $grade->titulacion = $request->input('titulacion');
        $editados = json_encode($grade->getDirty());
        $grade->save();
        $request->session()->flash('alert-success', 'Estudio creado correctamente');

        \App\Entities\Auditoria::create([
            'model_name' => 'Estudios',
            'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
            'action' => 'Crear el registro '.$grade->id.' '. $editados,
            'original_set' => $grade->toJson(),
            'changed_set' => ''
        ]);

        return redirect()->route('estudios.index')->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $grade = Grade::findOrFail($id);
        return view('grade.edit')->with(['grade' => $grade]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $values = array('description','nome'. 'nivelformativo','titulacion');
        $grade = Grade::find($id);
        $original = $grade->toJson();
        
        $editados = json_encode($request->all($values));
        $grade->update($request->all($values));
        $request->session()->flash('alert-success', 'Estudio editado correctamente');


        if ($original != json_encode($request->all($values))) {
            \App\Entities\Auditoria::create([
                'model_name' => 'Estudios',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Actualizó el registro ' . $grade->id.' '. $editados,
                'original_set' => $original,
                'changed_set' => json_encode($request->all($values))
            ]);
        }

        return redirect()->route('estudios.index')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
        Grade::destroy($id);
        $request->session()->flash('alert-success', 'Estudio eliminado correctamente');
        return redirect()->route('estudios.index')->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function show(Estudios $studie) {
        
    }

    public function remove(Request $request, $id) {
        $grade = Grade::find($id);
        $original = $grade->toJson();
        $grade->delete();
        $request->session()->flash('alert-success', 'Estudio eliminado correctamente');




        \App\Entities\Auditoria::create([
            'model_name' => 'Estudios',
            'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
            'action' => 'Eliminar el registro',
            'original_set' => $original,
            'changed_set' => ''
        ]);
        return redirect()->route('estudios.index');
    }

    public function especialidades($id) {
        $grade = Grade::findOrFail($id);
        
        return $grade->Specialties()->get();
    }

}
