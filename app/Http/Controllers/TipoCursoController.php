<?php

namespace App\Http\Controllers;
use Validator;
use App\Entities\TipoCurso;

use Illuminate\Http\Request;

class TipocursoController extends Controller
{
     public function __construct() {
         $this->middleware('auth');
        $this->middleware('admin', ['only' => ['index','create','edit','remove']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tipocursos = TipoCurso::all();
        $message = "No hay tipocursos registradas";
        return view('tipocurso.index')->with(['tipocursos' => $tipocursos, 'message' => $message]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tipocurso.create')->with([]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tipocurso = new TipoCurso();
        $tipocurso->nombre = $request->input('nombre');
        $tipocurso->nome = $request->input('nome');
        $editados = json_encode($tipocurso->getDirty());
        $tipocurso->save();
        
         \App\Entities\Auditoria::create([
                'model_name' => 'Tipocurso',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Creó el registro tipocurso '. $tipocurso->id . ' '. $editados,
                'original_set' => $tipocurso->toJson(),
                'changed_set' => ''
            ]);
        
        
        $request->session()->flash('alert-success', 'Tipocurso creado correctamente');
        return redirect()->route('tipocursos.index')->withInput();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tipocurso  $tipocurso
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipocurso = TipoCurso::findOrFail($id);
        return view('tipocurso.edit')->with(['tipocurso' => $tipocurso]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tipocurso  $tipocurso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $values = array('nombre','nome');
        $tipocurso= TipoCurso::find($id);
        $original = $tipoCurso->toJson();
        $tipocurso->update($request->all($values));
        $editados = json_encode($request->all($values));
        
        if($original != json_encode($request->all($values))){
        \App\Entities\Auditoria::create([
                'model_name' => 'Tipocurso',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Actualizó el registro '. $tipocurso->id . ' '.$editados,
                'original_set' => $original,
                'changed_set' => json_encode($request->all($values))
            ]);
        }
        
        $request->session()->flash('alert-success', 'Tipocurso editado correctamente');
        return redirect()->route('tipocursos.index')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tipocurso  $tipocurso
     * @return \Illuminate\Http\Response
     */
    public function remove(Request $request, $id)
    {
        $tipocurso= TipoCurso::find($id);
        $original= $tipoCurso->toJson();
        $tipocurso->delete();
        \App\Entities\Auditoria::create([
                'model_name' => 'Tipocurso',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Eliminar el registro',
                'original_set' => $original,
                'changed_set' => ''
            ]);
        
        
        $request->session()->flash('alert-success', 'Tipo curso eliminado correctamente');
        return redirect()->route('tipocursos.index')->withInput();
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Tipocurso  $tipocurso
     * @return \Illuminate\Http\Response
     */
    public function show(TipoCurso  $tipocurso)
    {}
}
