<?php

namespace App\Http\Controllers;

use Validator;
use App\Entities\Banner;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\Tags\Url;

class BannerController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('admin', ['only' => ['index', 'create', 'edit', 'remove']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function getBanners($banners) {
        $data = array();
        foreach ($banners as $banner) {
            $edit = route('banners.edit', $banner->id);
            $delete = url('/panel/admin/banners/' . $banner->id . '/remove');
            $nestedData['id'] = $banner->id;
            $nestedData['titulo'] = $banner->titulo;
            $nestedData['enlace'] = $banner->enlace;
            $nestedData['fecha_publicacion'] = Carbon::createFromFormat('Y-m-d', $banner->fecha_publicacion)->format('d/m/Y');
            $nestedData['fecha_expiracion'] = Carbon::createFromFormat('Y-m-d', $banner->fecha_expiracion)->format('d/m/Y');
            $nestedData['options'] = "&emsp;<a href='{$edit}' title='Edit' ><span class='glyphicon glyphicon-edit' ></span></a>
                                          &emsp;<a id='remove_link' href='{$delete}' title='Delete' ><span class='glyphicon glyphicon-remove' style='color:red'></span></a>";

            $data[] = $nestedData;
        }



        return $data;
    }

    public function index(Request $request) {
        $banners = Banner::orderBy('fecha_publicacion', 'DESC');

        $message = 'No hay banners registradas';
        return view('banners.index')->with(['banners' => $banners, 'message' => $message,]);
    }

    /**
     * Show the form for creating a banner resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        return view('banners.create');
    }

    /**
     * Store a bannerly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {



        $banner = new Banner();
        $banner->titulo = $request->input('titulo');
        $banner->enlace = $request->input('enlace');
        $banner->fecha_publicacion = Carbon::createFromFormat('d/m/Y', $request->input('fecha_publicacion'))->format('Y-m-d');
        $banner->fecha_expiracion = Carbon::createFromFormat('d/m/Y', $request->input('fecha_expiracion'))->format('Y-m-d');

        $editados = json_encode($banner->getDirty());

        $banner->save();


        $imagen_destacada = $request->file('imagen_destacada');
        $slug = $banner->id;
        $destinationPath = public_path('banners');

        if ($imagen_destacada) {
            $imagen_destacada_str = $slug . '_imagen_destacada.' . $imagen_destacada->getClientOriginalExtension();

            $imagen_destacada->move($destinationPath, $imagen_destacada_str);

            $banner->imagen_destacada = $imagen_destacada_str;
        }


        $banner->update();


        \App\Entities\Auditoria::create([
            'model_name' => 'Banners',
            'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
            'action' => 'Creó el registro banner ' . $banner->id . ' ' . $editados,
            'original_set' => $banner->toJson(),
            'changed_set' => ''
        ]);


        $request->session()->flash('alert-success', 'Banner creada correctamente');
        return redirect()->route('banners.index')->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $banner = Banner::findOrFail($id);
        return view('banners.edit')->with(['banner' => $banner]);
    }

    public function removeimgd(Request $request, $id, $img) {

        $destinationPath = public_path('banners');

        $mi_imagen = $destinationPath . '/' . $img;
        if (@getimagesize($mi_imagen)) {

            $banner = Banner::find($id);
            $banner->imagen_destacada = '';
            $banner->update();
            unlink($mi_imagen);
            $request->session()->flash('alert-success', 'Imagen borrada correctamente');

            \App\Entities\Auditoria::create([
                'model_name' => 'Banner',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Borró imagen',
                'original_set' => $banner,
                'changed_set' => ''
            ]);
        } else {
            $request->session()->flash('alert-success', 'Imagen o datos no encontrados');
        }
        return Redirect::back()->withInput();
    }

    
  

    public function update(Request $request, $id) {
        $values = array('titulo', 'fecha_publicacion', 'fecha_expiracion', 'imagen_destacada');
        $banner = Banner::find($id);
        $original = $banner->toJson();
        
       

        $banner->titulo = $request->input('titulo');
        $banner->enlace = $request->input('enlace');
        $banner->fecha_publicacion = Carbon::createFromFormat('d/m/Y', $request->input('fecha_publicacion'))->format('Y-m-d');
        $banner->fecha_expiracion = Carbon::createFromFormat('d/m/Y', $request->input('fecha_expiracion'))->format('Y-m-d');


        $imagen_destacada = $request->file('imagen_destacada');
      
    $editados = json_encode($banner->getDirty());

        $slug = $banner->id;
        $destinationPath = public_path('banners');

        if ($imagen_destacada) {
            $imagen_destacada_str = $slug . '_imagen_destacada.' . $imagen_destacada->getClientOriginalExtension();

            $imagen_destacada->move($destinationPath, $imagen_destacada_str);

            $banner->imagen_destacada = $imagen_destacada_str;
        }


        $banner->update();



        if ($original != json_encode($request->all($values))) {
            \App\Entities\Auditoria::create([
                'model_name' => 'Banners',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Actualizó el registro banner ' . $banner . ' ' . $editados,
                'original_set' => $original,
                'changed_set' => json_encode($request->all($values))
            ]);
        }

        $request->session()->flash('alert-success', 'Banner editada correctamente');
        return redirect()->route('banners.index')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
        Banner::destroy($id);
        $request->session()->flash('alert-success', 'Banner eliminado correctamente');
        return redirect()->route('banners.index')->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function show(Contract $puesto) {
        
    }

    public function remove(Request $request, $id) {
        $banner = Banner::find($id);
        $original = $banner->toJson();
        $banner->delete();
        \App\Entities\Auditoria::create([
            'model_name' => 'Banners',
            'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
            'action' => 'Eliminar el registro',
            'original_set' => $banner->toJson(),
            'changed_set' => ''
        ]);
        
         $sitemap = new \SimpleXMLElement('sitemap.xml', 0, true);

                //$sitemap = simplexml_load_file('sitemap.xml');

                $exitesitemap = 0;
                foreach ($sitemap->url as $url) {
                    if ((string) $url->loc == $banner->getSlug()) {
                        $exitesitemap = 1;
                        $urlRemove = $url;
                        break;
                    }
                }
                if ($exitesitemap == 1) {

                    $domxml = dom_import_simplexml($urlRemove);
                    $domxml->parentNode->removeChild($domxml);

                    $dom = new \DOMDocument('1.0');
                    $dom->preserveWhiteSpace = false;
                    $dom->formatOutput = true;
                    $dom->loadXML($sitemap->asXML());
                    $dom->save('sitemap.xml');
                }

        $request->session()->flash('alert-success', 'Banner eliminada correctamente');
        return redirect()->route('banners.index');
    }

    public function listBanners(Request $request) {
        $draw = $request->get('draw');
        $fields = array('id', 'titulo', 'enlace', 'fecha_publicacion', 'fecha_expiracion');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->input('search.value');
        $colums = $request->get('columns');
        $query = array();
        $params = [];
        if ($colums[0]['search']['value']) {
            $value = $colums[0]['search']['value'];
            array_push($query, 'id = ?');
            array_push($params, $value);
        }

        if ($colums[1]['search']['value']) {
            $value = $colums[1]['search']['value'];
            array_push($query, 'titulo like ?');
            array_push($params, '%' . $value . '%');
        }
        if ($colums[2]['search']['value']) {
            $value = $colums[2]['search']['value'];
            array_push($query, 'enlace like ?');
            array_push($params, '%' . $value . '%');
        }
        

        if ($colums[3]['search']['value'] && $colums[3]['search']['value'] != '-') {
            $value = $colums[3]['search']['value'];
            list($min, $max) = explode('-', $value);

            if (!empty($min) && !empty($max)) {


                $min = Carbon::createFromFormat('d/m/Y', $min)->format('Y-m-d');
                $max = Carbon::createFromFormat('d/m/Y', $max)->format('Y-m-d');


                $sql = 'date(fecha_publicacion) BETWEEN ? AND ?';
                array_push($query, $sql);

                array_push($params, stripslashes($min));

                array_push($params, stripslashes($max));
            } else {
                if (!empty($min)) {
                    $min = Carbon::createFromFormat('d/m/Y', $min)->format('Y-m-d');
                    $sql = 'date(fecha_publicacion) >= ? ';
                    array_push($query, $sql);

                    array_push($params, stripslashes($min));
                } else {
                    $max = Carbon::createFromFormat('d/m/Y', $max)->format('Y-m-d');
                    $sql = 'date(fecha_publicacion) <= ? ';
                    array_push($query, $sql);
                    array_push($params, stripslashes($max));
                }
            }
        }



        if ($colums[4]['search']['value'] && $colums[4]['search']['value'] != '-') {
            $value = $colums[4]['search']['value'];
            list($min, $max) = explode('-', $value);
            if (!empty($min) && !empty($max)) {

                $min = Carbon::createFromFormat('d/m/Y', $min)->format('Y-m-d');
                $max = Carbon::createFromFormat('d/m/Y', $max)->format('Y-m-d');

                $sql = ' date(fecha_expiracion) BETWEEN ? AND ?';
                array_push($query, $sql);
                array_push($params, stripslashes($min));

                array_push($params, stripslashes($max));
            } else {
                if (!empty($min)) {
                    $min = Carbon::createFromFormat('d/m/Y', $min)->format('Y-m-d');
                    $sql = 'date(fecha_expiracion) >= ? ';
                    array_push($query, $sql);

                    array_push($params, stripslashes($min));
                } else {
                    $max = Carbon::createFromFormat('d/m/Y', $max)->format('Y-m-d');
                    $sql = 'date(fecha_expiracion) <= ? ';
                    array_push($query, $sql);
                    array_push($params, stripslashes($max));
                }
            }
        }


        if (empty($search) && empty($query)) {
            $total_banners = Banner::count();
            $banners = Banner::orderBy('fecha_publicacion', 'DESC')->skip($start)->take($length)->get($fields);
        } else if (!empty($query)) {
            $query = implode(' AND ', $query);

            $total_banners = Banner::whereRaw($query, $params)->count();
            $banners = Banner::whereRaw($query, $params)->offset($start)->limit($length)
                            ->orderBy('fecha_publicacion', 'DESC')->get($fields);
        } else {
            $total_banners = Banner::where('id', 'LIKE', "%{$search}%")
                    ->orWhere('titulo', 'LIKE', "%{$search}%")
                    ->orWhere('enlace', 'LIKE', "%{$search}%")
                   
                    ->count();
            $banners = Banner::where('id', 'LIKE', "%{$search}%")
                    ->orWhere('titulo', 'LIKE', "%{$search}%")
                    ->orWhere('enlace', 'LIKE', "%{$search}%")
                   
                    ->offset($start)
                    ->limit($length)
                    ->orderBy('fecha_publicacion', 'DESC')
                    ->get($fields);
        }
        $banners = $this->getBanners($banners);
        $data = array(
            'draw' => $draw,
            'recordsTotal' => $total_banners,
            'recordsFiltered' => $total_banners,
            'data' => $banners,
        );
        echo json_encode($data);
    }

}
