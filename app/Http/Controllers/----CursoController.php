<?php

namespace App\Http\Controllers;

use Validator;
use App\Entities\TipoCurso;
use App\Entities\Curso;
use App\Entities\Puesto;
use App\Entities\Sector;
use App\Entities\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\Tags\Url;

class CursoController extends Controller {

    public function __construct() {
        //$this->middleware('auth');
        $this->middleware('admin', ['only' => ['index', 'create', 'edit', 'remove', 'buscardatos']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $query = "";
        $parametros = [];
        if ($request->input('nombre') != null) {
            $query = "nombre like ? ";
            array_push($parametros, '%' . $request->input('nombre') . '%');
        }
        if ($request->input('ubicacion') != null) {
            if (count($parametros)) {
                $query .= "and ";
            }
            $query .= "lugar_celebracion like ? ";
            array_push($parametros, '%' . $request->input('ubicacion') . '%');
        }
        if ($request->input('plazas') != null) {
            if (count($parametros)) {
                $query .= "and ";
            }
            $query .= "plazas_total > ? ";
            array_push($parametros, $request->input('plazas'));
        }
        if ($request->input('plazas_disponibles') != null) {
            if (count($parametros)) {
                $query .= "and ";
            }
            $query .= "plazas_disponibles != ? ";
            array_push($parametros, 0);
        }
//        if ($request->input('edades_recomendadas') != null) {
//            if (count($parametros)) {
//                $query .= "and ";
//            }
//            $query .= "edades_recomendadas > ? ";
//            array_push($parametros, $request->input('edades_recomendadas'));
//        }
        //dd(array($query,$parametros));
        if ($query != '') {
            $cursos = Curso::whereRaw($query, $parametros)->get();
            $mensaje_empty = "No se encontraron coincidencias";
        } else {
            $cursos = Curso::all();
            $mensaje_empty = "No hay cursos registrados";
        }


        return view('cursos.index')->with(['cursos' => $cursos, 'mensaje_empty' => $mensaje_empty]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $sectores = Sector::all();
        $puestos = Puesto::all();
        $tipoCursos = TipoCurso::all();
        return view('cursos.create')->with(['sectores' => $sectores, 'puestos' => $puestos, 'tipoCursos' => $tipoCursos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
//        $fecha_inicio = strtotime($request->input('fecha_inicio'));
//        $fecha_fin = strtotime($request->input('fecha_fin'));
//        $diferencia_fechas = $fecha_fin - $fecha_inicio;
//        $dias_diferencia = round($diferencia_fechas / (60 * 60 * 24));
//        $hora_duracion = $dias_diferencia * $request->input('duracion_horas');


        $curso = new Curso();
        $hora_duracion = $request->input('duracion_horas');
        $curso->nombre = $request->input('nombre');
        $curso->lugar_celebracion = $request->input('lugar_celebracion');

        //$curso->fecha_inicio = $request->input('fecha_inicio');
        $curso->fecha_inicio = Carbon::createFromFormat('d/m/Y', $request->input('fecha_inicio'));


        //$curso->fecha_fin = $request->input('fecha_fin');
        $curso->fecha_fin = Carbon::createFromFormat('d/m/Y', $request->input('fecha_fin'));


        $curso->hora_inicio = date("H:i", strtotime($request->input('hora_inicio')));
        $curso->hora_fin = date("H:i", strtotime($request->input('hora_fin')));

        $curso->duracion_horas = $hora_duracion;
        $curso->plazas_total = $request->input('plazas_total');
        $curso->plazas_disponibles = $request->input('plazas_total');

        $curso->edad_min = $request->input('edad_min');
        $curso->edad_max = $request->input('edad_max');

        $curso->pertenece_fie = true; //$request->input('pertenece_fie');
        if ($request->input('pertenece_fie'))
            $curso->pertenece_fie = 1;
        else
            $curso->pertenece_fie = 0;

        if ($request->input('estatus'))
            $curso->estatus = 1;
        else
            $curso->estatus = 0;



        $curso->created_by = \Illuminate\Support\Facades\Auth::user()->pk;
        $curso->created_at = \Carbon\Carbon::now()->toDateTimeString();

        $curso->updated_at = \Carbon\Carbon::now()->toDateTimeString();
        $curso->updated_by = \Illuminate\Support\Facades\Auth::user()->pk;

        $curso->contenido = $request->input('contenido');

        $curso->precio = $request->input('precio');

        $curso->requerimiento = $request->input('requerimiento');
        $curso->observaciones = $request->input('observaciones');

        $curso->tipocurso = $request->input('tipoCursos');
        // $curso->propietario = $request->input('propietario');
        $mensajes = [
            'nombre.required' => 'Nombre es obligatoria',
            'lugar_celebracion.required' => 'Lugar_celebracion es obligatoria',
            'fecha_inicio.required' => 'Fecha Inicio es obligatoria',
            'fecha_inicio.date' => 'Fecha Inicio debe ser en formato dd/mm/yyyy',
            'fecha_fin.required' => 'Fecha Fin es obligatoria',
            'fecha_fin.date' => 'Fecha Fin debe ser en formato dd/mm/yyyy',
            'hora_inicio.required' => 'Hora Inicio es obligatoria',
            'hora_fin.required' => 'Hora Fin es obligatoria',
            'duracion_horas.required' => 'Duracion_horas es obligatoria',
            'plazas_total.required' => 'Plazas Total es obligatoria',
            'plazas_disponibles.required' => 'Plazas Disponibles es obligatoria',
            //'pertenece_fie.required' => 'Pertenece Fie es obligatoria',
            'contenido.required' => 'la descripcion larga es obligatoria',
        ];
        $reglas = [
            'nombre' => 'required',
            'lugar_celebracion' => 'required',
            'fecha_inicio' => 'required',
            'fecha_fin' => 'required',
            'hora_inicio' => 'required',
            'hora_fin' => 'required',
            //'duracion_horas' => 'required',
            'plazas_total' => 'required',
            //'plazas_disponibles' => 'required',
            //'pertenece_fie' => 'required',
            'contenido' => 'required',
        ];
        $validator = Validator::make($request->all(), $reglas, $mensajes);
        if ($validator->fails()) {
            return redirect('panel/cursos/create')
                            ->withErrors($validator)
                            ->withInput();
        }

        $puestos = $request->input('puestos');
        $puestos = (!isset($puestos)) ? array() : $puestos;

        $curso->save();



        if ($curso->estatus == 1) {

            $sitemap = new \SimpleXMLElement('sitemap.xml', 0, true);
            //$sitemap = simplexml_load_file('sitemap.xml'); 
            // Añadimos , elementos y atributo
            $nuevaUrl = $sitemap->addChild('url');
            $nuevaUrl->addChild('loc', $curso->getSlug());
            $nuevaUrl->addChild('lastmod', str_replace(' ', 'T', Carbon::now()) . '+00:00');
            $nuevaUrl->addChild('priority', '0.8');
            //$sitemap->asXML('sitemap.xml');

            $domxml = new \DOMDocument('1.0');
            $domxml->preserveWhiteSpace = false;
            $domxml->formatOutput = true;
            $domxml->loadXML($sitemap->asXML());
            $domxml->save('sitemap.xml');
        }



        $imagen_destacada = $request->file('imagen_destacada');
        $slug = $curso->id;
        $destinationPath = public_path('cursos');



        if ($imagen_destacada) {
            $imagen_destacada_str = $slug . '_imagen_destacada.' . $imagen_destacada->getClientOriginalExtension();

            $imagen_destacada->move($destinationPath, $imagen_destacada_str);

            $values['imagen'] = $imagen_destacada_str;
            $curso->imagen = $imagen_destacada_str;
        }

        $editados = json_encode($curso->getDirty());

        $curso->update();




        \App\Entities\Auditoria::create([
            'model_name' => 'Cursos',
            'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
            'action' => 'Creó el registro Cursos ' . $curso->id . ' ' . $editados,
            'original_set' => $curso->toJson(),
            'changed_set' => ''
        ]);
        //$curso->puestos()->sync($puestos);
        $curso->sectores()->sync($request->input('sector'));

        $request->session()->flash('alert-success', 'Curso creado correctamente');
        return redirect()->route('cursos.index')->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Curso  $curso
     * @return \Illuminate\Http\Response
     */
    public function show(Curso $curso) {
        $sectores = Sector::all();
        $puestos = Puesto::all();
        return view('cursos.show')->with(['sectores' => $sectores, 'puestos' => $puestos, 'curso' => $curso]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Curso  $curso
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $curso = Curso::findOrFail($id);
        $sectores = Sector::all();
        $puestos = Puesto::all();

        $tipoCursos = TipoCurso::all();
        return view('cursos.edit')->with(['sectores' => $sectores, 'puestos' => $puestos, 'curso' => $curso, 'tipoCursos' => $tipoCursos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Curso  $curso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {


        $curso = Curso::findOrFail($id);
        $values = array('nombre', 'lugar_celebracion', 'fecha_inicio', 'fecha_fin', 'hora_inicio', 'hora_fin', 'duracion_horas', 'plazas_total', 'edades_recomendadas', 'pertenece_fie', 'contenido');
        $hora_duracion = $request->input('duracion_horas');
        $original = $curso->toJson();
        
        $slugS = $curso->getSlug();

        $hora_duracion = $request->input('duracion_horas');
        $curso->nombre = $request->input('nombre');
        $curso->lugar_celebracion = $request->input('lugar_celebracion');

        //$curso->fecha_inicio = $request->input('fecha_inicio');
        $curso->fecha_inicio = Carbon::createFromFormat('d/m/Y', $request->input('fecha_inicio'));


        //$curso->fecha_fin = $request->input('fecha_fin');
        $curso->fecha_fin = Carbon::createFromFormat('d/m/Y', $request->input('fecha_fin'));


        $curso->hora_inicio = date("H:i", strtotime($request->input('hora_inicio')));
        $curso->hora_fin = date("H:i", strtotime($request->input('hora_fin')));

        $curso->duracion_horas = $hora_duracion;

        $curso->plazas_total = $request->input('plazas_total');

//        if (!$curso->inscritos()->count() > 0 )
//        $curso->plazas_disponibles = $request->input('plazas_total');
//        

        $curso->edad_min = $request->input('edad_min');
        $curso->edad_max = $request->input('edad_max');

        $curso->pertenece_fie = true; //$request->input('pertenece_fie');

        if ($request->input('pertenece_fie'))
            $curso->pertenece_fie = 1;
        else
            $curso->pertenece_fie = 0;

        $old_status = $curso->estatus;
        if ($request->input('estatus'))
            $curso->estatus = 1;
        else
            $curso->estatus = 0;


        $curso->created_by = \Illuminate\Support\Facades\Auth::user()->pk;
        $curso->created_at = \Carbon\Carbon::now()->toDateTimeString();

        $curso->updated_at = \Carbon\Carbon::now()->toDateTimeString();
        $curso->updated_by = \Illuminate\Support\Facades\Auth::user()->pk;

        $curso->contenido = $request->input('contenido');

        $curso->precio = $request->input('precio');

        $curso->requerimiento = $request->input('requerimiento');
        $curso->observaciones = $request->input('observaciones');

        $curso->tipocurso = $request->input('tipoCursos');
        // $curso->propietario = $request->input('propietario');
        $mensajes = [
            'nombre.required' => 'Nombre es obligatoria',
            'lugar_celebracion.required' => 'Lugar_celebracion es obligatoria',
            'fecha_inicio.required' => 'Fecha Inicio es obligatoria',
            'fecha_inicio.date' => 'Fecha Inicio debe ser en formato dd/mm/yyyy',
            'fecha_fin.required' => 'Fecha Fin es obligatoria',
            'fecha_fin.date' => 'Fecha Fin debe ser en formato dd/mm/yyyy',
            'hora_inicio.required' => 'Hora Inicio es obligatoria',
            'hora_fin.required' => 'Hora Fin es obligatoria',
            'duracion_horas.required' => 'Duracion_horas es obligatoria',
            'plazas_total.required' => 'Plazas Total es obligatoria',
            'plazas_disponibles.required' => 'Plazas Disponibles es obligatoria',
            //'pertenece_fie.required' => 'Pertenece Fie es obligatoria',
            'contenido.required' => 'la descripcion larga es obligatoria',
        ];
        $reglas = [
            'nombre' => 'required',
            'lugar_celebracion' => 'required',
            'fecha_inicio' => 'required',
            'fecha_fin' => 'required',
            'hora_inicio' => 'required',
            'hora_fin' => 'required',
            'duracion_horas' => 'required',
            'plazas_total' => 'required',
            //'plazas_disponibles' => 'required',
            //'pertenece_fie' => 'required',
            'contenido' => 'required',
        ];
        $validator = Validator::make($request->all(), $reglas, $mensajes);
        if ($validator->fails()) {
            return redirect::back()
                            ->withErrors($validator)
                            ->withInput();
        }

        $puestos = $request->input('puestos');
        $puestos = (!isset($puestos)) ? array() : $puestos;



        $imagen_destacada = $request->file('imagen_destacada');
        $slug = $curso->id;
        $destinationPath = public_path('cursos');



        if ($imagen_destacada) {
            $imagen_destacada_str = $slug . '_imagen_destacada.' . $imagen_destacada->getClientOriginalExtension();

            $imagen_destacada->move($destinationPath, $imagen_destacada_str);

            $values['imagen'] = $imagen_destacada_str;
            $curso->imagen = $imagen_destacada_str;
        }
        $editados = json_encode($curso->getDirty());
        $curso->update();

        $curso->puestos()->sync($puestos);
        $curso->sectores()->sync($request->input('sector'));

        \App\Entities\Auditoria::create([
            'model_name' => 'Cursos',
            'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
            'action' => 'Actualizó el registro Cursos ' . $curso->id . ' ' . $editados,
            'original_set' => $original,
            'changed_set' => $curso->toJson()
        ]);

        if ($curso->estatus == 1) { //Estoy publicando, agregar a site map
            $sitemap = new \SimpleXMLElement('sitemap.xml', 0, true);

            //$sitemap = simplexml_load_file('sitemap.xml');

            /* Para cada <personaje>, se muestra cada <nombre>. */
            $exitesitemap = 0;
            foreach ($sitemap->url as $url) {
                if ((string) $url->loc == $slugS) {
                    $exitesitemap = 1;
                }
            }
            if ($exitesitemap == 0) {

                // Añadimos , elementos y atributo
                $nuevaUrl = $sitemap->addChild('url');
                $nuevaUrl->addChild('loc', $curso->getSlug());
                $nuevaUrl->addChild('lastmod', str_replace(' ', 'T', Carbon::now()) . '+00:00');
                $nuevaUrl->addChild('priority', '0.8');
                //$sitemap->asXML('sitemap.xml');

                $domxml = new \DOMDocument('1.0');
                $domxml->preserveWhiteSpace = false;
                $domxml->formatOutput = true;
                $domxml->loadXML($sitemap->asXML());
                $domxml->save('sitemap.xml');
            }
        } else {

            $sitemap = new \SimpleXMLElement('sitemap.xml', 0, true);

            //$sitemap = simplexml_load_file('sitemap.xml');

            $exitesitemap = 0;
            foreach ($sitemap->url as $url) {
                if ((string) $url->loc == $slugS) {
                    $exitesitemap = 1;
                    $urlRemove = $url;
                    break;
                }
            }
            if ($exitesitemap == 1) {

                $domxml = dom_import_simplexml($urlRemove);
                $domxml->parentNode->removeChild($domxml);

                $dom = new \DOMDocument('1.0');
                $dom->preserveWhiteSpace = false;
                $dom->formatOutput = true;
                $dom->loadXML($sitemap->asXML());
                $dom->save('sitemap.xml');
            }
        }
        
        if (Carbon::now()->format('Y-m-d') >= $curso->fecha_fin ) { //Curso Terminó

            $sitemap = new \SimpleXMLElement('sitemap.xml', 0, true);

            //$sitemap = simplexml_load_file('sitemap.xml');

            $exitesitemap = 0;
            foreach ($sitemap->url as $url) {
                if ((string) $url->loc == $slugS) {
                    $exitesitemap = 1;
                    $urlRemove = $url;
                    break;
                }
            }
            if ($exitesitemap == 1) {

                $domxml = dom_import_simplexml($urlRemove);
                $domxml->parentNode->removeChild($domxml);

                $dom = new \DOMDocument('1.0');
                $dom->preserveWhiteSpace = false;
                $dom->formatOutput = true;
                $dom->loadXML($sitemap->asXML());
                $dom->save('sitemap.xml');
            }
        }
        


        $request->session()->flash('alert-success', 'Curso editado correctamente');


        return redirect()->route('cursos.index')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Curso  $curso
     * @return \Illuminate\Http\Response
     */
    public function destroy(Curso $curso) {
        //
    }

    public function inscripcion($curso_id) {


        $curso = Curso::find($curso_id);



        //dd($curso->puestos);
        return view('cursos.inscripciones')->with(['curso' => $curso]);
    }

    public function inscribirme(Request $request, $curso) {
        $curso = Curso::find($curso);
        $user = User::find($request->input('usuario'));
        if ($curso->users()->count() < $curso->plazas_total) {

            try {
                $curso->users()->attach($user);
                $curso->plazas_disponibles = ($curso->plazas_total - $curso->users()->count());
                $curso->save();
                \App\Entities\Auditoria::create([
                    'model_name' => 'Cursos',
                    'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                    'action' => 'Incribió en Curso ' . $curso->id . ':' . $curso->nombre . ' al usuario ' . $user->cif . ': ' . $user->name . ' ' . $user->surname,
                    'original_set' => '',
                    'changed_set' => ''
                ]);
            } catch (Exception $e) {
                $request->session()->flash('alert-danger', 'Un error impide incribir el curso');
                return "error";
            }
        } else {
            $request->session()->flash('alert-danger', 'El curso al que intenta inscribirse, ya no tiene plazas disponibles');
            return redirect()->route('cursos.edit', $curso->id)->withInput();
        }

        $request->session()->flash('alert-success', 'Se ha inscrito Exitosamente en el curso ' . $curso->nombre);
        return redirect()->route('curso.index', $curso->id);
    }

    public function confirmarInscripcion(Request $request) {
        $curso = Curso::find($request->input('idCurso'));
        $user = User::find($request->input('idUser'));

        if ($curso->users()->count() < $curso->plazas_total) {

            try {
                $curso->users()->attach($user);
                $curso->plazas_disponibles = ($curso->plazas_total - $curso->users()->count());
                $curso->save();

                \App\Entities\Auditoria::create([
                    'model_name' => 'Cursos',
                    'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                    'action' => 'Incribió en Curso ' . $curso->id . ':' . $curso->nombre . ' al usuario ' . $user->cif . ': ' . $user->name . ' ' . $user->surname,
                    'original_set' => '',
                    'changed_set' => ''
                ]);


                $request->session()->flash('alert-success', 'Se ha inscrito Exitosamente en el curso ' . $curso->nombre);
            } catch (Exception $e) {
                $request->session()->flash('alert-danger', 'Ha ocurrido un error para inscribir el curso' . $curso->nombre);
                return "error";
            }
        } else {
            $request->session()->flash('alert-danger', 'El curso al que intenta inscribirse, ya no tiene plazas disponibles');
            return view('cursos.inscripciones')->with(['curso' => $curso, 'usuarios' => $curso->users()]);
        }
        return redirect()->action('CursoController@inscripcion', ['curso_id}' => $curso->id]
        );
    }

    public function buscardatos(Request $request) {



        $dni = $request->input('idUser');
        $curso = $request->input('idCurso');

        $Curso = Curso::find($curso);

        $inscrito = $Curso->users()->Where('cif', '=', $dni);

        if ($inscrito->count() > 0) {
            return response()->json(['message' => 'Ya se encuentra inscrito'], 200);
        };

        $usuario = User::Where('cif', '=', $dni)->where('status', '=', '1')->where('perfilcompletado', '=', '1');


        if ($usuario->count() > 0) {
            return response()->json(['message' => 'Exitoso', 'usuario' => $usuario->get(), 'curso' => $Curso], 200);
        } else {
            return response()->json(['message' => 'Usuario no encontrado, no válido o perfil no ha sido completado'], 200);
        }
    }

    public function retirarInscripcion(Request $request) {
        $curso = Curso::find($request['idCurso']);
        $user = User::find($request['idUser']);


        try {

            $curso->users()->detach($user);
            $curso->plazas_disponibles = $curso->plazas_disponibles + 1;
            $curso->save();
            \App\Entities\Auditoria::create([
                'model_name' => 'Cursos',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Retiró en Curso ' . $curso->id . ':' . $curso->nombre . ' al usuario ' . $user->cif . ': ' . $user->name . ' ' . $user->surname,
                'original_set' => '',
                'changed_set' => ''
            ]);
        } catch (Exception $e) {
            return "error";
        }
        $request->session()->flash('alert-success', 'Se ha retirado Exitosamente de el curso ' . $curso->nombre);

        return redirect()->action(
                        'CursoController@inscripcion', ['curso_id}' => $curso->id]
        );
    }

    public function remove(Request $request, $id) {
        $curso = Curso::find($id);
        $original = $curso->toJson();

        if (!$curso->users()->count() >= 1) {
            $curso->delete();

            \App\Entities\Auditoria::create([
                'model_name' => 'Cursos',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Eliminar el registro',
                'original_set' => $curso->toJson(),
                'changed_set' => ''
            ]);

            $request->session()->flash('alert-success', 'Curso eliminado correctamente');


            $sitemap = new \SimpleXMLElement('sitemap.xml', 0, true);

            //$sitemap = simplexml_load_file('sitemap.xml');

            $exitesitemap = 0;
            foreach ($sitemap->url as $url) {
                if ((string) $url->loc == $curso->getSlug()) {
                    $exitesitemap = 1;
                    $urlRemove = $url;
                    break;
                }
            }
            if ($exitesitemap == 1) {

                $domxml = dom_import_simplexml($urlRemove);
                $domxml->parentNode->removeChild($domxml);

                $dom = new \DOMDocument('1.0');
                $dom->preserveWhiteSpace = false;
                $dom->formatOutput = true;
                $dom->loadXML($sitemap->asXML());
                $dom->save('sitemap.xml');
            }
        } else {
            $request->session()->flash('alert-success', 'No puede eliminar este curso');
        }

        return redirect()->route('cursos.index');
    }

}
