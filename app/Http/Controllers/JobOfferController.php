<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Queue\Jobs\Job;
use Validator;
use App\Entities\User;
use Illuminate\Support\Facades\Auth;
use App\Entities\JobOffer;
use App\Entities\Contract;
use App\Entities\TipoOferta;
use App\Entities\Province;
use App\Entities\Disponibilidad;
use App\Entities\SectorOferta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\File;
use App\Entities\Concello;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\Tags\Url;
use Spatie\Sitemap\Sitemap;
use DB;
use Illuminate\Support\Facades\View;
use Zipper;

class JobOfferController extends Controller {

    public $estatus = ['1' => 'PENDENTE', '2' => 'PUBLICADA', '3' => 'REXEITADA', '4' => 'PAUSADA', '5' => 'PECHADA'];

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('admin', ['only' => ['index', 'create', 'edit', 'remove']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function getOffers($offers) {
        $data = array();

        foreach ($offers as $offer) {

            $edit = route('ofertas.edit', $offer->id);
            $delete = url('/panel/admin/ofertas/' . $offer->id . '/remove');

            $incripcion = url('/panel/admin/ofertas/' . $offer->id . '/inscripcion');


            if (\Illuminate\Support\Facades\Auth::user()->role_id == 2) {

                $delete = url('/empresas/ofertas/' . $offer->id . '/remove');
                $edit = url('/empresas/ofertas/' . $offer->id . '/edit');
                $incripcion = '';
            }


//            if ($offer->propietario_fk !='') {
//           $nestedData['propietario'] = User::Where('pk','=',$offer->propietario_fk)->name;
//                   
//            } else

            $nestedData['id'] = $offer->id;
            $nestedData['propietario'] = $offer->propietario->name;


            $talentia = '';
            //if ($offer->pertenece_fie == 1) $talentia = ' (TALENTIA)';

            $nestedData['requested_job'] = htmlentities($offer->requested_job . $talentia);

            $nestedData['vacancy'] = $offer->vacancy;
            $nestedData['tasks'] = $offer->tasks;



            if ($offer->created_at)
                $nestedData['created_at'] = Carbon::createFromFormat('Y-m-d H:i:s', $offer->created_at)->format('d/m/Y');
            else
                $nestedData['created_at'] = '';




            $contract = Contract::find($offer->contract_type);
            $nestedData['contract_type'] = $contract ? $contract->nombre : '';

            // $nestedData['start_date'] = Carbon::createFromFormat('Y-m-d H:i:s', $offer->start_date)->format('d/m/Y');


            if ($offer->start_date)
                $nestedData['start_date'] = Carbon::createFromFormat('Y-m-d H:i:s', $offer->start_date)->format('d/m/Y');
            else
                $nestedData['start_date'] = '';


            $nestedData['duration'] = $offer->duration;

            $nestedData['locality'] = $offer->locality;
            $nestedData['hours'] = $offer->hours;
            $nestedData['salary'] = $offer->salary;

            $nestedData['publicada'] = $this->estatus[$offer->publicada];


            $province = Province::find($offer->province_fk);
            if ($province)
                $nestedData['province'] = $province->name;
            else
                $nestedData['province'] = '';



            $tipooferta = TipoOferta::find($offer->tipooferta);
            $nestedData['tipooferta'] = $tipooferta->name;


//            $disponibilidad = Disponibilidad::find($offer->disponibilidad_fk);
//
//            $nestedData['disponibilidad'] = $disponibilidad ? $disponibilidad->nombre : '';
//            $nestedData['publicada'] = $this->estatus[0];
//            dd($this->estatus[$offer->publicada]);
            //$requirements = $offer->getRequirements() ? implode(',', $offer->getRequirements()) : '';
            //$nestedData['requirements'] = '<p style="word-break: break-word;">' . $requirements . '</p>';


            $nestedData['options'] = "&emsp;<a href='{$edit}' title='Edit' ><span class='glyphicon glyphicon-edit' ></span></a>"
                    . "&emsp;<a id='remove_link' href='{$delete}' title='Delete' ><span class='glyphicon glyphicon-remove' style='color:red'></span></a>";

            if (\Illuminate\Support\Facades\Auth::user()->role_id == 3) {
                $nestedData['options'] = $nestedData['options'] . "&emsp;<a href='{$incripcion}' title='Inscripcion' ><span class='glyphicon  glyphicon-tasks'></span> </a></a>";
            }

            if (($offer->publicada == 2) && (\Illuminate\Support\Facades\Auth::user()->role_id == 2)) {

                $nestedData['options'] = '';
            }

            $concello = Concello::find($offer->concello_fk);
            if ($concello)
                $nestedData['concello'] = $concello->name;
            else
                $nestedData['concello'] = '';

            if ($offer->due_date)
                $nestedData['due_date'] = Carbon::createFromFormat('Y-m-d H:i:s', $offer->due_date)->format('d/m/Y');
            else
                $nestedData['due_date'] = '';

            $nestedData['sector_oferta'] = $offer->sector_fk;

            $nestedData['talentia'] = $offer->pertenece_fie == 1 ? 'Talentia' : '';


            $nestedData['inscritos'] = count($offer->postulados);

            $data[] = $nestedData;
        }

//dd($data); 

        return $data;
    }

    public function validar_fecha_espanol($fecha) {
        $valores = explode('/', $fecha);
        if (count($valores) == 3)
            if ((intval($valores[0]) > 0) && (intval($valores[1] > 0) && (intval($valores[2] > 0)) ))
                if (checkdate($valores[1], $valores[0], $valores[2])) {
                    return true;
                }
        return false;
    }

    public function listOffers(Request $request) {


        $draw = $request->get('draw');
        $fields = array('id', 'requested_job', 'vacancy', 'tasks', 'due_date', 'contract_type', 'start_date', 'duration', 'locality', 'hours', 'salary', 'requirements', 'propietario_fk', 'publicada', 'job_offer.province_fk as province_fk ', 'tipooferta', 'disponibilidad_fk', 'job_offer.concello_fk', 'pertenece_fie', 'job_offer.created_at', 'user.name As propietarioname', 'sector_fk');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->input('search.value');
        $colums = $request->get('columns');
        $order = $request->get('order');

        $columnIndex = $order[0]['column']; // Column index
        $columnName = $request->get('columns')[$columnIndex]['data']; // Column name

        if (\strtoupper($columnName) == 'PROPIETARIO')
            $columnName = 'propietarioname';

//          if (\strtoupper($columnName) == 'PERTENECE_FIE')
//            $columnName = 'propietarioname';


        $columnSortOrder = $order[0]['dir']; // asc or desc
        $query = array();
        $params = [];

        $referrer = $request->headers->get('referer');
        if (strpos($referrer, 'empresas') !== false) {
            array_push($query, 'propietario_fk like ?');
            array_push($params, '%' . \Illuminate\Support\Facades\Auth::user()->pk . '%');
        };


        if ($colums[0]['search']['value'] && $colums[0]['search']['value'] != 'null') {
            $value = $colums[0]['search']['value'];

            array_push($query, 'id like ?');
            array_push($params, '%' . $value . '%');
        }


        if ($colums[1]['search']['value'] && $colums[1]['search']['value'] != 'null') {
            $value = $colums[1]['search']['value'];

            array_push($query, 'propietario_fk = ?');
            array_push($params, $value);
        }

        if ($colums[2]['search']['value']) {
            $value = $colums[2]['search']['value'];

            array_push($query, 'requested_job like ?');
            array_push($params, '%' . $value . '%');
        }
        if ($colums[3]['search']['value']) {
            $value = $colums[3]['search']['value'];
            array_push($query, 'vacancy = ?');
            array_push($params, $value);
        }
        if ($colums[4]['search']['value']) {
            $value = $colums[4]['search']['value'];
            array_push($query, 'tasks like ?');
            array_push($params, '%' . $value . '%');
        }


        if ($colums[5]['search']['value'] && $colums[5]['search']['value'] != '-') {
            $value = $colums[5]['search']['value'];
            list($min, $max) = explode('-', $value);

            if (!empty($min) && !empty($max)) {



                $min = Carbon::createFromFormat('d/m/Y', $min)->format('Y-m-d');
                $max = Carbon::createFromFormat('d/m/Y', $max)->format('Y-m-d');


                $sql = 'date(job_offer.created_at) BETWEEN ? AND ?';
                array_push($query, $sql);

                array_push($params, stripslashes($min));

                array_push($params, stripslashes($max));
            } else {
                if (!empty($min)) {

                    $min = Carbon::createFromFormat('d/m/Y', $min)->format('Y-m-d');
                    $sql = 'date(job_offer.created_at) >= ? ';
                    array_push($query, $sql);

                    array_push($params, stripslashes($min));
                } else {

                    $max = Carbon::createFromFormat('d/m/Y', $max)->format('Y-m-d');
                    $sql = 'date(job_offer.created_at) <= ? ';
                    array_push($query, $sql);
                    array_push($params, stripslashes($max));
                }
            }
        }


        if (!empty($colums[6]['search']['value']) && ($colums[6]['search']['value'] != "null")) {
            $value = $colums[6]['search']['value'];
            array_push($query, 'contract_type IN (?)');
            array_push($params, $value);
        }

        if ($colums[7]['search']['value'] && $colums[7]['search']['value'] != '-') {
            $value = $colums[7]['search']['value'];
            list($min, $max) = explode('-', $value);
            if (!empty($min) && !empty($max)) {

                $min = Carbon::createFromFormat('d/m/Y', $min)->format('Y-m-d');
                $max = Carbon::createFromFormat('d/m/Y', $max)->format('Y-m-d');

                $sql = ' date(start_date) BETWEEN ? AND ?';
                array_push($query, $sql);
                array_push($params, stripslashes($min));

                array_push($params, stripslashes($max));
            } else {
                if (!empty($min)) {
                    $min = Carbon::createFromFormat('d/m/Y', $min)->format('Y-m-d');
                    $sql = 'date(start_date) >= ? ';
                    array_push($query, $sql);

                    array_push($params, stripslashes($min));
                } else {
                    $max = Carbon::createFromFormat('d/m/Y', $max)->format('Y-m-d');
                    $sql = 'date(start_date) <= ? ';
                    array_push($query, $sql);
                    array_push($params, stripslashes($max));
                }
            }
        }


        if ($colums[8]['search']['value'] && $colums[8]['search']['value'] != '-') {
            $value = $colums[8]['search']['value'];
            list($min, $max) = explode('-', $value);
            if (!empty($min) && !empty($max)) {
                $sql = 'duration >=? AND duration<=?';
                array_push($query, $sql);
                array_push($params, $min);
                array_push($params, $max);
            } else {
                if (!empty($min)) {
                    $sql = 'duration >=?';
                    array_push($query, $sql);
                    array_push($params, $min);
                } else {
                    if (!empty($max)) {
                        $sql = 'duration <=?';
                        array_push($query, $sql);
                        array_push($params, $max);
                    }
                }
            }
        }

        if ($colums[9]['search']['value']) {
            $value = $colums[9]['search']['value'];
            array_push($query, 'locality like ?');
            array_push($params, '%' . $value . '%');
        }
        if ($colums[11]['search']['value'] && $colums[11]['search']['value'] != '-') {
            $value = $colums[11]['search']['value'];
            list($min, $max) = explode('-', $value);
            if (!empty($min) && !empty($max)) {
                $sql = 'salary>=? AND salary<=?';
                array_push($query, $sql);
                array_push($params, $min);
                array_push($params, $max);
            } else {
                if (!empty($min)) {
                    $sql = 'salary >=?';
                    array_push($query, $sql);
                    array_push($params, $min);
                } else {
                    if (!empty($max)) {
                        $sql = 'salary <=?';
                        array_push($query, $sql);
                        array_push($params, $max);
                    }
                }
            }
        }
        if ($colums[12]['search']['value'] && $colums[12]['search']['value'] >= '0') {
            $value = $colums[12]['search']['value'];
            array_push($query, 'publicada = ?');
            array_push($params, $value);
        }

        if ($colums[13]['search']['value'] && $colums[13]['search']['value'] >= '0') {
            $value = $colums[13]['search']['value'];
            array_push($query, 'job_offer.province_fk = ?');
            array_push($params, $value);
        }
        if ($colums[14]['search']['value'] && $colums[14]['search']['value'] >= '0') {
            $value = $colums[14]['search']['value'];
            array_push($query, 'tipooferta = ?');
            array_push($params, $value);
        }

        if ($colums[16]['search']['value'] && $colums[16]['search']['value'] >= '0') {
            $value = $colums[16]['search']['value'];
            array_push($query, 'job_offer.concello_fk = ?');
            array_push($params, $value);
        }




        if ($colums[17]['search']['value'] && $colums[17]['search']['value'] != '-') {
            $value = $colums[17]['search']['value'];
            list($min, $max) = explode('-', $value);

            if (!empty($min) && !empty($max)) {



                $min = Carbon::createFromFormat('d/m/Y', $min)->format('Y-m-d');
                $max = Carbon::createFromFormat('d/m/Y', $max)->format('Y-m-d');


                $sql = 'date(due_date) BETWEEN ? AND ?';
                array_push($query, $sql);

                array_push($params, stripslashes($min));

                array_push($params, stripslashes($max));
            } else {
                if (!empty($min)) {

                    $min = Carbon::createFromFormat('d/m/Y', $min)->format('Y-m-d');
                    $sql = 'date(due_date) >= ? ';
                    array_push($query, $sql);

                    array_push($params, stripslashes($min));
                } else {

                    $max = Carbon::createFromFormat('d/m/Y', $max)->format('Y-m-d');
                    $sql = 'date(due_date) <= ? ';
                    array_push($query, $sql);
                    array_push($params, stripslashes($max));
                }
            }
        }

        if ($colums[18]['search']['value']) {
            $value = $colums[18]['search']['value'];
            array_push($query, 'sector_fk = ?');
            array_push($params, $value);
        }

        if ($colums[19]['search']['value']) {
            $value = $colums[19]['search']['value'];
            array_push($query, 'pertenece_fie = ?');
            if ($value == 2)
                $value = 0;
            array_push($params, $value);
        }



        if (empty($search) && empty($query)) {
            $total_offers = JobOffer::count();
            //$offers = JobOffer::orderBy('id', 'DESC')->skip($start)->take($length)->get($fields);
            $offers = JobOffer::join('user', 'user.pk', '=', 'job_offer.propietario_fk')->orderBy($columnName, $columnSortOrder)->skip($start)->take($length)->get($fields);
            //
        } else if (!empty($query)) {
            $query = implode(' AND ', $query);

            $total_offers = JobOffer::whereRaw($query, $params)->count();

//            $offers = JobOffer::whereRaw($query, $params)->offset($start)->limit($length)
//                            ->orderBy('id', 'DESC')->get($fields);
            $offers = JobOffer::join('user', 'user.pk', '=', 'job_offer.propietario_fk')->whereRaw($query, $params)->offset($start)->limit($length)
                            ->orderBy($columnName, $columnSortOrder)->get($fields);


            //orderBy($columnName,$columnSortOrder)
        } else {
            $filtroFecha = $search;
            if ($this->validar_fecha_espanol($search)) {


                $filtroFecha = Carbon::createFromFormat('d/m/Y', $search)->format('Y-m-d');
            };

            $talentia = strpos(strtolower($search), 'talentia') >= 0;



            $propietarios = User::Where('name', 'LIKE', "%{$search}%")->orWhere('surname', 'LIKE', "%{$search}%")->pluck('pk');
            if ($propietarios) {
                $arPropietarios = $propietarios->toArray();
                // dd($arPropietarios);
            }

            $tipoContrato = \App\Entities\Contract::Where('nome', 'LIKE', "%{$search}%")->orWhere('nombre', 'LIKE', "%{$search}%")->pluck('id');

            if ($tipoContrato) {
                $arTtipoContrato = $tipoContrato->toArray();

                // dd($arPropietarios);
            }

            $total_offers = JobOffer::join('user', 'user.pk', '=', 'job_offer.propietario_fk')->where('requested_job', 'LIKE', "%{$search}%")
                    ->orWhere('id', 'LIKE', "%{$search}%")
                    ->orWhereIn('propietario_fk', $arPropietarios)
                    ->orWhere('vacancy', 'LIKE', "%{$search}%")
                    ->orWhere('tasks', 'LIKE', "%{$search}%")
                    ->orWhere('due_date', 'LIKE', "%{$filtroFecha}%")
                    ->orWhere('job_offer.created_at', 'LIKE', "%{$filtroFecha}%")

                    //->orWhere('\DATE_FORMAT(due_date,"d/m/Y")', 'LIKE',"%{$search}%")
                    ->orWhereIn('contract_type', $arTtipoContrato)
                    ->orWhere('start_date', 'LIKE', "%{$filtroFecha}%")
                    ->orWhere('duration', 'LIKE', "%{$search}%")
                    ->orWhere('locality', 'LIKE', "%{$search}%")
                    ->orWhere('hours', 'LIKE', "%{$search}%")
                    ->orWhere('salary', 'LIKE', "%{$search}%")
                    ->orWhere('requirements', 'LIKE', "%{$search}%");
            if ($talentia)
                $total_offers = $total_offers->orWhere('pertenece_fie', 1);

            $total_offers = $total_offers->count();

            $offers = JobOffer::join('user', 'user.pk', '=', 'job_offer.propietario_fk')->where('requested_job', 'LIKE', "%{$search}%")
                    ->orWhere('id', 'LIKE', "%{$search}%")
                    ->orWhereIn('propietario_fk', $arPropietarios)
                    ->orWhere('vacancy', 'LIKE', "%{$search}%")
                    ->orWhere('tasks', 'LIKE', "%{$search}%")
                    ->orWhere('due_date', 'LIKE', "%{$filtroFecha}%")
                    ->orWhere('job_offer.created_at', 'LIKE', "%{$filtroFecha}%")
                    ->orWhereIn('contract_type', $arTtipoContrato)
                    ->orWhere('start_date', 'LIKE', "%{$filtroFecha}%")
                    ->orWhere('duration', 'LIKE', "%{$search}%")
                    ->orWhere('locality', 'LIKE', "%{$search}%")
                    ->orWhere('hours', 'LIKE', "%{$search}%")
                    ->orWhere('salary', 'LIKE', "%{$search}%")
                    ->orWhere('requirements', 'LIKE', "%{$search}%");

            if ($talentia)
                $offers = $offers->orWhere('pertenece_fie', 1);

            $offers = $offers->offset($start)
                    ->limit($length)
//                    ->orderBy('id', 'DESC')
                    ->orderBy($columnName, $columnSortOrder)
                    ->get($fields);
        }
        $offers = $this->getOffers($offers);

        // renderView con offers, luego mando ese render a un pdf con un


        $data = array(
            'draw' => $draw,
            'recordsTotal' => $total_offers,
            'recordsFiltered' => $total_offers,
            'data' => $offers,
        );

        echo json_encode($data);
    }

    public function index(Request $request) {
        $this->middleware('admin');
        $contracts = Contract::all();
        $disponibilidades = \App\Entities\Disponibilidad::all();
        $tipoofertas = TipoOferta::all();
        $provinces = Province::orderBy('name', 'ASC')->get();
        $empresas = User::Where('role_id', '=', '2')->get();
        $sectores = SectorOferta::all();
//        dd($sectores);
        //dd($empresas[0]->name);
        $offers = JobOffer::orderBy('id', 'DESC')->paginate(25);


        return view('job_offer.index')->with(['contracts' => $contracts, 'empresas' => $empresas, 'disponibilidades' => $disponibilidades, 'tipoofertas' => $tipoofertas, 'disponibilidades' => $disponibilidades, 'provinces' => $provinces, 'sectores' => $sectores]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $contracts = Contract::all();
        $disponibilidades = \App\Entities\Disponibilidad::all();
        $tipoofertas = TipoOferta::all();
        $empresas = User::Where('role_id', '=', '2')->get();
        $provinces = Province::all();
        $sectores = SectorOferta::all();

        return view('job_offer.create')->with(['contracts' => $contracts, 'empresas' => $empresas, 'tipoofertas' => $tipoofertas, 'provinces' => $provinces, 'disponibilidades' => $disponibilidades, 'sectores' => $sectores]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $job_offer = new JobOffer();


        $job_offer->propietario_fk = $request->input('propietario');

        $job_offer->requested_job = $request->input('requested_job');
        $job_offer->vacancy = $request->input('vacancy');
        $job_offer->tasks = $request->input('tasks');

        if ($request->input('due_date'))
            $job_offer->due_date = Carbon::createFromFormat('d/m/Y', $request->input('due_date'))->format('Y-m-d');
        else
            $job_offer->due_date = Null;



        //$job_offer->due_date = new \Date($fecha); 
        //$fecha= Carbon::createFromFormat('d/m/Y', $request->input('start_date'));

        if ($request->input('start_date'))
            $job_offer->start_date = $fecha = Carbon::createFromFormat('d/m/Y', $request->input('start_date'))->format('Y-m-d');
        else
            $job_offer->start_date = Null;


        //$job_offer->start_date = new \Date($fecha); 
        //$job_offer->start_date = new \DateTime($request->input('start_date').' 00:00:00');

        $job_offer->contract_type = $request->input('contract_type');

        $job_offer->sector_fk = $request->input('sector');


        $job_offer->duration = $request->input('duration');
        $job_offer->tiempo = $request->input('tiempo');



        $job_offer->locality = $request->input('locality');
        $job_offer->hours = $request->input('hours');
        $job_offer->salary = $request->input('salary');
        $job_offer->rexperiencia = $request->input('rexperiencia');
        $job_offer->rformacion = $request->input('rformacion');
        $job_offer->rotros = $request->input('rotros');

        if ($request->input('observaciones'))
            $job_offer->observaciones = $request->input('observaciones');

        $job_offer->propietario_fk = $request->input('propietario');
        $job_offer->disponibilidad_fk = $request->input('disponibilidad');

        $other_province = $request->input('other_province');
        $other_concello = $request->input('other_concello');


        $province_id = $request->input('province');
        $concello_id = $request->input('concello_chosen');

        if ($province_id)
            $province = Province::find($province_id);

        if ($other_province != '') {
            $province = Province::firstOrNew(array('name' => $other_province));
            $province->name = $other_province;
            $province->country_fk = 'ES';
            $province->save();
        }
        $job_offer->province_fk = isset($other_province) ? $province->pk : $request->input('province');

        if ($other_concello != '') {
            $concello = Concello::firstOrNew(array('name' => $other_concello));
            $concello->name = $other_concello;
            $concello->province_pk = $province->pk;
            $concello->save();
            $concello_id = $concello->id;
        };
        $job_offer->concello_fk = isset($other_concello) ? $concello->id : $request->input('concello_chosen');



        if ($request->input('tipogestion'))
            $job_offer->tipogestion = $request->input('tipogestion');
        else
            $job_offer->tipogestion = 0;

        if ($job_offer->tipogestion == 1 && $request->input('urlTercero'))
            $job_offer->urltercero = $request->input('urlTercero');
        else
            $job_offer->urltercero = '';



        if ($request->input('pertenece_fie'))
            $job_offer->pertenece_fie = 1;
        else
            $job_offer->pertenece_fie = 0;

        $job_offer->tipooferta = $request->input('tipooferta');

        $job_offer->created_by = \Illuminate\Support\Facades\Auth::user()->pk;

        $estatus = $request->input('publicada');

        if ($request->input('limitar_inscripcion')) {
            $job_offer->maximo_inscripciones = $request->input('maximo_inscripciones');
            $job_offer->limitar_inscripcion = 1;
        } else {
            $job_offer->maximo_inscripciones = null;
            $job_offer->limitar_inscripcion = 0;
        }




        if (isset($estatus)) {
            $job_offer->publicada = $request->input('publicada');
        }




        if ($job_offer->contract_type == 1) { // si el contrato es de tiempo determinado
            $job_offer->duration = $request->input('duration');
            $job_offer->tiempo = $request->input('tiempo');
        }
        if ($job_offer->contract_type == 2) { // si el contrato es de tiempo indfinido
            $job_offer->duration = 0;
            $job_offer->tiempo = '';
        }

        $editados = json_encode($job_offer->getDirty());


        //$job_offer->requirements = serialize($request->input('requirements'));
        $job_offer->save();

        if ($job_offer->publicada == 2) {

            $sitemap = new \SimpleXMLElement('sitemap.xml', 0, true);
            //$sitemap = simplexml_load_file('sitemap.xml'); 
            // Añadimos , elementos y atributo
            $nuevaUrl = $sitemap->addChild('url');
            $nuevaUrl->addChild('loc', $job_offer->getSlug());
            $nuevaUrl->addChild('lastmod', str_replace(' ', 'T', Carbon::now()) . '+00:00');
            $nuevaUrl->addChild('priority', '0.8');
            //$sitemap->asXML('sitemap.xml');

            $domxml = new \DOMDocument('1.0');
            $domxml->preserveWhiteSpace = false;
            $domxml->formatOutput = true;
            $domxml->loadXML($sitemap->asXML());
            $domxml->save('sitemap.xml');
        }


        $imagen_destacada = $request->file('imagen_destacada');
        $slug = $job_offer->id;
        $destinationPath = public_path('offers');



        if ($imagen_destacada) {
            $imagen_destacada_str = $slug . '_imagen_destacada.' . $imagen_destacada->getClientOriginalExtension();

            $imagen_destacada->move($destinationPath, $imagen_destacada_str);

            $values['imagen'] = $imagen_destacada_str;
            $job_offer->imagen = $imagen_destacada_str;
        }



        $job_offer->update();



        \App\Entities\Auditoria::create([
            'model_name' => 'Ofertas de Empleo',
            'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
            'action' => 'Creó el registro Oferta ' . $job_offer->id . ' ' . $editados,
            'original_set' => $job_offer->toJson(),
            'changed_set' => ''
        ]);

        $request->session()->flash('alert-success', 'Oferta creada correctamente');

        $referrer = $request->headers->get('referer');
        if (strpos($referrer, 'empresas') !== false) {
            $user = \Auth::user();

            \App\Entities\NotificacionesAdmin::create([
                'mensaje' => 'Se ha dado de alta una oferta por la empresa ' . $user->cif . ': ' . $user->surname,
                'tabla' => 'ofertas',
                'tabla_id' => $job_offer->id,
                'ruta' => url('/panel/ofertas/' . $job_offer->id . '/edit'),
                'leido' => 0,
                'procesado' => 0
            ]);

            return Redirect::back();
        }




        return redirect()->route('ofertas.index')->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JobOffer  $job_offer
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $contracts = Contract::all();
        $disponibilidades = \App\Entities\Disponibilidad::all();
        $job_offer = JobOffer::findOrFail($id);
        $tipoofertas = TipoOferta::all();
        $provinces = Province::all();
        $sectores = SectorOferta::all();
        $empresas = User::Where('role_id', '=', '2')->get();

        $concellos = Concello::Where('province_pk', $job_offer->province_fk)->get();


//        var_dump($job_offer->due_date);
//        die();
//        $requirements = $job_offer->getRequirements();
//        $experience = false;
//        $training = false;
//        $others = false;
//        $others_text = '';
//        $expkey = array_search('experience', $requirements);
//        $traikey = array_search('training', $requirements);
//        if ($expkey !== false) {
//            $experience = true;
//            //$key = array_search('Experiencia',$requirements);
//            array_splice($requirements, $expkey, 1);
//        }
//        if ($traikey !== false) {
//            $training = true;
//            //$key = array_search('training',$requirements);
//            array_splice($requirements, $traikey, 1);
//        }
//        if ($requirements[0]) {
//            $others = true;
//            $others_text = $requirements[0];
//        }
        //return view('job_offer.edit')->with(['others' => $others, 'others_text' => $others_text, 'training' => $training, 'experience' => $experience, 'job_offer' => $job_offer, 'contracts' => $contracts, 'requirements' => $requirements]);
        return view('job_offer.edit')->with(['job_offer' => $job_offer, 'contracts' => $contracts, 'empresas' => $empresas, 'tipoofertas' => $tipoofertas, 'provinces' => $provinces, 'concellos' => $concellos, 'disponibilidades' => $disponibilidades, 'sectores' => $sectores]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JobOffer  $job_offer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //$values = array('requested_job','vacancy','tasks','due_date','contract_type','start_date','duration','locality','hours','salary','requirements');
        $jobOffer = JobOffer::find($id);
        $original = $jobOffer->toJson();
        $slugS = $jobOffer->getSlug();
        $jobOffer->requested_job = $request->input('requested_job');
        $jobOffer->propietario_fk = $request->input('propietario');
        $jobOffer->disponibilidad_fk = $request->input('disponibilidad');
        $jobOffer->sector_fk = $request->input('sector');

        $jobOffer->vacancy = $request->input('vacancy');
        $jobOffer->tasks = $request->input('tasks');

        if ($request->input('due_date'))
            $jobOffer->due_date = Carbon::createFromFormat('d/m/Y', $request->input('due_date'))->format('Y-m-d');
        else
            $jobOffer->due_date = Null;


        $jobOffer->contract_type = $request->input('contract_type');

        if ($request->input('start_date'))
            $jobOffer->start_date = Carbon::createFromFormat('d/m/Y', $request->input('start_date'))->format('Y-m-d');
        else
            $jobOffer->start_date = Null;


        $jobOffer->duration = $request->input('duration');


//        $other_province = $request->input('other_province');
//        $province_id = $request->input('province');
//
//        if ($province_id)
//            $province = Province::find($province_id);
//
//        if ($other_province != '') {
//            $province = Province::firstOrNew(array('name' => $other_province));
//            $province->name = $other_province;
//            $province->country_fk = 'ES';
//            $province->save();
//        }
//        $values['province_fk'] = isset($other_province) ? $province->pk : $request->input('province');


        $other_province = $request->input('other_province');
        $other_concello = $request->input('other_concello');


        $province_id = $request->input('province');
        $concello_id = $request->input('concello_chosen');






        if ($province_id)
            $province = Province::find($province_id);

        if ($other_province != '') {
            $province = Province::firstOrNew(array('name' => $other_province));
            $province->name = $other_province;
            $province->country_fk = 'ES';
            $province->save();
            $province_id = $province->pk;
        }

        if ($other_concello != '') {
            $concello = Concello::firstOrNew(array('name' => $other_concello));
            $concello->name = $other_concello;
            $concello->province_pk = $province->pk;
            $concello->save();
            $concello_id = $concello->id;
        };

        if ($concello_id)
            $concello = Concello::find($concello_id);

        $jobOffer->province_fk = $province_id;
        $jobOffer->concello_fk = $concello_id;



        $jobOffer->locality = $request->input('locality');
        $jobOffer->hours = $request->input('hours');
        $jobOffer->salary = $request->input('salary');
        // $values['requirements'] = serialize($request->input('requirements'));
        $jobOffer->rexperiencia = $request->input('rexperiencia');
        $jobOffer->rformacion = $request->input('rformacion');
        $jobOffer->rotros = $request->input('rotros');

        if ($request->input('observaciones'))
            $jobOffer->observaciones = $request->input('observaciones');




        if ($request->input('tipogestion'))
            $jobOffer->tipogestion = $request->input('tipogestion');
        else
            $jobOffer->tipogestion = 0;

        if ($jobOffer->tipogestion == 1 && $request->input('urlTercero'))
            $jobOffer->urltercero = $request->input('urlTercero');
        else
            $jobOffer->urltercero = '';



        if ($request->input('pertenece_fie'))
            $jobOffer->pertenece_fie = 1;
        else
            $jobOffer->pertenece_fie = 0;




        $imagen_destacada = $request->file('imagen_destacada');
        $slug = $jobOffer->id;
        $destinationPath = public_path('offers');


        if ($request->input('limitar_inscripcion')) {
            $jobOffer->maximo_inscripciones = $request->input('maximo_inscripciones');
            $jobOffer->limitar_inscripcion = 1;
        } else {
            $jobOffer->maximo_inscripciones = null;
            $jobOffer->limitar_inscripcion = 0;
        }



        if ($jobOffer->contract_type == 1) { // si el contrato es de tiempo determinado
            $jobOffer->duration = $request->input('duration');
            $jobOffer->tiempo = $request->input('tiempo');
        }
        if ($jobOffer->contract_type == 2) { // si el contrato es de tiempo indfinido
            $jobOffer->duration = 0;
            $jobOffer->tiempo = '';
        }

        $jobOffer->tipooferta = $request->input('tipooferta');

        $now = new \DateTime();



        //$values['updated_at']= $now;
        $jobOffer->updated_by = \Illuminate\Support\Facades\Auth::user()->pk;

        if ($imagen_destacada) {

            $image_path = public_path("{$jobOffer->imagen}");

            if (File::exists($image_path)) {
                File::delete($image_path);
                //unlink($image_path);
            }


            $imagen_destacada_str = $slug . '_imagen_destacada.' . $imagen_destacada->getClientOriginalExtension();

            $imagen_destacada->move($destinationPath, $imagen_destacada_str);

            $jobOffer->imagen = $imagen_destacada_str;
        }


        $estatus = $request->input('publicada');
        if (isset($estatus)) {
            $old_status = $jobOffer->publicada;
            $jobOffer->publicada = $estatus;

            if (($old_status == 1) && ($estatus != $old_status)) { //hubo cambio de estatus
                //buscar en notificaciones si hay algun registro relacionado o no con la activacion 
                $notificaciones = \App\Entities\NotificacionesAdmin::Where('tabla_id', '=', $jobOffer->id)
                        ->where('tabla', '=', 'ofertas')
                        ->where('procesado', '=', '0');

                if ($notificaciones) {
                    //dd($notificaciones);
                    $notificaciones->procesado = 1;
                    $notificaciones->update(['procesado' => 1]);
                }
            }

            if (($estatus == 2)) { //Estoy publicando, agregar a site map
                $sitemap = new \SimpleXMLElement('sitemap.xml', 0, true);

                //$sitemap = simplexml_load_file('sitemap.xml');

                /* Para cada <personaje>, se muestra cada <nombre>. */
                $exitesitemap = 0;
                foreach ($sitemap->url as $url) {
                    if ((string) $url->loc == $slugS) {
                        $exitesitemap = 1;
                    }
                }
                if ($exitesitemap == 0) {

                    // Añadimos , elementos y atributo
                    $nuevaUrl = $sitemap->addChild('url');
                    $nuevaUrl->addChild('loc', $jobOffer->getSlug());
                    $nuevaUrl->addChild('lastmod', str_replace(' ', 'T', Carbon::now()) . '+00:00');
                    $nuevaUrl->addChild('priority', '0.8');
                    //$sitemap->asXML('sitemap.xml');

                    $domxml = new \DOMDocument('1.0');
                    $domxml->preserveWhiteSpace = false;
                    $domxml->formatOutput = true;
                    $domxml->loadXML($sitemap->asXML());
                    $domxml->save('sitemap.xml');
                }
            } else {

                $sitemap = new \SimpleXMLElement('sitemap.xml', 0, true);

                //$sitemap = simplexml_load_file('sitemap.xml');

                $exitesitemap = 0;
                foreach ($sitemap->url as $url) {
                    if ((string) $url->loc == $slugS) {
                        $exitesitemap = 1;
                        $urlRemove = $url;
                        break;
                    }
                }
                if ($exitesitemap == 1) {

                    $domxml = dom_import_simplexml($urlRemove);
                    $domxml->parentNode->removeChild($domxml);

                    $dom = new \DOMDocument('1.0');
                    $dom->preserveWhiteSpace = false;
                    $dom->formatOutput = true;
                    $dom->loadXML($sitemap->asXML());
                    $dom->save('sitemap.xml');
                }
            }
        }

        $editados = json_encode($jobOffer->getDirty());

        $jobOffer->update();



        if ($original != $jobOffer->toJson()) {
            \App\Entities\Auditoria::create([
                'model_name' => 'Ofertas de Empleo',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Actualizó el registro oferta ' . $jobOffer->id . ' ' . $editados,
                'original_set' => $original,
                'changed_set' => $jobOffer->toJson()
            ]);
        }



        $request->session()->flash('alert-success', 'Oferta editada correctamente');

        $referrer = $request->headers->get('referer');
        if (strpos($referrer, 'empresas') !== false) {

            return Redirect::back()->withInput();
        }

        return redirect()->route('ofertas.index')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JobOffer  $job_offer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {

        $destinationPath = public_path('offers');
        $oferta = JobOffer::find($id);
        if ($oferta) {
            if ($oferta->imagen) {
                $mi_imagen = $destinationPath . '/' . $oferta->imagen;
                unlink($mi_imagen);
            }
        }

        JobOffer::destroy($id);
        $request->session()->flash('alert-success', 'Oferta eliminadas correctamente');
        return redirect()->route('ofertas.index')->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JobOffer  $job_offer
     * @return \Illuminate\Http\Response
     */
    public function show(JobOffer $job_offer) {
        
    }

    public function remove(Request $request, $id) {
        $joboffer = JobOffer::find($id);


        $destinationPath = public_path('offers');

        if ($joboffer) {
            if ($joboffer->imagen) {
                $mi_imagen = $destinationPath . '/' . $joboffer->imagen;
                unlink($mi_imagen);
            }
        }
        $original = $joboffer->toJson();

        if (!$joboffer->postulados()->count() >= 1) {


            $joboffer->delete();

            \App\Entities\Auditoria::create([
                'model_name' => 'Ofertas de Empleo',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Eliminar el registro ID ' . $id,
                'original_set' => $original,
                'changed_set' => ''
            ]);
            $request->session()->flash('alert-success', 'Oferta eliminada correctamente');

            $sitemap = new \SimpleXMLElement('sitemap.xml', 0, true);

            //$sitemap = simplexml_load_file('sitemap.xml');

            $exitesitemap = 0;
            foreach ($sitemap->url as $url) {
                if ((string) $url->loc == $joboffer->getSlug()) {
                    $exitesitemap = 1;
                    $urlRemove = $url;
                    break;
                }
            }
            if ($exitesitemap == 1) {

                $domxml = dom_import_simplexml($urlRemove);
                $domxml->parentNode->removeChild($domxml);

                $dom = new \DOMDocument('1.0');
                $dom->preserveWhiteSpace = false;
                $dom->formatOutput = true;
                $dom->loadXML($sitemap->asXML());
                $dom->save('sitemap.xml');
            }
        } else {
            $request->session()->flash('alert-warning', 'No puede eliminar esta oferta');
        }
        return redirect()->route('ofertas.index');
    }

    public function removeimgd(Request $request, $id, $img) {

        $destinationPath = public_path('offers');

        $mi_imagen = $destinationPath . '/' . $img;
        if (@getimagesize($mi_imagen)) {

            $oferta = JobOffer::find($id);
            $oferta->imagen = '';
            $oferta->update();
            unlink($mi_imagen);
            $request->session()->flash('alert-success', 'Imagen borrada correctamente');

            \App\Entities\Auditoria::create([
                'model_name' => 'Ofertas de Empleo',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Borró imagen',
                'original_set' => $oferta,
                'changed_set' => ''
            ]);
        } else {
            $request->session()->flash('alert-success', 'Imagen o datos no encontrados');
        }
        return Redirect::back()->withInput();
    }

    public function inscripcion($oferta_id) {


        $oferta = Joboffer::find($oferta_id);



        return view('job_offer.inscripciones')->with(['oferta' => $oferta]);
    }


    //********************************************
    //Funcion Añadida para bajar lote de cv en un archivo comprimido Zip
    //********************************************

    public function comprimirYDescargarZip($oferta_id){

        $oferta = Joboffer::find($oferta_id);

        $recicle = glob(storage_path('app/lotes/*'));
        $files = glob(storage_path('app/*'));
        $filesToAdd = array();

        foreach($recicle as $recicleFile){
            unlink($recicleFile);
        }

        foreach($oferta->postulados as $inscrito){

            foreach($files as $file){

                if (strpos($file, $inscrito->pivot->curriculo)) {
                    array_push($filesToAdd, storage_path('app/'.$inscrito->pivot->curriculo));
                }

            }
        
        }

        if (empty($filesToAdd)) {

            return back()->withInput();
            
        } else {

            Zipper::make(storage_path('app/lotes/Lote_CV_Oferta_ID-'.$oferta_id.'.zip'))->add($filesToAdd)->close();
        
            return response()->download(storage_path('app/lotes/Lote_CV_Oferta_ID-'.$oferta_id.'.zip'));

        }

    }

    public function confirmarInscripcion(Request $request) {


        $oferta = JobOffer::where('publicada', '=', '2')->findOrFail($request->input('idOferta'));
        $user = User::find($request->input('idUser'));
        $curriculo = $request->input('curriculo');


        $inscribir = 1;
        if ($oferta->limitar_inscripcion > 0) {
            $postulados = $oferta->postulados()->get()->count();
            if ($postulados >= $oferta->maximo_inscripciones) {
                $inscribir = 0;
            }
        }

        try {
            // $oferta->postulados()->attach($user);
            if ($inscribir == 1) {
                $oferta->postulados()->attach($user, ['created' => Carbon::now(), 'curriculo' => $curriculo]);
                //$oferta->puestos_cubiertos= $oferta->puestos_cubiertos + 1;


                $postulados = $oferta->postulados()->get()->count();
                if ($postulados >= $oferta->maximo_inscripciones) {
                    $oferta->publicada = 5;
                }
                $oferta->save();

                \App\Entities\Auditoria::create([
                    'model_name' => 'Ofertas',
                    'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                    'action' => 'Incribió en oferta ' . $oferta->id . ':' . $oferta->requested_job . ' al usuario ' . $user->cif . ': ' . $user->name . ' ' . $user->surname,
                    'original_set' => '',
                    'changed_set' => ''
                ]);


                $request->session()->flash('alert-success', 'Se ha inscrito Exitosamente en la oferta ' . $oferta->nombre);
            } else {
                $request->session()->flash('alert-danger', 'Se ha alcanzado número màximo de inscripciones ' . $oferta->nombre);
            }
        } catch (Exception $e) {
            $request->session()->flash('alert-danger', 'Hubo un error en la inscripción de la oferta ' . $oferta->nombre);
            //return "error";
        }





        return redirect()->action('JobOfferController@inscripcion', ['oferta_id}' => $oferta->id]);
    }

    public function buscardatos(Request $request) {



        $dni = $request->input('idUser');
        $oferta = $request->input('idOferta');

        $Oferta = JobOffer::find($oferta);

        $inscrito = $Oferta->postulados()->Where('cif', '=', $dni);

        if ($inscrito->count() > 0) {
            return response()->json(['message' => 'Ya se encuentra inscrito'], 200);
        };

        $usuario = User::Where('cif', '=', $dni)->where('status', '=', '1')->where('perfilcompletado', '=', '1');


        if ($usuario->count() > 0) {
            return response()->json(['message' => 'Exitoso', 'usuario' => $usuario->get(), 'oferta' => $Oferta], 200);
        } else {
            return response()->json(['message' => 'Usuario no encontrado, no válido o perfil no ha sido completado'], 200);
        }
    }

    public function retirarInscripcion(Request $request) {
        $oferta = JobOffer::find($request['idOferta']);
        $user = User::find($request['idUser']);


        try {

            $oferta->postulados()->detach($user);
            \App\Entities\Auditoria::create([
                'model_name' => 'Ofertas',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Retiró en oferta ' . $oferta->id . ':' . $oferta->requested_job . ' al usuario ' . $user->cif . ': ' . $user->name . ' ' . $user->surname,
                'original_set' => '',
                'changed_set' => ''
            ]);
            //$oferta->puestos_cubiertos = $oferta->puestos_cubiertos - 1;
        } catch (Exception $e) {
            return "error";
        }
        $request->session()->flash('alert-success', 'Se ha retirado Exitosamente de la oferta ' . $oferta->requested_job);

        return redirect()->action(
                        'JobOfferController@inscripcion', ['oferta_id}' => $oferta->id]
        );
    }

    public function reclutamiento() {
        //$this->middleware('admin');
        //dd($empresas[0]->name);
        
      
        $offers = JobOffer::orderBy('id', 'DESC')->get();
        
         $empresas = User::where('role_id', '=', '2')->where('status','=','1')->get();
         
        //dd($offers);
         
        
        return view('job_offer.reclutamiento')->with(['ofertas' => $offers, 'empresas'=>$empresas]);
    }

    public function descargarXml() {
//        header('Content-type: text/xml');
//        header('Content-Disposition: attachment; filename="NombreDelArchivo.xml"');

        $ruta = "AXENCIA_XML_DEFINITIVO.xml";
        header("Content-Disposition: attachment; filename=" . $ruta);
        header("Content-Type: application/octet-stream");
        header("Content-Length: " . filesize($ruta));
        readfile($ruta);
    }

    public function inscritos(Request $request) {


        $id = $request->input('oferta');




        $inscritos = JobOffer::where('id', $id)->first()->postulados()->get()->toArray();
//dd($inscritos);

        return response()->json([
                    'state' => 'success',
                    'data' => json_encode($inscritos),
        ]);
    }

    public function actualizarCampo(Request $request) {


        $id = $request->input('id');
        $campo = $request->input('campo');
        $valor = $request->input('valor') == 'true' ? 1 : 0;

        DB::table('cv')
                ->where('pk', $id)
                ->update([$campo => $valor]);

        if ($campo == 'contratado' && $valor == '0') {
            DB::table('cv')
                    ->where('pk', $id)
                    ->update(['data_contrato' => null, 'tipo_contrato' => null, 'empresa' => '', 'cif_empresa' => '']);

            $idOferta = DB::table('cv')->select('job_fk')
                            ->where('pk', $id)->first();

            $oferta = JobOffer::find($idOferta->job_fk);
            // dd($oferta);

            $oferta->puestos_cubiertos = $oferta->puestos_cubiertos - 1;
            $oferta->update();
        } else {
            if ($campo == 'contratado' && $valor == '1') {
                $fechaContrato = $request->input('fechaContrato');
                $tipoContrato = $request->input('tipoContrato');
                $cifEmpresa = $request->input('cifEmpresa');
                $razonSocialEmpresa = $request->input('razonSocial');
                $fechaContrato = Carbon::createFromFormat('d/m/Y', $fechaContrato)->format('Y-m-d');

                DB::table('cv')
                        ->where('pk', $id)
                        ->update(['data_contrato' => $fechaContrato, 'tipo_contrato' => $tipoContrato, 'empresa' => $razonSocialEmpresa, 'cif_empresa' => $cifEmpresa]);

                $idOferta = DB::table('cv')->select('job_fk')
                                ->where('pk', $id)->first();

                $oferta = JobOffer::find($idOferta->job_fk);

                // dd($oferta);

                $oferta->puestos_cubiertos = $oferta->puestos_cubiertos + 1;
                $oferta->update();
            }
        }


        return response()->json([
                    'state' => 'success',
                    'data' => json_encode([]),
        ]);
    }

    public function sepeEstadisticas(Request $request) {


        // $this->middleware('admin');
        $codigoAxencia = '1200000008';

//        if ($request->input('mes') && $request->input('ano')) {
//            $mes = $request->input('mes');
//            $ano = $request->input('ano');
//        } else {
//            $mes = [ date("m")];
//            $ano = date("Y");
//        }
        if (!$request->input('ano') && !$request->input('mes')) {
            $ano = date("Y");
            $mes = [date("m")];
        } else
        if ($request->input('mes')) {
            $mes = $request->input('mes');
            $ano = $request->input('ano');
        } else {
            $ano = $request->input('ano');
            $mes = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        }
        
        $mesArr = implode(',', $mes);
        //dd($mesArr);

//DB::raw($codigoAxencia . ' as CODIGO_AGENCIA'), DB::raw($ano . $mes . ' as ANO_MES_ENVIO'),
        $usuarioRegistrados = DB::table('user') 
                ->select(DB::raw('UCASE(cif) as ID_TRABAJADOR'), DB::raw('SUBSTRING(UCASE(name),1, 15) as NOMBRE_TRABAJADOR'), DB::raw('SUBSTRING(UCASE(surname),1, 20) as APELLIDO1_TRABAJADOR'), DB::raw('"" as APELLIDO2_TRABAJADOR'), DB::raw('date_format(birth, "%Y%m%d") as FECHA_NACIMIENTO'), DB::raw('IF(genero_fk  = "1", 2, 1) as SEXO_TRABAJADOR'), DB::raw('MAX(IFNULL(nivelformativo,"00")) as NIVEL_FORMATIVO'), DB::raw('IF(user.disability  = "1", "S", "N") as DISCAPACIDAD'), DB::raw('IF(user.country_fk   = "ES", "N", "S") as INMIGRANTE'), DB::raw('"N" as COLOCACION'), DB::raw('"" as FECHA_COLOCACION'), DB::raw('"" as TIPO_CONTRATO'), DB::raw('"" as CIF_NIF_EMPRESA'), DB::raw('"" as RAZON_SOCIAL_EMPRESA'))
                ->distinct()
                ->leftJoin('user_especialidad as ue', 'ue.user_id', '=', 'user.pk')
                ->leftJoin('specialty as s', 's.id', '=', 'ue.especialidad_id')
                ->leftJoin('grade as g', 'g.id', '=', 's.grade_id')
                ->whereRaw('YEAR(user.created_at) = ?', $ano)
                ->WhereIn(DB::raw('MONTH(user.created_at)'), $mes)
                ->where('user.role_id', '1')
                ->where('user.cif', '!=', '')
                ->groupBy('cif', 'name', 'surname', 'birth', 'genero_fk', 'user.disability', 'user.country_fk');
        


//DB::raw($codigoAxencia . ' as CODIGO_AGENCIA'), DB::raw($ano . $mes . ' as ANO_MES_ENVIO'), 
        $usuarioColocados = DB::table('user')
                        ->select(DB::raw('UCASE(cif) as ID_TRABAJADOR'), DB::raw('SUBSTRING(UCASE(name),1, 15) as NOMBRE_TRABAJADOR'), DB::raw('SUBSTRING(UCASE(surname),1, 20) as APELLIDO1_TRABAJADOR'), DB::raw('"" as APELLIDO2_TRABAJADOR'), DB::raw('date_format(birth, "%Y%m%d") as FECHA_NACIMIENTO'), DB::raw('IF(genero_fk  = "1", 2, 1) as SEXO_TRABAJADOR'), DB::raw('MAX(IFNULL(nivelformativo,"00")) as NIVEL_FORMATIVO'), DB::raw('IF(user.disability  = "1", "S", "N") as DISCAPACIDAD'), DB::raw('IF(user.country_fk   = "ES", "N", "S") as INMIGRANTE'), DB::raw('IF(cv.contratado = "1", "S", "N") as COLOCACION'), DB::raw('date_format(cv.data_contrato, "%Y%m%d") as FECHA_COLOCACION'), 'cv.tipo_contrato as TIPO_CONTRATO', 'cv.cif_empresa as CIF_NIF_EMPRESA', DB::raw('SUBSTRING(UCASE(cv.empresa),1, 55) as RAZON_SOCIAL_EMPRESA'))
                        ->leftJoin('user_especialidad as ue', 'ue.user_id', '=', 'user.pk')
                        ->leftJoin('specialty as s', 's.id', '=', 'ue.especialidad_id')
                        ->leftJoin('grade as g', 'g.id', '=', 's.grade_id')
                        ->join('cv', 'user.pk', '=', 'cv.user_fk')
                        ->where('cv.contratado', '1')
                        ->WhereIn(DB::raw('MONTH(cv.data_contrato)'), $mes)
                        ->whereRaw('YEAR(cv.data_contrato) = ?', $ano)
                        ->where('role_id', '1')
                        ->where('user.cif', '!=', '')
                        ->groupBy('cif', 'name', 'surname', 'birth', 'genero_fk', 'user.disability', 'user.country_fk', 'cv.contratado', 'cv.data_contrato', 'cv.tipo_contrato', 'cv.cif_empresa', 'cv.empresa')
                        ->union($usuarioRegistrados)->distinct()->orderBy('NOMBRE_TRABAJADOR','ASC')->orderBy('APELLIDO1_TRABAJADOR','ASC')->get();

        //  TOTAL_PERSONAS : total personas que enviaron curriculos en el mes      
        $totalPersonas = DB::table('cv')
                        ->WhereIn(DB::raw('MONTH(cv.created)'), $mes)
                        ->whereRaw('YEAR(cv.created) = ?', $ano)->count(DB::raw('DISTINCT user_fk'));



        //  TOTAL_NUEVAS_RESGITRADAS: total usuarios que se registraron en el mes      
        $totalNuevasRegistradas = DB::table('user')
                ->WhereIn(DB::raw('MONTH(user.created_at)'), $mes)
                ->whereRaw('YEAR(user.created_at) = ?', $ano)
                ->where('user.role_id', '1')
                ->where('user.cif', '!=', '')
                ->count();

 
      

        $totalPersonasPerceptores = DB::select('select  count(*) as valor
                                                        from user
                                                        WHERE pk in (select distinct(user_fk) from cv where YEAR(cv.created)=' . $ano . ' and MONTH(cv.created) in(' . $mesArr . ')) '
                        . ' and prestacion=1');
        
        $totalPersonasPerceptores = $totalPersonasPerceptores[0]->valor;
        
   
        



        //TOTAL_PERSONAS_INSERCION
        $totalPersonasInsercion = DB::table('cv')
                ->join('user', 'user.pk', '=', 'cv.user_fk')
                ->WhereIn(DB::raw('MONTH(cv.created)'), $mes)
                ->whereRaw('YEAR(cv.created) = ?', $ano)
                ->where('user.role_id', '1')
                ->Where(function($query) {
                    $query->Where('user.genero_fk', '!=', '2')
                    ->orWhere('user.disability', '1')
//                        ->orWhere('user.prestacion', '1')
                    ->orWhereRaw(' ( YEAR(CURDATE())-YEAR(user.birth) > 29 and YEAR(CURDATE())-YEAR(user.birth) < 46 )')
                    ->orWhere('user.country_fk', '!=', "ES");
                })
                ->count(DB::raw('distinct cv.user_fk'));

//                ->whereRaw(' ( YEAR(CURDATE())-YEAR(user.birth) > 29 and YEAR(CURDATE())-YEAR(user.birth) < 46 )')
//                ->orWhere('user.disability', 1)
//                ->orWhere('user.country_fk', "ES")
//                ->count();
//                
//                
//                 ->whereIn('id', function($query)
//    {
//        $query->select(DB::raw(1))
//              ->from('orders')
//              ->whereRaw('orders.user_id = users.id');
//    })
//                
        //TOTAL_OFERTAS: Ofertas regsitradas en el mes
                


        $totalOfertas = DB::table('job_offer')
                ->WhereIn(DB::raw('MONTH(job_offer.created_at)'), $mes)
                ->whereRaw('YEAR(job_offer.created_at) = ?', $ano)
                ->count();


        //TOTAL_OFERTAS_ENVIADAS

//        $totalOfertaEnviadas = DB::table('cv')
//                        ->WhereIn(DB::raw('MONTH(cv.created)'), $mes)
//                        ->whereRaw('YEAR(cv.created) = ?', $ano)
//                        ->where('cv.cv_enviado', '1')->count(DB::raw('cv.job_fk'));

        $totalOfertaEnviadas =  DB::select("select  sum(cvs.cuantos) as valor "
        . "from (select user_fk, count(user_fk) as cuantos "
                . "from cv where YEAR(cv.created)= $ano and MONTH(cv.created) in( $mesArr ) and cv_enviado = 1 group by user_fk) as cvs" );       
                      
        $totalOfertaEnviadas = $totalOfertaEnviadas[0]->valor?$totalOfertaEnviadas[0]->valor:0;
        
        


//TOTAL_OFERTAS_CUBIERTAS 
        $totalOfertasCubiertas = DB::table('job_offer')
                ->WhereIn(DB::raw('MONTH(job_offer.created_at)'), $mes)
                ->whereRaw('YEAR(job_offer.created_at) = ?', $ano)
                ->where('seleccion_rematada', '1')
                ->where('expediente_rematado', '1')
                ->count();

//TOTAL_PUESTOS
        $totalPuestos = DB::table('job_offer')
                ->WhereIn(DB::raw('MONTH(job_offer.created_at)'), $mes)
                ->whereRaw('YEAR(job_offer.created_at) = ?', $ano)
                ->sum('job_offer.vacancy');



//TOTAL_PUESTOS_CUBIERTOS

//        $totalPuestosCubiertos = DB::table('job_offer')
//                ->WhereIn(DB::raw('MONTH(job_offer.created_at)'), $mes)
//                ->whereRaw('YEAR(job_offer.created_at) = ?', $ano)
//                ->sum('job_offer.puestos_cubiertos');
        
        
        
        
        $totalPuestosCubiertos = DB::select('Select count(*) as valor '
                . 'from job_offer '
                . 'inner join cv on cv.job_fk = job_offer.id '
                
                . 'where YEAR(job_offer.created_at)=' . $ano . ' and MONTH(job_offer.created_at) in(' . $mesArr . ') and cv.contratado = 1 and not cv.data_contrato is null');
        $totalPuestosCubiertos = $totalPuestosCubiertos[0]->valor;
        
        
        
//TOTAL_CONTRATOS
        $totalContratos = DB::table('cv')
                        ->WhereIn(DB::raw('MONTH(cv.data_contrato)'), $mes)
                        ->whereRaw('YEAR(cv.data_contrato) = ?', $ano)
                        ->where('cv.contratado', '1')->count();
        
      



//TOTAL_CONTRATOS_INDEFINIDOS    
        $totalContratosIndefinidos = DB::table('cv')
                ->WhereIn(DB::raw('MONTH(cv.data_contrato)'), $mes)
                ->whereRaw('YEAR(cv.data_contrato) = ?', $ano)
                ->where('cv.contratado', '1')
                ->where('cv.tipo_contrato', '<=', '003')
                ->count();
//TOTAL_PERSONAS_COLOCADAS
        $totalPersonasColocadas = DB::table('cv')
                        ->WhereIn(DB::raw('MONTH(cv.data_contrato)'), $mes)
                        ->whereRaw('YEAR(cv.data_contrato) = ?', $ano)
                        ->where('cv.contratado', '1')->count();




        return view('job_offer.estadisticas_sepe')->with(['UserStats' => $usuarioColocados, 'ano' => $ano, 'mes' => $mes,
                    'totalPersonas' => $totalPersonas,
                    'totalNuevasRegistradas' => $totalNuevasRegistradas,
                    'totalPersonasPerceptores' => $totalPersonasPerceptores,
                    'totalPersonasInsercion' => $totalPersonasInsercion,
                    'totalOfertas' => $totalOfertas,
                    'totalOfertaEnviadas' => $totalOfertaEnviadas,
                    'totalOfertasCubiertas' => $totalOfertasCubiertas,
                    'totalPuestos' => $totalPuestos,
                    'totalPuestosCubiertos' => $totalPuestosCubiertos,
                    'totalContratos' => $totalContratos,
                    'totalContratosIndefinidos' => $totalContratosIndefinidos,
                    'totalPersonasColocadas' => $totalPersonasColocadas,
                    'ano' => $ano
        ]);
    }

//  `NIVEL_FORMATIVO` varchar(255) DEFAULT NULL,
//  `DISCAPACIDAD` varchar(255) DEFAULT NULL,
//  `INMIGRANTE` varchar(255) DEFAULT NULL,
//  `COLOCACION` varchar(255) DEFAULT NULL,
//  `FECHA_COLOCACION` varchar(255) DEFAULT NULL,
//  `TIPO_CONTRATO` varchar(255) DEFAULT NULL,
//  `CIF_NIF_EMPRESA` varchar(255) DEFAULT NULL,
//  `RAZON_SOCIAL_EMPRESA` varchar(255) DEFAULT NULL


    public function actualizarRematado(Request $request) {


        $id = $request->input('id');
        $campo = $request->input('campo');
        $valor = $request->input('valor') == 'true' ? 1 : 0;

        //dd($id.'-'.$campo.'-'.$valor);

        $oferta = JobOffer::find($id);

        if ($campo == 'expediente_rematado') {
            $oferta->expediente_rematado = $valor;
        }
        if ($campo == 'seleccion_rematada') {
            $oferta->seleccion_rematada = $valor;
        }
        // dd($oferta->seleccion_rematada.'-'.$oferta->expediente_rematado);
        $oferta->update();



        return response()->json([
                    'state' => 'success',
                    'data' => json_encode(['seleccion_rematada' => $oferta->seleccion_rematada, 'expediente_rematado' => $oferta->expediente_rematado]),
        ]);
    }

    public function contratosPorEdad(Request $request) {




        if (!$request->input('ano') && !$request->input('mes')) {
            $ano = date("Y");
            $mes = [date("m")];
        } else
        if ($request->input('mes')) {
            $mes = $request->input('mes');
            $ano = $request->input('ano');
        } else {
            $ano = $request->input('ano');
            $mes = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        }



        $totalContratosEdad = DB::table('user')
                ->selectRaw("SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 15 AND 20 THEN 1 ELSE 0 END) AS '15-20', " .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 21 AND 30 THEN 1 ELSE 0 END) AS '21-30'," .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 31 AND 40 THEN 1 ELSE 0 END) AS '31-40'," .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 41 AND 50 THEN 1 ELSE 0 END) AS '41-50'," .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 51 AND 60 THEN 1 ELSE 0 END) AS '51-60'," .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 61 AND 65 THEN 1 ELSE 0 END) AS '61-65'"
                )
                ->join('cv', 'user.pk', '=', 'cv.user_fk')
                ->where('cv.contratado', '1')
                ->where('user.role_id', '1')
                //->WhereIn(DB::raw('MONTH(cv.data_contrato)'), $mes)
                ->whereRaw('YEAR(cv.data_contrato) = ?', $ano)
                ->where('user.cif', '!=', '')
                ->first();



        //return response()->json(['cantidad' => $notificaciones->count(), 'lista' => $notificaciones->toJson()]);
        return response()->json([
                    'totalContratosEdad' => $totalContratosEdad
        ]);
    }

    public function renderGrafico(Request $request) {

        $vista = $request->input('ruta');
        
        $ano = $request->input('ano');




        if (View::exists($vista)) {
            //
            $ViewContents = view($vista, ['ano' => $ano])->render();
        } else
            $ViewContents = "<<< Gráfico no encontrado >>>";





        return $ViewContents;
    }

    public function contratoPorGenero(Request $request) {




        if (!$request->input('ano') && !$request->input('mes')) {
            $ano = date("Y");
            $mes = [date("m")];
        } else
        if ($request->input('mes')) {
            $mes = $request->input('mes');
            $ano = $request->input('ano');
        } else {
            $ano = $request->input('ano');
            $mes = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        }



        $totalContratosGenero = DB::table('user')
                ->selectRaw("CASE WHEN user.genero_fk=1 THEN 'MASCULINO' else 'FEMENINO' END AS name, count(*) total"
                )
                ->join('cv', 'user.pk', '=', 'cv.user_fk')
                ->where('cv.contratado', '1')
                //->WhereIn(DB::raw('MONTH(cv.data_contrato)'), $mes)
                ->whereRaw('YEAR(cv.data_contrato) = ?', $ano)
                ->where('user.cif', '!=', '')
                ->where('user.role_id', '1')
                ->groupBy(DB::raw("CASE WHEN user.genero_fk=1 THEN 'MASCULINO' else 'FEMENINO'  END"))
                ->get();


        //return response()->json(['cantidad' => $notificaciones->count(), 'lista' => $notificaciones->toJson()]);
        return response()->json([
                    'totalContratosGenero' => $totalContratosGenero
        ]);
    }

    public function contratoPorFormacion(Request $request) {

        if (!$request->input('ano') && !$request->input('mes')) {
            $ano = date("Y");
            $mes = [date("m")];
        } else
        if ($request->input('mes')) {
            $mes = $request->input('mes');
            $ano = $request->input('ano');
        } else {
            $ano = $request->input('ano');
            $mes = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        }

        $totalContratosFormacion = DB::select("select nivelFormativo.titulacion, count(DISTINCT nivelFormativo.pk) total
                                                from (
                                                SELECT `user`.pk, grade.titulacion  as titulacion
                                                From `user`
                                                inner join user_especialidad on user_especialidad.user_id = `user`.pk
                                                inner join specialty on specialty.id = user_especialidad.especialidad_id
                                                inner join grade on grade.id = specialty.grade_id



                                                Inner Join cv on `user`.pk = cv.user_fk

                                                Where YEAR(cv.data_contrato) = $ano
                                                and cv.contratado

                                                ) as nivelFormativo

                                                GROUP BY nivelFormativo.titulacion");


        //return response()->json(['cantidad' => $notificaciones->count(), 'lista' => $notificaciones->toJson()]);
        return response()->json([
                    'totalContratosFormacion' => $totalContratosFormacion
        ]);
    }

    public function curriculosPorEdad(Request $request) {




        if (!$request->input('ano') && !$request->input('mes')) {
            $ano = date("Y");
            $mes = [date("m")];
        } else
        if ($request->input('mes')) {
            $mes = $request->input('mes');
            $ano = $request->input('ano');
        } else {
            $ano = $request->input('ano');
            $mes = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        }



        $totalCurriculosEdad = DB::table('user')
                ->selectRaw("SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 15 AND 20 THEN 1 ELSE 0 END) AS '15-20', " .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 21 AND 30 THEN 1 ELSE 0 END) AS '21-30'," .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 31 AND 40 THEN 1 ELSE 0 END) AS '31-40'," .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 41 AND 50 THEN 1 ELSE 0 END) AS '41-50'," .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 51 AND 60 THEN 1 ELSE 0 END) AS '51-60'," .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 61 AND 65 THEN 1 ELSE 0 END) AS '61-65'"
                )
                ->join('cv', 'user.pk', '=', 'cv.user_fk')
                ->where('cv.cv_enviado', '=', '1')
                ->where('user.role_id', '1')
                //->WhereIn(DB::raw('MONTH(cv.data_contrato)'), $mes)
                ->whereRaw('YEAR(cv.created) = ?', $ano)
                ->where('user.cif', '!=', '')
                ->first();

//dd($totalCurriculosEdad);
        //return response()->json(['cantidad' => $notificaciones->count(), 'lista' => $notificaciones->toJson()]);
        return response()->json([
                    'totalCurriculosEdad' => $totalCurriculosEdad
        ]);
    }

    public function curriculosPorGenero(Request $request) {




        if (!$request->input('ano') && !$request->input('mes')) {
            $ano = date("Y");
            $mes = [date("m")];
        } else
        if ($request->input('mes')) {
            $mes = $request->input('mes');
            $ano = $request->input('ano');
        } else {
            $ano = $request->input('ano');
            $mes = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        }



        $totalCurriculosGenero = DB::table('user')
                ->selectRaw("CASE WHEN user.genero_fk=1 THEN 'MASCULINO' else 'FEMENINO' END AS name, count(*) total"
                )
                ->join('cv', 'user.pk', '=', 'cv.user_fk')
                //->WhereIn(DB::raw('MONTH(cv.data_contrato)'), $mes)
                ->whereRaw('YEAR(cv.created) = ?', $ano)
                ->where('cv.cv_enviado', '=', '1')
                ->where('user.cif', '!=', '')
                ->where('user.role_id', '1')
                ->groupBy(DB::raw("CASE WHEN user.genero_fk=1 THEN 'MASCULINO' else 'FEMENINO' END"))
                ->get();


        //return response()->json(['cantidad' => $notificaciones->count(), 'lista' => $notificaciones->toJson()]);
        return response()->json([
                    'totalCurriculosGenero' => $totalCurriculosGenero
        ]);
    }

    public function curriculosPorFormacion(Request $request) {

        if (!$request->input('ano') && !$request->input('mes')) {
            $ano = date("Y");
            $mes = [date("m")];
        } else
        if ($request->input('mes')) {
            $mes = $request->input('mes');
            $ano = $request->input('ano');
        } else {
            $ano = $request->input('ano');
            $mes = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        }

        $totalCurriculosFormacion = DB::select("select nivelFormativo.titulacion, count(DISTINCT nivelFormativo.pk) total
                                                from (
                                                SELECT `user`.pk, grade.titulacion  as titulacion
                                                From `user`
                                                inner join user_especialidad on user_especialidad.user_id = `user`.pk
                                                inner join specialty on specialty.id = user_especialidad.especialidad_id
                                                inner join grade on grade.id = specialty.grade_id



                                                Inner Join cv on `user`.pk = cv.user_fk

                                                Where YEAR(cv.created) = $ano
                                                
                                                and cv.cv_enviado
                                                ) as nivelFormativo

                                                GROUP BY nivelFormativo.titulacion");


        //return response()->json(['cantidad' => $notificaciones->count(), 'lista' => $notificaciones->toJson()]);
        return response()->json([
                    'totalCurriculosFormacion' => $totalCurriculosFormacion
        ]);
    }

    public function curriculosPrestacionPorEdad(Request $request) {




        if (!$request->input('ano') && !$request->input('mes')) {
            $ano = date("Y");
            $mes = [date("m")];
        } else
        if ($request->input('mes')) {
            $mes = $request->input('mes');
            $ano = $request->input('ano');
        } else {
            $ano = $request->input('ano');
            $mes = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        }



        $totalCurriculosEdad = DB::table('user')
                ->selectRaw("SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 15 AND 20 THEN 1 ELSE 0 END) AS '15-20', " .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 21 AND 30 THEN 1 ELSE 0 END) AS '21-30'," .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 31 AND 40 THEN 1 ELSE 0 END) AS '31-40'," .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 41 AND 50 THEN 1 ELSE 0 END) AS '41-50'," .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 51 AND 60 THEN 1 ELSE 0 END) AS '51-60'," .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 61 AND 65 THEN 1 ELSE 0 END) AS '61-65'"
                )
                ->join('cv', 'user.pk', '=', 'cv.user_fk')
                ->where('cv.cv_enviado', '=', '1')
                ->where('user.prestacion', '=', '1')
                ->where('user.role_id', '1')
                //->WhereIn(DB::raw('MONTH(cv.data_contrato)'), $mes)
                ->whereRaw('YEAR(cv.created) = ?', $ano)
                ->where('user.cif', '!=', '')
                ->first();

//dd($totalCurriculosEdad);
        //return response()->json(['cantidad' => $notificaciones->count(), 'lista' => $notificaciones->toJson()]);
        return response()->json([
                    'totalCurriculosEdad' => $totalCurriculosEdad
        ]);
    }

    public function distribucionAltasPorEdad(Request $request) {




        if (!$request->input('ano') && !$request->input('mes')) {
            $ano = date("Y");
            $mes = [date("m")];
        } else
        if ($request->input('mes')) {
            $mes = $request->input('mes');
            $ano = $request->input('ano');
        } else {
            $ano = $request->input('ano');
            $mes = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        }



        $distribucionAltasPorEdad = DB::table('user')
                ->selectRaw("SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 15 AND 20 THEN 1 ELSE 0 END) AS '15-20', " .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 21 AND 30 THEN 1 ELSE 0 END) AS '21-30'," .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 31 AND 40 THEN 1 ELSE 0 END) AS '31-40'," .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 41 AND 50 THEN 1 ELSE 0 END) AS '41-50'," .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 51 AND 60 THEN 1 ELSE 0 END) AS '51-60'," .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 61 AND 65 THEN 1 ELSE 0 END) AS '61-65'"
                )

                //->WhereIn(DB::raw('MONTH(cv.data_contrato)'), $mes)
                ->whereRaw('YEAR(user.created_at) = ?', $ano)
                ->where('user.cif', '!=', '')
                ->where('user.role_id', '1')
                ->where('user.cif','!=','')
                
                ->first();

//dd($totalCurriculosEdad);
        //return response()->json(['cantidad' => $notificaciones->count(), 'lista' => $notificaciones->toJson()]);
        return response()->json([
                    'distribucionAltasPorEdad' => $distribucionAltasPorEdad
        ]);
    }

    public function distribucionAltasPorFormacion(Request $request) {

        if (!$request->input('ano') && !$request->input('mes')) {
            $ano = date("Y");
            $mes = [date("m")];
        } else
        if ($request->input('mes')) {
            $mes = $request->input('mes');
            $ano = $request->input('ano');
        } else {
            $ano = $request->input('ano');
            $mes = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        }

        $totalDistribucionAltasFormacion = DB::select("select nivelFormativo.titulacion, count(DISTINCT nivelFormativo.pk) total
                                                from (
                                                SELECT `user`.pk, grade.titulacion  as titulacion
                                                From `user`
                                                inner join user_especialidad on user_especialidad.user_id = `user`.pk
                                                inner join specialty on specialty.id = user_especialidad.especialidad_id
                                                inner join grade on grade.id = specialty.grade_id



                                                Where YEAR(user.created_at) = $ano and user.role_id = 1 and user.cif != ''
                                                
                                               
                                                ) as nivelFormativo

                                                GROUP BY nivelFormativo.titulacion");


        //return response()->json(['cantidad' => $notificaciones->count(), 'lista' => $notificaciones->toJson()]);
        return response()->json([
                    'distribucionAltasFormacion' => $totalDistribucionAltasFormacion
        ]);
    }

    public function distribucionAltasPorGenero(Request $request) {




        if (!$request->input('ano') && !$request->input('mes')) {
            $ano = date("Y");
            $mes = [date("m")];
        } else
        if ($request->input('mes')) {
            $mes = $request->input('mes');
            $ano = $request->input('ano');
        } else {
            $ano = $request->input('ano');
            $mes = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        }



        $distribucionAltasGenero = DB::table('user')
                ->selectRaw("CASE WHEN user.genero_fk=1 THEN 'MASCULINO' else 'FEMENINO' END AS name, count(*) total"
                )
                //->WhereIn(DB::raw('MONTH(cv.data_contrato)'), $mes)
                ->whereRaw('YEAR(user.created_at) = ?', $ano)
                ->where('user.cif', '!=', '')
                ->where('user.role_id', '1')
                
                ->groupBy(DB::raw("CASE WHEN user.genero_fk=1 THEN 'MASCULINO' else 'FEMENINO' END"))
                ->get();


        //return response()->json(['cantidad' => $notificaciones->count(), 'lista' => $notificaciones->toJson()]);
        return response()->json([
                    'distribucionAltasGenero' => $distribucionAltasGenero
        ]);
    }

    public function evolucionMensualAltas(Request $request) {
        if (!$request->input('ano') && !$request->input('mes')) {
            $ano = date("Y");
            $mes = [date("m")];
        } else
        if ($request->input('mes')) {
            $mes = $request->input('mes');
            $ano = $request->input('ano');
        } else {
            $ano = $request->input('ano');
            $mes = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        }



        $evolucionMensualAltas = DB::select(DB::raw("SELECT 1 mes,
                                            SUM(CASE WHEN MONTH(user.created_at) = 1 THEN 1 ELSE 0 END) AS total
                                            FROM user
                                            WHERE YEAR(user.created_at)=$ano
                                            AND MONTH(user.created_at) = 1
                                            AND user.role_id = 1
                                            AND user.cif !=''
                                            UNION
                                            SELECT 2 mes,
                                            SUM(CASE WHEN MONTH(user.created_at) = 2 THEN 1 ELSE 0 END) AS total
                                            FROM user
                                            WHERE YEAR(user.created_at)=$ano
                                            AND MONTH(user.created_at) = 2
                                            AND user.role_id = 1
                                             AND user.cif !=''
                                            UNION
                                            SELECT 3 mes,
                                            SUM(CASE WHEN MONTH(user.created_at) = 3 THEN 1 ELSE 0 END) AS total
                                            FROM user
                                            WHERE YEAR(user.created_at)=$ano
                                            AND MONTH(user.created_at) = 3
                                            AND user.role_id = 1
                                             AND user.cif !=''

                                            UNION
                                            SELECT 4 mes,
                                            SUM(CASE WHEN MONTH(user.created_at) = 4 THEN 1 ELSE 0 END) AS total
                                            FROM user
                                            WHERE YEAR(user.created_at)=$ano
                                            AND MONTH(user.created_at) = 4
                                            AND user.role_id = 1
                                             AND user.cif !=''
                                            UNION
                                            SELECT 5 mes,
                                            SUM(CASE WHEN MONTH(user.created_at) = 5 THEN 1 ELSE 0 END) AS total
                                            FROM user
                                            WHERE YEAR(user.created_at)=$ano
                                            AND MONTH(user.created_at) = 5
                                            AND user.role_id = 1
                                             AND user.cif !=''
                                            UNION
                                            SELECT 6 mes,
                                            SUM(CASE WHEN MONTH(user.created_at) = 6 THEN 1 ELSE 0 END) AS total
                                            FROM user
                                            WHERE YEAR(user.created_at)=$ano
                                            AND MONTH(user.created_at) = 6
                                            AND user.role_id = 1
                                             AND user.cif !=''
                                            UNION
                                            SELECT 7 mes,
                                            SUM(CASE WHEN MONTH(user.created_at) = 7 THEN 1 ELSE 0 END) AS total
                                            FROM user
                                            WHERE YEAR(user.created_at)=$ano
                                            AND MONTH(user.created_at) = 7
                                            AND user.role_id = 1
                                             AND user.cif !=''
                                            UNION
                                            SELECT 8 mes,
                                            SUM(CASE WHEN MONTH(user.created_at) = 8 THEN 1 ELSE 0 END) AS total
                                            FROM user
                                            WHERE YEAR(user.created_at)=$ano
                                            AND MONTH(user.created_at) = 8
                                            AND user.role_id = 1
                                             AND user.cif !=''
                                            UNION
                                            SELECT 9 mes,
                                            SUM(CASE WHEN MONTH(user.created_at) = 9 THEN 1 ELSE 0 END) AS total
                                            FROM user
                                            WHERE YEAR(user.created_at)=$ano
                                            AND MONTH(user.created_at) = 9
                                            AND user.role_id = 1
                                             AND user.cif !=''
                                            UNION
                                            SELECT 10 mes,
                                            SUM(CASE WHEN MONTH(user.created_at) = 10 THEN 1 ELSE 0 END) AS total
                                            FROM user
                                            WHERE YEAR(user.created_at)=$ano
                                            AND MONTH(user.created_at) = 10
                                            AND user.role_id = 1
                                             AND user.cif !=''
                                            UNION
                                            SELECT 11 mes,
                                            SUM(CASE WHEN MONTH(user.created_at) = 11 THEN 1 ELSE 0 END) AS total
                                            FROM user
                                            WHERE YEAR(user.created_at)=$ano
                                            AND MONTH(user.created_at) = 11
                                            AND user.role_id = 1
                                            AND user.cif !=''
                                            UNION
                                            SELECT 12 mes,
                                            SUM(CASE WHEN MONTH(user.created_at) = 12 THEN 1 ELSE 0 END) AS total
                                            FROM user
                                            WHERE YEAR(user.created_at)=$ano
                                            AND MONTH(user.created_at) = 12 AND user.role_id = 1  AND user.cif !='' "));


        //return response()->json(['cantidad' => $notificaciones->count(), 'lista' => $notificaciones->toJson()]);
        return response()->json([
                    'evolucionMensualAltas' => $evolucionMensualAltas
        ]);
    }

    public function evolucionMensualContratos(Request $request) {
        if (!$request->input('ano') && !$request->input('mes')) {
            $ano = date("Y");
            $mes = [date("m")];
        } else
        if ($request->input('mes')) {
            $mes = $request->input('mes');
            $ano = $request->input('ano');
        } else {
            $ano = $request->input('ano');
            $mes = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        }



        $evolucionMensualContratos = DB::select(DB::raw("SELECT 1 mes,
                                            SUM(CASE WHEN MONTH(cv.data_contrato) = 1 THEN 1 ELSE 0 END) AS total
                                            FROM cv
                                            WHERE YEAR(cv.data_contrato)=$ano
                                            AND MONTH(cv.data_contrato) = 1
                                            UNION
                                            SELECT 2 mes,
                                            SUM(CASE WHEN MONTH(cv.data_contrato) = 2 THEN 1 ELSE 0 END) AS total
                                            FROM cv
                                            WHERE YEAR(cv.data_contrato)=$ano
                                            AND MONTH(cv.data_contrato) = 2
                                            UNION
                                            SELECT 3 mes,
                                            SUM(CASE WHEN MONTH(cv.data_contrato) = 3 THEN 1 ELSE 0 END) AS total
                                            FROM cv
                                            WHERE YEAR(cv.data_contrato)=$ano
                                            AND MONTH(cv.data_contrato) = 3

                                            UNION
                                            SELECT 4 mes,
                                            SUM(CASE WHEN MONTH(cv.data_contrato) = 4 THEN 1 ELSE 0 END) AS total
                                            FROM cv
                                            WHERE YEAR(cv.data_contrato)=$ano
                                            AND MONTH(cv.data_contrato) = 4
                                            UNION
                                            SELECT 5 mes,
                                            SUM(CASE WHEN MONTH(cv.data_contrato) = 5 THEN 1 ELSE 0 END) AS total
                                            FROM cv
                                            WHERE YEAR(cv.data_contrato)=$ano
                                            AND MONTH(cv.data_contrato) = 5
                                            UNION
                                            SELECT 6 mes,
                                            SUM(CASE WHEN MONTH(cv.data_contrato) = 6 THEN 1 ELSE 0 END) AS total
                                            FROM cv
                                            WHERE YEAR(cv.data_contrato)=$ano
                                            AND MONTH(cv.data_contrato) = 6
                                            UNION
                                            SELECT 7 mes,
                                            SUM(CASE WHEN MONTH(cv.data_contrato) = 7 THEN 1 ELSE 0 END) AS total
                                            FROM cv
                                            WHERE YEAR(cv.data_contrato)=$ano
                                            AND MONTH(cv.data_contrato) = 7
                                            UNION
                                            SELECT 8 mes,
                                            SUM(CASE WHEN MONTH(cv.data_contrato) = 8 THEN 1 ELSE 0 END) AS total
                                            FROM cv
                                            WHERE YEAR(cv.data_contrato)=$ano
                                            AND MONTH(cv.data_contrato) = 8
                                            UNION
                                            SELECT 9 mes,
                                            SUM(CASE WHEN MONTH(cv.data_contrato) = 9 THEN 1 ELSE 0 END) AS total
                                            FROM cv
                                            WHERE YEAR(cv.data_contrato)=$ano
                                            AND MONTH(cv.data_contrato) = 9
                                            UNION
                                            SELECT 10 mes,
                                            SUM(CASE WHEN MONTH(cv.data_contrato) = 10 THEN 1 ELSE 0 END) AS total
                                            FROM cv
                                            WHERE YEAR(cv.data_contrato)=$ano
                                            AND MONTH(cv.data_contrato) = 10
                                            UNION
                                            SELECT 11 mes,
                                            SUM(CASE WHEN MONTH(cv.data_contrato) = 11 THEN 1 ELSE 0 END) AS total
                                            FROM cv
                                            WHERE YEAR(cv.data_contrato)=$ano
                                            AND MONTH(cv.data_contrato) = 11
                                            UNION
                                            SELECT 12 mes,
                                            SUM(CASE WHEN MONTH(cv.data_contrato) = 12 THEN 1 ELSE 0 END) AS total
                                            FROM cv
                                            WHERE YEAR(cv.data_contrato)=$ano
                                            AND MONTH(cv.data_contrato) = 12"));


        //return response()->json(['cantidad' => $notificaciones->count(), 'lista' => $notificaciones->toJson()]);
        return response()->json([
                    'evolucionMensualContratos' => $evolucionMensualContratos
        ]);
    }

    public function evolucionMensualCursos(Request $request) {
        if (!$request->input('ano') && !$request->input('mes')) {
            $ano = date("Y");
            $mes = [date("m")];
        } else
        if ($request->input('mes')) {
            $mes = $request->input('mes');
            $ano = $request->input('ano');
        } else {
            $ano = $request->input('ano');
            $mes = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        }



        $evolucionMensualCursos = DB::select(DB::raw("SELECT 1 mes,
                                            SUM(CASE WHEN MONTH(cursos.created_at) = 1 THEN 1 ELSE 0 END) AS total
                                            FROM cursos
                                            WHERE YEAR(cursos.created_at)=$ano
                                            AND MONTH(cursos.created_at) = 1
                                            UNION
                                            SELECT 2 mes,
                                            SUM(CASE WHEN MONTH(cursos.created_at) = 2 THEN 1 ELSE 0 END) AS total
                                            FROM cursos
                                            WHERE YEAR(cursos.created_at)=$ano
                                            AND MONTH(cursos.created_at) = 2
                                            UNION
                                            SELECT 3 mes,
                                            SUM(CASE WHEN MONTH(cursos.created_at) = 3 THEN 1 ELSE 0 END) AS total
                                            FROM cursos
                                            WHERE YEAR(cursos.created_at)=$ano
                                            AND MONTH(cursos.created_at) = 3

                                            UNION
                                            SELECT 4 mes,
                                            SUM(CASE WHEN MONTH(cursos.created_at) = 4 THEN 1 ELSE 0 END) AS total
                                            FROM cursos
                                            WHERE YEAR(cursos.created_at)=$ano
                                            AND MONTH(cursos.created_at) = 4
                                            UNION
                                            SELECT 5 mes,
                                            SUM(CASE WHEN MONTH(cursos.created_at) = 5 THEN 1 ELSE 0 END) AS total
                                            FROM cursos
                                            WHERE YEAR(cursos.created_at)=$ano
                                            AND MONTH(cursos.created_at) = 5
                                            UNION
                                            SELECT 6 mes,
                                            SUM(CASE WHEN MONTH(cursos.created_at) = 6 THEN 1 ELSE 0 END) AS total
                                            FROM cursos
                                            WHERE YEAR(cursos.created_at)=$ano
                                            AND MONTH(cursos.created_at) = 6
                                            UNION
                                            SELECT 7 mes,
                                            SUM(CASE WHEN MONTH(cursos.created_at) = 7 THEN 1 ELSE 0 END) AS total
                                            FROM cursos
                                            WHERE YEAR(cursos.created_at)=$ano
                                            AND MONTH(cursos.created_at) = 7
                                            UNION
                                            SELECT 8 mes,
                                            SUM(CASE WHEN MONTH(cursos.created_at) = 8 THEN 1 ELSE 0 END) AS total
                                            FROM cursos
                                            WHERE YEAR(cursos.created_at)=$ano
                                            AND MONTH(cursos.created_at) = 8
                                            UNION
                                            SELECT 9 mes,
                                            SUM(CASE WHEN MONTH(cursos.created_at) = 9 THEN 1 ELSE 0 END) AS total
                                            FROM cursos
                                            WHERE YEAR(cursos.created_at)=$ano
                                            AND MONTH(cursos.created_at) = 9
                                            UNION
                                            SELECT 10 mes,
                                            SUM(CASE WHEN MONTH(cursos.created_at) = 10 THEN 1 ELSE 0 END) AS total
                                            FROM cursos
                                            WHERE YEAR(cursos.created_at)=$ano
                                            AND MONTH(cursos.created_at) = 10
                                            UNION
                                            SELECT 11 mes,
                                            SUM(CASE WHEN MONTH(cursos.created_at) = 11 THEN 1 ELSE 0 END) AS total
                                            FROM cursos
                                            WHERE YEAR(cursos.created_at)=$ano
                                            AND MONTH(cursos.created_at) = 11
                                            UNION
                                            SELECT 12 mes,
                                            SUM(CASE WHEN MONTH(cursos.created_at) = 12 THEN 1 ELSE 0 END) AS total
                                            FROM cursos
                                            WHERE YEAR(cursos.created_at)=$ano
                                            AND MONTH(cursos.created_at) = 12"));


        //return response()->json(['cantidad' => $notificaciones->count(), 'lista' => $notificaciones->toJson()]);
        return response()->json([
                    'evolucionMensualCursos' => $evolucionMensualCursos
        ]);
    }

    public function evolucionMensualMatricula(Request $request) {
        if (!$request->input('ano') && !$request->input('mes')) {
            $ano = date("Y");
            $mes = [date("m")];
        } else
        if ($request->input('mes')) {
            $mes = $request->input('mes');
            $ano = $request->input('ano');
        } else {
            $ano = $request->input('ano');
            $mes = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        }



        $evolucionMensualMatricula = DB::select(DB::raw("SELECT 1 mes,
                                            SUM(CASE WHEN MONTH(curso_user.fechainscripcion) = 1 THEN 1 ELSE 0 END) AS total
                                            FROM curso_user
                                            WHERE YEAR(curso_user.fechainscripcion)=$ano
                                            AND MONTH(curso_user.fechainscripcion) = 1
                                            UNION
                                            SELECT 2 mes,
                                            SUM(CASE WHEN MONTH(curso_user.fechainscripcion) = 2 THEN 1 ELSE 0 END) AS total
                                            FROM curso_user
                                            WHERE YEAR(curso_user.fechainscripcion)=$ano
                                            AND MONTH(curso_user.fechainscripcion) = 2
                                            UNION
                                            SELECT 3 mes,
                                            SUM(CASE WHEN MONTH(curso_user.fechainscripcion) = 3 THEN 1 ELSE 0 END) AS total
                                            FROM curso_user
                                            WHERE YEAR(curso_user.fechainscripcion)=$ano
                                            AND MONTH(curso_user.fechainscripcion) = 3

                                            UNION
                                            SELECT 4 mes,
                                            SUM(CASE WHEN MONTH(curso_user.fechainscripcion) = 4 THEN 1 ELSE 0 END) AS total
                                            FROM curso_user
                                            WHERE YEAR(curso_user.fechainscripcion)=$ano
                                            AND MONTH(curso_user.fechainscripcion) = 4
                                            UNION
                                            SELECT 5 mes,
                                            SUM(CASE WHEN MONTH(curso_user.fechainscripcion) = 5 THEN 1 ELSE 0 END) AS total
                                            FROM curso_user
                                            WHERE YEAR(curso_user.fechainscripcion)=$ano
                                            AND MONTH(curso_user.fechainscripcion) = 5
                                            UNION
                                            SELECT 6 mes,
                                            SUM(CASE WHEN MONTH(curso_user.fechainscripcion) = 6 THEN 1 ELSE 0 END) AS total
                                            FROM curso_user
                                            WHERE YEAR(curso_user.fechainscripcion)=$ano
                                            AND MONTH(curso_user.fechainscripcion) = 6
                                            UNION
                                            SELECT 7 mes,
                                            SUM(CASE WHEN MONTH(curso_user.fechainscripcion) = 7 THEN 1 ELSE 0 END) AS total
                                            FROM curso_user
                                            WHERE YEAR(curso_user.fechainscripcion)=$ano
                                            AND MONTH(curso_user.fechainscripcion) = 7
                                            UNION
                                            SELECT 8 mes,
                                            SUM(CASE WHEN MONTH(curso_user.fechainscripcion) = 8 THEN 1 ELSE 0 END) AS total
                                            FROM curso_user
                                            WHERE YEAR(curso_user.fechainscripcion)=$ano
                                            AND MONTH(curso_user.fechainscripcion) = 8
                                            UNION
                                            SELECT 9 mes,
                                            SUM(CASE WHEN MONTH(curso_user.fechainscripcion) = 9 THEN 1 ELSE 0 END) AS total
                                            FROM curso_user
                                            WHERE YEAR(curso_user.fechainscripcion)=$ano
                                            AND MONTH(curso_user.fechainscripcion) = 9
                                            UNION
                                            SELECT 10 mes,
                                            SUM(CASE WHEN MONTH(curso_user.fechainscripcion) = 10 THEN 1 ELSE 0 END) AS total
                                            FROM curso_user
                                            WHERE YEAR(curso_user.fechainscripcion)=$ano
                                            AND MONTH(curso_user.fechainscripcion) = 10
                                            UNION
                                            SELECT 11 mes,
                                            SUM(CASE WHEN MONTH(curso_user.fechainscripcion) = 11 THEN 1 ELSE 0 END) AS total
                                            FROM curso_user
                                            WHERE YEAR(curso_user.fechainscripcion)=$ano
                                            AND MONTH(curso_user.fechainscripcion) = 11
                                            UNION
                                            SELECT 12 mes,
                                            SUM(CASE WHEN MONTH(curso_user.fechainscripcion) = 12 THEN 1 ELSE 0 END) AS total
                                            FROM curso_user
                                            WHERE YEAR(curso_user.fechainscripcion)=$ano
                                            AND MONTH(curso_user.fechainscripcion) = 12"));


        //return response()->json(['cantidad' => $notificaciones->count(), 'lista' => $notificaciones->toJson()]);
        return response()->json([
                    'evolucionMensualMatricula' => $evolucionMensualMatricula
        ]);
    }

    public function evolucionMensualPuestos(Request $request) {
        if (!$request->input('ano') && !$request->input('mes')) {
            $ano = date("Y");
            $mes = [date("m")];
        } else
        if ($request->input('mes')) {
            $mes = $request->input('mes');
            $ano = $request->input('ano');
        } else {
            $ano = $request->input('ano');
            $mes = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        }



        $evolucionMensualPuestos = DB::select(DB::raw("SELECT 1 mes,
                                            SUM(CASE WHEN MONTH(job_offer.created_at) = 1 THEN vacancy ELSE 0 END) AS total
                                            FROM job_offer
                                            WHERE YEAR(job_offer.created_at)=$ano
                                            AND MONTH(job_offer.created_at) = 1
                                            UNION
                                            SELECT 2 mes,
                                            SUM(CASE WHEN MONTH(job_offer.created_at) = 2 THEN vacancy ELSE 0 END) AS total
                                            FROM job_offer
                                            WHERE YEAR(job_offer.created_at)=$ano
                                            AND MONTH(job_offer.created_at) = 2
                                            UNION
                                            SELECT 3 mes,
                                            SUM(CASE WHEN MONTH(job_offer.created_at) = 3 THEN vacancy ELSE 0 END) AS total
                                            FROM job_offer
                                            WHERE YEAR(job_offer.created_at)=$ano
                                            AND MONTH(job_offer.created_at) = 3

                                            UNION
                                            SELECT 4 mes,
                                            SUM(CASE WHEN MONTH(job_offer.created_at) = 4 THEN vacancy ELSE 0 END) AS total
                                            FROM job_offer
                                            WHERE YEAR(job_offer.created_at)=$ano
                                            AND MONTH(job_offer.created_at) = 4
                                            UNION
                                            SELECT 5 mes,
                                            SUM(CASE WHEN MONTH(job_offer.created_at) = 5 THEN vacancy ELSE 0 END) AS total
                                            FROM job_offer
                                            WHERE YEAR(job_offer.created_at)=$ano
                                            AND MONTH(job_offer.created_at) = 5
                                            UNION
                                            SELECT 6 mes,
                                            SUM(CASE WHEN MONTH(job_offer.created_at) = 6 THEN vacancy ELSE 0 END) AS total
                                            FROM job_offer
                                            WHERE YEAR(job_offer.created_at)=$ano
                                            AND MONTH(job_offer.created_at) = 6
                                            UNION
                                            SELECT 7 mes,
                                            SUM(CASE WHEN MONTH(job_offer.created_at) = 7 THEN vacancy ELSE 0 END) AS total
                                            FROM job_offer
                                            WHERE YEAR(job_offer.created_at)=$ano
                                            AND MONTH(job_offer.created_at) = 7
                                            UNION
                                            SELECT 8 mes,
                                            SUM(CASE WHEN MONTH(job_offer.created_at) = 8 THEN vacancy ELSE 0 END) AS total
                                            FROM job_offer
                                            WHERE YEAR(job_offer.created_at)=$ano
                                            AND MONTH(job_offer.created_at) = 8
                                            UNION
                                            SELECT 9 mes,
                                            SUM(CASE WHEN MONTH(job_offer.created_at) = 9 THEN vacancy ELSE 0 END) AS total
                                            FROM job_offer
                                            WHERE YEAR(job_offer.created_at)=$ano
                                            AND MONTH(job_offer.created_at) = 9
                                            UNION
                                            SELECT 10 mes,
                                            SUM(CASE WHEN MONTH(job_offer.created_at) = 10 THEN vacancy ELSE 0 END) AS total
                                            FROM job_offer
                                            WHERE YEAR(job_offer.created_at)=$ano
                                            AND MONTH(job_offer.created_at) = 10
                                            UNION
                                            SELECT 11 mes,
                                            SUM(CASE WHEN MONTH(job_offer.created_at) = 11 THEN vacancy ELSE 0 END) AS total
                                            FROM job_offer
                                            WHERE YEAR(job_offer.created_at)=$ano
                                            AND MONTH(job_offer.created_at) = 11
                                            UNION
                                            SELECT 12 mes,
                                            SUM(CASE WHEN MONTH(job_offer.created_at) = 12 THEN vacancy ELSE 0 END) AS total
                                            FROM job_offer
                                            WHERE YEAR(job_offer.created_at)=$ano
                                            AND MONTH(job_offer.created_at) = 12"));


        //return response()->json(['cantidad' => $notificaciones->count(), 'lista' => $notificaciones->toJson()]);
        return response()->json([
                    'evolucionMensualPuestos' => $evolucionMensualPuestos
        ]);
    }

    public function inscritosPorEdad(Request $request) {




        if (!$request->input('ano') && !$request->input('mes')) {
            $ano = date("Y");
            $mes = [date("m")];
        } else
        if ($request->input('mes')) {
            $mes = $request->input('mes');
            $ano = $request->input('ano');
        } else {
            $ano = $request->input('ano');
            $mes = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        }



        $totalInscritosEdad = DB::table('user')
                ->selectRaw("SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 15 AND 20 THEN 1 ELSE 0 END) AS '15-20', " .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 21 AND 30 THEN 1 ELSE 0 END) AS '21-30'," .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 31 AND 40 THEN 1 ELSE 0 END) AS '31-40'," .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 41 AND 50 THEN 1 ELSE 0 END) AS '41-50'," .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 51 AND 60 THEN 1 ELSE 0 END) AS '51-60'," .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  BETWEEN 61 AND 65 THEN 1 ELSE 0 END) AS '61-65'," .
                        "SUM(CASE WHEN DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birth)), '%Y')+0  > 65 THEN 1 ELSE 0 END) AS '65 o mas'"
                )
                ->where('user.role_id', '1')
                ->where('user.cif', '!=', '')
                ->first();



        //return response()->json(['cantidad' => $notificaciones->count(), 'lista' => $notificaciones->toJson()]);
        return response()->json([
                    'totalInscritosEdad' => $totalInscritosEdad
        ]);
    }

    public function inscritosPorFormacion(Request $request) {

        if (!$request->input('ano') && !$request->input('mes')) {
            $ano = date("Y");
            $mes = [date("m")];
        } else
        if ($request->input('mes')) {
            $mes = $request->input('mes');
            $ano = $request->input('ano');
        } else {
            $ano = $request->input('ano');
            $mes = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        }

        $totalInscritosFormacion = DB::select("select nivelFormativo.titulacion, count(DISTINCT nivelFormativo.pk) total
                                                from (
                                                SELECT `user`.pk, grade.titulacion  as titulacion
                                                From `user`
                                                inner join user_especialidad on user_especialidad.user_id = `user`.pk
                                                inner join specialty on specialty.id = user_especialidad.especialidad_id
                                                inner join grade on grade.id = specialty.grade_id
                                                

                                                ) as nivelFormativo

                                                GROUP BY nivelFormativo.titulacion");


        //return response()->json(['cantidad' => $notificaciones->count(), 'lista' => $notificaciones->toJson()]);
        return response()->json([
                    'totalInscritosFormacion' => $totalInscritosFormacion
        ]);
    }

    public function inscritosPorGenero(Request $request) {




        if (!$request->input('ano') && !$request->input('mes')) {
            $ano = date("Y");
            $mes = [date("m")];
        } else
        if ($request->input('mes')) {
            $mes = $request->input('mes');
            $ano = $request->input('ano');
        } else {
            $ano = $request->input('ano');
            $mes = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        }

        $distribucionAltasGenero = DB::table('user')
                ->selectRaw("CASE WHEN user.genero_fk=1 THEN 'MASCULINO' else 'FEMENINO' END AS name, count(*) total"
                )
                ->where('user.cif', '!=', '')
                ->where('user.role_id', '1')
                ->groupBy(DB::raw("CASE WHEN user.genero_fk=1 THEN 'MASCULINO' else 'FEMENINO' END"))
                ->get();


        //return response()->json(['cantidad' => $notificaciones->count(), 'lista' => $notificaciones->toJson()]);
        return response()->json([
                    'inscritosGenero' => $distribucionAltasGenero
        ]);
    }

}
