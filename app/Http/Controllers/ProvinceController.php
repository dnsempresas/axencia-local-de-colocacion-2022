<?php

namespace App\Http\Controllers;
use Validator;
use App\Entities\Province;

use Illuminate\Http\Request;

class ProvinceController extends Controller
{
     public function __construct() {
        //$this->middleware('auth');
    }
    public function concellos(Request $request)
    {
        
        $id= $request['id'];
        
        $province = Province::findorFail($id);
        $concellos = $province->concellos()->get();
        //return view('province.concellos')->with(['concellos' => $concellos]);
        return $concellos;
    }
}
