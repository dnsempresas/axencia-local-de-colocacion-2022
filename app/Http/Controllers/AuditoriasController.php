<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Validator;
use App\Entities\Auditoria;
use Illuminate\Http\Request;
use App\Entities\User;

class AuditoriasController extends Controller {

    public function __construct() {
          $this->middleware('auth');
        $this->middleware('admin', ['only' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function getAuditorias($auditorias) {

        $data = array();
        foreach ($auditorias as $auditoria) {

            $nestedData['model_name'] = $auditoria->model_name;
            $nestedData['publishedAt'] = Carbon::createFromFormat('Y-m-d H:i:s', $auditoria->publishedAt)->format('d/m/Y H:i:s');
            $nestedData['usuario_id'] = $auditoria->user_id;
            $usuario = User::find($auditoria->user_id);
            $nestedData['usuario'] = $usuario->getFullNameEmailAttribute();
            $nestedData['cif'] = $usuario->cif;
            //var_dump($nestedData['usuario']);
            //$nestedData['action'] = '<a class="auditoriasdetalles" id='.$auditoria->id.' href=/panel/auditorias/detalle/'.$auditoria->id.'> '. $auditoria->action .'</a>';
            $nestedData['action'] = '<a class="auditoriasdetalles" id=' . $auditoria->id . ' data-toggle="modal" href=""> ' . $auditoria->action . '</a>';

            //<a href="#modaldetalles" >

            $nestedData['original_set'] = $auditoria->original_set;
            $nestedData['changed_set'] = $auditoria->changed_set;
            $data[] = $nestedData;
        }
        return $data;
    }

    public function listAuditorias(Request $request) {
        $draw = $request->get('draw');
        $fields = array('id', 'model_name', 'publishedAt', 'user_id', 'action', 'original_set', 'changed_set');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->input('search.value');
        $colums = $request->get('columns');
         $order = $request->get('order');
         
        $columnIndex = $order[0]['column']; // Column index
        $columnName = $request->get('columns')[$columnIndex]['data']; // Column name
        $columnSortOrder = $order[0]['dir'];
        if ($columnName=='usuario') $columnName='user_id';



        $query = array();
        $params = [];
        if ($colums[0]['search']['value']) {
            $value = $colums[0]['search']['value'];
            array_push($query, 'model_name like ?');
            array_push($params, '%' . $value . '%');
        }



        if ($colums[1]['search']['value'] && $colums[1]['search']['value'] != '-') {
            $value = $colums[1]['search']['value'];
            list($min, $max) = explode('-', $value);

            if (!empty($min) && !empty($max)) {



                $min = Carbon::createFromFormat('d/m/Y', $min)->format('Y-m-d');
                $max = Carbon::createFromFormat('d/m/Y', $max)->format('Y-m-d');


                $sql = 'date(publishedAt) BETWEEN ? AND ?';
                array_push($query, $sql);

                array_push($params, stripslashes($min));

                array_push($params, stripslashes($max));
            } else {
                if (!empty($min)) {

                    $min = Carbon::createFromFormat('d/m/Y', $min)->format('Y-m-d');
                    $sql = 'date(publishedAt) >= ? ';
                    array_push($query, $sql);

                    array_push($params, stripslashes($min));
                } else {

                    $max = Carbon::createFromFormat('d/m/Y', $max)->format('Y-m-d');
                    $sql = 'date(publishedAt) <= ? ';
                    array_push($query, $sql);
                    array_push($params, stripslashes($max));
                }
            }
        }


//        if ($colums[2]['search']['value']) {
//            $value = $colums[2]['search']['value'];
//            array_push($query, 'usuario like ?');
//            array_push($params, '%' . $value . '%');
//        }


        if ($colums[2]['search']['value']) {
            $value = $colums[2]['search']['value'];
            array_push($query, 'action like ?');
            array_push($params, '%' . $value . '%');
        };

        if ($colums[3]['search']['value']) {
            $value = $colums[3]['search']['value'];
            
            $usuarios = User::where('name', 'like', '%' . $value . '%')->orwhere('surname', 'like', '%' . $value . '%')->pluck('pk')->toArray();
            $usuarios= implode(",", $usuarios);
            
            array_push($query, 'user_id IN(' . $usuarios .')');
            //dd($query);
            //array_push($params,  $usuarios);
        };
        

        if (empty($search) && empty($query)) {
            $total_auditorias = Auditoria::count();
            $auditorias = Auditoria::orderBy($columnName,$columnSortOrder)->orderBy('publishedAt', 'DESC')->skip($start)->take($length)->get($fields);
        } else if (!empty($query)) {
            $query = implode(' AND ', $query);
                  

            $total_auditorias = Auditoria::whereRaw($query, $params)->count();
            $auditorias = Auditoria::whereRaw($query, $params)->offset($start)->limit($length)
                            ->orderBy($columnName,$columnSortOrder)->orderBy('publishedAt', 'DESC')->get($fields);
        } else {
            $total_auditorias = Auditoria::where('model_name', 'LIKE', "%{$search}%")
                    ->orWhere('publishedAt', 'LIKE', "%{$search}%")
                    //->orWhere('usuario', 'LIKE', "%{$search}%")
                    ->orWhere('action', 'LIKE', "%{$search}%")
                    ->count();
            $auditorias = Auditoria::where('model_name', 'LIKE', "%{$search}%")
                    ->orWhere('publishedAt', 'LIKE', "%{$search}%")
                    //->orWhere('usuario', 'LIKE', "%{$search}%")
                    ->orWhere('action', 'LIKE', "%{$search}%")
                    ->offset($start)
                    ->limit($length)->orderBy($columnName,$columnSortOrder)
                    ->orderBy('model_name', 'DESC')
                    ->get($fields);
        }
        $auditorias = $this->getAuditorias($auditorias);
        $data = array(
            'draw' => $draw,
            'recordsTotal' => $total_auditorias,
            'recordsFiltered' => $total_auditorias,
            'data' => $auditorias,
        );
        echo json_encode($data);
    }

    public function index(Request $request) {
        $auditorias = Auditoria::orderBy('publishedAt', 'DESC')->paginate(25);
        return view('auditoria.index');
    }

}
