<?php

namespace App\Http\Controllers;

use Validator;
use App\Entities\Disponibilidad;
use Illuminate\Http\Request;

class DisponibilidadController extends Controller {

    public function __construct() {
         $this->middleware('auth');
        $this->middleware('admin', ['only' => ['index','create','edit','remove']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $disponibilidads = Disponibilidad::all();
        $message = 'No hay puestos registradas';
        return view('disponibilidad.index')->with(['disponibilidads' => $disponibilidads, 'message' => $message]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('disponibilidad.create')->with([]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $disponibilidad = new Disponibilidad();
        $disponibilidad->nombre = $request->input('name');
        $disponibilidad->nome = $request->input('name_gl');
        
        $editados = json_encode($disponibilidad->getDirty());
       
        $disponibilidad->save();
        
        \App\Entities\Auditoria::create([
                'model_name' => 'Disponibilidades',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Crear el registro'. $disponibilidad->id. ' ' . $editados,
                'original_set' => $disponibilidad->toJson(),
                'changed_set' => ''
            ]);
        $request->session()->flash('alert-success', 'Disponibilidad creada correctamente');
        return redirect()->route('disponibilidades.index')->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Disponibilidad  $disponibilidad
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $disponibilidad = Disponibilidad::findOrFail($id);
        return view('disponibilidad.edit')->with(['disponibilidad' => $disponibilidad]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Disponibilidad  $disponibilidad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $values = array('nombre', 'nome');
        $disponibilidad=Disponibilidad::find( $id);
        $original = $disponibilidad->toJson();
        $disponibilidad->update($request->all($values));
        $editados= json_encode($request->all($values));
                
        if($original != json_encode($request->all($values))){
        \App\Entities\Auditoria::create([
                'model_name' => 'Disponibilidades',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Actualizó el registro disponibilidad '. $disponibilidad->id. ' '. $editados,
                'original_set' => $original,
                'changed_set' => json_encode($request->all($values))
            ]);
        }
        
        $request->session()->flash('alert-success', 'Disponibilidad editada correctamente');
        return redirect()->route('disponibilidades.index')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Disponibilidad  $disponibilidad
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
        Disponibilidad::destroy($id);
        $request->session()->flash('alert-success', 'Tipo de disponibilidad eliminada correctamente');
        return redirect()->route('disponibilidades.index')->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Disponibilidad  $disponibilidad
     * @return \Illuminate\Http\Response
     */
    public function show(Disponibilidad $puesto) {
        
    }

    public function remove(Request $request, $id) {

        $disponibilidad = Disponibilidad::find($id);
        $offer = $disponibilidad->joboffer()->get();


        if ($offer->count() > 0) {
            $message = 'Imposible eliminar, error de integridad referencial';
            $tipomensaje = 'alert-warning';
        } else {
            Disponibilidad::destroy($id);
            $message = 'Contrato eliminada correctamente';
            $tipomensaje = 'alert-success';

            \App\Entities\Auditoria::create([
                'model_name' => 'Disponibilidades',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Eliminar el registro',
                'original_set' => $disponibilidad->toJson(),
                'changed_set' => ''
            ]);
        }

        $request->session()->flash($tipomensaje, $message);
        return redirect()->route('disponibilidades.index');
    }

}
