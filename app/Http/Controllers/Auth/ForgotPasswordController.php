<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\Password;
use Exception;
use App\Entities\User;

class ForgotPasswordController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Password Reset Controller
      |--------------------------------------------------------------------------
      |
      | This controller is responsible for handling password reset emails and
      | includes a trait which assists in sending these notifications from
      | your application to your users. Feel free to explore this trait.
      |
     */

use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
    }

    public function sendResetLinkEmail(Request $request) {
        $this->validate($request, ['email' => 'required|email']);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
//        $response = $this->broker()->sendResetLink(
//                $request->only('email'), $this->resetNotifier()
//        );
//
//        switch ($response) {
//            case Password::RESET_LINK_SENT:
//                return response()->json([
//                            'success' => true,
//                    'message' => 'enviado'
//                ]);
//
//            case Password::INVALID_USER:
//            default:
//                return response()->json([
//                            'success' => false,
//                            'message' => 'Invalid user'
//                ]);
//        }

        $uservalido = User::Where('email', '=', $request->only('email'))->orderBy('created_at', 'desc')->first();

        if ($uservalido) {
            if ($uservalido->status==1) {
            if ($uservalido->confirmed==0 && $uservalido->role_id==1 ) 
//                ($uservalido->confirmation_code <> '' && $uservalido->confirmation_code <> null) 
                {

                //return redirect()->back()->withErrors(['email' => 'El usuario no ha confirmado su email']);
                
                return redirect()->back()->withInput()->with(['id_reenvio'=>$uservalido->pk, 'mensaje'=>'El usuario no ha confirmado su email']);

            }

            try {
                $response = Password::sendResetLink(
                                $request->only('email'), $this->resetNotifier()
                );
            } catch (Exception $e) {
                return redirect()->back()->withErrors(['email' => trans($e->getMessage())])->withInput();
            }


            switch ($response) {
                case Password::RESET_LINK_SENT:
                    return redirect()->back()->withInput()->with('status', 'Enviamoslle un email a ese enderezo electrónico. Por favor, faga click no enlace do interior do email para cambiala sua contrasinal');

                case Password::INVALID_USER:
                    return redirect()->back()->withInput()->withErrors(['email' => trans($response)]);
            }
            
            } else {
       
                 return redirect()->back()->withInput()->with(['id_reenvio'=>$uservalido->pk, 'mensaje'=>'O usuario debe estar activo para que se lle poida cambiar a contrasinal, faga click no enlace para activarlo ']);

            }
        } else {
            return redirect()->back()->withInput()->withErrors(['email' => 'Email no resgistrado']);
        }
        
        
    }

// overwritte function resetNotifier() on trait SendsPasswordResetEmails
    protected function resetNotifier() {
        return function($token) {
            return new ResetPasswordNotification($token);
        };
    }

}
