<?php

namespace App\Http\Controllers;

use Validator;
use App\Entities\Noticia;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Entities\Categoria;
use Illuminate\Support\Facades\Redirect;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\Tags\Url;

class NoticiaController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('admin', ['only' => ['index', 'create', 'edit', 'remove']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function getNews($news) {
        $data = array();
        foreach ($news as $new) {
            $edit = route('noticias.edit', $new->id);
            $delete = url('/panel/admin/noticias/' . $new->id . '/remove');
            $nestedData['id'] = $new->id;
            $nestedData['titulo'] = $new->titulo;
            $nestedData['antetitulo'] = $new->antetitulo;
            $nestedData['entradilla'] = $new->entradilla;
            $nestedData['noticia'] = $new->noticia;
            $nestedData['fecha_publicacion'] = Carbon::createFromFormat('Y-m-d', $new->fecha_publicacion)->format('d/m/Y');
            $nestedData['fecha_expiracion'] = Carbon::createFromFormat('Y-m-d', $new->fecha_expiracion)->format('d/m/Y');
            $nestedData['options'] = "&emsp;<a href='{$edit}' title='Edit' ><span class='glyphicon glyphicon-edit' ></span></a>
                                          &emsp;<a id='remove_link' href='{$delete}' title='Delete' ><span class='glyphicon glyphicon-remove' style='color:red'></span></a>";

            $data[] = $nestedData;
        }



        return $data;
    }

    public function index(Request $request) {
        $news = Noticia::orderBy('fecha_publicacion', 'DESC');

        $message = 'No hay noticias registradas';
        return view('news.index')->with(['news' => $news, 'message' => $message,]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {


        $categorias = Categoria::all();



        return view('news.create')->with(['categorias' => $categorias]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {



        $noticia = new Noticia();
        $noticia->categoria_fk = $request->input('categoria');
        $noticia->titulo = $request->input('titulo');
        $noticia->antetitulo = $request->input('antetitulo');
        $noticia->entradilla = $request->input('entradilla');
        $noticia->noticia = $request->input('noticia');

        //$noticia->fecha_publicacion = $request->input('fecha_publicacion');

        $noticia->fecha_publicacion = Carbon::createFromFormat('d/m/Y', $request->input('fecha_publicacion'))->format('Y-m-d');


        //$noticia->fecha_expiracion = $request->input('fecha_expiracion');
        $noticia->fecha_expiracion = Carbon::createFromFormat('d/m/Y', $request->input('fecha_expiracion'))->format('Y-m-d');


        $editados = json_encode($noticia->getDirty());

        $noticia->save();


        $imagen_destacada = $request->file('imagen_destacada');
        $imagen_1 = $request->file('imagen_1');
        $imagen_2 = $request->file('imagen_2');
        //$slug = str_slug($request->input('titulo'), '-');
        $slug = $noticia->id;
        $destinationPath = public_path('noticias');

        if ($imagen_destacada) {
            $imagen_destacada_str = $slug . '_imagen_destacada.' . $imagen_destacada->getClientOriginalExtension();

            $imagen_destacada->move($destinationPath, $imagen_destacada_str);

            $noticia->imagen_destacada = $imagen_destacada_str;
        }
        if ($imagen_1) {

            $imagen_1_str = $slug . '_imagen_1.' . $imagen_1->getClientOriginalExtension();
            $imagen_1->move($destinationPath, $imagen_1_str);
            $noticia->imagen1 = $imagen_1_str;
        }

        if ($imagen_2) {
            $imagen_2_str = $slug . '_imagen_2.' . $imagen_2->getClientOriginalExtension();
            $imagen_2->move($destinationPath, $imagen_2_str);

            $noticia->imagen2 = $imagen_2_str;
        }



//        $imagen_destacada_str = $slug . '_imagen_destacada.' . $imagen_destacada->getClientOriginalExtension();
//        $destinationPath = public_path('noticias');
//
//        $imagen_destacada->move($destinationPath, $imagen_destacada_str);
//        
//        $imagen_1 = $request->file('imagen_1');
//        $imagen_1_str = $slug . '_imagen1.' . $imagen_1->getClientOriginalExtension();
//
//
//
//        $destinationPath = public_path('noticias');
//        $imagen_1->move($destinationPath, $imagen_1_str);
//        $imagen_2 = $request->file('imagen_2');
//        $imagen_2_str = $slug . '_imagen2.' . $imagen_2->getClientOriginalExtension();
//
//
//
//        $destinationPath = public_path('noticias');
//        $imagen_2->move($destinationPath, $imagen_2_str);
//        $noticia->imagen_destacada = $imagen_destacada_str;
//        $noticia->imagen1 = $imagen_1_str;
//        $noticia->imagen2 = $imagen_2_str;



        $noticia->update();

        //dd($noticia->fecha_publicacion);
        //dd(Carbon::now()->format('Y-m-d'));

        if (Carbon::now()->format('Y-m-d') >= $noticia->fecha_publicacion && Carbon::now()->format('Y-m-d') <= $noticia->fecha_expiracion) {

            $sitemap = new \SimpleXMLElement('sitemap.xml', 0, true);
            //$sitemap = simplexml_load_file('sitemap.xml'); 
            // Añadimos , elementos y atributo
            $nuevaUrl = $sitemap->addChild('url');
            $nuevaUrl->addChild('loc', $noticia->getSlug());
            $nuevaUrl->addChild('lastmod', str_replace(' ', 'T', Carbon::now()).'+00:00');
            $nuevaUrl->addChild('priority', '0.8');
            //$sitemap->asXML('sitemap.xml');

            $domxml = new \DOMDocument('1.0');
            $domxml->preserveWhiteSpace = false;
            $domxml->formatOutput = true;
            $domxml->loadXML($sitemap->asXML());
            $domxml->save('sitemap.xml');
        }



        \App\Entities\Auditoria::create([
            'model_name' => 'Noticias',
            'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
            'action' => 'Creó el registro noticia ' . $noticia->id . ' ' . $editados,
            'original_set' => $noticia->toJson(),
            'changed_set' => ''
        ]);


        $request->session()->flash('alert-success', 'Noticia creada correctamente');
        return redirect()->route('noticias.index')->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $categorias = Categoria::all();
        $noticia = Noticia::findOrFail($id);
        return view('news.edit')->with(['noticia' => $noticia, 'categorias' => $categorias]);
    }

    public function removeimgd(Request $request, $id, $img) {

        $destinationPath = public_path('noticias');

        $mi_imagen = $destinationPath . '/' . $img;
        if (@getimagesize($mi_imagen)) {

            $noticia = Noticia::find($id);
            $noticia->imagen_destacada = '';
            $noticia->update();
            unlink($mi_imagen);
            $request->session()->flash('alert-success', 'Imagen borrada correctamente');

            \App\Entities\Auditoria::create([
                'model_name' => 'Ofertas de Empleo',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Borró imagen',
                'original_set' => $noticia,
                'changed_set' => ''
            ]);
        } else {
            $request->session()->flash('alert-success', 'Imagen o datos no encontrados');
        }
        return Redirect::back()->withInput();
    }

    public function removeimg1(Request $request, $id, $img) {

        $destinationPath = public_path('noticias');

        $mi_imagen = $destinationPath . '/' . $img;
        if (@getimagesize($mi_imagen)) {

            $noticia = Noticia::find($id);
            $noticia->imagen1 = '';
            $noticia->update();
            unlink($mi_imagen);
            $request->session()->flash('alert-success', 'Imagen borrada correctamente');
        } else {
            $request->session()->flash('alert-success', 'Imagen o datos no encontrados');
        }
        return Redirect::back()->withInput();
    }

    public function removeimg2(Request $request, $id, $img) {

        $destinationPath = public_path('noticias');

        $mi_imagen = $destinationPath . '/' . $img;
        if (@getimagesize($mi_imagen)) {

            $noticia = Noticia::find($id);
            $noticia->imagen2 = '';
            $noticia->update();
            unlink($mi_imagen);
            $request->session()->flash('alert-success', 'Imagen borrada correctamente');
        } else {
            $request->session()->flash('alert-success', 'Imagen o datos no encontrados');
        }
        return Redirect::back()->withInput();
    }

    public function update(Request $request, $id) {
        $values = array('titulo', 'antetitulo', 'entradilla', 'noticia', 'fecha_publicacion', 'fecha_expiracion', 'imagen_destacada', 'imagen1', 'imagen2');
        $noticia = Noticia::find($id);
        $original = $noticia->toJson();
        
        $slugS = $noticia->getSlug();

        $noticia->titulo = $request->input('titulo');
        $noticia->categoria_fk = $request->input('categoria');
        $noticia->antetitulo = $request->input('antetitulo');
        $noticia->entradilla = $request->input('entradilla');
        $noticia->noticia = $request->input('noticia');

        //$noticia->fecha_publicacion = $request->input('fecha_publicacion');

        $noticia->fecha_publicacion = Carbon::createFromFormat('d/m/Y', $request->input('fecha_publicacion'))->format('Y-m-d');


        //$noticia->fecha_expiracion = $request->input('fecha_expiracion');
        $noticia->fecha_expiracion = Carbon::createFromFormat('d/m/Y', $request->input('fecha_expiracion'))->format('Y-m-d');



        $imagen_destacada = $request->file('imagen_destacada');
        $imagen_1 = $request->file('imagen_1');
        $imagen_2 = $request->file('imagen_2');
        //$slug = str_slug($request->input('titulo'), '-');
        $slug = $noticia->id;
        $destinationPath = public_path('noticias');

        if ($imagen_destacada) {
            $imagen_destacada_str = $slug . '_imagen_destacada.' . $imagen_destacada->getClientOriginalExtension();

            $imagen_destacada->move($destinationPath, $imagen_destacada_str);

            $noticia->imagen_destacada = $imagen_destacada_str;
        }
        if ($imagen_1) {

            $imagen_1_str = $slug . '_imagen_1.' . $imagen_1->getClientOriginalExtension();
            $imagen_1->move($destinationPath, $imagen_1_str);
            $noticia->imagen1 = $imagen_1_str;
        }

        if ($imagen_2) {
            $imagen_2_str = $slug . '_imagen_2.' . $imagen_2->getClientOriginalExtension();
            $imagen_2->move($destinationPath, $imagen_2_str);

            $noticia->imagen2 = $imagen_2_str;
        }



//        $imagen_destacada = $request->file('imagen_destacada');
//        
//        $slug = str_slug($request->input('titulo'), '-');
//        $imagen_destacada_str = $slug.'_imagen_destacada.'.$imagen_destacada->getClientOriginalExtension();
//        $destinationPath = public_path('/noticias');
//        $imagen_destacada->move($destinationPath, $imagen_destacada_str);
//        $imagen_1 = $request->file('imagen_1');
//        $imagen_1_str = $slug.'_imagen1.'.$imagen_1->getClientOriginalExtension();
//        $destinationPath = public_path('/noticias');
//        $imagen_1->move($destinationPath, $imagen_1_str);
//        $imagen_2 = $request->file('imagen_2');
//        $imagen_2_str = $slug.'_imagen2.'.$imagen_2->getClientOriginalExtension();
//        $destinationPath = public_path('/noticias');
//        $imagen_2->move($destinationPath, $imagen_2_str);
//        $noticia->imagen_destacada = $slug.'_imagen_destacada.';
//        $noticia->imagen_1 = $slug.'_imagen_1.';
//        $noticia->imagen_2 = $slug.'_imagen_2.';
        $editados = json_encode($noticia->getDirty());

        $noticia->update();


        if (Carbon::now()->format('Y-m-d') >= $noticia->fecha_publicacion && Carbon::now()->format('Y-m-d') <= $noticia->fecha_expiracion) {

            $sitemap = new \SimpleXMLElement('sitemap.xml', 0, true);
            //$sitemap = simplexml_load_file('sitemap.xml');
            $exitesitemap = 0;
            foreach ($sitemap->url as $url) {
                if ((string) $url->loc == $slugS) {
                    $exitesitemap = 1;
                }
            }
            if ($exitesitemap == 0) {
                // Añadimos , elementos y atributo
                $nuevaUrl = $sitemap->addChild('url');
                $nuevaUrl->addChild('loc', $noticia->getSlug());
                $nuevaUrl->addChild('lastmod', str_replace(' ', 'T', Carbon::now()).'+00:00');
                $nuevaUrl->addChild('priority', '0.8');
                //$sitemap->asXML('sitemap.xml');

                $domxml = new \DOMDocument('1.0');
                $domxml->preserveWhiteSpace = false;
                $domxml->formatOutput = true;
                $domxml->loadXML($sitemap->asXML());
                $domxml->save('sitemap.xml');
            }
        }
        else {
            $sitemap = new \SimpleXMLElement('sitemap.xml', 0, true);

                //$sitemap = simplexml_load_file('sitemap.xml');

                $exitesitemap = 0;
                foreach ($sitemap->url as $url) {
                    if ((string) $url->loc == $slugS) {
                        $exitesitemap = 1;
                        $urlRemove = $url;
                        break;
                    }
                }
                if ($exitesitemap == 1) {

                    $domxml = dom_import_simplexml($urlRemove);
                    $domxml->parentNode->removeChild($domxml);

                    $dom = new \DOMDocument('1.0');
                    $dom->preserveWhiteSpace = false;
                    $dom->formatOutput = true;
                    $dom->loadXML($sitemap->asXML());
                    $dom->save('sitemap.xml');
                }
        }



        if ($original != json_encode($request->all($values))) {
            \App\Entities\Auditoria::create([
                'model_name' => 'Noticias',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Actualizó el registro noticia ' . $noticia . ' ' . $editados,
                'original_set' => $original,
                'changed_set' => json_encode($request->all($values))
            ]);
        }

        $request->session()->flash('alert-success', 'Noticia editada correctamente');
        return redirect()->route('noticias.index')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
        Noticia::destroy($id);
        $request->session()->flash('alert-success', 'Noticia eliminado correctamente');
        return redirect()->route('noticias.index')->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function show(Contract $puesto) {
        
    }

    public function remove(Request $request, $id) {
        $noticia = Noticia::find($id);
        $original = $noticia->toJson();
        $noticia->delete();
        \App\Entities\Auditoria::create([
            'model_name' => 'Noticias',
            'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
            'action' => 'Eliminar el registro',
            'original_set' => $noticia->toJson(),
            'changed_set' => ''
        ]);
        
         $sitemap = new \SimpleXMLElement('sitemap.xml', 0, true);

                //$sitemap = simplexml_load_file('sitemap.xml');

                $exitesitemap = 0;
                foreach ($sitemap->url as $url) {
                    if ((string) $url->loc == $noticia->getSlug()) {
                        $exitesitemap = 1;
                        $urlRemove = $url;
                        break;
                    }
                }
                if ($exitesitemap == 1) {

                    $domxml = dom_import_simplexml($urlRemove);
                    $domxml->parentNode->removeChild($domxml);

                    $dom = new \DOMDocument('1.0');
                    $dom->preserveWhiteSpace = false;
                    $dom->formatOutput = true;
                    $dom->loadXML($sitemap->asXML());
                    $dom->save('sitemap.xml');
                }

        $request->session()->flash('alert-success', 'Noticia eliminada correctamente');
        return redirect()->route('noticias.index');
    }

    public function listNews(Request $request) {
        $draw = $request->get('draw');
        $fields = array('id', 'titulo', 'antetitulo', 'entradilla', 'noticia', 'fecha_publicacion', 'fecha_expiracion');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->input('search.value');
        $colums = $request->get('columns');
        $query = array();
        $params = [];
        if ($colums[0]['search']['value']) {
            $value = $colums[0]['search']['value'];
            array_push($query, 'id = ?');
            array_push($params, $value);
        }

        if ($colums[1]['search']['value']) {
            $value = $colums[1]['search']['value'];
            array_push($query, 'titulo like ?');
            array_push($params, '%' . $value . '%');
        }
        if ($colums[2]['search']['value']) {
            $value = $colums[2]['search']['value'];
            array_push($query, 'antetitulo like ?');
            array_push($params, '%' . $value . '%');
        }
        if ($colums[3]['search']['value']) {
            $value = $colums[3]['search']['value'];
            array_push($query, 'entradilla like ?');
            array_push($params, '%' . $value . '%');
        }
        if ($colums[4]['search']['value']) {
            $value = $colums[4]['search']['value'];
            array_push($query, 'noticia like ?');
            array_push($params, '%' . $value . '%');
        }

        if ($colums[5]['search']['value'] && $colums[5]['search']['value'] != '-') {
            $value = $colums[5]['search']['value'];
            list($min, $max) = explode('-', $value);

            if (!empty($min) && !empty($max)) {


                $min = Carbon::createFromFormat('d/m/Y', $min)->format('Y-m-d');
                $max = Carbon::createFromFormat('d/m/Y', $max)->format('Y-m-d');


                $sql = 'date(fecha_publicacion) BETWEEN ? AND ?';
                array_push($query, $sql);

                array_push($params, stripslashes($min));

                array_push($params, stripslashes($max));
            } else {
                if (!empty($min)) {
                    $min = Carbon::createFromFormat('d/m/Y', $min)->format('Y-m-d');
                    $sql = 'date(fecha_publicacion) >= ? ';
                    array_push($query, $sql);

                    array_push($params, stripslashes($min));
                } else {
                    $max = Carbon::createFromFormat('d/m/Y', $max)->format('Y-m-d');
                    $sql = 'date(fecha_publicacion) <= ? ';
                    array_push($query, $sql);
                    array_push($params, stripslashes($max));
                }
            }
        }



        if ($colums[6]['search']['value'] && $colums[6]['search']['value'] != '-') {
            $value = $colums[6]['search']['value'];
            list($min, $max) = explode('-', $value);
            if (!empty($min) && !empty($max)) {

                $min = Carbon::createFromFormat('d/m/Y', $min)->format('Y-m-d');
                $max = Carbon::createFromFormat('d/m/Y', $max)->format('Y-m-d');

                $sql = ' date(fecha_expiracion) BETWEEN ? AND ?';
                array_push($query, $sql);
                array_push($params, stripslashes($min));

                array_push($params, stripslashes($max));
            } else {
                if (!empty($min)) {
                    $min = Carbon::createFromFormat('d/m/Y', $min)->format('Y-m-d');
                    $sql = 'date(fecha_expiracion) >= ? ';
                    array_push($query, $sql);

                    array_push($params, stripslashes($min));
                } else {
                    $max = Carbon::createFromFormat('d/m/Y', $max)->format('Y-m-d');
                    $sql = 'date(fecha_expiracion) <= ? ';
                    array_push($query, $sql);
                    array_push($params, stripslashes($max));
                }
            }
        }


        if (empty($search) && empty($query)) {
            $total_news = Noticia::count();
            $news = Noticia::orderBy('fecha_publicacion', 'DESC')->skip($start)->take($length)->get($fields);
        } else if (!empty($query)) {
            $query = implode(' AND ', $query);

            $total_news = Noticia::whereRaw($query, $params)->count();
            $news = Noticia::whereRaw($query, $params)->offset($start)->limit($length)
                            ->orderBy('fecha_publicacion', 'DESC')->get($fields);
        } else {
            $total_news = Noticia::where('id', 'LIKE', "%{$search}%")
                    ->orWhere('titulo', 'LIKE', "%{$search}%")
                    ->orWhere('antetitulo', 'LIKE', "%{$search}%")
                    //->orWhere('due_date', 'LIKE',"%{$search}%")
                    ->orWhere('entradilla', 'LIKE', "%{$search}%")
                    //->orWhere('start_date', 'LIKE',"%{$search}%")
                    ->orWhere('noticia', 'LIKE', "%{$search}%")
                    ->count();
            $news = Noticia::where('id', 'LIKE', "%{$search}%")
                    ->orWhere('titulo', 'LIKE', "%{$search}%")
                    ->orWhere('antetitulo', 'LIKE', "%{$search}%")
                    //->orWhere('due_date', 'LIKE',"%{$search}%")
                    ->orWhere('entradilla', 'LIKE', "%{$search}%")
                    //->orWhere('start_date', 'LIKE',"%{$search}%")
                    ->offset($start)
                    ->limit($length)
                    ->orderBy('fecha_publicacion', 'DESC')
                    ->get($fields);
        }
        $news = $this->getNews($news);
        $data = array(
            'draw' => $draw,
            'recordsTotal' => $total_news,
            'recordsFiltered' => $total_news,
            'data' => $news,
        );
        echo json_encode($data);
    }

}
