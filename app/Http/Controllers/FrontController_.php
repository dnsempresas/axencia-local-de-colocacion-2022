<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use \App\Entities\Noticia;
use \App\Entities\JobOffer;
use App\Entities\Curso;
use App\Entities\Concello;
use App\Entities\Sector;
use App\Entities\User;
use App\Entities\Grade;
use App\Entities\Puesto;
use App\Entities\Genero;
use App\Entities\Language;
use App\Entities\Province;
use App\Entities\Boletin;
use App\Entities\DrivingPermit;
use Illuminate\Pagination\Paginator;


use \App\Entities\Banner;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;


class FrontController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $ofertas = JobOffer::latest()->where('publicada', '=', '2')->take(4)->get();
        $banners = Banner::OrderBy('fecha_publicacion', 'DESC')->get();
        //$contracts = Contract::all();

        $cursos = Curso::latest()->where('estatus', '=', '1')->take(4)->get();
        return view('frontend.index')->with(['ofertas' => $ofertas, 'cursos' => $cursos, 'banners'=>$banners]);
        //return view('frontend.index1')->with(['ofertas'=>$ofertas ]);
    }

    public function editarPerfil() {



        $grades = Grade::all();
        $jobPosts = Puesto::all();
        $positions = Puesto::all();
        $langs = Language::all();
        $sectors = Sector::all();
        $provinces = Province::all();
        $user = Auth::user();
        $permits = DrivingPermit::all();
        $generos = Genero::all();


        if ($user->driving_permit != '')
            $user_permit = $user->driving_permit;
        else
            $user_permit = '';

        if ($user->sector != '') {

            $user_sector = $user->sector;
        } else
            $user_sector = '';


        if ($user->province_fk)
            $province = Province::findorFail($user->province_fk);
        else
            $province = 0;



        $concellos = Concello::Where('province_pk', $user->province_fk)->get();
        $user_concello = $user->concello_fk;

//        if ($user->specialty != '') {
//            $specialities = \App\Entities\Specialty::whereIn('id', explode('/', $user->specialty))->get();
//            //$user_specialty = $user->specialty;
//        } else
//            $specialities = [];

        $specialities = $user->especialidades()->get();

        $userpuestos = $user->experiencias()->get();
        $experiencias = [];
        foreach ($userpuestos as $userpuesto) {

            array_push($experiencias, $userpuesto->id);
        }
        $userpuestos = $experiencias;



        $usersectores = $user->sectoresInteres()->get();
        $sectoresinteres = [];
        foreach ($usersectores as $usersector) {

            array_push($sectoresinteres, $usersector->id);
        }
        $usersectores = $sectoresinteres;

        $userpositions = $user->puestosInteres()->get();
        $puestosinteres = [];
        foreach ($userpositions as $userposition) {

            array_push($puestosinteres, $userposition->id);
        }
        $userpositions = $puestosinteres;

        $anchor = 'ancla';

        if (($user->role_id == '1') || ($user->role_id == '3')) {
            return view('frontend.editarperfil')->with(['specialities' => $specialities, 'user_concello' => $user_concello, 'generos' => $generos, 'concellos' => $concellos, 'usersectores' => $usersectores, 'user_permit' => $user_permit, 'permits' => $permits, 'user' => $user, 'provinces' => $provinces, 'grades' => $grades, 'jobPosts' => $jobPosts, 'langs' => $langs, 'sectors' => $sectors, 'userpuestos' => $userpuestos, 'usersectores' => $usersectores, 'userposition' => $userpositions, 'anchor' => $anchor]);
        } else {
            return view('enterprise.perfil')->with(['user_concello' => $user_concello, 'concellos' => $concellos, 'user' => $user, 'provinces' => $provinces]);
        }
    }

    public function ofertas(Request $request) {
        
        $provincias = $request->get('provincias');
        $perPage = $request->get('pageSize') ? $request->get('pageSize') : 10;
        $page = $request->get('page') ? $request->get('page') : 1;
        
        if ($provincias) {
            if ($provincias != 'otras') {
                $ofertas = JobOffer::where('publicada', '=', '2')->where('province_fk', '=', $provincias)->orderBy('created_at', 'DESC');
            } else {
                $ofertas = JobOffer::where('publicada', '=', '2')->whereNotIn('province_fk', [15, 27, 32, 36])->orderBy('created_at', 'DESC');
            }
        } else {
            $ofertas = JobOffer::where('publicada', '=', '2')->orderBy('created_at', 'DESC');
        }
        
        if ($perPage >= count($ofertas->get())) $page=1;
         Paginator::currentPageResolver(function () use ($page) {
        return $page;
    });
        
$ofertas = $ofertas->paginate($perPage);

        //$contracts = Contract::all();
        //$cursos = Curso::latest()->take(4)->get();
        return view('frontend.ofertas')->with(['ofertas' => $ofertas, 'pageSize' => $perPage, 'filtro' => $provincias]);
        //return view('frontend.index1')->with(['ofertas'=>$ofertas ]);
    }
    
    public function buscarOfertas(Request $request) {
        
           
        $search = $request->input('buscarOferta');
        
        $perPage = $request->get('pageSize') ? $request->get('pageSize') : 10;
        
        
        
        $ofertas = JobOffer::Where(function ($ofertas) use ($search) { 
                    
                    $ofertas->where('requested_job', 'LIKE', "%{$search}%")
                    ->orWhere('id', 'LIKE', "%{$search}%")
                    ->orWhere('propietario_fk', 'LIKE', "%{$search}%")
                    ->orWhere('vacancy', 'LIKE', "%{$search}%")
                    ->orWhere('tasks', 'LIKE', "%{$search}%")
                    ->orWhere('contract_type', 'LIKE', "%{$search}%")
                    ->orWhere('duration', 'LIKE', "%{$search}%")
                    ->orWhere('locality', 'LIKE', "%{$search}%")
                    ->orWhere('hours', 'LIKE', "%{$search}%")
                    ->orWhere('salary', 'LIKE', "%{$search}%")
                    ->orWhere('requirements', 'LIKE', "%{$search}%");
                }
                
                )->where('publicada', '=', '2')
                 ->orderBy('created_at', 'DESC')->paginate($perPage);
                
                
               

        //$contracts = Contract::all();
        //$cursos = Curso::latest()->take(4)->get();
        return view('frontend.ofertas-buscar')->with(['ofertas' => $ofertas, 'pageSize' => $perPage]);
        //return view('frontend.index1')->with(['ofertas'=>$ofertas ]);
    }
    
    public function ofertasEmpresa(Request $request, $slug) {
        
        $id = substr($slug,1, strpos($slug,'emp')-1);
        
        $perPage = $request->get('pageSize') ? $request->get('pageSize') : 10;
        
        $empresa= user::find($id);
       
        
        $ofertas = JobOffer::where('publicada', '=', '2')
                   ->where('propietario_fk','=',$id)
                 ->where('pertenece_fie','=',1)
                 ->orderBy('created_at', 'DESC')->paginate($perPage);
                
                
               

        //$contracts = Contract::all();
        //$cursos = Curso::latest()->take(4)->get();
        return view('frontend.ofertas-empresa')->with(['ofertas' => $ofertas, 'empresa' => $empresa,'pageSize' => $perPage]);
        //return view('frontend.index1')->with(['ofertas'=>$ofertas ]);
    }
    
    

    public function cursos(Request $request) {
        $fecha_inicio = $request->get('fecha_inicio');
        $fecha_fin = $request->get('fecha_fin');

        $perPage = $request->get('pageSize') ? $request->get('pageSize') : 10;
       // $cursos = Curso::where('estatus', '=', '1')->where('plazas_disponibles', '>', '0');
         $cursos = Curso::where('estatus', '=', '1');

        if (($fecha_inicio) || ($fecha_fin)) {
            if (!empty($fecha_inicio) && !empty($fecha_fin)) {

                $min = Carbon::createFromFormat('d/m/Y', $fecha_inicio)->format('Y-m-d');
                $max = Carbon::createFromFormat('d/m/Y', $fecha_fin)->format('Y-m-d');
                $cursos = $cursos->whereBetween('fecha_inicio', [$min, $max]);
            } else {
                if (!empty($fecha_inicio)) {

                    $min = Carbon::createFromFormat('d/m/Y', $fecha_inicio)->format('Y-m-d');
                    $cursos = $cursos->where('fecha_inicio', '>=', $min);
                } else {

                    $max = Carbon::createFromFormat('d/m/Y', $fecha_fin)->format('Y-m-d');
                    $cursos = $cursos->where('fecha_inicio', '<=', $max);
                }
            }
        }
        $cursos = $cursos->orderBy('created_at', 'DESC')->paginate($perPage);

        //$contracts = Contract::all();
        //$cursos = Curso::latest()->take(4)->get();
        return view('frontend.cursos')->with(['cursos' => $cursos, 'pageSize' => $perPage,]);
        //return view('frontend.index1')->with(['ofertas'=>$ofertas ]);
    }

    public function cursoFicha($id,$slug) {
        
        $curso = Curso::findOrFail($id);
        $anchor = 'ancla';

        return view('frontend.curso-ficha')->with(['curso' => $curso, 'anchor' => $anchor]);
    }

    public function actualidadeFicha($id) {

        $noticia = Noticia::WhereNotIn('categoria_fk', ['16', '17', '18', '20'])->where('id', '=', $id)->first();
        return view('frontend.noticias-ficha')->with(['noticia' => $noticia]);



        return view('frontend.noticias-ficha')->with(['noticia' => $noticia]);
    }

    public function ofertaFicha($id,$slug) {
        
        $oferta = JobOffer::where('id','=',$id)->where('publicada', '=', '2')->firstOrFail();
        $anchor = 'ancla';

        return view('frontend.oferta-ficha')->with(['oferta' => $oferta, 'anchor' => $anchor]);
    }

    public function pactoporempleo() {
        $noticia = Noticia::where('categoria_fk', '=', '18')->first();
        return view('frontend.pactopolo-ficha')->with(['noticia' => $noticia]);
    }

    public function talentia() {
        $noticia = Noticia::where('categoria_fk', '=', '16')->first();
        return view('frontend.talentia-ficha')->with(['noticia' => $noticia]);
    }

    public function recursos() {
        $noticia = Noticia::where('categoria_fk', '=', '20')->first();
        return view('frontend.recursos-ficha')->with(['noticia' => $noticia]);
    }

    public function axencia() {
        $noticia = Noticia::where('categoria_fk', '=', '13')->first();
        return view('frontend.axencia-ficha')->with(['noticia' => $noticia]);
    }

    public function intermediacionlaboral() {
        $noticia = Noticia::where('categoria_fk', '=', '14')->first();
        return view('frontend.noticias-ficha')->with(['noticia' => $noticia]);
    }

    public function avisolegal() {
        $noticia = Noticia::where('categoria_fk', '=', '15')->first();
        return view('frontend.noticias-ficha')->with(['noticia' => $noticia]);
    }

    public function accesibilidad() {
        $noticia = Noticia::where('categoria_fk', '=', '21')->first();
        return view('frontend.accesibilidad-ficha')->with(['noticia' => $noticia]);
    }

    public function boletines(Request $request) {

        $perPage = $request->get('pageSize') ? $request->get('pageSize') : 10;

        $boletines = Boletin::OrderBy('id', 'DESC')->paginate($perPage);

        return view('frontend.boletines')->with(['boletines' => $boletines, 'pageSize' => $perPage]);
    }

    public function actualidade(Request $request) {
        
         $ano = $request->get('ano');
         $search = $request->get('search');
         
        $result = Noticia::select(DB::raw('YEAR(fecha_publicacion) as year'))->orderBy( 'year', 'DESC')->distinct()->get();
        $years = $result->pluck('year');

        $now = \Carbon\Carbon::now()->toDateTimeString();
        $perPage = $request->get('pageSize') ? $request->get('pageSize') : 10;
        
          if ($ano) {
            $actualidades = Noticia::where('categoria_fk', '=', '23')
                    ->whereYear('fecha_publicacion', '=', $ano)
                    ->orderBy('fecha_publicacion', 'DESC')
                    ->paginate($perPage);
            ;
        } else {
            $actualidades = Noticia::where('categoria_fk', '=', '23')
                    ->where('fecha_publicacion', '<=', $now)
                    ->where('fecha_expiracion', '>=', $now)
                    ->orderBy('fecha_publicacion', 'DESC')
                    ->paginate($perPage);
           
        }
        
        
        
        if ($search) {
             $actualidades = Noticia::where('categoria_fk', '=', '23')
                    ->where(function($query) use ($search) {
                                $query->where('titulo', 'LIKE', "%{$search}%")
                                ->orWhere('antetitulo', 'LIKE', "%{$search}%")
                                ->orWhere('entradilla', 'LIKE', "%{$search}%")
                                ->orWhere('noticia', 'LIKE', "%{$search}%");
                            })
                    ->orderBy('fecha_publicacion', 'DESC')
                    ->paginate($perPage);
            
        }

      


        //$actualidades = Noticia::WhereNotIn('categoria_fk', ['16', '17', '18', '20'])




        return view('frontend.actualidade')->with(['actualidades' => $actualidades, 'pageSize' => $perPage,'anos'=>$years, 'ano'=>$ano]);
        //return view('frontend.index1')->with(['ofertas'=>$ofertas ]);
    }

//    public function consollidadperfil($id) {
//        $usuario = User::find($id);
//
//
//        return view('frontend.oferta-ficha')->with(['oferta' => $oferta]);
//    }

    public function registrarPerfil(Request $request) {
        $tipo = $request->get('tipo') ?  $request->get('tipo'): '2' ;
        $grades = Grade::all();
        $especialidades = \App\Entities\Specialty::all();
        $jobPosts = Puesto::all();
        $langs = Language::all();
        $sectors = Sector::all();
        $provinces = Province::all();
        $permits = DrivingPermit::all();
        $generos = Genero::all();
        return view('frontend.registrarperfil')->with(['tipo' => $tipo, 'permits' => $permits, 'generos' => $generos, 'provinces' => $provinces, 'grades' => $grades, 'especialidades' => $especialidades, 'jobPosts' => $jobPosts, 'langs' => $langs, 'sectors' => $sectors]);
    }
    
      public function errorCode404()

    {

    	return view('errors.404');

    }



    public function errorCode405()

    {

    	return view('errors.405');

    }

}
