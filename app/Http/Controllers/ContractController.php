<?php

namespace App\Http\Controllers;

use Validator;
use App\Entities\Contract;
use Illuminate\Http\Request;

class ContractController extends Controller {

    public function __construct() {
         $this->middleware('auth');
        $this->middleware('admin', ['only' => ['index','create','edit','remove']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $contracts = Contract::all();
        $message = 'No hay puestos registradas';
        return view('contract.index')->with(['contracts' => $contracts, 'message' => $message]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('contract.create')->with([]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $contract = new Contract();
        $contract->nombre = $request->input('name');
        $contract->nome = $request->input('name_gl');
        
         $editados = json_encode($contract->getDirty()); 
        $contract->save();
        
        \App\Entities\Auditoria::create([
                'model_name' => 'Contratos',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Creó el registro Contratos '. $contract->id . ' '. $editados,
                'original_set' => $contract->toJson(),
                'changed_set' => ''
            ]);
        $request->session()->flash('alert-success', 'Contrato creado correctamente');
        return redirect()->route('contratos.index')->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $contract = Contract::findOrFail($id);
        return view('contract.edit')->with(['contract' => $contract]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $values = array('nombre', 'nome');
        $contract=Contract::find( $id);
        $original = $contract->toJson();
        $editados = json_encode($request->all($values));
        $contract->update($request->all($values));
        if($original != json_encode($request->all($values))){
        \App\Entities\Auditoria::create([
                'model_name' => 'Contratos',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Actualizó el registro contract '. $contract->id . ' '.$editados,
                'original_set' => $original,
                'changed_set' => json_encode($request->all($values))
            ]);
        }
        
        $request->session()->flash('alert-success', 'Contrato editado correctamente');
        return redirect()->route('contratos.index')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
        Contract::destroy($id);
        $request->session()->flash('alert-success', 'Contrato eliminado correctamente');
        return redirect()->route('contratos.index')->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function show(Contract $puesto) {
        
    }

    public function remove(Request $request, $id) {

        $contract = Contract::find($id);
        $offer = $contract->joboffer()->get();


        if ($offer->count() > 0) {
            $message = 'Imposible eliminar, error de integridad referencial';
            $tipomensaje = 'alert-warning';
        } else {
            Contract::destroy($id);
            $message = 'Contrato eliminada correctamente';
            $tipomensaje = 'alert-success';

            \App\Entities\Auditoria::create([
                'model_name' => 'Contratos',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Eliminar el registro',
                'original_set' => $contract->toJson(),
                'changed_set' => ''
            ]);
        }

        $request->session()->flash($tipomensaje, $message);
        return redirect()->route('contratos.index');
    }

}
