<?php

namespace App\Http\Controllers;

use Validator;
use App\Entities\Genero;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GeneroController extends Controller {
 public function __construct() {
         $this->middleware('auth');
        $this->middleware('admin', ['only' => ['index','create','edit','remove']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $generos = Genero::all();
        $message = "No hay generos registradas";
        return view('genero.index')->with(['generos' => $generos, 'message' => $message]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('genero.create')->with([]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $genero = new Genero();
        $genero->nombre = $request->input('nombre');
        $genero->nome = $request->input('nome');
        $editados = json_encode($genero->getDirty());
        $genero->save();
        
        \App\Entities\Auditoria::create([
                'model_name' => 'Generos',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Creó el registro Genero '. $genero->id . ' '. $editados,
                'original_set' => $genero->toJson(),
                'changed_set' => ''
            ]);
        
        
        $request->session()->flash('alert-success', 'Genero creado correctamente');
        return redirect()->route('generos.index')->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Genero  $genero
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $genero = Genero::findOrFail($id);
        return view('genero.edit')->with(['genero' => $genero]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Genero  $genero
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        
        $values = array('nombre', 'nome');
        $genero = Genero::find($id);
        $original= $genero->toJson();
        
         $editados = json_encode($request->all($values));

        $genero->update($request->all($values));
        $request->session()->flash('alert-success', 'Genero editado correctamente');
       
        
         if($original != json_encode($request->all($values))){ // changes have been made }
        \App\Entities\Auditoria::create([
            'model_name' => 'Genero',
            'user_id' => Auth::user()->pk,
            'action' => 'Actualizó el registro'. $genero->id .' '. $editados,
            'original_set' => $original,
            'changed_set' => json_encode($request->all($values)),
            ''
        ]);
        }

        return redirect()->route('generos.index')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Genero  $genero
     * @return \Illuminate\Http\Response
     */
    public function remove(Request $request, $id) {
        $genero = Genero::find($id);
        $original= $genero->toJson();
        $genero->delete();
        
         \App\Entities\Auditoria::create([
                'model_name' => 'Generos',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Eliminar el registro',
                'original_set' => $original,
                'changed_set' => ''
            ]);
         
        $request->session()->flash('alert-success', 'Genero eliminado correctamente');
        return redirect()->route('generos.index')->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Genero  $genero
     * @return \Illuminate\Http\Response
     */
    public function show(Genero $genero) {
        
    }

}
