<?php

namespace App\Http\Controllers;

use Validator;
use App\Entities\DrivingPermit;
use Illuminate\Http\Request;

class DrivingPermitController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('admin', ['only' => ['index', 'create', 'edit', 'remove']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $driving_permits = DrivingPermit::all();
        $message = 'No hay permisos de conducir registradas';
        return view('drivingPermit.index')->with(['drivingPermits' => $driving_permits, 'message' => $message]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('drivingPermit.create')->with([]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $driving_permit = new DrivingPermit();
        $driving_permit->code = $request->input('code');
        $driving_permit->description = $request->input('description');
        $driving_permit->nome = $request->input('nome');

        $icono = $request->file('imagen_icono');
        $imagen_str = $driving_permit->code . '.' . strtolower($icono->getClientOriginalExtension());
        $destinationPath = public_path('permisosconducir');
        $driving_permit->icono = $imagen_str;
        $icono->move($destinationPath, $imagen_str);

        $editados = json_encode($driving_permit->getDirty());
        $driving_permit->save();
        \App\Entities\Auditoria::create([
            'model_name' => 'Permiso de Conducir',
            'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
            'action' => 'Crear el registro' . $driving_permit->id . ' ' . $editados,
            'original_set' => $driving_permit->toJson(),
            'changed_set' => ''
        ]);
        $request->session()->flash('alert-success', 'Permiso de conducir creado correctamente');
        return redirect()->route('permisos-conducir.index')->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $drivingPermit = DrivingPermit::findOrFail($id);
        return view('drivingPermit.edit')->with(['drivingPermit' => $drivingPermit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {


        $values = array('code', 'description', 'nome', 'imagen_icono');


        $drivingpermit = DrivingPermit::find($id);

        $original = $drivingpermit->toJson();

        $drivingpermit->code = $request->input('code');
        $drivingpermit->description = $request->input('description');
        $drivingpermit->nome = $request->input('nome');


        $icono = $request->file('imagen_icono');

        $destinationPath = public_path('permisosconducir');

        if ($icono) {
            $imagen_str = $drivingpermit->code . '.' . strtolower($icono->getClientOriginalExtension());

            if (file_exists("$destinationPath.'/'.$imagen_str"))
                unlink("$destinationPath.'/'.$imagen_str");


            $icono->move($destinationPath, $imagen_str);
            $drivingpermit->icono = $imagen_str;
        }


        $drivingpermit->update();



        if ($original != $drivingpermit->toJson()) {
            \App\Entities\Auditoria::create([
                'model_name' => 'Permiso de Conducir',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Actualizó el registro',
                'original_set' => $original,
                'changed_set' => json_encode($request->all($values))
            ]);
        }


        $request->session()->flash('alert-success', 'Permiso de conducir editado correctamente');
        return redirect()->route('permisos-conducir.index')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
        DrivingPermit::destroy($id);
        $request->session()->flash('alert-success', 'Permiso de conducir eliminado correctamente');
        return redirect()->route('permisos-conducir.index')->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function show(Contract $puesto) {
        
    }

    public function remove(Request $request, $id) {


        $drivingpermit = DrivingPermit::find($id);
        $original = $drivingpermit->toJson();


        $drivingpermit->delete();
        $request->session()->flash('alert-success', 'Permiso de conducir eliminada correctamente');

        \App\Entities\Auditoria::create([
            'model_name' => 'Permiso de Conducir',
            'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
            'action' => 'Eliminar el registro',
            'original_set' => $original,
            'changed_set' => ''
        ]);


        return redirect()->route('permisos-conducir.index');
    }

}
