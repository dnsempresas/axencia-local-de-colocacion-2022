<?php

namespace App\Http\Controllers;

use Validator;
use App\Entities\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Entities\JobOffer;
use App\Entities\TipoOferta;
use App\Entities\Contract;
use App\Entities\Concello;
use App\Entities\Sector;
use App\Entities\SectorOferta;
use App\Entities\Province;
use App\Entities\Language;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class EnterpriseController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('admin', ['only' => ['index', 'create', 'edit', 'remove']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function getEnterprises($enterprises) {

        $data = array();

        foreach ($enterprises as $enterprise) {
            $edit = route('empresas.edit', $enterprise->pk);
            $delete = url('/panel/admin/empresas/' . $enterprise->pk . '/remove');
            $nestedData['pk'] = $enterprise->pk;
            $nestedData['surname'] = $enterprise->surname;
            $nestedData['name'] = $enterprise->name;
            //$nestedData['age'] = Carbon::createFromFormat('Y-m-d', $user->birth)->age;
            $nestedData['email'] = $enterprise->email;
            $nestedData['cif'] = $enterprise->cif;
            
            $nestedData['telephone'] = $enterprise->telephone;
            $nestedData['address'] = $enterprise->address;
            $nestedData['contacto'] = $enterprise->contacto;
            $nestedData['options'] = "&emsp;<a href='{$edit}' title='Edit' ><span class='glyphicon glyphicon-edit' ></span></a>
                                          &emsp;<a id='remove_link' href='{$delete}' title='Delete' ><span class='glyphicon glyphicon-remove' style='color:red'></span></a>";
            $nestedData['options'] = $nestedData['options'] . "&emsp;<a class='resetpassword' href='#' title='Cambiar contrasinal' data-value='$enterprise->pk' data-email='$enterprise->email' ><span class='glyphicon glyphicon-lock'></span> </a>";




            if ($enterprise->province_fk) {
                $province = Province::find($enterprise->province_fk);
                $nestedData['province'] = $province->name;
            } else
                $nestedData['province'] = '';


            $concello = Concello::find($enterprise->concello_fk);
            if ($concello)
                $nestedData['concello'] = $concello->name;
            else
                $nestedData['concello'] = '';
            $data[] = $nestedData;
        }
        return $data;
    }

    public function listEnterprises(Request $request) {


        $draw = $request->get('draw');

        $fields = array('name', 'surname', 'email', 'cif', 'telephone', 'contacto', 'pk', 'province_fk', 'concello_fk', 'address');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->input('search.value');
        $colums = $request->get('columns');
        $order = $request->get('order');
        
        $query = array();
        $params = [];
        
        $columnIndex = $order[0]['column']; // Column index
        $columnName = $request->get('columns')[$columnIndex]['data']; // Column name
        $columnSortOrder = $order[0]['dir'];
        
         if ($colums[0]['search']['value']) {
            $value = $colums[0]['search']['value'];
            array_push($query, 'pk like ?');
            array_push($params, '%' . $value . '%');
        }
        
        
        if ($colums[1]['search']['value']) {
            $value = $colums[1]['search']['value'];
            array_push($query, 'name like ?');
            array_push($params, '%' . $value . '%');
        }
        if ($colums[2]['search']['value']) {
            $value = $colums[2]['search']['value'];
            array_push($query, 'surname like ?');
            array_push($params, '%' . $value . '%');
        }

        if ($colums[3]['search']['value']) {
            $value = $colums[3]['search']['value'];
            array_push($query, 'email like ?');
            array_push($params, '%' . $value . '%');
        }
        if ($colums[4]['search']['value']) {
            $value = $colums[4]['search']['value'];
            array_push($query, 'cif like ?');
            array_push($params, '%' . $value . '%');
        }
        if ($colums[5]['search']['value']) {
            
            $value = $colums[5]['search']['value'];
            array_push($query, 'telephone like ?');
            array_push($params, '%' . $value . '%');
        }
        if ($colums[7]['search']['value']) {
            $value = $colums[7]['search']['value'];
            array_push($query, 'address like ?');
            array_push($params, '%' . $value . '%');
        }
        if ($colums[6]['search']['value']) {
            $value = $colums[6]['search']['value'];
            array_push($query, 'contacto like ?');
            array_push($params, '%' . $value . '%');
        }
        if ($colums[9]['search']['value'] && $colums[9]['search']['value'] >= '0') {
            $value = $colums[9]['search']['value'];
            array_push($query, 'province_fk = ?');
            array_push($params, $value);
        }
        if ($colums[10]['search']['value'] && $colums[10]['search']['value'] >= '0') {
            $value = $colums[10]['search']['value'];
            array_push($query, 'concello_fk = ?');
            array_push($params, $value);
        }

        if (empty($search) && empty($query)) {
            $total_users = User::where('role_id', '=', '2')->count();
            $users = User::where('role_id', '=', '2')->orderBy($columnName,$columnSortOrder)->skip($start)->take($length)->get($fields);
        } else if (!empty($query)) {

            $query = implode(' AND ', $query);



            $total_users = User::where('role_id', '=', '2')->whereRaw($query, $params)->count();

            $users = User::where('role_id', '=', '2')->whereRaw($query, $params)->offset($start)->limit($length)
                            ->orderBy($columnName,$columnSortOrder)->get($fields);
        } else {
            $total_users = User::where('role_id', '=', '2')
                            ->where(function($query) use ($search) {
                                $query->where('name', 'LIKE', "%{$search}%")
                                ->orWhere('cif', 'LIKE', "%{$search}%")
                                ->orWhere('pk', 'LIKE', "%{$search}%")
                                ->orWhere('email', 'LIKE', "%{$search}%")
                                ->orWhere('telephone', 'LIKE', "%{$search}%")
                                ->orWhere('contacto', 'LIKE', "%{$search}%")
                                ->orWhere('province_fk', 'LIKE', "%{$search}%")
                                ->orWhere('concello_fk', 'LIKE', "%{$search}%")
                                ->orWhere('address', 'LIKE', "%{$search}%");
                            })->count();
            $users = User::where('role_id', '=', '2')->where(function($query) use ($search) {
                        $query->where('name', 'LIKE', "%{$search}%")
                        ->orWhere('pk', 'LIKE', "%{$search}%")
                        ->orWhere('cif', 'LIKE', "%{$search}%")
                        ->orWhere('email', 'LIKE', "%{$search}%")
                        ->orWhere('telephone', 'LIKE', "%{$search}%")
                        ->orWhere('contacto', 'LIKE', "%{$search}%")
                        ->orWhere('province_fk', 'LIKE', "%{$search}%")
                        ->orWhere('concello_fk', 'LIKE', "%{$search}%")
                        ->orWhere('address', 'LIKE', "%{$search}%");
                    })->offset($start)
                    ->limit($length)
                    ->orderBy($columnName,$columnSortOrder)
                    ->get($fields);
        }
        $users = $this->getEnterprises($users);
        $data = array(
            'draw' => $draw,
            'recordsTotal' => $total_users,
            'recordsFiltered' => $total_users,
            'data' => $users,
        );
        echo json_encode($data);
    }

    public function index(Request $request) {

        $enterprises = User::orderBy('surname', 'ASC')->where('role_id', '=', '2')->paginate(25);
        $provinces = Province::all();
        $message = 'No hay empresas registradas';

        // return view('enterprise.index')->with(['enterprises' => $enterprises,'message'=>$message ]);
        return view('enterprise.index')->with(['provinces' => $provinces]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $langs = Language::all();
        $sectors = Sector::all();
        $provinces = Province::all();
        return view('enterprise.create')->with(['provinces' => $provinces, 'langs' => $langs, 'sectors' => $sectors]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $enterprise = new Enterprise();
        $enterprise->name = $request->input('name');
        $enterprise->social = $request->input('social');
        $enterprise->nif = $request->input('nif');
        $enterprise->contact = $request->input('contact');
        $enterprise->position = $request->input('position');
        $enterprise->phone = $request->input('phone');
        $enterprise->email = $request->input('email');


        $enterprise->approval = $request->input('approval') == true ? 1 : 0;
        $editados = json_encode($enterprise->getDirty());
        $enterprise->save();
        \App\Entities\Auditoria::create([
            'model_name' => 'Empresas',
            'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
            'action' => 'Crear el registro'. $enterprise->pk . ' '. $editados,
            'original_set' => $enterprise->toJson(),
            'changed_set' => ''
        ]);

        $request->session()->flash('alert-success', 'Empresa creada correctamente');
        return redirect()->route('empresas.index')->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $langs = Language::all();
        $sectors = Sector::all();
        $provinces = Province::all();
        $user = User::find($id);
       
        if ($user->sector != '') {

            $user_sector = $user->sector;
        } else
            $user_sector = '';


        $province = Province::find($user->province_fk);

        $concellos = Concello::Where('province_pk', $user->province_fk)->get();
        $user_concello = $user->concello_fk;



//        $show = ($user->study === '1' || $user->study === '2' || $user->study === '3') ? 'none' : 'block';
        // return view('users.edit')->with(['show' => $show, 'user_specialty' => $user_specialty, 'specialties' => $speciaties, 'user_concello' => $user_concello, 'concellos' => $concellos, 'user_sector' => $user_sector, 'user_permit' => $user_permit, 'permits' => $permits, 'user' => $user, 'provinces' => $provinces, 'grades' => $grades, 'jobPosts' => $jobPosts, 'langs' => $langs, 'sectors' => $sectors]);
        return view('enterprise.edit')->with(['user_concello' => $user_concello, 'concellos' => $concellos, 'user_sector' => $user_sector, 'user' => $user, 'provinces' => $provinces, 'langs' => $langs, 'sectors' => $sectors]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //$values = array('name','social','nif','contact','position','phone','email','approval');




        $enterprise = Enterprise::find($id);
        $original = $enterprise->toJson();

        $enterprise->name = $request->input('name');
        $enterprise->social = $request->input('social');
        $enterprise->nif = $request->input('nif');
        $enterprise->contact = $request->input('contact');
        $enterprise->position = $request->input('position');
        $enterprise->phone = $request->input('phone');
        $enterprise->email = $request->input('email');


        $enterprise->approval = $request->input('approval');
        
        $foto = $request->file('foto_perfil');
        if ($foto) {
            
          

            //

            $extension = $foto->getClientOriginalExtension();
            //obtenemos el nombre del archivo
            $nombre = 'Foto_' . $enterprise->cif . '.' . $extension;

            \Storage::disk('public')->delete('/fotos/' . $enterprise->foto);


            //indicamos que queremos guardar un nuevo archivo en el disco local
            \Storage::disk('public')->put('/fotos/' . $nombre, \File::get($foto));


            //$storagePath = \Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();

            $enterprise->foto = $nombre;
        } 
        

        $enterprise->save();
        $request->session()->flash('alert-success', 'Empresa editada correctamente');


        $values = array('name', 'social', 'nif', 'contact', 'position', 'phone', 'email', 'approval');

        if ($original != json_encode($request->all($values))) {
            \App\Entities\Auditoria::create([
                'model_name' => 'Empresas',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Actualizó el registro',
                'original_set' => $original,
                'changed_set' => json_encode($request->all($values))
            ]);
        }


        return redirect()->route('empresas.index')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
//    public function destroy(Request $request, $id) {
//        Enterprise::destroy($id);
//        $request->session()->flash('alert-success', 'Empresa eliminada correctamente');
//        return redirect()->route('empresas.index');
//    }

//    public function remove(Request $request, $id) {
//
//        $enterprise = Enterprise::find($id);
//        $enterprise->delete();
//        \App\Entities\Auditoria::create([
//            'model_name' => 'Empresas',
//            'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
//            'action' => 'Eliminar el registro',
//            'original_set' => $enterprise->toJson(),
//            'changed_set' => ''
//        ]);
//
//        $request->session()->flash('alert-success', 'Empresa eliminada correctamente');
//        return redirect()->route('empresas.index');
//    }

    public function panel() {

        //$enterprise = Enterprise::find($id);
        return view('enterprise.panel');
    }

    public function ofertasEmpresa() {
        $contracts = Contract::all();
        $disponibilidades = \App\Entities\Disponibilidad::all();
        $tipoofertas = TipoOferta::all();
        $provinces = Province::all();
        $sectores = SectorOferta::all();

        $empresas = User::Where('pk', '=', Auth::user()->pk)->get();

        //dd($empresas[0]->name);
        $offers = JobOffer::Where('propietario_fk', '=', Auth::user()->pk)->orderBy('requested_job', 'ASC')->paginate(25);
        return view('enterprise.ofertas.index')->with(['contracts' => $contracts, 'empresas' => $empresas, 'disponibilidades' => $disponibilidades, 'tipoofertas' => $tipoofertas, 'disponibilidades' => $disponibilidades, 'provinces' => $provinces]);
    }

    public function ofertasEmpresaCreate() {
        $contracts = Contract::all();
        $disponibilidades = \App\Entities\Disponibilidad::all();
        $tipoofertas = TipoOferta::all();
        $provinces = Province::all();
        $sectores = SectorOferta::all();
        $empresas = User::Where('role_id', '=', '2')->where('pk', '=', Auth::user()->pk)->get();
        return view('enterprise.ofertas.create')->with(['contracts' => $contracts, 'empresas' => $empresas, 'tipoofertas' => $tipoofertas, 'provinces' => $provinces, 'disponibilidades' => $disponibilidades, 'sectores'=>$sectores]);
    }

    public function ofertasEmpresaedit($id) {
        $contracts = Contract::all();
        $disponibilidades = \App\Entities\Disponibilidad::all();
        $job_offer = JobOffer::findOrFail($id);
        $tipoofertas = TipoOferta::all();
        $provinces = Province::all();
        $sectores = SectorOferta::all();
        $concellos = Concello::Where('province_pk', $job_offer->province_fk)->get();
        $empresas = User::Where('role_id', '=', '2')->where('pk', '=', Auth::user()->pk)->get();


        return view('enterprise.ofertas.edit')->with(['job_offer' => $job_offer, 'contracts' => $contracts, 'empresas' => $empresas, 'tipoofertas' => $tipoofertas, 'provinces' => $provinces, 'concellos' => $concellos, 'disponibilidades' => $disponibilidades,'sectores'=>$sectores]);
    }

    public function perfilEmpresa() {


        $sectors = Sector::all();
        $provinces = Province::all();
        $user = Auth::user();




        if ($user->province_fk)
            $province = Province::findorFail($user->province_fk);


        $concellos = Concello::Where('province_pk', $user->province_fk)->get();
        $user_concello = '';
        if ($user->concello_fk)
            $user_concello = $user->concello_fk;



        return view('enterprise.perfil')->with(['user_concello' => $user_concello, 'concellos' => $concellos, 'user' => $user, 'provinces' => $provinces]);
    }
    
     public function chart1() {

      

//************************************************************* OFERTAS
         
         $user = Auth::user()->pk;
         
         $totalOfertas = DB::table('job_offer')
                        ->select(DB::raw('count(*) as total'))
                        ->where('propietario_fk', '=', Auth::user()->pk)->get();

        $totalOfertasPublicadas = DB::table('job_offer')
                        ->select(DB::raw('count(*) as total'))
                        ->where('propietario_fk', '=', Auth::user()->pk)
                        ->where('publicada', '=', '2')->get();
        

        $totalOfertasNuevas = DB::table('job_offer')
                        ->select(DB::raw('count(*) as total'))
                        ->where('publicada', '=', '2')
                        ->where('propietario_fk', '=', Auth::user()->pk)
                        ->whereDate('created_at', Carbon::today())->get();
        if (!$totalOfertasNuevas)
            $totalOfertasNuevas = 0;

        $totalOfertaContrato = DB::table('job_offer')
                        ->select('contract.nome as name', DB::raw('count(*) as total'))
                        ->join('contract', 'contract.id', '=', 'job_offer.contract_type')
                        ->where('publicada', '=', '2')
                ->where('propietario_fk', '=', Auth::user()->pk)
                        ->groupBy('contract.nome')->get();



        //return response()->json(['cantidad' => $notificaciones->count(), 'lista' => $notificaciones->toJson()]);
        return response()->json([
                   
                    'totalOfertas'=>$totalOfertas,
                    'totalOfertasPublicadas' => $totalOfertasPublicadas,
                    'totalOfertasNuevas' => $totalOfertasNuevas,
                    'totalOfertaContrato' => $totalOfertaContrato,
                    
        ]);
    }
    
    public function ofertasEmpresaRemove(Request $request, $id) {
        
        
        $joboffer = JobOffer::find($id);


        $destinationPath = public_path('offers');

        if ($joboffer) {
            if ($joboffer->imagen) {
                $mi_imagen = $destinationPath . '/' . $joboffer->imagen;
                unlink($mi_imagen);
            }
        }
        $original = $joboffer->toJson();

        if (!$joboffer->postulados()->count() >= 1) {


            $joboffer->delete();

            \App\Entities\Auditoria::create([
                'model_name' => 'Ofertas de Empleo',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Eliminar el registro ID ' . $id,
                'original_set' => $original,
                'changed_set' => ''
            ]);
            

            $sitemap = new \SimpleXMLElement('sitemap.xml', 0, true);

            //$sitemap = simplexml_load_file('sitemap.xml');

            $exitesitemap = 0;
            foreach ($sitemap->url as $url) {
                if ((string) $url->loc == $joboffer->getSlug()) {
                    $exitesitemap = 1;
                    $urlRemove = $url;
                    break;
                }
            }
            if ($exitesitemap == 1) {

                $domxml = dom_import_simplexml($urlRemove);
                $domxml->parentNode->removeChild($domxml);

                $dom = new \DOMDocument('1.0');
                $dom->preserveWhiteSpace = false;
                $dom->formatOutput = true;
                $dom->loadXML($sitemap->asXML());
                $dom->save('sitemap.xml');
            }
            $request->session()->flash('alert-success', 'Oferta eliminada correctamente');
        } else {
            $request->session()->flash('alert-warning', 'No puede eliminar esta oferta');
        }
        //return redirect()->route('/empresas/ofertas');
        return redirect()->action('EnterpriseController@ofertasEmpresa');
        
    }

    
     public function remove(Request $request, $id) {
        $eliminable = true;

        $user = User::find($id);
        $original = $user->toJson();


        if ($user->ofertasPublicadas()->count() >= 1
                or $user->postulaciones()->count() >= 1
                or $user->cursos()->count() >= 1
        ) {

            $eliminable = false;
        }

        if ($eliminable)
            $user->delete();
        else {
            $request->session()->flash('alert-danger', 'Usuario no puede eliminarse');
            return Redirect::back();
        }




        \App\Entities\Auditoria::create([
            'model_name' => 'Usuarios',
            'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
            'action' => 'Eliminar el registro',
            'original_set' => $original,
            'changed_set' => ''
        ]);
        $request->session()->flash('alert-success', 'Empresa eliminada correctamente');
        return Redirect::back();
    }
    
    
}
