<?php

namespace App\Http\Controllers;

use Validator;
use App\Entities\Categoria;
use Illuminate\Http\Request;

class CategoriaController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('admin', ['only' => ['index','create','edit','remove']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $categorias = Categoria::all();
        $message = "No hay categorias registradas";
        return view('categoria.index')->with(['categorias' => $categorias, 'message' => $message]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('categoria.create')->with([]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $categoria = new Categoria();
        $categoria->nombre = $request->input('nombre');
        $categoria->nome = $request->input('nome');
        
         $editados = json_encode($categoria->getDirty());
         
        $categoria->save();

        \App\Entities\Auditoria::create([
            'model_name' => 'Categoria',
            'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
            'action' => 'Creó el registro Categoria '. $categoria->id . ' '. $editados,
            'original_set' => $categoria->toJson(),
            'changed_set' => ''
        ]);


        $request->session()->flash('alert-success', 'Categoria creado correctamente');
        return redirect()->route('categorias.index')->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $categoria = Categoria::findOrFail($id);
        return view('categoria.edit')->with(['categoria' => $categoria]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $values = array('nombre', 'nome');
        $categoria = Categoria::find($id);
        $original = $categoria->toJson();
        
        $editados = json_encode($request->all($values));
        $categoria->update($request->all($values));
        

        if ($original != json_encode($request->all($values))) {
            \App\Entities\Auditoria::create([
                'model_name' => 'Categoria',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Actualizó el registro categoria '. $categoria->id . ' ' . $editados,
                'original_set' => $original,
                'changed_set' => json_encode($request->all($values))
            ]);
        }

        $request->session()->flash('alert-success', 'Categoria editado correctamente');
        return redirect()->route('categorias.index')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function remove(Request $request, $id) {
        $categoria = Categoria::find($id);
        $noticias = $categoria->noticias()->get();

        if ($noticias->count() > 0) {
            $message = 'Imposible eliminar, error de integridad referencial';
            $tipomensaje = 'alert-warning';
        } else {
            $original = $categoria->toJson();
            $categoria->delete();
            \App\Entities\Auditoria::create([
                'model_name' => 'Categoria',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Eliminar el registro',
                'original_set' => $original,
                'changed_set' => ''
            ]);
            $message = 'Categoria eliminada correctamente';
            $tipomensaje = 'alert-success';
        }
        $request->session()->flash($tipomensaje, $message);
        return redirect()->route('categorias.index')->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function show(Categoria $categoria) {
        
    }

}
