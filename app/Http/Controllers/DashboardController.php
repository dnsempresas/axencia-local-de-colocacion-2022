<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DashboardController extends Controller {

    public function __construct() {


        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('dashboard.index');
    }

    public function show() {
        return view('dashboard.index');
    }

    public function notificaciones() {
        $notificaciones = \App\Entities\NotificacionesAdmin::Where('procesado', '=', '0')->get();

        return response()->json(['cantidad' => $notificaciones->count(), 'lista' => $notificaciones->toJson()]);

//        return $notificaciones->toJson();
    }

    public function desmarcarnotificaciones($id) {

        $notificaciones = \App\Entities\NotificacionesAdmin::Where('id', '=', $id)->get();
    }

    public function chart1() {

        $totalUsuarios = DB::table('user')
                        ->select(DB::raw('count(*) as total'))
                        ->where('status', '=', '1')
                        ->where('role_id', '=', '1')->get();

        $totalUsuariosHoy = DB::table('user')
                        ->select(DB::raw('count(*) as total'))
                        
                        ->whereDate('created_at', Carbon::today())
                        ->get();
        
        
        if (!$totalUsuariosHoy)
            $totalUsuariosHoy = 0;

        $totalEmpresas = DB::table('user')
                        ->select(DB::raw('count(*) as total'))
                        ->where('status', '=', '1')
                        ->where('role_id', '=', '2')->get();

        $totalGeneros = DB::table('user')
                        ->select('generos.nombre', DB::raw('count(*) as total'))
                        ->join('generos', 'generos.id', '=', 'user.genero_fk')
                        ->where('role_id', '=', '1')
                        ->where('status', '=', '1')
                        ->groupBy('generos.nombre')->get();

        $totalConcellos = DB::table('user')
                        ->select('concello.name', DB::raw('count(*) as total'))
                        ->join('concello', 'concello.id', '=', 'user.concello_fk')
                        ->where('role_id', '=', '1')
                        ->where('status', '=', '1')
                        ->groupBy('concello.name')->orderBy('total', 'DESC')->take(5)->get();

//************************************************************* OFERTAS

        $totalOfertasPublicadas = DB::table('job_offer')
                        ->select(DB::raw('count(*) as total'))
                        ->where('publicada', '=', '2')->get();

        $totalOfertasNuevas = DB::table('job_offer')
                        ->select(DB::raw('count(*) as total'))
                        ->where('publicada', '=', '2')
                        ->whereDate('created_at', Carbon::today())->get();
        if (!$totalOfertasNuevas)
            $totalOfertasNuevas = 0;

        $totalOfertaContrato = DB::table('job_offer')
                        ->select('contract.nome as name', DB::raw('count(*) as total'))
                        ->join('contract', 'contract.id', '=', 'job_offer.contract_type')
                        ->where('publicada', '=', '2')
                        ->groupBy('contract.nome')->get();
//********************************* FORMACION ********************************

        $totalCursosPublicados = DB::table('cursos')
                        ->select(DB::raw('count(*) as total'))
                        ->where('estatus', '=', '1')->get();

        $totalCursosNuevos = DB::table('cursos')
                        ->select(DB::raw('count(*) as total'))
                        ->where('estatus', '=', '1')
                        ->whereDate('created_at', Carbon::today())->get();
        if (!$totalCursosNuevos)
            $totalCursosNuevos = 0;

        $totalCursosTipo = DB::table('cursos')
                        ->select('tiposdecurso.nome as name', DB::raw('count(*) as total'))
                        ->join('tiposdecurso', 'tiposdecurso.id', '=', 'cursos.tipocurso')
                        ->where('estatus', '=', '1')
                        ->groupBy('tiposdecurso.nome')->get();


        //********************************* Noticias ********************************
        $now = \Carbon\Carbon::now()->toDateTimeString();
        $totalNoticiasPublicadas = DB::table('noticia')
                        ->select(DB::raw('count(*) as total'))
                        ->where('categoria_fk', '=', '23')
                        ->where('fecha_publicacion', '<=', $now)
                        ->where('fecha_expiracion', '>=', $now)->get();

        $totalNoticiasNuevas = DB::table('noticia')
                        ->select(DB::raw('count(*) as total'))
                        ->where('categoria_fk', '=', '23')
                        ->where('fecha_publicacion', '<=', $now)
                        ->where('fecha_expiracion', '>=', $now)
                        ->whereDate('createdAt', Carbon::today())->get();
        if (!$totalNoticiasNuevas)
            $totalNoticiasNuevas = 0;

        $totalNoticiasCategoria = DB::table('noticia')
                       ->select(DB::raw('YEAR(fecha_publicacion) as name'), DB::raw('count(*) as total'))
                       ->where('categoria_fk', '=', '23')
                  ->groupBy('name')->get();



        //return response()->json(['cantidad' => $notificaciones->count(), 'lista' => $notificaciones->toJson()]);
        return response()->json([
                    'totalGeneros' => $totalGeneros,
                    'totalUsuarios' => $totalUsuarios,
                    'totalUsuariosHoy' => $totalUsuariosHoy,
                    'totalEmpresas' => $totalEmpresas,
                    'totalConcellos' => $totalConcellos,
                    'totalOfertasPublicadas' => $totalOfertasPublicadas,
                    'totalOfertasNuevas' => $totalOfertasNuevas,
                    'totalOfertaContrato' => $totalOfertaContrato,
                    'totalCursosPublicados' => $totalCursosPublicados,
                    'totalCursosNuevos' => $totalCursosNuevos,
                    'totalCursosTipo' => $totalCursosTipo,
                    'totalNoticiasPublicadas' => $totalNoticiasPublicadas,
                    'totalNoticiasNuevas' => $totalNoticiasNuevas,
                    'totalNoticiasCategoria' => $totalNoticiasCategoria
        ]);
    }

}
