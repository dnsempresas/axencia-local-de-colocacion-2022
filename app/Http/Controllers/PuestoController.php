<?php

namespace App\Http\Controllers;

use Validator;
use App\Entities\Puesto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PuestoController extends Controller {
 public function __construct() {
         $this->middleware('auth');
        $this->middleware('admin', ['only' => ['index','create','edit','remove']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $puestos = Puesto::all();
        $message = "No hay puestos registradas";
        return view('puesto.index')->with(['puestos' => $puestos, 'message' => $message]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('puesto.create')->with([]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $puesto = new Puesto();
        $puesto->nombre = $request->input('nombre');
        $puesto->nome = $request->input('nome');
        $editados = json_encode($puesto->getDirty());
        $puesto->save();
        
        \App\Entities\Auditoria::create([
                'model_name' => 'Puestos',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Creó el registro '. $puesto->id . ' '. $editados,
                'original_set' => $puesto->toJson(),
                'changed_set' => ''
            ]);
        
        
        $request->session()->flash('alert-success', 'Puesto creado correctamente');
        return redirect()->route('puestos.index')->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Puesto  $puesto
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $puesto = Puesto::findOrFail($id);
        return view('puesto.edit')->with(['puesto' => $puesto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Puesto  $puesto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        
        $values = array('nombre', 'nome');
        $puesto = Puesto::find($id);
        $original= $puesto->toJson();
        $editados = json_encode($request->all($values));
         
        $puesto->update($request->all($values));
        $request->session()->flash('alert-success', 'Puesto editado correctamente');
       
        
         if($original != json_encode($request->all($values))){ // changes have been made }
        \App\Entities\Auditoria::create([
            'model_name' => 'Puesto',
            'user_id' => Auth::user()->pk,
            'action' => 'actualizó el registro '. $puesto->id . ' '. $editados,
            'original_set' => $original,
            'changed_set' => json_encode($request->all($values)),
            ''
        ]);
        }

        return redirect()->route('puestos.index')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Puesto  $puesto
     * @return \Illuminate\Http\Response
     */
    public function remove(Request $request, $id) {
        $puesto = Puesto::find($id);
        $original= $puesto->toJson();
        $puesto->delete();
        
         \App\Entities\Auditoria::create([
                'model_name' => 'Puestos',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Eliminar el registro',
                'original_set' => $original,
                'changed_set' => ''
            ]);
         
        $request->session()->flash('alert-success', 'Puesto eliminado correctamente');
        return redirect()->route('puestos.index')->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Puesto  $puesto
     * @return \Illuminate\Http\Response
     */
    public function show(Puesto $puesto) {
        
    }

}
