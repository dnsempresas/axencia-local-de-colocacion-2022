<?php

namespace App\Http\Controllers;

use App\Entities\Concello;
use App\Entities\Sector;
use App\Entities\User;
use App\Entities\Grade;
use App\Entities\Puesto;
use App\Entities\Genero;
use App\Entities\Language;
use App\Entities\Province;
use App\Entities\DrivingPermit;
use App\Entities\JobOffer;
use App\Entities\Curso;
use Illuminate\Http\Request;
//use Elibyy\TCPDF\Facades\TCPDF;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;
use Carbon\Carbon;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Mail;
use Codedge\Fpdf\Fpdf\Fpdf;

class UsersController extends Controller {

    public function __construct() {
        $this->middleware('auth', ['only' => ['index', 'create', 'edit', 'remove']]);
        $this->middleware('admin', ['only' => ['index', 'create', 'edit', 'remove', 'resetpassword']]);
    }

    private function getUsers($users) {

        $data = array();

        foreach ($users as $user) {
            $clave = url('/panel/usuarios/' . $user->pk . '/resetpassword');
            $confirmacion = url('/panel/usuarios/' . $user->pk . '/reenviarconfirmacion');
            $edit = url('/panel/usuarios/' . $user->pk . '/edit'); //route('usuarios.edit',$user->pk);
            $delete = url('/panel/admin/usuarios/' . $user->pk . '/remove'); //route('usuarios.destroy',$user->pk);
            $incripciones = url('/panel/admin/inscripciones/' . $user->pk . '/mostrar'); //route('usuarios.destroy',$user->pk);
            $certificado = url('user/' . $user->pk . '/certificado');
            $certificadoOfertas = url('user/' . $user->pk . '/certificadoOfertas');
            
            $nestedData['pk'] = $user->pk;
            $nestedData['surname'] = $user->surname;
            $nestedData['name'] = $user->name;
            if ($user->birth)
                $nestedData['age'] = Carbon::createFromFormat('Y-m-d', $user->birth)->age;
            else
                $nestedData['age'] = 0;
            $nestedData['email'] = $user->email;
            $nestedData['cif'] = $user->cif;

            $nestedData['telephone'] = $user->telephone;
            $nestedData['address'] = $user->address;

            $nestedData['options'] = "&emsp;<a href='{$edit}' title='Modificar' ><span class='glyphicon glyphicon-edit'></span></a>";
//            if ($user->role_id != 3) {
//                $nestedData['options'] = $nestedData['options'] . " &emsp;<a id='remove_link' href='{$delete}' title='Borrar' ><span class='glyphicon glyphicon-remove' style='color:red'></span></a>";
//            }



            if (\Illuminate\Support\Facades\Auth::user()->role_id == 3) {
                $nestedData['options'] = $nestedData['options'] . "&emsp;<a class='mostrarincripciones' href='{$incripciones}' title='Inscripcions' data-value='$user->pk'><span class='glyphicon  glyphicon-tasks'></span> </a>";
                $nestedData['options'] = $nestedData['options'] . "&emsp;<a class='resetpassword' href='#' title='Cambiar contrasinal' data-value='$user->pk' data-email='$user->email' data-status='$user->status' data-confirmed='$user->confirmed'  data-role_id='$user->role_id'><span class='glyphicon glyphicon-lock'></span> </a>";
                $nestedData['options'] = $nestedData['options'] . "&emsp;<a class='mostrarauditorias' href='#' title=' Auditoría do usuario' data-value='$user->pk' ><span class='glyphicon glyphicon-screenshot'></span> </a>";
                $nestedData['options'] = $nestedData['options'] . "&emsp;<a id ='getCertificado' class='getcertificado' href='{$certificado}' target='_blank' title='Certificado de inscriciónna Axencia' data-value='$user->pk' ><span class='glyphicon glyphicon-check'></span> </a>";
                $nestedData['options'] = $nestedData['options'] . "&emsp;<a id ='getCertificadoOferta' class='getcertificadoOfertas' href='{$certificadoOfertas}' target='_blank' title='Certificado de inscrición na Axencia coas ofertas en que se inscribiu' data-value='$user->pk' ><span class='fa fa-external-link'></span> </a>";


//                if ($user->confirmed == 0 && strlen($user->confirmation_code) > 0) {
                $nestedData['options'] = $nestedData['options'] . "&emsp;<a class='reenviarconfirmacion' href='#' title='Reenviar confirmación de correo' data-value='$user->pk' ><span class='glyphicon glyphicon-envelope'></span> </a>";
//                }correo
                if ($user->role_id != 3) {
                    $nestedData['options'] = $nestedData['options'] . "&emsp;<a id = 'dardebajaadm' class='dardebaja' href='#' title='Dar de baixa o usuario' data-value='$user->pk' ><span class='glyphicon glyphicon-remove' style='color:red' ></span> </a>";
                }
            }

            if ($user->province_fk) {
                $province = Province::find($user->province_fk);
                if ($province) {
                    $nestedData['province'] = $province->name;
                }
            } else
                $nestedData['province'] = '';


            $concello = Concello::find($user->concello_fk);
            if ($concello)
                $nestedData['concello'] = $concello->name;
            else
                $nestedData['concello'] = '';


            $data[] = $nestedData;
        }
        return $data;
    }

    public function listUsers(Request $request) {
        $draw = $request->get('draw');
        $fields = array('surname', 'name', 'email', 'cif', 'telephone', 'pk', 'birth', 'province_fk', 'concello_fk', 'confirmed', 'confirmation_code', 'role_id', 'address', 'status');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = $request->input('search.value');
        $colums = $request->get('columns');
        $query = array();
        $order = $request->get('order');
        $params = [];

        $columnIndex = $order[0]['column']; // Column index
        $columnName = $request->get('columns')[$columnIndex]['data']; // Column name
        $columnSortOrder = $order[0]['dir'];
        if ($columnName == 'age')
            $columnName = 'birth';

        //array_push($query, 'fecha_baja is null');


        if ($colums[1]['search']['value']) {
            $value = $colums[1]['search']['value'];
            array_push($query, 'name like ?');
            array_push($params, '%' . $value . '%');
        }
        if ($colums[2]['search']['value']) {
            $value = $colums[2]['search']['value'];
            array_push($query, 'surname like ?');
            array_push($params, '%' . $value . '%');
        }
        if ($colums[3]['search']['value']) {
            $value = $colums[3]['search']['value'];
            array_push($query, 'cif like ?');
            array_push($params, '%' . $value . '%');
        }
        if ($colums[4]['search']['value']) {
            $value = $colums[4]['search']['value'];
            array_push($query, 'email like ?');
            array_push($params, '%' . $value . '%');
        }
        if ($colums[5]['search']['value']) {
            $value = $colums[5]['search']['value'];
            array_push($query, 'telephone like ?');
            array_push($params, '%' . $value . '%');
        }
        if ($colums[6]['search']['value'] && $colums[6]['search']['value'] != '-') {
            $value = $colums[6]['search']['value'];
            list($min, $max) = explode('-', $value);
            if (!empty($min) && !empty($max)) {
                $sql = 'TIMESTAMPDIFF(YEAR, birth, CURDATE()) BETWEEN ? AND ?';
                array_push($query, $sql);
                array_push($params, $min);
                array_push($params, $max);
            } else {
                if (!empty($min)) {
                    $sql = 'TIMESTAMPDIFF(YEAR, birth, CURDATE()) >=?';
                    array_push($query, $sql);
                    array_push($params, $min);
                } else {
                    if (!empty($max)) {
                        $sql = 'TIMESTAMPDIFF(YEAR, birth, CURDATE()) <=?';
                        array_push($query, $sql);
                        array_push($params, $max);
                    }
                }
            }
        }
        if ($colums[7]['search']['value']) {
            $value = $colums[7]['search']['value'];
            array_push($query, 'address like ?');
            array_push($params, '%' . $value . '%');
        }
        if ($colums[9]['search']['value'] && $colums[9]['search']['value'] >= '0') {
            $value = $colums[9]['search']['value'];
            array_push($query, 'province_fk = ?');
            array_push($params, $value);
        }
        if ($colums[10]['search']['value'] && $colums[10]['search']['value'] >= '0') {
            $value = $colums[10]['search']['value'];
            array_push($query, 'concello_fk = ?');
            array_push($params, $value);
        }


        if (empty($search) && empty($query)) {
            $total_users = User::WhereNull('fecha_baja')->count();
            $users = User::WhereNull('fecha_baja')->orderBy($columnName, $columnSortOrder)->skip($start)->take($length)->get($fields);
        } else if (!empty($query)) {

            $query = implode(' AND ', $query);
            $total_users = User::whereRaw($query, $params)->whereNull('fecha_baja')->count();

            $users = User::whereRaw($query, $params)->whereNull('fecha_baja')->offset($start)->limit($length)
                            ->orderBy($columnName, $columnSortOrder)->get($fields);
        } else {
            $total_users = User::whereNull('fecha_baja')->where(function ($total_users) use ($search) {
                        $total_users->where('name', 'LIKE', "%{$search}%")
                                ->orWhere('cif', 'LIKE', "%{$search}%")
                                ->orWhere('surname', 'LIKE', "%{$search}%")
                                ->orWhere('email', 'LIKE', "%{$search}%")
                                ->orWhere('telephone', 'LIKE', "%{$search}%")
                                ->orWhere('province_fk', 'LIKE', "%{$search}%")
                                ->orWhere('concello_fk', 'LIKE', "%{$search}%")
                                ->orWhere('address', 'LIKE', "%{$search}%");
                    })->count();

            $users = User::whereNull('fecha_baja')->where(function ($users) use ($search) {
                        $users->where('name', 'LIKE', "%{$search}%")
                        ->orWhere('cif', 'LIKE', "%{$search}%")
                        ->orWhere('surname', 'LIKE', "%{$search}%")
                        ->orWhere('email', 'LIKE', "%{$search}%")
                        ->orWhere('telephone', 'LIKE', "%{$search}%")
                        ->orWhere('province_fk', 'LIKE', "%{$search}%")
                        ->orWhere('concello_fk', 'LIKE', "%{$search}%")
                        ->orWhere('address', 'LIKE', "%{$search}%");
                    })->offset($start)
                    ->limit($length)
                    ->orderBy($columnName, $columnSortOrder)
                    ->get($fields);
        }
        $users = $this->getUsers($users);
        $data = array(
            'draw' => $draw,
            'recordsTotal' => $total_users,
            'recordsFiltered' => $total_users,
            'data' => $users,
        );
        echo json_encode($data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $provinces = Province::all();
        $users = User::orderBy('surname', 'ASC')->paginate(25);
        /* $html = '<h1>Hello world</h1>';
          $pdf = new TCPDF();
          $pdf::SetTitle('Hello World');
          $pdf::AddPage();
          $pdf::writeHTML($html, true, false, true, false, '');
          $pdf::Output('hello_world.pdf'); */
        //return Excel::download(new UsersExport, 'users.xlsx');
        return view('users.index')->with(['provinces' => $provinces]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $grades = Grade::all();
        $especialidades = \App\Entities\Specialty::all();
        $jobPosts = Puesto::all();
        $langs = Language::all();
        $sectors = Sector::all();
        $provinces = Province::all();
        $permits = DrivingPermit::all();
        $generos = Genero::all();
        return view('users.create')->with(['permits' => $permits, 'generos' => $generos, 'provinces' => $provinces, 'grades' => $grades, 'especialidades' => $especialidades, 'jobPosts' => $jobPosts, 'langs' => $langs, 'sectors' => $sectors]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $input = Input::all();

        $other_province = $request->input('other_province');
        $other_concello = $request->input('other_concello');
        $other_genero = $request->input('other_genero');

        $province_id = $request->input('province');
        $concello_id = $request->input('concello_chosen');

        if ($province_id)
            $province = Province::find($province_id);

        if ($other_province != '') {
            $province = Province::firstOrNew(array('name' => $other_province));
            $province->name = $other_province;
            $province->country_fk = 'ES';
            $province->save();
        }
        if ($other_concello != '') {
            $concello = Concello::firstOrNew(array('name' => $other_concello));
            $concello->name = $other_concello;
            $concello->province_pk = $province->pk;
            $concello->save();
            $concello_id = $concello->id;
        };

//        if ($other_genero != '') {
//            $genero = Genero::firstOrNew(array('nombre' => $other_genero));
//            $genero->nombre = $other_genero;
//            $genero->nome = $other_genero;
//            $genero->save();
//        }

        if ($concello_id)
            $concello = Concello::find($concello_id);



//        $request->validate([
//            'cvprincipal' => 'required|file|max:4096',
//            'email' => 'required|email|unique:user',
//            'cif' => 'unique:user'
//        ]);

        $validator = Validator::make($request->all(), [
                    'email' => 'required|email|unique:user',
                    'cif' => 'required|string|unique:user',
                    'password' => 'required|string|min:8',
        ]);


        $referrer = $request->headers->get('referer');

        if ($validator->fails()) {
            Input::flash('');
//            if (strpos($referrer, 'registrarperfil') !== false) {
//
//                return Redirect::back()->withErrors($validator)
//                                ->withInput();
//            }
//            if (strpos($referrer, '/panel/empresas/create') !== false) {
//
//                return Redirect::back()->withErrors($validator)
//                                ->withInput();
//            }

            return Redirect::back()->withErrors($validator)
                            ->withInput();
        }

        $user = new User();

        $user->email = $request->input('email');

        $user->name = $request->input('name');
        $user->surname = $request->input('last_name');

        $user->password = MD5($request->input('password'));


        $user->cif = $request->input('cif');
        // $user->dni = $request->input('cif');
        //$user->sex = $request->input('gender');
        
       
        
        $user->disability = $request->input('disability') === 'on' ? 1 : 0;
        
        $user->prestacion = $request->input('prestacion') === 'on' ? 1 : 0;
        
        
        
        if (strlen($request->input('birth')) > 0)
            $user->birth = Carbon::createFromFormat('d/m/Y', $request->input('birth'))->format('Y-m-d');

        $user->country_fk = 'ES';
        $user->province_fk = isset($other_province) ? $province->pk : $request->input('province');

        //$user->province_fk = isset($other_concello) ? $province->pk : $request->input('province');
        //$user->sex = $request->input('gender');
        //$user->genero = $user->genero ? $genero->id : $request->input('genero');
        $user->genero_fk = $request->input('genero');
        if ($request->input('genero') == 4) { //otro genero
            $user->otro_genero = $other_genero;
        } else
            $user->otro_genero = '';



        $user->concello_fk = $concello_id;

        $user->address = $request->input('address');
        $user->zip = $request->input('postal_code');
        $user->telephone = $request->input('phone');
        $user->other_phone = $request->input('other_phone');
        //$user->mobile = $request->input('mobile');
        // $user->grade = $request->input('studies');
//        if (is_array($request->input('specialty2')))
//            $user->specialty = implode('/', $request->input('specialty2'));
//        else
//            $user->specialty = $request->input('specialty2');

        $user->especialidades()->sync($request->input('specialty2'));


        $user->other_courses = $request->input('others_courses');

//        $no_experience = $request->input('no_experience');
//
//        if (is_array($request->input('job_posts'))){
//        $user->experience = $no_experience !== 'on' ? implode('/', $request->input('job_posts')) : null;
//        $user->experiencias()->attach($user->experience);
//        }
//        
//        else{
//        $user->experience = $no_experience !== 'on' ? $request->input('job_posts') : null;}

        $no_experience = $request->input('no_experience');

        if ($no_experience == 'on')
            $user->experiencias()->sync([]);
        else
            $user->experiencias()->sync($request->input('job_posts'));


        if (is_array($request->input('languages')))
            $user->languages = implode('/', $request->input('languages'));
        else
            $user->languages = $request->input('languages');

        if (is_array($request->input('computer_skills')))
            $user->computer_skills = implode('/', $request->input('computer_skills'));
        else
            $user->computer_skills = $request->input('computer_skills');

        //

        if (is_array($request->input('driving_permit')))
            $user->driving_permit = implode('/', $request->input('driving_permit'));
        else
            $user->driving_permit = $request->input('driving_permit');


//        if (is_array($request->input('sectors')))
//            $user->sector = implode('/', $request->input('sectors'));
//        else
//            $user->sector = $request->input('sectors');
//
//        if (is_array($request->input('positions')))
//            $user->position = implode('/', $request->input('positions'));
//        else
//            $user->position = $request->input('positions');

        $user->sectoresInteres()->sync($request->input('sectors'));

        $user->puestosInteres()->sync($request->input('positions'));



        $filecvprincipal = $request->file('cvprincipal');

        if ($filecvprincipal) {
            $extension = $filecvprincipal->getClientOriginalExtension();
            //obtenemos el nombre del archivo
            $nombre = 'CV_' . $user->cif . '_1.' . $extension;
            //indicamos que queremos guardar un nuevo archivo en el disco local
            \Storage::disk('local')->put($nombre, \File::get($filecvprincipal));

            $storagePath = \Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();

            $user->cvprincipal = $nombre;

            //Buscar
        }
        $filecvsecundario = $request->file('cvsecundario');
        if ($filecvsecundario) {
            //obtenemos el curriculo 2 definido en el formulario
            $filecvsecundario = $request->file('cvsecundario');
            $extension = $filecvsecundario->getClientOriginalExtension();
            //obtenemos el nombre del archivo
            $nombre = 'CV_' . $user->cif . '_2.' . $extension;
            //indicamos que queremos guardar un nuevo archivo en el disco local
            \Storage::disk('local')->put($nombre, \File::get($filecvsecundario));

            $user->cvsecundario = $nombre;
        }

        $user->role_id = $request->input('tiporole');
        $user->contacto = $request->input('contacto');
        $user->cargocontacto = $request->input('cargocontacto');


        $estatus = $request->input('status');
        if (isset($estatus)) {
            $user->status = $estatus;
        }

        $editados = json_encode($user->getDirty());

        $user->save();

        $foto = $request->file('foto_perfil');

        if ($foto) {


            $extension = $foto->getClientOriginalExtension();
            //obtenemos el nombre del archivo
            $nombre = 'Foto_' . $user->cif . '.' . $extension;
            //indicamos que queremos guardar un nuevo archivo en el disco local
            \Storage::disk('public')->put('/fotos/' . $nombre, \File::get($foto));

            //$storagePath = \Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();

            $user->foto = $nombre;
            $user->update();
        }

        $foto_e = $request->file('foto_perfil_e');
        if ($foto_e) {


            $extension = $foto_e->getClientOriginalExtension();
            //obtenemos el nombre del archivo
            $nombre = 'Foto_' . $user->cif . '.' . $extension;
            //indicamos que queremos guardar un nuevo archivo en el disco local
            \Storage::disk('public')->put('/fotos/' . $nombre, \File::get($foto_e));

            //$storagePath = \Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();

            $user->foto = $nombre;
            $user->update();
        }


        if (strpos($referrer, 'registrarperfil') !== false) {

            \App\Entities\Auditoria::create([
                'model_name' => 'Usuarios',
                'user_id' => $user->pk,
                'action' => 'Creó el registro user ' . $user->pk . ' ' . $editados,
                'original_set' => $user->toJson(),
                'changed_set' => ''
            ]);
        } else {
            \App\Entities\Auditoria::create([
                'model_name' => 'Usuarios',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Creó el registro user ' . $user->pk . ' ' . $editados,
                'original_set' => $user->toJson(),
                'changed_set' => ''
            ]);
        }
        $request->session()->flash('alert-success', 'Usuario creado correctamente');
        if (strpos($referrer, 'registrarperfil') !== false) {

            $confirmation_code = str_random(25);
            $user->confirmation_code = $confirmation_code;
            $user->update();
            $data = $user->toArray();
            // Send confirmation code
            // Send confirmation code
            Mail::send('emails.confirmation_code', $data, function($message) use ($data) {
                $message->to($data['email'], $data['name'])->subject('Por favor confirma tu correo');
            });

            $request->session()->flash('alert-success', 'Usuario creado exitosamente , revise su email y confirme su registro');
            return redirect()->to('/');
        } else {
            if (strpos($referrer, '/panel/empresas/create') !== false) {

                //$confirmation_code = str_random(25);
                //$user->confirmation_code = $confirmation_code;
                $user->confirmed = 1;
                $user->alta = 1;
                $user->perfilcompletado = 1;
                $user->fechacompletado = \Carbon\Carbon::now()->toDateTimeString();
                $user->fecha_alta = \Carbon\Carbon::now()->toDateTimeString();


                $user->update();
                $data = $user->toArray();
                // Send confirmation code
                // Send confirmation code
//                Mail::send('emails.confirmation_code', $data, function($message) use ($data) {
//                    $message->to($data['email'], $data['name'])->subject('Por favor confirma tu correo');
//                });

                $request->session()->flash('alert-success', 'A empresas creouse con éxito');
                return Redirect::back()->withInput();


                return redirect()->to('/panel/empresas/');
            } else {
                if (strpos($referrer, '/panel/usuarios/create') !== false) {

                    //$confirmation_code = str_random(25);
                    //$user->confirmation_code = $confirmation_code;
                    $user->confirmed = 1;
                    $user->alta = 1;
                    $user->perfilcompletado = 1;
                    $user->fechacompletado = \Carbon\Carbon::now()->toDateTimeString();
                    $user->fecha_alta = \Carbon\Carbon::now()->toDateTimeString();


                    $user->update();
                    $data = $user->toArray();
                    // Send confirmation code
                    // Send confirmation code
//                Mail::send('emails.confirmation_code', $data, function($message) use ($data) {
//                    $message->to($data['email'], $data['name'])->subject('Por favor confirma tu correo');
//                });

                    $request->session()->flash('alert-success', 'Usuario creado correctamente');



                    return redirect()->to('/panel/usuarios/');
                }
            }
        }
        return redirect()->route('usuarios.index')->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $grades = Grade::all();
        $jobPosts = Puesto::all();
        $positions = Puesto::all();
        $langs = Language::all();
        $sectors = Sector::all();
        $provinces = Province::all();
        $user = User::findOrFail($id);
        $permits = DrivingPermit::all();
        $generos = Genero::all();


        if ($user->driving_permit != '')
            $user_permit = $user->driving_permit;
        else
            $user_permit = '';



        //$user_permit = implode(',', $user_permit);





        if ($user->province_fk)
            $province = Province::findorFail($user->province_fk);


        $concellos = Concello::Where('province_pk', $user->province_fk)->get();
        $user_concello = '';
        if ($user->concello_fk)
            $user_concello = $user->concello_fk;


//        $user_concello = '';
//        if ($user->specialty != '') {
//            $specialities = \App\Entities\Specialty::whereIn('id', explode('/', $user->specialty))->get();
//            //$user_specialty = $user->specialty;
//        } else
//            $specialities = [];


        $specialities = $user->especialidades()->get();

        $userpuestos = $user->experiencias()->get();
        $experiencias = [];
        foreach ($userpuestos as $userpuesto) {

            array_push($experiencias, $userpuesto->id);
        }
        $userpuestos = $experiencias;



        $usersectores = $user->sectoresInteres()->get();
        $sectoresinteres = [];
        foreach ($usersectores as $usersector) {

            array_push($sectoresinteres, $usersector->id);
        }
        $usersectores = $sectoresinteres;

        $userpositions = $user->puestosInteres()->get();
        $puestosinteres = [];
        foreach ($userpositions as $userposition) {

            array_push($puestosinteres, $userposition->id);
        }
        $userpositions = $puestosinteres;




//        $show = ($user->study === '1' || $user->study === '2' || $user->study === '3') ? 'none' : 'block';
        // return view('users.edit')->with(['show' => $show, 'user_specialty' => $user_specialty, 'specialties' => $speciaties, 'user_concello' => $user_concello, 'concellos' => $concellos, 'user_sector' => $user_sector, 'user_permit' => $user_permit, 'permits' => $permits, 'user' => $user, 'provinces' => $provinces, 'grades' => $grades, 'jobPosts' => $jobPosts, 'langs' => $langs, 'sectors' => $sectors]);
        return view('users.edit')->with(['specialities' => $specialities, 'user_concello' => $user_concello, 'generos' => $generos, 'concellos' => $concellos, 'usersectores' => $usersectores, 'user_permit' => $user_permit, 'permits' => $permits, 'user' => $user, 'provinces' => $provinces, 'grades' => $grades, 'jobPosts' => $jobPosts, 'langs' => $langs, 'sectors' => $sectors, 'userpuestos' => $userpuestos, 'usersectores' => $usersectores, 'userposition' => $userpositions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

//        $validator = Validator::make($request->all(), [
//                    'email' => 'required|email|unique:user,email,' . $id . ',pk',
//                    'cif' => 'required|string|unique:user,cif,' . $id . ',pk',
//        ]);


        $referrer = $request->headers->get('referer');

//        if ($validator->fails()) {
//            Input::flash('');
//            return Redirect::back()->withErrors($validator)
//                            ->withInput();
//        }


        $other_province = $request->input('other_province');
        $other_concello = $request->input('other_concello');
        $other_genero = $request->input('other_genero');

        $province_id = $request->input('province');
        $concello_id = $request->input('concello_chosen');

        if ($province_id)
            $province = Province::find($province_id);

        if ($other_province != '') {
            $province = Province::firstOrNew(array('name' => $other_province));
            $province->name = $other_province;
            $province->country_fk = 'ES';
            $province->save();
            $province_id = $province->pk;
        }

        if ($other_concello != '') {
            $concello = Concello::firstOrNew(array('name' => $other_concello));
            $concello->name = $other_concello;
            $concello->province_pk = $province->pk;
            $concello->save();
            $concello_id = $concello->id;
        };
//         if ($other_genero != '') {
//            $genero = Genero::firstOrNew(array('nombre' => $other_genero ));rol
//            $genero->nombre = $other_genero;
//            $genero->nome = $other_genero;
//            $genero->save();
//        }

        if ($concello_id)
            $concello = Concello::find($concello_id);



        $user = User::find($id);
        $original = $user->toJson();



        $user->email = $request->input('email');

        $user->name = $request->input('name');
        $user->surname = $request->input('last_name');

        //$user->cif = $request->input('dni');
        $user->cif = $request->input('cif');
        
       

        $user->disability =  $request->input('disability') === 'on' ? 1 : 0;
       
        //dd($request->input('prestacion'));
        
        $user->prestacion =  $request->input('prestacion') === 'on' ? 1 : 0;
        
        
        if (strlen($request->input('birth')) > 0)
            $user->birth = Carbon::createFromFormat('d/m/Y', $request->input('birth'))->format('Y-m-d');

        $user->country_fk = 'ES';
        $user->province_fk = isset($other_province) ? $province->pk : $request->input('province');

        //$user->sex = $request->input('gender');
//        $user->genero = isset($other_genero) ? $genero->id : $request->input('genero');
//        
        $user->genero_fk = $request->input('genero');
        if ($request->input('genero') == 4) { //otro genero
            $user->otro_genero = $other_genero;
        } else
            $user->otro_genero = '';


        $user->concello_fk = $concello_id;


        $user->address = $request->input('address');
        $user->zip = $request->input('postal_code');
        $user->telephone = $request->input('phone');
        $user->other_phone = $request->input('other_phone');
        //$user->mobile = $request->input('mobile');
        // $user->grade = $request->input('studies');
//        if (is_array($request->input('specialty2'))){
//        $user->specialty = implode('/', $request->input('specialty2'));}
//        else
//            $user->specialty = $request->input('specialty2');
//        
        $user->especialidades()->sync($request->input('specialty2'));

        $user->other_courses = $request->input('others_courses');

        $no_experience = $request->input('no_experience');

        if ($no_experience == 'on')
            $user->experiencias()->sync([]);
        else
            $user->experiencias()->sync($request->input('job_posts'));



        if (is_array($request->input('languages')))
            $user->languages = implode('/', $request->input('languages'));
        else
            $user->languages = $request->input('languages');

        if (is_array($request->input('computer_skills')))
            $user->computer_skills = implode('/', $request->input('computer_skills'));
        else
            $user->computer_skills = $request->input('computer_skills');

        //

        if (is_array($request->input('driving_permit')))
            $user->driving_permit = implode('/', $request->input('driving_permit'));
        else
            $user->driving_permit = $request->input('driving_permit');

//        if (is_array($request->input('sectors'))){
//            $user->sector = implode('/', $request->input('sectors'));
//           
//        }
//        else{
//        $user->sector = $request->input('sectors');
//        
//        }

        $user->sectoresInteres()->sync($request->input('sectors'));

//        if (is_array($request->input('positions'))){
//        $user->position = implode('/', $request->input('positions'));
//        }
//        else{
//        $user->position = $request->input('positions');
//        }
        $user->puestosInteres()->sync($request->input('positions'));

        //obtenemos el cv principal definido en el formulario
        $filecvprincipal = $request->file('cvprincipal');

        if ($filecvprincipal) {
            $extension = $filecvprincipal->getClientOriginalExtension();
            //obtenemos el nombre del archivo
            $nombre = 'CV_' . $user->cif . '_1.' . $extension;


            if ($user->cvprincipal != '') {
                \Storage::delete($user->cvprincipal);
            };
            //indicamos que queremos guardar un nuevo archivo en el disco local
            \Storage::disk('local')->put($nombre, \File::get($filecvprincipal));

            //$storagePath = \Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();

            $postulaciones = $user->postulaciones()->where('curriculo', '=', $user->cvprincipal)->get();
            if ($postulaciones) {

                foreach ($postulaciones as $postulacion) {

                    $postulacion->pivot->curriculo = $nombre;
                    $postulacion->pivot->save();
                }
            }


            $user->cvprincipal = $nombre;

            //buscar todas las postulaciones donde se envio el curriculo principal y actualizarlo con el nuevo nombre
        }
        $filecvsecundario = $request->file('cvsecundario');
        if ($filecvsecundario) {
            //obtenemos el curriculo 2 definido en el formulario
            $filecvsecundario = $request->file('cvsecundario');
            $extension = $filecvsecundario->getClientOriginalExtension();
            //obtenemos el nombre del archivo
            $nombre = 'CV_' . $user->cif . '_2.' . $extension;

            if ($user->cvsecundario != '') {
                \Storage::delete($user->cvsecundario);

                $postulaciones = $user->postulaciones()->where('curriculo', '=', $user->cvsecundario)->get();
                if ($postulaciones) {
                    foreach ($postulaciones as $postulacion) {
                        $postulacion->pivot->curriculo = $nombre;
                        $postulacion->pivot->save();
                    }
                }
            };

            //indicamos que queremos guardar un nuevo archivo en el disco local
            \Storage::disk('local')->put($nombre, \File::get($filecvsecundario));

            //buscar todas las postulaciones donde se envio el curriculo cvsecundario y actualizarlo con el nuevo nombre

            $user->cvsecundario = $nombre;

            //buscar todas las postulaciones donde se envio el curriculo cvsecundario y actualizarlo con el nuevo nombre
        } else {

            $cvsecundario_user = $request->input('cvsecundario_user');

            if ($cvsecundario_user == '') {


                \Storage::delete($user->cvsecundario);

                // } 

                $postulaciones = $user->postulaciones()->where('curriculo', '=', $user->cvsecundario)->get();
                if ($postulaciones) {
                    foreach ($postulaciones as $postulacion) {
                        $postulacion->pivot->curriculo = $user->cvprincipal;
                        $postulacion->pivot->save();
                    }
                }

                $user->cvsecundario = '';
            }
        }

        $user->role_id = $request->input('tiporole');
        $user->contacto = $request->input('contacto');
        $user->cargocontacto = $request->input('cargocontacto');


        if ($user->role_id == 2) {

            $foto_e = $request->file('foto_perfil_e');
//        dd($foto_e);
            $foto_user_e = $request->input('foto_user_e');
            if ($foto_e) {


                $extension = $foto_e->getClientOriginalExtension();
                //obtenemos el nombre del archivo
                $nombre = 'Foto_' . $user->cif . '.' . $extension;

                \Storage::disk('public')->delete('/fotos/' . $user->foto);


                //indicamos que queremos guardar un nuevo archivo en el disco local
                \Storage::disk('public')->put('/fotos/' . $nombre, \File::get($foto_e));


                //$storagePath = \Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();

                $user->foto = $nombre;
            } else {

                if ($user->foto !== $foto_user_e) {

                    \Storage::disk('public')->delete('/fotos/' . $user->foto);
                    $user->foto = '';
                }
            }
        } else {

            $foto = $request->file('foto_perfil');
//        dd($foto);
            $foto_user = $request->input('foto_user');
            if ($foto) {
//            dd('entre');
                //
            $extension = $foto->getClientOriginalExtension();
                //obtenemos el nombre del archivo
                $nombre = 'Foto_' . $user->cif . '.' . $extension;

                \Storage::disk('public')->delete('/fotos/' . $user->foto);


                //indicamos que queremos guardar un nuevo archivo en el disco local
                \Storage::disk('public')->put('/fotos/' . $nombre, \File::get($foto));


                //$storagePath = \Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();

                $user->foto = $nombre;
//            dd($user->foto);
            } else {
                if ($user->foto !== $foto_user) {
                    \Storage::disk('public')->delete('/fotos/' . $user->foto);
                    $user->foto = '';
                }
            }
        }




        $estatus = $request->input('status');
        if ($estatus){
            //        dd($estatus);
        $old_status = $user->status;
        $user->status = $estatus;
            
        }
        


        $newpass = $request->input('password');
        if ($newpass) {
            if ($newpass != '')
                $user->password = MD5($request->input('password'));
            $user->status = true;
        }

//        if (isset($estatus)) {
//            if (($estatus != $old_status)) { //hubo cambio de estatus
        //buscar en notificaciones si hay algun registro relacionado o no con la activacion 
        $notificaciones = \App\Entities\NotificacionesAdmin::Where('tabla_id', '=', $user->pk)
                ->where('tabla', '=', 'user')
                ->where('procesado', '=', '0');

        if ($notificaciones) {
            //dd($notificaciones);
            $notificaciones->procesado = 1;
            $notificaciones->update(['procesado' => 1]);
        }
//             }
//        }

        $editados = json_encode($user->getDirty());

        $user->update();



        //if($original != json_encode($request->all($values))){
        \App\Entities\Auditoria::create([
            'model_name' => 'Usuarios',
            'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
            'action' => 'Actualizó el registro ' . $user->pk . ' campos editados: ' . $editados,
            'original_set' => $original,
            'changed_set' => $user->toJson()
        ]);
        // }
        //User::where('id',$id)->update($request->all($values));

        $request->session()->flash('alert-success', 'Os datos do usuario foron actualizados correctamente');
        $referrer = $request->headers->get('referer');
        if (strpos($referrer, 'editarperfil') !== false) {
            if ($user->perfilcompletado == 0) {
                $user->perfilcompletado = 1;
                $user->status = 1;
                $user->fechacompletado = \Carbon\Carbon::now()->toDateTimeString();
                $user->update();
            }

            return Redirect::back();
        }

        $request->session()->flash('alert-success', 'Os datos do usuario foron actualizados correctamente');
        if (strpos($referrer, 'empresas') !== false) {

            if ($user->perfilcompletado == 0) {
                $user->perfilcompletado = 1;
                $user->fechacompletado = \Carbon\Carbon::now()->toDateTimeString();
                $user->update();
            }


            return Redirect::back();
        }

        return redirect()->route('usuarios.index')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
        User::destroy($id);
        $request->session()->flash('alert-success', 'Usuario eliminado correctamente');
        return redirect()->route('usuarios.index');
    }

    public function remove(Request $request, $id) {
        $eliminable = true;

        $user = User::find($id);
        $original = $user->toJson();


        if ($user->ofertasPublicadas()->count() >= 1
                or $user->postulaciones()->count() >= 1
                or $user->cursos()->count() >= 1
        ) {

            $eliminable = false;
        }

        if ($eliminable)
            $user->delete();
        else {
            $request->session()->flash('alert-danger', 'Usuario no puede eliminarse');
            return Redirect::back();
        }




        \App\Entities\Auditoria::create([
            'model_name' => 'Usuarios',
            'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
            'action' => 'Eliminar el registro',
            'original_set' => $original,
            'changed_set' => ''
        ]);
        $request->session()->flash('alert-success', 'Usuario eliminado correctamente');
        return redirect()->route('usuarios.index');
    }

    public function darsedebaja(Request $request) {

        $id = Auth::user()->pk;

        $user = User::find($id);
        $original = $user->toJson();

        $user->name = '';
        $user->surname = '';
        $user->birth = Null;
        $user->email = '';
        $user->sector = '';
        $user->telephone = 0;
        $user->other_phone = 0;
        $user->mobile = 0;
        $user->address = '';
        $user->city = '';
        $user->specialty = '';
        $user->prestacion = 0;
        $user->cif = '';
        $user->experience = '';
        $user->status = 0;


        if ($user->foto != '') {

            //unlink('/public/storage/fotos/' . $user->foto);
            \Storage::disk('public')->delete('/fotos/' . $user->foto);
        }

        if ($user->cvprincipal != '') {
            \Storage::delete($user->cvprincipal);
        }
        if ($user->cvsecundario != '') {
            \Storage::delete($user->cvsecundario);
        }

        $user->cvprincipal = '';

        $user->cvsecundario = '';

        $user->contacto = '';
        $user->cargocontacto = '';
        $user->foto = '';
        $user->status = 0;
        $user->fecha_baja = \Carbon\Carbon::now()->toDateTimeString();
        $user->update();


        \App\Entities\Auditoria::create([
            'model_name' => 'Usuarios',
            'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
            'action' => 'Eliminar el registro',
            'original_set' => $original,
            'changed_set' => ''
        ]);
        Session::flush();

        return response()->json(['message' => 'Exitoso', 'userdata' => $original], 200);
    }

    public function dardebaja(Request $request) {

        $id = $request->input('id');

        $user = User::find($id);
        $original = $user;

        $user->name = '';
        $user->surname = '';
        $user->birth = Null;
        $user->email = '';
        $user->sector = '';
        $user->telephone = 0;
        $user->other_phone = 0;
        $user->mobile = 0;
        $user->address = '';
        $user->city = '';
        $user->specialty = '';
        $user->prestacion = 0;
        $user->cif = '';
        $user->experience = '';
        $user->status = 0;


        if ($user->foto != '') {

            // unlink('/public/storage/fotos/' . $user->foto);
            \Storage::disk('public')->delete('/fotos/' . $user->foto);
        }

        if ($user->cvprincipal != '') {
            \Storage::delete($user->cvprincipal);
        }
        if ($user->cvsecundario != '') {
            \Storage::delete($user->cvsecundario);
        }

        $user->cvprincipal = '';

        $user->cvsecundario = '';

        $user->contacto = '';
        $user->cargocontacto = '';
        $user->foto = '';
        $user->status = 0;



        \App\Entities\Auditoria::create([
            'model_name' => 'Usuarios',
            'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
            'action' => 'Eliminar el registro ID' . $original->pk . ' ' . $original->name . ' ' . $original->surname,
            'original_set' => $original->toJson(),
            'changed_set' => ''
        ]);
        $user->fecha_baja = \Carbon\Carbon::now()->toDateTimeString();
        $user->update();
        //Session::flush();

        return response()->json(['message' => 'Exitoso', 'userdata' => $original], 200);
    }

    public function login(Request $request) {

        $email = $request->input('name');

        $password = $request->input('password');


//        if (Hash::check('12345', $hashedPassword)) {
//            // The passwords match...
//        }



        $usuario = Auth::attempt(array('email' => $email, 'password' => $password, 'status' => '1'));




        if ($usuario) {

            $userdata = Auth::user();

            if ($userdata->status == 1) {

                // verificar si cuenta de usuario está activada
                // 
                //si la cuenta esta activada seguir de largo
                //si la cuenta no esta activda debe buscar dupilicados duplicados por nombre, apellido, Telefono y que no exista uno igual habilitado
                //si consigio duplicados redirigir
                // sino tiene duplicados seguir.
                $usuariosdupliados = [];
                if ($userdata->alta == 0) {

//                    $usuariosdupliados = User::where([
//                                'name' => $userdata->name,
//                                'surname' => $userdata->surname,
//                                'telephone' => $userdata->telephone,
//                                'status' => '1',
//                            ])->orWhere(function($query)use($userdata) {
//                                return $query->where('cif', '=', $userdata->cif)->where('pk', '!=', $userdata->pk)->where('cif', '!=', '')->whereNotNull('cif');
//                            })->orderBy('created_at', 'desc')->get();

                    $name = explode(" ", $userdata->name);
                    $name = $name[0];
                    $surname = explode(" ", $userdata->surname);
                    $surname = $surname[0];

                    $usuariosdupliados = User:: where('name', 'LIKE', "{$name}%")
                                    ->where('surname', 'LIKE', "{$surname}%")
                                    ->where('telephone', '=', $userdata->telephone)
                                    ->where('status', '=', 1)
                                    ->orWhere(function($query)use($userdata) {
                                        return $query->where('cif', '=', $userdata->cif)->where('pk', '!=', $userdata->pk)->where('cif', '!=', '')->whereNotNull('cif');
                                    })->orderBy('created_at', 'desc')->get();





                    if (count($usuariosdupliados) > 1) {
                        Auth::logout();

                        return response()->json(['message' => 'Pefil duplicado', 'userdata' => $userdata, 'usuariosdupliados' => $usuariosdupliados, 'home' => '/consolidarperfil/' . $userdata->pk], 200);
                    }
                    $userdata->alta = 1;
                    $userdata->update();
                }

                if ($userdata->perfilcompletado == 0) {

                    return response()->json(['message' => 'Debe completar el perfil', 'userdata' => $userdata], 200);
                }

                if ($userdata->role_id == 3) {
                    $home = '/admin';
                } else {
                    if ($userdata->role_id == 2) {
                        $home = '/empresas';
                    } else {
                        $home = '/home';
                    }
                }

                \App\Entities\Auditoria::create([
                    'model_name' => 'Login',
                    'user_id' => $userdata->pk,
                    'action' => 'Inicio de sesión: ' . \Carbon\Carbon::now('Europe/Madrid')->toDateTimeString(),
                    'original_set' => $userdata->toJson(),
                    'changed_set' => ''
                ]);

                return response()->json(['message' => 'Exitoso', 'userdata' => $userdata, 'home' => $home], 200);
            }
        }


        return response()->json(['message' => 'Fallo'], 200);
    }

    public function logout(Request $request) {
        Session::flush();
        //Auth::logout();
        return Redirect::to("/");
    }

    public function downloadcv(Request $request) {
        $filename = $request['filename'];

        return \Storage::download($filename);

        //return Storage::download('file.jpg', $name, $headers);
    }

    public function descargarFoto(Request $request) {
        $filename = $request['filename'];

        $public_path = public_path();
        $url = $public_path . '/storage/fotos/' . $filename; // depende de root en el archivo filesystems.php.
        //verificamos si el archivo existe y lo retornamos
        if (\Storage::exists($filename)) {
            return response()->download($url);
        }
        //si no se encuentra lanzamos un error 404.
        abort(404);
    }

    public function authorizeRoles($roles) {
        if ($this->hasAnyRole($roles)) {
            return true;
        }
        abort(401, 'Esta acción no está autorizada.');
    }

    public function hasAnyRole($roles) {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }

    public function hasRole($role) {
        if ($this->role()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }

    public function consolidar(Request $request) {
        $id = $request->input('id');
        $usuario = User::find($id);
        if ($usuario) {

            $usuario->alta = 1;

            $name = explode(" ", $usuario->name);
            $name = $name[0];
            $surname = explode(" ", $usuario->surname);
            $surname = $surname[0];




            $usuariosdupliados = User:: where('name', 'LIKE', "{$name}%")
                    ->where('surname', 'LIKE', "{$surname}%")
                    ->where('telephone', '=', $usuario->telephone)
                    ->where('status', '=', 1)
                    ->where('pk', '!=', $usuario->pk)
                    ->orWhere(function($query)use($usuario) {
                        return $query->where('cif', '=', $usuario->cif)->where('pk', '!=', $usuario->pk)->where('cif', '!=', '')->whereNotNull('cif');
                    })
                    ->update([
                'status' => 0,
                'alta' => 1,
                'fecha_alta' => \Carbon\Carbon::now()->toDateTimeString(),
            ]);
            $usuario->fecha_alta = \Carbon\Carbon::now()->toDateTimeString();
            $usuario->update();

            \App\Entities\Auditoria::create([
                'model_name' => 'Usuarios',
                'user_id' => $usuario->pk,
                'action' => 'Consolidó Perfiles',
                'original_set' => $usuario->toJson(),
                'changed_set' => ''
            ]);

            //PENDIENTE codigo para que todas las postulaciones pasen al nuevo perfil
            //mover todos los archivos de los id descartados a la carpeta a borrar
            //mover el curriculo del perfil seleccionado a la nueva ubicacion. 


            return response()->json(['message' => 'Exitoso'], 200);
        }
    }

    public function postularme(Request $request) {



        if (!Auth::check()) {
            return response()->json(['message' => 'Debe Iniciar Sesión'], 200);
        }

        if (Auth::user()->perfilcompletado == 0) {

            return response()->json(['message' => 'Debe completar el perfil'], 200);
        }

        $id = $request->input('id');
        $curriculo = $request->input('curriculo');

        $idUser = Auth::user()->pk;
        $oferta = JobOffer::where('publicada', '=', '2')->find($id);


        if ($oferta) {

            $postulacion = $oferta->postulados->where('user.pk', '=', $idUser);
            if ($postulacion->count() > 0) {
                return response()->json(['message' => 'Ya se encuentra postulado'], 200);
            }


            $inscribir = 1;
            if ($oferta->limitar_inscripcion > 0) {
                $postulados = $oferta->postulados()->get()->count();
                if ($postulados >= $oferta->maximo_inscripciones) {
                    $inscribir = 0;
                }
            }

            if ($inscribir == 1) {

                $oferta->postulados()->attach($idUser, ['created' => Carbon::now(), 'curriculo' => $curriculo]);


                \App\Entities\Auditoria::create([
                    'model_name' => 'Usuarios',
                    'user_id' => $idUser,
                    'action' => 'Se inscribió en oferta ' . $oferta->id . ':' . $oferta->requested_job,
                    'original_set' => '',
                    'changed_set' => ''
                ]);

                return response()->json(['message' => 'Exitoso'], 200);
            } else {
                return response()->json(['message' => 'Límite de inscripciones superado'], 200);
            }
        } else {
            return response()->json(['message' => 'Oferta inválida'], 200);
        }
    }

    public function postulaciones(Request $request) {
        $perPage = 10;
        $id = $request->input('id');


        if (!Auth::check()) {
            return response()->json(['message' => 'Debe Iniciar Sesión'], 200);
        }

        $idUser = Auth::user()->pk;
        $postulaciones = Auth::user()->postulaciones()->paginate($perPage);



        $ofertas = $postulaciones;
        return view('frontend.mispostulaciones')->with(['ofertas' => $ofertas, 'pageSize' => 10]);
    }

    public function inscribirme(Request $request) {

        if (!Auth::check()) {
            return response()->json(['message' => 'Debe Iniciar Sesión'], 200);
        }

        if (Auth::user()->perfilcompletado == 0) {

            return response()->json(['message' => 'Debe completar el perfil'], 200);
        }

//        if (!Auth::user()->cvpreincipal != '') {
//
//            return response()->json(['message' => 'Debe ingresar Curriculo'], 200);
//        }




        $curso = $request->input('curso');

        $idUser = Auth::user()->pk;
        $curso = Curso::find($curso);

        $postulacion = $curso->users()->where('pk', '=', $idUser);
        if ($postulacion->count() > 0) {
            return response()->json(['message' => 'Ya se encuentra inscrito'], 200);
        }


        if ($curso->plazas_disponibles > 0) {
            $curso->plazas_disponibles = $curso->plazas_disponibles - 1;
            try {
                $curso->users()->attach($idUser);

                $curso->save();

                \App\Entities\Auditoria::create([
                    'model_name' => 'Usuarios',
                    'user_id' => $idUser,
                    'action' => 'Se inscribió en Curso ' . $curso->id . ':' . $curso->nombre,
                    'original_set' => '',
                    'changed_set' => ''
                ]);
            } catch (Exception $e) {
                return response()->json(['message' => 'Error'], 200);
            }
        } else {
            //$request->session()->flash('alert-danger', 'El curso al que intenta inscribirse, ye no tiene plazas desponibles');
            return response()->json(['message' => 'Sin cupo'], 200);
        }


        return response()->json(['message' => 'Exitoso'], 200);
    }

    public function verify(Request $request, $code) {
        $user = User::where('confirmation_code', $code)->first();

        if (!$user) {
            $request->session()->flash('alert-danger', 'Datos de confirmación no coinciden!');
            return Redirect::to("/");
        }

        $user->confirmed = true;
        $user->confirmation_code = null;
        $user->fecha_alta = \Carbon\Carbon::now()->toDateTimeString();
        $user->alta = 1;

        if ($user->role_id == 1) { //solo doy de alta y activo los usuarios normales, las empresas debe ser aprobadas por el tecnico de empleo
            $user->status = true;

            $request->session()->flash('alert-success', 'Confirmaches correctamente a teu correo!, xa podes entrar a nosa web');
        } else { //las empresas quedan con status Activo y se envia notifcacion al tecnico que las aprueba o las pone inactivas
            //$user->status = -1;
            $user->status = true;

            \App\Entities\NotificacionesAdmin::create([
                'mensaje' => 'Se ha dado de alta una nueva empresa',
                'tabla' => 'user',
                'tabla_id' => $user->pk,
                'ruta' => '/panel/empresas/' . $user->pk . '/edit',
                'leido' => 0,
                'procesado' => 0
            ]);
            $request->session()->flash('alert-success', 'Confirmaches correctamente a teu correo!, pronto le aprobaremos su ingreso!');
        }

        $user->save();

        \App\Entities\Auditoria::create([
            'model_name' => 'Usuarios',
            'user_id' => $user->pk,
            'action' => 'Confirmó cuenta de email',
            'original_set' => '',
            'changed_set' => ''
        ]);


        return Redirect::to("/");
    }

    public function misInscripciones(Request $request, $user_id) {
        $perPage = 10;
        $id = $user_id;


        if (!Auth::check()) {
            return response()->json(['message' => 'Debe Iniciar Sesión'], 200);
        }

        //$idUser = Auth::user()->pk;
        $user = User::find($id);

        $postulaciones = $user->postulaciones()->get();
        $ofertas = $postulaciones;

        $inscripciones = $user->cursos()->get();
        $cursos = $inscripciones;


        return view('frontend.misinscripciones')->with(['ofertas' => $ofertas, 'cursos' => $cursos, 'user_id' => $id, 'pageSize' => 10]);
    }

    public function resumenInscripciones(Request $request) {
        $perPage = 2;
        $id = $request->input('id');


        if (!Auth::check()) {
            return response()->json(['message' => 'Debe Iniciar Sesión'], 200);
        }

        //$idUser = Auth::user()->pk;
        $user = User::find($id);

        $postulaciones = $user->postulaciones()->get();
        $ofertas = $postulaciones;

        $inscripciones = $user->cursos()->get();
        $cursos = $inscripciones;


        return view('users.misinscripcionesresumen')->with(['ofertas' => $ofertas, 'cursos' => $cursos, 'user_id' => $id, 'pageSize' => 10]);
    }

    public function resetpassword(Request $request) {

        $email = $request->input('email');
        $password = MD5($request->input('password'));
        $user_id = $request->input('user_id');

        $usuario = User::find($user_id);



        if ($usuario) {
            $usuario->password = $password;
            $usuario->update();

            \App\Entities\Auditoria::create([
                'model_name' => 'Usuarios',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Cambio Contrasinal : ' . $email,
                'original_set' => '',
                'changed_set' => ''
            ]);


            return response()->json(['message' => 'Exitoso'], 200);
        } else {
            return response()->json(['message' => 'Error'], 200);
        }
    }

    public function mostrarauditorias(Request $request) {
        $perPage = 10;
        $id = $request->input('id');


        if (!Auth::check()) {
            return response()->json(['message' => 'Debe Iniciar Sesión'], 200);
        }

        //$idUser = Auth::user()->pk;
        $user = User::find($id);

        $auditorias = $user->auditorias()->orderBy('publishedAt', 'desc')->paginate($perPage);


        return view('users.misrauditoriasresumen')->with(['auditorias' => $auditorias, 'user_id' => $id, 'pageSize' => 10]);
    }

    public function reenviarconfirmacion(Request $request) {
        $id = $request->input('id');

        $user = User::find($id);

        if ($user->confirmation_code == '' || $user->confirmation_code == null) {
            $user->confirmation_code = str_random(25);
            $user->update();
        };

        $data = $user->toArray();
        // Send confirmation code
        // Send confirmation code
        Mail::send('emails.confirmation_code', $data, function($message) use ($data) {
            $message->to($data['email'], $data['name'])->subject('Por favor confirma tu correo');
        });

        return response()->json(['message' => 'Envio Exitoso'], 200);
    }

    public function certificado(Request $request, $id) {

        $user = User::find($id);

        $pdf = new PDF();


        $pdf->AddPage('P', 'Letter');
        $pdf->skipFooter = true;
        $pdf->SetMargins(200, 25, 30);




        $pdf->Image(asset('assets/images/logoconcello.jpg'), 30, 15, 18, 24);

        $pdf->Image(asset('assets/images/logo-gl.png'), 120, 20, 80, 15);


        $pdf->SetFont('Times', '', 10);

        $pdf->SetY(70);
        $pdf->SetX(30);


        $pdf->MultiCell(170, 6, mb_convert_encoding('Francisco Xabier Ferreiro Campos, Responsable da Área de Emprego do Concello de Santiago de Compostela, e en calidade de responsable técnico da Axencia Local de Colocación, autorizada co número de inscrición 1200000008 ', 'CP1252'), 0, 'J', false);


        $pdf->SetY(90);
        $pdf->SetX(30);

        $pdf->SetFont('Times', 'B', 12);

        $pdf->Cell(20, 10, 'INFORMA:');

        $pdf->SetY(100);
        $pdf->SetX(30);

        $pdf->SetFont('Times', '', 10);


        $pdf->MultiCell(160, 6, mb_convert_encoding('Que ' . $user->name . ' ' . $user->surname . ', con DNI ' . $user->cif . ', está inscrita como solicitante de emprego  na Axencia Local de Colocación dende ' . $user->getFechaLetras() . '.', 'CP1252'), 0, 'J', false);

        $pdf->SetY(120);
        $pdf->SetX(30);

        $pdf->MultiCell(170, 10, mb_convert_encoding('Para que conste aos efectos oportunos, asino o presente informe en Santiago de Compostela o ' . $this->_getFechaLetras(now()) . '.', 'CP1252'), 0, 'J', false);


        $pdf->SetFont('Times', '', 10);
//


        $pdf->Image(asset('assets/images/firma-xabier.png'), 30, 210, 60, 20);

        $pdf->SetY(230);
        $pdf->SetX(30);
        $pdf->Cell(230, 6, 'Asdo. Francisco Xabier Ferreiro Campos');
        $pdf->SetY(235);
        $pdf->SetX(30);
        $pdf->Cell(235, 6, mb_convert_encoding('Responsable Área de Emprego do Concello', 'CP1252'));


        return $pdf->Output('I', 'Certificado.pdf');
    }

    public function _getFechaLetras($date) {

        if (session('lang') == 'es')
            $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        else
            $meses = array("Xaneiro", "Febreiro", "Marzo", "Abril", "Maio", "Xuño", "Xullo", "Agosto", "Setembro", "Outubro", "Novembro", "Decembro");


        //$fecha = Carbon::parse($this->due_date);
        $fecha = Carbon::parse($date);
        $mes = $meses[($fecha->format('n')) - 1];
        return $fecha->format('d') . ' de ' . $mes . ' de ' . $fecha->format('Y');
    }

    public function certificadoOferta(Request $request, $id, $jobOffer) {

        $user = User::find($id);
        $job_offer = $user->postulaciones()->find($jobOffer);


        $pdf = new PDF();



        $pdf->AddPage('P', 'Letter');
        $pdf->skipFooter = true;
        $pdf->SetMargins(200, 25, 30);

        $pdf->Image(asset('assets/images/logoconcello.jpg'), 30, 15, 18, 24);

        $pdf->Image(asset('assets/images/logo-gl.png'), 120, 20, 80, 15);


        $pdf->SetFont('Times', '', 10);

        $pdf->SetY(60);
        $pdf->SetX(30);


        $pdf->MultiCell(170, 8, mb_convert_encoding('Francisco Xabier Ferreiro Campos, Responsable da Área de Emprego do Concello de Santiago de Compostela, e en calidade de responsable técnico da Axencia Local de Colocación, autorizada co número de inscrición 1200000008 ', 'CP1252'), 0, 'J', false);


        $pdf->SetY(90);
        $pdf->SetX(30);

        $pdf->SetFont('Times', 'B', 12);

        $pdf->Cell(20, 10, 'INFORMA:');

        $pdf->SetY(100);
        $pdf->SetX(30);

        $pdf->SetFont('Times', '', 10);


        $pdf->MultiCell(160, 8, mb_convert_encoding('Que ' . $user->name . ' ' . $user->surname . ', con DNI ' . $user->cif . ', está inscrita como solicitante de emprego  na Axencia Local de Colocación dende ' . $user->getFechaLetras() . '.', 'CP1252'), 0, 'J', false);

        $pdf->SetY(130);
        $pdf->SetX(30);

        $pdf->MultiCell(160, 8, mb_convert_encoding(' Que o ' . $this->_getFechaLetras(date($job_offer->pivot->created)) . ' se inscribiu na oferta de emprego ' . $job_offer->requested_job . ' [nº ' . $job_offer->id . '].', 'CP1252'), 0, 'J', false);

        $pdf->SetY(150);
        $pdf->SetX(30);

        $pdf->MultiCell(170, 8, mb_convert_encoding('Para que conste aos efectos oportunos, asino o presente informe en Santiago de Compostela o ' . $this->_getFechaLetras(now()) . '.', 'CP1252'), 0, 'J', false);


        $pdf->SetFont('Times', '', 10);
//
        $pdf->Image(asset('assets/images/firma-xabier.png'), 30, 200, 60, 20);

        $pdf->SetY(230);
        $pdf->SetX(30);
        $pdf->Cell(230, 6, 'Asdo. Francisco Xabier Ferreiro Campos');
        $pdf->SetY(235);
        $pdf->SetX(30);
        $pdf->Cell(235, 6, mb_convert_encoding('Responsable Área de Emprego do Concello', 'CP1252'));


        return $pdf->Output('I', 'Certificado.pdf');
    }

    public function generarCertificadoOfertas(Request $request) {

        $user = User::find($request->input('user'));

        $job_offers = explode(',', $request->input('offers'));


        $job_offers = $user->postulaciones()->wherein('job_fk', $job_offers)->orderBy('created', 'ASC')->get();
        //$job_offers =  = $user->postulaciones()->get();


        $pdf = new PDF();

        $pdf->SetFont('Times');

//        $pdf->AddFont('ApexSansBook', '', 'ApexSansBook.php');
//        $pdf->AddFont('ApexNew-Bold', 'B', 'ApexNew-Bold.php');
//        $pdf->AddFont('ApexNew-Light', '', 'ApexNew-Light.php');
//        $pdf->AddFont('ApexNew-Medium', '', 'ApexNew-Medium.php');

        $pdf->AddPage('P', 'Letter');
        $pdf->skipFooter = true;
        $pdf->SetMargins(200, 25, 30);

        //$pdf->SetFont('ApexSansBook', '', 18);



        $pdf->Image(asset('assets/images/logoconcello.jpg'), 30, 15, 18, 24);

        $pdf->Image(asset('assets/images/logo-gl.png'), 120, 20, 80, 15);


//        $pdf->SetFont('ApexNew-Light', '', 10);
        $pdf->SetFont('Times', '', 10);

        $pdf->SetY(50);
        $pdf->SetX(30);


        $pdf->MultiCell(170, 6, mb_convert_encoding('Francisco Xabier Ferreiro Campos, Responsable da Área de Emprego do Concello de Santiago de Compostela, e en calidade de responsable técnico da Axencia Local de Colocación, autorizada co número de inscrición 1200000008 ', 'CP1252'), 0, 'J', false);


        $pdf->SetY(65);
        $pdf->SetX(30);

        // $pdf->SetFont('ApexNew-Bold', 'B', 12);
        $pdf->SetFont('Times', 'B', 12);

        $pdf->Cell(20, 8, 'INFORMA:');

        $pdf->SetY(75);
        $pdf->SetX(30);

        $pdf->SetFont('Times', '', 10);


        $pdf->MultiCell(160, 6, mb_convert_encoding('Que ' . $user->name . ' ' . $user->surname . ', con DNI ' . $user->cif . ', está inscrita como solicitante de emprego  na Axencia Local de Colocación dende ' . $user->getFechaLetras() . '.', 'CP1252'), 0, 'J', false);

        $pdf->SetY(92);
        $pdf->SetX(30);

        $linea = 0;
        foreach ($job_offers as $job_offer) {
            $linea++;

            $pdf->MultiCell(160, 6, mb_convert_encoding('Que o ' . $this->_getFechaLetras(date($job_offer->pivot->created)) . ' se inscribiu na oferta de emprego ' . $job_offer->requested_job . ' [nº ' . $job_offer->id . '].', 'CP1252'), 0, 'J', false);
            $pdf->SetY(92 + $linea * 12);
            $pdf->SetX(30);
        };

        $pdf->MultiCell(170, 6, mb_convert_encoding('Para que conste aos efectos oportunos, asino o presente informe en Santiago de Compostela o ' . $this->_getFechaLetras(now()) . '.', 'CP1252'), 0, 'J', false);


        // $pdf->SetFont('ApexNew-Light', '', 12);
//
        $pdf->Image(asset('assets/images/firma-xabier.png'), 30, 210, 60, 20);

        $pdf->SetY(230);
        $pdf->SetX(30);
        $pdf->Cell(230, 6, 'Asdo. Francisco Xabier Ferreiro Campos');
        $pdf->SetY(235);
        $pdf->SetX(30);
        $pdf->Cell(235, 6, mb_convert_encoding('Responsable Área de Emprego do Concello', 'CP1252'));

        return $pdf->Output('I', 'Certificado.pdf');
    }

    public function resumenInscripcionesOfertas(Request $request) {
        $perPage = 2;
        $id = $request->input('id');


        if (!Auth::check()) {
            return response()->json(['message' => 'Debe Iniciar Sesión'], 200);
        }

        //$idUser = Auth::user()->pk;
        $user = User::find($id);

        $postulaciones = $user->postulaciones()->get();
        $ofertas = $postulaciones;

        $inscripciones = $user->cursos()->get();
        $cursos = $inscripciones;


        return view('users.misinscripcionesOfertas')->with(['ofertas' => $ofertas, 'user_id' => $id, 'pageSize' => 10]);
    }

}

class PDF extends FPDF {
    
   

    public $col = 0; // Current column
    public $y0;      // Ordinate of column start

    function SetDash($black = null, $white = null) {
        if ($black !== null)
            $s = sprintf('[%.3F %.3F] 0 d', $black * $this->k, $white * $this->k);
        else
            $s = '[] 0 d';
        $this->_out($s);
    }

    function Header() {
        // Page header
        global $title;

//        $this->SetFont('Arial', 'B', 15);
//        $w = $this->GetStringWidth($title) + 6;
//        $this->SetX((210 - $w) / 2);
////        $this->SetDrawColor(0, 80, 180);
////        $this->SetFillColor(230, 230, 0);
////        $this->SetTextColor(220, 50, 50);
////        $this->SetLineWidth(1);
////        $this->Cell($w, 9, $title, 1, 1, 'C', true);
        $this->Ln(10);
//        // Save ordinate
        $this->y0 = $this->GetY();
    }

    function Footer() {
        // Page footer
        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);
        $this->SetTextColor(128);
        //$this->Cell(0, 10, 'Page ' . $this->PageNo(), 0, 0, 'C');
    }

    function SetCol($col) {
        // Set position at a given column
        $this->col = $col;
        $x = 10 + $col * 51;
        $this->SetLeftMargin($x);
        $this->SetX($x);
    }

    function AcceptPageBreak() {
        // Method accepting or not automatic page break
        if ($this->col < 3) {
            // Go to next column
            $this->SetCol($this->col + 1);
            // Set ordinate to top
            $this->SetY($this->y0);
            $this->SetDash(1, 1);

            $this->line($this->getX() - 3, $this->getY(), $this->getX() - 3, 260);
            $this->SetDash();


//            $this->SetX(10 + $this->col * 65 );
            // Keep on page
            return false;
        } else {
            // Go back to first column
            $this->SetCol(0);
            // Page break
            return true;
        }
    }

//    function ChapterTitle($num, $label) {
//        // Title
//        $this->SetFont('Arial', '', 12);
//        $this->SetFillColor(200, 220, 255);
//        $this->Cell(0, 6, "Chapter $num : $label", 0, 1, 'L', true);
//        $this->Ln(4);
//        // Save ordinate
//        $this->y0 = $this->GetY();
//    }
//    function ChapterBody($text) {
//        // Read text file
//        $txt = $text;
//        // Font
//        $this->SetFont('Times', '', 8);
//        // Output text in a 6 cm width column
//        $this->MultiCell(48, 5, $txt);
//        $this->Ln();
//        // Mention
//        $this->SetFont('', 'I');
//        $this->Cell(0, 5, '(end of excerpt)');
//        // Go back to first column
//        $this->SetCol(0);
//    }
//    function PrintChapter($num, $title, $file) {
//        // Add chapter
//        $this->AddPage();
//        $this->ChapterTitle($num, $title);
//        $this->ChapterBody($file);
//    }

    function Circle($x, $y, $r, $style = 'D') {
        $this->Ellipse($x, $y, $r, $r, $style);
    }

    function Ellipse($x, $y, $rx, $ry, $style = 'D') {
        if ($style == 'F')
            $op = 'f';
        elseif ($style == 'FD' || $style == 'DF')
            $op = 'B';
        else
            $op = 'S';
        $lx = 4 / 3 * (M_SQRT2 - 1) * $rx;
        $ly = 4 / 3 * (M_SQRT2 - 1) * $ry;
        $k = $this->k;
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F m %.2F %.2F %.2F %.2F %.2F %.2F c', ($x + $rx) * $k, ($h - $y) * $k, ($x + $rx) * $k, ($h - ($y - $ly)) * $k, ($x + $lx) * $k, ($h - ($y - $ry)) * $k, $x * $k, ($h - ($y - $ry)) * $k));
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c', ($x - $lx) * $k, ($h - ($y - $ry)) * $k, ($x - $rx) * $k, ($h - ($y - $ly)) * $k, ($x - $rx) * $k, ($h - $y) * $k));
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c', ($x - $rx) * $k, ($h - ($y + $ly)) * $k, ($x - $lx) * $k, ($h - ($y + $ry)) * $k, $x * $k, ($h - ($y + $ry)) * $k));
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c %s', ($x + $lx) * $k, ($h - ($y + $ry)) * $k, ($x + $rx) * $k, ($h - ($y + $ly)) * $k, ($x + $rx) * $k, ($h - $y) * $k, $op));
    }

}
