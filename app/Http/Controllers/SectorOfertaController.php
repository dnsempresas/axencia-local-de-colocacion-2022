<?php

namespace App\Http\Controllers;

use Validator;
use App\Entities\SectorOferta;
use Illuminate\Http\Request;

class SectorOfertaController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('admin', ['only' => ['index', 'create', 'edit', 'remove']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $sectores = SectorOferta::all();
        $message = "No hay sectores registradas";
        return view('sector_oferta.index')->with(['sectores' => $sectores, 'message' => $message]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('sector_oferta.create')->with([]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $sector = new SectorOferta();
        $sector->nombre = $request->input('nombre');
        $sector->nome = $request->input('nome');
        $editados = json_encode($sector->getDirty());
        $sector->save();

        \App\Entities\Auditoria::create([
            'model_name' => 'Sector Oferta',
            'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
            'action' => 'Creó el registro ' . $sector->id . ' ' . $editados,
            'original_set' => $sector->toJson(),
            'changed_set' => ''
        ]);


        $request->session()->flash('alert-success', 'Sector creado correctamente');

        return redirect('/panel/sectores_ofertas')->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $sector = SectorOferta::findOrFail($id);
        return view('sector_oferta.edit')->with(['sector' => $sector]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $id = $request->input('id');
        $values = array('nombre', 'nome');
        $sector = SectorOferta::find($id);
        $original = $sector->toJson();
        $editados = json_encode($sector->getDirty());
        $sector->update($request->all($values));

        if ($original != json_encode($request->all($values))) {
            \App\Entities\Auditoria::create([
                'model_name' => 'Sector Oferta',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Actualizó el registro ' . $sector->id . ' ' . $editados,
                'original_set' => $original,
                'changed_set' => json_encode($request->all($values))
            ]);
        }

        $request->session()->flash('alert-success', 'Sector editado correctamente');

        return redirect('/panel/sectores_ofertas')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SectorOferta  $sector
     * @return \Illuminate\Http\Response
     */
    public function remove(Request $request, $id) {
        $sector = SectorOferta::find($id);
        $original = $sector->toJson();
        if (count($sector->ofertas) > 0) {
            $msg = ['alert-error', 'Sector esta asociado a una oferta, no puede eliminar'];
        } else {
            $sector->delete();
            $msg = ['alert-success', 'Sector eliminado correctamente'];
            \App\Entities\Auditoria::create([
                'model_name' => 'Sector Oferta',
                'user_id' => \Illuminate\Support\Facades\Auth::user()->pk,
                'action' => 'Eliminar el registro',
                'original_set' => $original,
                'changed_set' => ''
            ]);
        }

        $request->session()->flash($msg[0], $msg[1]);
        return redirect('/panel/sectores_ofertas')->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function show(Sector $sector) {
        
    }

}
